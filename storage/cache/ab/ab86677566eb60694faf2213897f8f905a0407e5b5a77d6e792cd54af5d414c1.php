<?php

/* default/template/product/special.twig */
class __TwigTemplate_db6436f290495423382e625e17a35975758417878d88f15144dfb42a00bdb8ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"products\">
    <section class=\"products__wrap\">
        <div class=\"products__filter-block\">
            ";
        // line 13
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
        </div>
        <div class=\"products__products-block\">
            <div class=\"products__description-block cat_description\">
                <h1 class=\"cat_description__title\">";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
            </div>
            <div class=\"products__sort-block sort\">

                <div class=\"sort__title\">";
        // line 21
        echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
        echo "</div>

                ";
        // line 23
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 24
            echo "                    <a class=\"sort__item sort__item--active\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "href", array());
            echo "\">
                        <span>";
            // line 25
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "text", array());
            echo "</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        } else {
            // line 29
            echo "                    <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "href", array());
            echo "\">
                        <span>";
            // line 30
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "text", array());
            echo "</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        }
        // line 34
        echo "
                ";
        // line 35
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 36
            echo "                    <a class=\"sort__item sort__item--desc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 2, array(), "array"), "href", array());
            echo "\">
                        <span>Имя</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 40
(isset($context["sorts"]) ? $context["sorts"] : null), 2, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 41
            echo "                    <a class=\"sort__item sort__item--asc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "href", array());
            echo "\">
                        <span>Имя</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        } else {
            // line 46
            echo "                    <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "href", array());
            echo "\">
                        <span>Имя</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        }
        // line 51
        echo "

                ";
        // line 53
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 54
            echo "                    <a class=\"sort__item sort__item--desc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 4, array(), "array"), "href", array());
            echo "\">
                        <span>Цена</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 58
(isset($context["sorts"]) ? $context["sorts"] : null), 4, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 59
            echo "                    <a class=\"sort__item sort__item--asc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "href", array());
            echo "\">
                        <span>Цена</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        } else {
            // line 64
            echo "                    <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "href", array());
            echo "\">
                        <span>Цена</span>
                        <i class=\"icon-arrow-b\"></i>
                    </a>
                ";
        }
        // line 69
        echo "
            </div>
            <a class=\"sort__toggle\" href=\"javascript:void(0)\">";
        // line 71
        echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
        echo "</a>
            <div class=\"products__list product\">
                ";
        // line 73
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 74
            echo "                    <div class=\"product__item\">
                        <a class=\"product__wrap\" href=\"";
            // line 75
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">
                            <img class=\"product__image\" src=\"";
            // line 76
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\">
                            <div class=\"product__group\">
                                <div class=\"product__title\">";
            // line 78
            echo $this->getAttribute($context["product"], "name", array());
            echo "</div>
                                <div class=\"product__desc\">";
            // line 79
            echo $this->getAttribute($context["product"], "description", array());
            echo "</div>
                                <div class=\"product__details\">
                                    <div class=\"product__price\">";
            // line 81
            echo $this->getAttribute($context["product"], "price", array());
            echo "</div>

                                    ";
            // line 83
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "tags", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 84
                echo "                                        <div class=\"product__label product__label--new\">";
                echo $this->getAttribute($context["tag"], "tag", array());
                echo "</div>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 86
            echo "
                                </div>
                            </div>
                        </a>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "
                ";
        // line 93
        if ( !(isset($context["products"]) ? $context["products"] : null)) {
            // line 94
            echo "                    <p class=\"products__empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
                ";
        }
        // line 96
        echo "            </div>
            <div class=\"products__pagination_wrap\">
                ";
        // line 98
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
            </div>

            <ul class=\"products__breadcrumb-block breadcrumb\">
                ";
        // line 102
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 103
            echo "                    <li class=\"breadcrumb__item\">
                        <a href=\"";
            // line 104
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\" class=\"breadcrumb__link\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "            </ul>
        </div>
    </section>
</main>

";
        // line 112
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "    <link href=\"catalog/view/theme/default/stylesheet/products.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"catalog/view/theme/default/js/products.bundle.js\" type=\"text/javascript\"></script>
    <script src=\"catalog/view/javascript/brainyfilter.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/product/special.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 6,  273 => 5,  268 => 3,  265 => 2,  259 => 112,  252 => 107,  241 => 104,  238 => 103,  234 => 102,  227 => 98,  223 => 96,  217 => 94,  215 => 93,  212 => 92,  201 => 86,  192 => 84,  188 => 83,  183 => 81,  178 => 79,  174 => 78,  167 => 76,  163 => 75,  160 => 74,  156 => 73,  151 => 71,  147 => 69,  138 => 64,  129 => 59,  127 => 58,  119 => 54,  117 => 53,  113 => 51,  104 => 46,  95 => 41,  93 => 40,  85 => 36,  83 => 35,  80 => 34,  73 => 30,  68 => 29,  61 => 25,  56 => 24,  54 => 23,  49 => 21,  42 => 17,  35 => 13,  29 => 9,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/products.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/products.bundle.js" type="text/javascript"></script>*/
/*     <script src="catalog/view/javascript/brainyfilter.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* <main class="products">*/
/*     <section class="products__wrap">*/
/*         <div class="products__filter-block">*/
/*             {{ column_left }}*/
/*         </div>*/
/*         <div class="products__products-block">*/
/*             <div class="products__description-block cat_description">*/
/*                 <h1 class="cat_description__title">{{ heading_title }}</h1>*/
/*             </div>*/
/*             <div class="products__sort-block sort">*/
/* */
/*                 <div class="sort__title">{{ text_sort }}</div>*/
/* */
/*                 {% if sorts[0].value == '%s-%s'|format(sort, order) %}*/
/*                     <a class="sort__item sort__item--active" href="{{ sorts[0].href }}">*/
/*                         <span>{{ sorts[0].text }}</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% else %}*/
/*                     <a class="sort__item" href="{{ sorts[0].href }}">*/
/*                         <span>{{ sorts[0].text }}</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% endif %}*/
/* */
/*                 {% if sorts[1].value == '%s-%s'|format(sort, order) %}*/
/*                     <a class="sort__item sort__item--desc" href="{{ sorts[2].href }}">*/
/*                         <span>Имя</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% elseif sorts[2].value == '%s-%s'|format(sort, order) %}*/
/*                     <a class="sort__item sort__item--asc" href="{{ sorts[1].href }}">*/
/*                         <span>Имя</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% else %}*/
/*                     <a class="sort__item" href="{{ sorts[1].href }}">*/
/*                         <span>Имя</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% endif %}*/
/* */
/* */
/*                 {% if sorts[3].value == '%s-%s'|format(sort, order) %}*/
/*                     <a class="sort__item sort__item--desc" href="{{ sorts[4].href }}">*/
/*                         <span>Цена</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% elseif sorts[4].value == '%s-%s'|format(sort, order) %}*/
/*                     <a class="sort__item sort__item--asc" href="{{ sorts[3].href }}">*/
/*                         <span>Цена</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% else %}*/
/*                     <a class="sort__item" href="{{ sorts[3].href }}">*/
/*                         <span>Цена</span>*/
/*                         <i class="icon-arrow-b"></i>*/
/*                     </a>*/
/*                 {% endif %}*/
/* */
/*             </div>*/
/*             <a class="sort__toggle" href="javascript:void(0)">{{ text_sort }}</a>*/
/*             <div class="products__list product">*/
/*                 {% for product in products %}*/
/*                     <div class="product__item">*/
/*                         <a class="product__wrap" href="{{ product.href }}">*/
/*                             <img class="product__image" src="{{ product.thumb }}" alt="{{ product.name }}">*/
/*                             <div class="product__group">*/
/*                                 <div class="product__title">{{ product.name }}</div>*/
/*                                 <div class="product__desc">{{ product.description }}</div>*/
/*                                 <div class="product__details">*/
/*                                     <div class="product__price">{{ product.price }}</div>*/
/* */
/*                                     {% for tag in product.tags %}*/
/*                                         <div class="product__label product__label--new">{{ tag.tag }}</div>*/
/*                                     {% endfor %}*/
/* */
/*                                 </div>*/
/*                             </div>*/
/*                         </a>*/
/*                     </div>*/
/*                 {% endfor %}*/
/* */
/*                 {% if not products %}*/
/*                     <p class="products__empty">{{ text_empty }}</p>*/
/*                 {% endif %}*/
/*             </div>*/
/*             <div class="products__pagination_wrap">*/
/*                 {{ pagination }}*/
/*             </div>*/
/* */
/*             <ul class="products__breadcrumb-block breadcrumb">*/
/*                 {% for breadcrumb in breadcrumbs %}*/
/*                     <li class="breadcrumb__item">*/
/*                         <a href="{{ breadcrumb.href }}" class="breadcrumb__link">{{ breadcrumb.text }}</a>*/
/*                     </li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*     </section>*/
/* </main>*/
/* */
/* {{ footer }}*/
/* */
