<?php

/* default/template/extension/module/brainyfilter.twig */
class __TwigTemplate_afe86b122f9be379ee33cc70f4ea7a164c131198d21f43d733def81b56936c8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        $context["isHorizontal"] = (((isset($context["layout_position"]) ? $context["layout_position"] : null) === "content_top") || ((isset($context["layout_position"]) ? $context["layout_position"] : null) === "content_bottom"));
        // line 8
        $context["isResponsive"] = (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "style", array(), "array"), "responsive", array(), "array"), "enabled", array(), "array")) ? (true) : (false));
        // line 9
        $context["responsivePos"] = ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "style", array(), "array"), "responsive", array(), "array"), "position", array(), "array") === "right")) ? ("bf-right") : ("bf-left"));
        // line 10
        echo "

";
        // line 12
        if (twig_length_filter($this->env, (isset($context["filters"]) ? $context["filters"] : null))) {
            // line 13
            echo "
            <ul class=\"filter\">
                <form class=\"bf-form\"
                    data-height-limit=\"";
            // line 16
            echo (isset($context["limit_height_opts"]) ? $context["limit_height_opts"] : null);
            echo "\"
                    data-visible-items=\"";
            // line 17
            echo (isset($context["slidingOpts"]) ? $context["slidingOpts"] : null);
            echo "\"
                    data-hide-items=\"";
            // line 18
            echo (isset($context["slidingMin"]) ? $context["slidingMin"] : null);
            echo "\"
                    data-submit-type=\"";
            // line 19
            echo $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "submission", array(), "array"), "submit_type", array(), "array");
            echo "\"
                    data-submit-delay=\"";
            // line 20
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "submission", array(), "array"), "submit_delay_time", array(), "array"));
            echo "\"
                    data-submit-hide-panel =\"";
            // line 21
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "submission", array(), "array"), "hide_panel", array(), "array"));
            echo "\"
                    data-resp-max-width=\"";
            // line 22
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "style", array(), "array"), "responsive", array(), "array"), "max_width", array(), "array"));
            echo "\"
                    data-resp-collapse=\"";
            // line 23
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "style", array(), "array"), "responsive", array(), "array"), "collapsed", array(), "array"));
            echo "\"
                    data-resp-max-scr-width =\"";
            // line 24
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "style", array(), "array"), "responsive", array(), "array"), "max_screen_width", array(), "array"));
            echo "\"
                    method=\"get\" action=\"index.php\">

                ";
            // line 27
            if (((isset($context["currentRoute"]) ? $context["currentRoute"] : null) === "product/search")) {
                // line 28
                echo "                    <input type=\"hidden\" name=\"route\" value=\"product/search\" />
                ";
            } else {
                // line 30
                echo "                    <input type=\"hidden\" name=\"route\" value=\"product/category\" />
                ";
            }
            // line 32
            echo "                ";
            if ((isset($context["currentPath"]) ? $context["currentPath"] : null)) {
                // line 33
                echo "                    <input type=\"hidden\" name=\"path_f\" value=\"";
                echo (isset($context["currentPath"]) ? $context["currentPath"] : null);
                echo "\" />
                ";
            }
            // line 35
            echo "                ";
            if ((isset($context["manufacturerId"]) ? $context["manufacturerId"] : null)) {
                // line 36
                echo "                    <input type=\"hidden\" name=\"manufacturer_id\" value=\"";
                echo (isset($context["manufacturerId"]) ? $context["manufacturerId"] : null);
                echo "\" />
                ";
            }
            // line 38
            echo "
                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["filters"]) ? $context["filters"] : null));
            foreach ($context['_seq'] as $context["i"] => $context["section"]) {
                // line 40
                echo "
                        ";
                // line 42
                echo "                    ";
                if (($this->getAttribute($context["section"], "type", array(), "array") == "price")) {
                    // line 43
                    echo "
                        <li class=\"filter__item ";
                    // line 44
                    if ( !$this->getAttribute($context["section"], "collapsed", array(), "array")) {
                        echo " is-show ";
                    }
                    echo "\">
                            <a class=\"filter__title\" href=\"javascript:void(0)\">
                                ";
                    // line 46
                    echo (isset($context["lang_price"]) ? $context["lang_price"] : null);
                    echo "
                                <i class=\"filter__icon icon-arrow-r\"></i>
                            </a>
                            <ul class=\"filter__subitem\">
                                <li class=\"input\">
                                    <label>От</label>
                                    <input type=\"number\" name=\"bfp_price_min\" value=\"";
                    // line 52
                    echo (isset($context["lowerlimit"]) ? $context["lowerlimit"] : null);
                    echo "\" placeholder=\"";
                    echo (isset($context["lowerlimit"]) ? $context["lowerlimit"] : null);
                    echo " ";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
                    echo "\">
                                </li>
                                <li class=\"input\">
                                    <label>До</label>
                                    <input type=\"number\" name=\"bfp_price_max\" value=\"";
                    // line 56
                    echo (isset($context["upperlimit"]) ? $context["upperlimit"] : null);
                    echo "\" placeholder=\"";
                    echo (isset($context["upperlimit"]) ? $context["upperlimit"] : null);
                    echo " ";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
                    echo "\">
                                </li>
                            </ul>
                        </li>

                        ";
                    // line 62
                    echo "                    ";
                } elseif (($this->getAttribute($context["section"], "type", array(), "array") == "category")) {
                    // line 63
                    echo "                        ";
                    $context["groupUID"] = "c0";
                    // line 64
                    echo "                        <li class=\"filter__item bf-attr-";
                    echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                    echo " ";
                    if ( !$this->getAttribute($context["section"], "collapsed", array(), "array")) {
                        echo " is-show ";
                    }
                    echo "\">
                            <a class=\"filter__title\" href=\"javascript:void(0)\">";
                    // line 65
                    echo (isset($context["lang_categories"]) ? $context["lang_categories"] : null);
                    echo "<i class=\"filter__icon icon-arrow-r\"></i></a>
                            <ul class=\"filter__subitem\">
                                ";
                    // line 67
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["section"], "values", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                        // line 68
                        echo "
                                    ";
                        // line 69
                        $context["catId"] = $this->getAttribute($context["cat"], "id", array(), "array");
                        // line 70
                        echo "                                    ";
                        if ((array_key_exists("totals", $context) && $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "hide_empty", array(), "array"))) {
                            // line 71
                            echo "                                        ";
                            $context["inStock"] = ((isset($context["postponedCount"]) ? $context["postponedCount"] : null) || ($this->getAttribute($this->getAttribute((isset($context["totals"]) ? $context["totals"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", false, true), (isset($context["catId"]) ? $context["catId"] : null), array(), "array", true, true) && $this->getAttribute($this->getAttribute((isset($context["totals"]) ? $context["totals"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array"), (isset($context["catId"]) ? $context["catId"] : null), array(), "array")));
                            // line 72
                            echo "                                        ";
                            $context["inSelected"] = ($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["catId"]) ? $context["catId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")));
                            // line 73
                            echo "                                        ";
                            if (( !(isset($context["inStock"]) ? $context["inStock"] : null) &&  !(isset($context["inSelected"]) ? $context["inSelected"] : null))) {
                                // line 74
                                echo "                                            bf-disabled
                                        ";
                            }
                            // line 76
                            echo "                                    ";
                        }
                        // line 77
                        echo "
                                    ";
                        // line 78
                        if (($this->getAttribute($context["section"], "control", array(), "array") == "checkbox")) {
                            // line 79
                            echo "                                        <li class=\"checkbox\">
                                            <input
                                                    type=\"checkbox\"
                                                    name=\"bfp_";
                            // line 82
                            echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                            if (($this->getAttribute($context["section"], "control", array(), "array") === "checkbox")) {
                                echo ("_" . (isset($context["catId"]) ? $context["catId"] : null));
                            }
                            echo "\"
                                                    id=\"bf-attr-";
                            // line 83
                            echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                            echo "\"
                                                    value=\"";
                            // line 84
                            echo (isset($context["catId"]) ? $context["catId"] : null);
                            echo "\"
                                                    ";
                            // line 85
                            if (($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["catId"]) ? $context["catId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")))) {
                                echo " checked=\"true\" ";
                            }
                            echo " />

                                            <label for=\"bf-attr-";
                            // line 87
                            echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                            echo "\"><span>";
                            echo $this->getAttribute($context["cat"], "name", array(), "array");
                            echo "</span></label>

                                        </li>
                                    ";
                        } else {
                            // line 91
                            echo "                                        <li class=\"radio\">
                                            <input
                                                    type=\"radio\"
                                                    name=\"bfp_";
                            // line 94
                            echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                            if (($this->getAttribute($context["section"], "control", array(), "array") === "checkbox")) {
                                echo ("_" . (isset($context["catId"]) ? $context["catId"] : null));
                            }
                            echo "\"
                                                    id=\"bf-attr-";
                            // line 95
                            echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                            echo "\"
                                                    value=\"";
                            // line 96
                            echo (isset($context["catId"]) ? $context["catId"] : null);
                            echo "\"
                                                    ";
                            // line 97
                            if (($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["catId"]) ? $context["catId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")))) {
                                echo " checked=\"true\" ";
                            }
                            echo " />

                                            <label for=\"bf-attr-";
                            // line 99
                            echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                            echo "\"><span>";
                            echo $this->getAttribute($context["cat"], "name", array(), "array");
                            echo "</span></label>

                                        </li>
                                    ";
                        }
                        // line 103
                        echo "                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 104
                    echo "                            </ul>
                        </li>

                        ";
                    // line 108
                    echo "                    ";
                } else {
                    // line 109
                    echo "
                        ";
                    // line 110
                    $context["curGroupId"] = null;
                    // line 111
                    echo "
                        ";
                    // line 112
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["section"], "array", array(), "array"));
                    foreach ($context['_seq'] as $context["groupId"] => $context["group"]) {
                        // line 113
                        echo "
                            ";
                        // line 114
                        $context["collapsedGroup"] = false;
                        // line 115
                        echo "
                            ";
                        // line 116
                        if (($this->getAttribute($context["group"], "attr_id", array(), "array") && $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "attribute", array(), "array"), $this->getAttribute($context["group"], "attr_id", array(), "array"), array(), "array"))) {
                            // line 117
                            echo "
                                ";
                            // line 118
                            $context["collapsedGroup"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "attribute", array(), "array"), $this->getAttribute($context["group"], "attr_id", array(), "array"), array(), "array"), "collapsed", array(), "array");
                            // line 119
                            echo "
                            ";
                        } elseif (($this->getAttribute(                        // line 120
$context["group"], "option_id", array(), "array") && $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "options", array(), "array"), $this->getAttribute($context["group"], "option_id", array(), "array"), array(), "array"))) {
                            // line 121
                            echo "
                                ";
                            // line 122
                            $context["collapsedGroup"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "options", array(), "array"), $this->getAttribute($context["group"], "option_id", array(), "array"), array(), "array"), "collapsed", array(), "array");
                            // line 123
                            echo "
                            ";
                        } elseif (($this->getAttribute(                        // line 124
$context["group"], "filter_id", array(), "array") && $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "filter", array(), "array"), $this->getAttribute($context["group"], "filter_id", array(), "array"), array(), "array"))) {
                            // line 125
                            echo "
                                ";
                            // line 126
                            $context["collapsedGroup"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "filter", array(), "array"), $this->getAttribute($context["group"], "filter_id", array(), "array"), array(), "array"), "collapsed", array(), "array");
                            // line 127
                            echo "
                            ";
                        }
                        // line 129
                        echo "

                            ";
                        // line 131
                        $context["groupUID"] = (twig_slice($this->env, $this->getAttribute($context["section"], "type", array(), "array"), 0, 1) . $context["groupId"]);
                        // line 132
                        echo "
                            <li class=\"filter__item bf-attr-";
                        // line 133
                        echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                        echo " ";
                        if ( !$this->getAttribute($context["section"], "collapsed", array(), "array")) {
                            echo " is-show ";
                        }
                        echo "\">
                                <a class=\"filter__title\" href=\"javascript:void(0)\">";
                        // line 134
                        echo ($this->getAttribute($context["group"], "name", array(), "array") . " ");
                        echo "<i class=\"filter__icon icon-arrow-r\"></i></a>
                                <ul class=\"filter__subitem\">
                                    ";
                        // line 136
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["group"], "values", array(), "array"));
                        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
                            // line 137
                            echo "
                                        ";
                            // line 138
                            $context["catId"] = $this->getAttribute($context["cat"], "id", array(), "array");
                            // line 139
                            echo "
                                        ";
                            // line 140
                            if ((array_key_exists("totals", $context) && $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "hide_empty", array(), "array"))) {
                                // line 141
                                echo "                                            ";
                                $context["inStock"] = ((isset($context["postponedCount"]) ? $context["postponedCount"] : null) || ($this->getAttribute($this->getAttribute((isset($context["totals"]) ? $context["totals"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", false, true), (isset($context["valueId"]) ? $context["valueId"] : null), array(), "array", true, true) && $this->getAttribute($this->getAttribute((isset($context["totals"]) ? $context["totals"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array"), (isset($context["catId"]) ? $context["catId"] : null), array(), "array")));
                                // line 142
                                echo "                                            ";
                                $context["inSelected"] = ($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["valueId"]) ? $context["valueId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")));
                                // line 143
                                echo "                                            ";
                                if (( !(isset($context["inStock"]) ? $context["inStock"] : null) &&  !(isset($context["inSelected"]) ? $context["inSelected"] : null))) {
                                    // line 144
                                    echo "                                                bf-disabled
                                            ";
                                }
                                // line 146
                                echo "                                        ";
                            }
                            // line 147
                            echo "
                                        ";
                            // line 148
                            if (($this->getAttribute($context["group"], "type", array(), "array") == "checkbox")) {
                                // line 149
                                echo "                                            <li class=\"checkbox\">
                                                <input
                                                        type=\"checkbox\"
                                                        name=\"bfp_";
                                // line 152
                                echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                                if (($this->getAttribute($context["section"], "control", array(), "array") === "checkbox")) {
                                    echo ("_" . (isset($context["catId"]) ? $context["catId"] : null));
                                }
                                echo "\"
                                                        id=\"bf-attr-";
                                // line 153
                                echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                                echo "\"
                                                        value=\"";
                                // line 154
                                echo (isset($context["catId"]) ? $context["catId"] : null);
                                echo "\"
                                                        ";
                                // line 155
                                if (($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["catId"]) ? $context["catId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")))) {
                                    echo " checked=\"true\" ";
                                }
                                echo " />

                                                <label for=\"bf-attr-";
                                // line 157
                                echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                                echo "\"><span>";
                                echo $this->getAttribute($context["cat"], "name", array(), "array");
                                echo "</span></label>

                                            </li>
                                        ";
                            } else {
                                // line 161
                                echo "                                            <li class=\"radio\">
                                                <input
                                                        type=\"radio\"
                                                        name=\"bfp_";
                                // line 164
                                echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                                if (($this->getAttribute($context["section"], "control", array(), "array") === "checkbox")) {
                                    echo ("_" . (isset($context["catId"]) ? $context["catId"] : null));
                                }
                                echo "\"
                                                        id=\"bf-attr-";
                                // line 165
                                echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                                echo "\"
                                                        value=\"";
                                // line 166
                                echo (isset($context["catId"]) ? $context["catId"] : null);
                                echo "\"
                                                        ";
                                // line 167
                                if (($this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array", true, true) && twig_in_filter((isset($context["catId"]) ? $context["catId"] : null), $this->getAttribute((isset($context["selected"]) ? $context["selected"] : null), (isset($context["groupUID"]) ? $context["groupUID"] : null), array(), "array")))) {
                                    echo " checked=\"true\" ";
                                }
                                echo " />

                                                <label for=\"bf-attr-";
                                // line 169
                                echo (((((isset($context["groupUID"]) ? $context["groupUID"] : null) . "_") . (isset($context["catId"]) ? $context["catId"] : null)) . "_") . (isset($context["layout_id"]) ? $context["layout_id"] : null));
                                echo "\"><span>";
                                echo $this->getAttribute($context["cat"], "name", array(), "array");
                                echo "</span></label>

                                            </li>
                                        ";
                            }
                            // line 173
                            echo "                                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 174
                        echo "                                </ul>
                            </li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['groupId'], $context['group'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 177
                    echo "                    ";
                }
                // line 178
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['section'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 180
            echo "
                <li class=\"filter__action\">
                    <a class=\"btn__default-outline btn__default-outline--filter filter__toggle\"
                       href=\"javascript:void(0)\"
                       onclick=\"BrainyFilter.reset()\">
                        ";
            // line 185
            echo (isset($context["reset"]) ? $context["reset"] : null);
            echo "
                    </a>
                    <a class=\"btn__primary btn__primary--filter filter__toggle\" href=\"javascript:void(0)\">Применить</a>
                </li>

                </form>
            </ul>
            <a class=\"filter__toggle-btn filter__toggle\" href=\"javascript:void(0)\">Фильтр</a>


<script>
var bfLang = {
    show_more : '";
            // line 197
            echo (isset($context["lang_show_more"]) ? $context["lang_show_more"] : null);
            echo "',
    show_less : '";
            // line 198
            echo (isset($context["lang_show_less"]) ? $context["lang_show_less"] : null);
            echo "',
    empty_list : '";
            // line 199
            echo (isset($context["lang_empty_list"]) ? $context["lang_empty_list"] : null);
            echo "'
};
BrainyFilter.requestCount = BrainyFilter.requestCount || ";
            // line 201
            echo (($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "product_count", array(), "array")) ? ("true") : ("false"));
            echo ";
BrainyFilter.requestPrice = BrainyFilter.requestPrice || ";
            // line 202
            echo (($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "sections", array(), "array"), "price", array(), "array"), "enabled", array(), "array")) ? ("true") : ("false"));
            echo ";
BrainyFilter.separateCountRequest = BrainyFilter.separateCountRequest || ";
            // line 203
            echo (((isset($context["postponedCount"]) ? $context["postponedCount"] : null)) ? ("true") : ("false"));
            echo ";
BrainyFilter.min = BrainyFilter.min || ";
            // line 204
            echo (isset($context["priceMin"]) ? $context["priceMin"] : null);
            echo ";
BrainyFilter.max = BrainyFilter.max || ";
            // line 205
            echo (isset($context["priceMax"]) ? $context["priceMax"] : null);
            echo ";
BrainyFilter.lowerValue = BrainyFilter.lowerValue || ";
            // line 206
            echo (isset($context["lowerlimit"]) ? $context["lowerlimit"] : null);
            echo ";
BrainyFilter.higherValue = BrainyFilter.higherValue || ";
            // line 207
            echo (isset($context["upperlimit"]) ? $context["upperlimit"] : null);
            echo ";
BrainyFilter.currencySymb = BrainyFilter.currencySymb || '";
            // line 208
            echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
            echo "';
BrainyFilter.hideEmpty = BrainyFilter.hideEmpty || ";
            // line 209
            echo twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "hide_empty", array(), "array"));
            echo ";
BrainyFilter.baseUrl = BrainyFilter.baseUrl || \"";
            // line 210
            echo (isset($context["base"]) ? $context["base"] : null);
            echo "\";
BrainyFilter.currentRoute = BrainyFilter.currentRoute || \"";
            // line 211
            echo (isset($context["currentRoute"]) ? $context["currentRoute"] : null);
            echo "\";
BrainyFilter.selectors = BrainyFilter.selectors || {
    'container' : '";
            // line 213
            echo $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "containerSelector", array(), "array");
            echo "',
    'paginator' : '";
            // line 214
            echo $this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "behaviour", array(), "array"), "paginatorSelector", array(), "array");
            echo "'
};
";
            // line 216
            if ((isset($context["redirectToUrl"]) ? $context["redirectToUrl"] : null)) {
                // line 217
                echo "BrainyFilter.redirectTo = BrainyFilter.redirectTo || \"";
                echo (isset($context["redirectToUrl"]) ? $context["redirectToUrl"] : null);
                echo "\";
";
            }
            // line 219
            echo "jQuery(function() {
    if (!BrainyFilter.isInitialized) {
        BrainyFilter.isInitialized = true;
        var def = jQuery.Deferred();
        def.then(function() {
            if('ontouchend' in document && jQuery.ui) {
                jQuery('head').append('<script src=\"catalog/view/javascript/jquery.ui.touch-punch.min.js\"><\\/script>');
            }
        });
        if (typeof jQuery.fn.slider === 'undefined') {
            jQuery.getScript('catalog/view/javascript/jquery-ui.slider.min.js', function(){
                def.resolve();
                jQuery('head').append('<link rel=\"stylesheet\" href=\"catalog/view/theme/default/stylesheet/jquery-ui.slider.min.css\" type=\"text/css\" />');
                BrainyFilter.init();
            });
        } else {
            def.resolve();
            BrainyFilter.init();
        }
    }
});
BrainyFilter.sliderValues = BrainyFilter.sliderValues || {};
";
            // line 241
            if (twig_length_filter($this->env, (isset($context["filters"]) ? $context["filters"] : null))) {
                // line 242
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["filters"]) ? $context["filters"] : null));
                foreach ($context['_seq'] as $context["i"] => $context["section"]) {
                    // line 243
                    echo "        ";
                    if (($this->getAttribute($context["section"], "array", array(), "array", true, true) && twig_length_filter($this->env, $this->getAttribute($context["section"], "array", array(), "array")))) {
                        // line 244
                        echo "            ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["section"], "array", array(), "array"));
                        foreach ($context['_seq'] as $context["groupId"] => $context["group"]) {
                            // line 245
                            echo "                ";
                            $context["groupUID"] = (twig_slice($this->env, $this->getAttribute($context["section"], "type", array(), "array"), 0, 1) . $context["groupId"]);
                            // line 246
                            echo "                ";
                            if (twig_in_filter($this->getAttribute($context["group"], "type", array(), "array"), array(0 => "slider", 1 => "slider_lbl", 2 => "slider_lbl_inp"))) {
                                // line 247
                                echo "                    BrainyFilter.sliderValues['";
                                echo (isset($context["groupUID"]) ? $context["groupUID"] : null);
                                echo "'] = ";
                                echo twig_jsonencode_filter($this->getAttribute($context["group"], "values", array(), "array"));
                                echo ";
                ";
                            }
                            // line 249
                            echo "            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['groupId'], $context['group'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 250
                        echo "        ";
                    }
                    // line 251
                    echo "    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['i'], $context['section'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
            // line 253
            echo "</script>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/brainyfilter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  666 => 253,  659 => 251,  656 => 250,  650 => 249,  642 => 247,  639 => 246,  636 => 245,  631 => 244,  628 => 243,  623 => 242,  621 => 241,  597 => 219,  591 => 217,  589 => 216,  584 => 214,  580 => 213,  575 => 211,  571 => 210,  567 => 209,  563 => 208,  559 => 207,  555 => 206,  551 => 205,  547 => 204,  543 => 203,  539 => 202,  535 => 201,  530 => 199,  526 => 198,  522 => 197,  507 => 185,  500 => 180,  493 => 178,  490 => 177,  482 => 174,  476 => 173,  467 => 169,  460 => 167,  456 => 166,  452 => 165,  445 => 164,  440 => 161,  431 => 157,  424 => 155,  420 => 154,  416 => 153,  409 => 152,  404 => 149,  402 => 148,  399 => 147,  396 => 146,  392 => 144,  389 => 143,  386 => 142,  383 => 141,  381 => 140,  378 => 139,  376 => 138,  373 => 137,  369 => 136,  364 => 134,  356 => 133,  353 => 132,  351 => 131,  347 => 129,  343 => 127,  341 => 126,  338 => 125,  336 => 124,  333 => 123,  331 => 122,  328 => 121,  326 => 120,  323 => 119,  321 => 118,  318 => 117,  316 => 116,  313 => 115,  311 => 114,  308 => 113,  304 => 112,  301 => 111,  299 => 110,  296 => 109,  293 => 108,  288 => 104,  282 => 103,  273 => 99,  266 => 97,  262 => 96,  258 => 95,  251 => 94,  246 => 91,  237 => 87,  230 => 85,  226 => 84,  222 => 83,  215 => 82,  210 => 79,  208 => 78,  205 => 77,  202 => 76,  198 => 74,  195 => 73,  192 => 72,  189 => 71,  186 => 70,  184 => 69,  181 => 68,  177 => 67,  172 => 65,  163 => 64,  160 => 63,  157 => 62,  145 => 56,  134 => 52,  125 => 46,  118 => 44,  115 => 43,  112 => 42,  109 => 40,  105 => 39,  102 => 38,  96 => 36,  93 => 35,  87 => 33,  84 => 32,  80 => 30,  76 => 28,  74 => 27,  68 => 24,  64 => 23,  60 => 22,  56 => 21,  52 => 20,  48 => 19,  44 => 18,  40 => 17,  36 => 16,  31 => 13,  29 => 12,  25 => 10,  23 => 9,  21 => 8,  19 => 7,);
    }
}
/* {#*/
/*  * Brainy Filter Pro 5.1.3 OC3, September 18, 2017 / brainyfilter.com */
/*  * Copyright 2015-2017 Giant Leap Lab / www.giantleaplab.com */
/*  * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store. */
/*  * Support: http://support.giantleaplab.com */
/* #}*/
/* {% set isHorizontal = layout_position is same as ('content_top') or layout_position is same as ('content_bottom') %}*/
/* {% set isResponsive = settings['style']['responsive']['enabled'] ? true : false %}*/
/* {% set responsivePos = settings['style']['responsive']['position'] is same as ('right') ? 'bf-right' : 'bf-left' %}*/
/* */
/* */
/* {% if filters|length %}*/
/* */
/*             <ul class="filter">*/
/*                 <form class="bf-form"*/
/*                     data-height-limit="{{ limit_height_opts }}"*/
/*                     data-visible-items="{{ slidingOpts }}"*/
/*                     data-hide-items="{{ slidingMin }}"*/
/*                     data-submit-type="{{ settings['submission']['submit_type'] }}"*/
/*                     data-submit-delay="{{ settings['submission']['submit_delay_time']|number_format }}"*/
/*                     data-submit-hide-panel ="{{ settings['submission']['hide_panel']|number_format }}"*/
/*                     data-resp-max-width="{{ settings['style']['responsive']['max_width']|number_format }}"*/
/*                     data-resp-collapse="{{ settings['style']['responsive']['collapsed']|number_format }}"*/
/*                     data-resp-max-scr-width ="{{ settings['style']['responsive']['max_screen_width']|number_format }}"*/
/*                     method="get" action="index.php">*/
/* */
/*                 {% if currentRoute is same as ('product/search') %}*/
/*                     <input type="hidden" name="route" value="product/search" />*/
/*                 {% else %}*/
/*                     <input type="hidden" name="route" value="product/category" />*/
/*                 {% endif %}*/
/*                 {% if currentPath %}*/
/*                     <input type="hidden" name="path_f" value="{{ currentPath }}" />*/
/*                 {% endif %}*/
/*                 {% if manufacturerId %}*/
/*                     <input type="hidden" name="manufacturer_id" value="{{ manufacturerId }}" />*/
/*                 {% endif %}*/
/* */
/*                 {% for i, section in filters %}*/
/* */
/*                         {# PRICE FILTER#}*/
/*                     {% if section['type'] == 'price' %}*/
/* */
/*                         <li class="filter__item {% if not section['collapsed'] %} is-show {% endif %}">*/
/*                             <a class="filter__title" href="javascript:void(0)">*/
/*                                 {{ lang_price }}*/
/*                                 <i class="filter__icon icon-arrow-r"></i>*/
/*                             </a>*/
/*                             <ul class="filter__subitem">*/
/*                                 <li class="input">*/
/*                                     <label>От</label>*/
/*                                     <input type="number" name="bfp_price_min" value="{{ lowerlimit }}" placeholder="{{ lowerlimit }} {{ currency_symbol }}">*/
/*                                 </li>*/
/*                                 <li class="input">*/
/*                                     <label>До</label>*/
/*                                     <input type="number" name="bfp_price_max" value="{{ upperlimit }}" placeholder="{{ upperlimit }} {{ currency_symbol }}">*/
/*                                 </li>*/
/*                             </ul>*/
/*                         </li>*/
/* */
/*                         {# Categories FILTER #}*/
/*                     {% elseif section['type'] == 'category' %}*/
/*                         {% set groupUID = 'c0' %}*/
/*                         <li class="filter__item bf-attr-{{ groupUID }} {% if not section['collapsed'] %} is-show {% endif %}">*/
/*                             <a class="filter__title" href="javascript:void(0)">{{ lang_categories }}<i class="filter__icon icon-arrow-r"></i></a>*/
/*                             <ul class="filter__subitem">*/
/*                                 {% for cat in section['values'] %}*/
/* */
/*                                     {% set catId = cat['id'] %}*/
/*                                     {% if totals is defined and settings['behaviour']['hide_empty'] %}*/
/*                                         {% set inStock = postponedCount or (totals[groupUID][catId] is defined and totals[groupUID][catId]) %}*/
/*                                         {% set inSelected = selected[groupUID] is defined and catId in selected[groupUID] %}*/
/*                                         {% if not inStock and not inSelected %}*/
/*                                             bf-disabled*/
/*                                         {% endif %}*/
/*                                     {% endif %}*/
/* */
/*                                     {% if section['control'] == 'checkbox' %}*/
/*                                         <li class="checkbox">*/
/*                                             <input*/
/*                                                     type="checkbox"*/
/*                                                     name="bfp_{{ groupUID }}{% if section['control'] is same as ('checkbox') %}{{ '_' ~ catId }}{% endif %}"*/
/*                                                     id="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"*/
/*                                                     value="{{ catId }}"*/
/*                                                     {% if selected[groupUID] is defined and catId in selected[groupUID] %} checked="true" {% endif %} />*/
/* */
/*                                             <label for="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"><span>{{ cat['name'] }}</span></label>*/
/* */
/*                                         </li>*/
/*                                     {% else %}*/
/*                                         <li class="radio">*/
/*                                             <input*/
/*                                                     type="radio"*/
/*                                                     name="bfp_{{ groupUID }}{% if section['control'] is same as ('checkbox') %}{{ '_' ~ catId }}{% endif %}"*/
/*                                                     id="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"*/
/*                                                     value="{{ catId }}"*/
/*                                                     {% if selected[groupUID] is defined and catId in selected[groupUID] %} checked="true" {% endif %} />*/
/* */
/*                                             <label for="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"><span>{{ cat['name'] }}</span></label>*/
/* */
/*                                         </li>*/
/*                                     {% endif %}*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         </li>*/
/* */
/*                         {# OTHER FILTERS#}*/
/*                     {% else %}*/
/* */
/*                         {% set curGroupId = null %}*/
/* */
/*                         {% for groupId, group in section['array'] %}*/
/* */
/*                             {% set collapsedGroup = false %}*/
/* */
/*                             {% if group['attr_id'] and settings['behaviour']['sections']['attribute'][group['attr_id']] %}*/
/* */
/*                                 {% set collapsedGroup = settings['behaviour']['sections']['attribute'][group['attr_id']]['collapsed'] %}*/
/* */
/*                             {% elseif group['option_id'] and settings['behaviour']['sections']['options'][group['option_id']] %}*/
/* */
/*                                 {% set collapsedGroup = settings['behaviour']['sections']['options'][group['option_id']]['collapsed'] %}*/
/* */
/*                             {% elseif group['filter_id'] and settings['behaviour']['sections']['filter'][group['filter_id']] %}*/
/* */
/*                                 {% set collapsedGroup = settings['behaviour']['sections']['filter'][group['filter_id']]['collapsed'] %}*/
/* */
/*                             {% endif %}*/
/* */
/* */
/*                             {% set groupUID = section['type']|slice(0, 1) ~ groupId %}*/
/* */
/*                             <li class="filter__item bf-attr-{{ groupUID }} {% if not section['collapsed'] %} is-show {% endif %}">*/
/*                                 <a class="filter__title" href="javascript:void(0)">{{ group['name'] ~ ' ' }}<i class="filter__icon icon-arrow-r"></i></a>*/
/*                                 <ul class="filter__subitem">*/
/*                                     {% for cat in group['values'] %}*/
/* */
/*                                         {% set catId  = cat['id'] %}*/
/* */
/*                                         {% if totals is defined and settings['behaviour']['hide_empty'] %}*/
/*                                             {% set inStock = postponedCount or (totals[groupUID][valueId] is defined and totals[groupUID][catId]) %}*/
/*                                             {% set inSelected = selected[groupUID] is defined and valueId in selected[groupUID] %}*/
/*                                             {% if not inStock and not inSelected %}*/
/*                                                 bf-disabled*/
/*                                             {% endif %}*/
/*                                         {% endif %}*/
/* */
/*                                         {% if group['type'] == 'checkbox' %}*/
/*                                             <li class="checkbox">*/
/*                                                 <input*/
/*                                                         type="checkbox"*/
/*                                                         name="bfp_{{ groupUID }}{% if section['control'] is same as ('checkbox') %}{{ '_' ~ catId }}{% endif %}"*/
/*                                                         id="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"*/
/*                                                         value="{{ catId }}"*/
/*                                                         {% if selected[groupUID] is defined and catId in selected[groupUID] %} checked="true" {% endif %} />*/
/* */
/*                                                 <label for="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"><span>{{ cat['name'] }}</span></label>*/
/* */
/*                                             </li>*/
/*                                         {% else %}*/
/*                                             <li class="radio">*/
/*                                                 <input*/
/*                                                         type="radio"*/
/*                                                         name="bfp_{{ groupUID }}{% if section['control'] is same as ('checkbox') %}{{ '_' ~ catId }}{% endif %}"*/
/*                                                         id="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"*/
/*                                                         value="{{ catId }}"*/
/*                                                         {% if selected[groupUID] is defined and catId in selected[groupUID] %} checked="true" {% endif %} />*/
/* */
/*                                                 <label for="bf-attr-{{ groupUID ~ '_' ~ catId ~ '_' ~ layout_id }}"><span>{{ cat['name'] }}</span></label>*/
/* */
/*                                             </li>*/
/*                                         {% endif %}*/
/*                                     {% endfor %}*/
/*                                 </ul>*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     {% endif %}*/
/* */
/*                 {% endfor %}*/
/* */
/*                 <li class="filter__action">*/
/*                     <a class="btn__default-outline btn__default-outline--filter filter__toggle"*/
/*                        href="javascript:void(0)"*/
/*                        onclick="BrainyFilter.reset()">*/
/*                         {{ reset }}*/
/*                     </a>*/
/*                     <a class="btn__primary btn__primary--filter filter__toggle" href="javascript:void(0)">Применить</a>*/
/*                 </li>*/
/* */
/*                 </form>*/
/*             </ul>*/
/*             <a class="filter__toggle-btn filter__toggle" href="javascript:void(0)">Фильтр</a>*/
/* */
/* */
/* <script>*/
/* var bfLang = {*/
/*     show_more : '{{ lang_show_more }}',*/
/*     show_less : '{{ lang_show_less }}',*/
/*     empty_list : '{{ lang_empty_list }}'*/
/* };*/
/* BrainyFilter.requestCount = BrainyFilter.requestCount || {{ settings['behaviour']['product_count'] ? 'true' : 'false' }};*/
/* BrainyFilter.requestPrice = BrainyFilter.requestPrice || {{ settings['behaviour']['sections']['price']['enabled'] ? 'true' : 'false' }};*/
/* BrainyFilter.separateCountRequest = BrainyFilter.separateCountRequest || {{ postponedCount ? 'true' : 'false' }};*/
/* BrainyFilter.min = BrainyFilter.min || {{ priceMin }};*/
/* BrainyFilter.max = BrainyFilter.max || {{ priceMax }};*/
/* BrainyFilter.lowerValue = BrainyFilter.lowerValue || {{ lowerlimit }};*/
/* BrainyFilter.higherValue = BrainyFilter.higherValue || {{ upperlimit }};*/
/* BrainyFilter.currencySymb = BrainyFilter.currencySymb || '{{ currency_symbol }}';*/
/* BrainyFilter.hideEmpty = BrainyFilter.hideEmpty || {{ settings['behaviour']['hide_empty']|number_format }};*/
/* BrainyFilter.baseUrl = BrainyFilter.baseUrl || "{{ base }}";*/
/* BrainyFilter.currentRoute = BrainyFilter.currentRoute || "{{ currentRoute }}";*/
/* BrainyFilter.selectors = BrainyFilter.selectors || {*/
/*     'container' : '{{ settings['behaviour']['containerSelector'] }}',*/
/*     'paginator' : '{{ settings['behaviour']['paginatorSelector'] }}'*/
/* };*/
/* {% if redirectToUrl %}*/
/* BrainyFilter.redirectTo = BrainyFilter.redirectTo || "{{ redirectToUrl }}";*/
/* {% endif %}*/
/* jQuery(function() {*/
/*     if (!BrainyFilter.isInitialized) {*/
/*         BrainyFilter.isInitialized = true;*/
/*         var def = jQuery.Deferred();*/
/*         def.then(function() {*/
/*             if('ontouchend' in document && jQuery.ui) {*/
/*                 jQuery('head').append('<script src="catalog/view/javascript/jquery.ui.touch-punch.min.js"><\/script>');*/
/*             }*/
/*         });*/
/*         if (typeof jQuery.fn.slider === 'undefined') {*/
/*             jQuery.getScript('catalog/view/javascript/jquery-ui.slider.min.js', function(){*/
/*                 def.resolve();*/
/*                 jQuery('head').append('<link rel="stylesheet" href="catalog/view/theme/default/stylesheet/jquery-ui.slider.min.css" type="text/css" />');*/
/*                 BrainyFilter.init();*/
/*             });*/
/*         } else {*/
/*             def.resolve();*/
/*             BrainyFilter.init();*/
/*         }*/
/*     }*/
/* });*/
/* BrainyFilter.sliderValues = BrainyFilter.sliderValues || {};*/
/* {% if filters|length %}*/
/*     {% for i, section in filters %}*/
/*         {% if section['array'] is defined and section['array']|length %}*/
/*             {% for groupId, group in section['array'] %}*/
/*                 {% set groupUID = section['type']|slice(0, 1) ~ groupId %}*/
/*                 {% if group['type'] in ['slider', 'slider_lbl', 'slider_lbl_inp'] %}*/
/*                     BrainyFilter.sliderValues['{{ groupUID }}'] = {{ group['values']|json_encode() }};*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         {% endif %}*/
/*     {% endfor %}*/
/* {% endif %}*/
/* </script>*/
/* {% endif %}*/
