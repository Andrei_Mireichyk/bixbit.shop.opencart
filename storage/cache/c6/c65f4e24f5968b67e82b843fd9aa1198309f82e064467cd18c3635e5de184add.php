<?php

/* default/template/product/manufacturer_list.twig */
class __TwigTemplate_f70afc67cae648d10e2cd27dcd2c8a8c8ad577f56c9df7628b154c5ba6e15535 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 8
        echo "<main class=\"manufacturer\">
    <div class=\"manufacturer__wrap\">
        <h1 class=\"manufacturer__title\">";
        // line 10
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
        <ul class=\"manufacturer__letter_nav\">
            <li>";
        // line 12
        echo (isset($context["text_index"]) ? $context["text_index"] : null);
        echo "</li>
            ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 14
            echo "                <li><a href=\"index.php?route=product/manufacturer#";
            echo $this->getAttribute($context["category"], "name", array());
            echo "\">";
            echo $this->getAttribute($context["category"], "name", array());
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </ul>
        <ul class=\"manufacturer__item\">
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 19
            echo "            <li><span id=\"";
            echo $this->getAttribute($context["category"], "name", array());
            echo "\">";
            echo $this->getAttribute($context["category"], "name", array());
            echo "</span>
            ";
            // line 20
            if ($this->getAttribute($context["category"], "manufacturer", array())) {
                // line 21
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "manufacturer", array()), 4));
                foreach ($context['_seq'] as $context["_key"] => $context["manufacturers"]) {
                    // line 22
                    echo "                <ul>
                        ";
                    // line 23
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["manufacturers"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                        // line 24
                        echo "                            <li><a href=\"";
                        echo $this->getAttribute($context["manufacturer"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["manufacturer"], "name", array());
                        echo "</a></li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 26
                    echo "
                </ul>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturers'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "            ";
            }
            // line 30
            echo "            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        </ul>

    </div>

    <div class=\"manufacturer__breadcrumb\">
        <ul class=\"breadcrumb\">
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 39
            echo "                <li class=\"breadcrumb__item\"><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\" class=\"breadcrumb__link\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "        </ul>
    </div>
</main>

";
        // line 45
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "    <link href=\"catalog/view/theme/default/stylesheet/manufacturer.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"catalog/view/theme/default/js/manufacturer.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/product/manufacturer_list.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 6,  156 => 5,  151 => 3,  148 => 2,  144 => 45,  138 => 41,  127 => 39,  123 => 38,  115 => 32,  108 => 30,  105 => 29,  97 => 26,  86 => 24,  82 => 23,  79 => 22,  74 => 21,  72 => 20,  65 => 19,  61 => 18,  57 => 16,  46 => 14,  42 => 13,  38 => 12,  33 => 10,  29 => 8,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/manufacturer.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/manufacturer.bundle.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* <main class="manufacturer">*/
/*     <div class="manufacturer__wrap">*/
/*         <h1 class="manufacturer__title">{{ heading_title }}</h1>*/
/*         <ul class="manufacturer__letter_nav">*/
/*             <li>{{ text_index }}</li>*/
/*             {% for category in categories %}*/
/*                 <li><a href="index.php?route=product/manufacturer#{{ category.name }}">{{ category.name }}</a></li>*/
/*             {% endfor %}*/
/*         </ul>*/
/*         <ul class="manufacturer__item">*/
/*         {% for category in categories %}*/
/*             <li><span id="{{ category.name }}">{{ category.name }}</span>*/
/*             {% if category.manufacturer %}*/
/*                 {% for manufacturers in category.manufacturer|batch(4) %}*/
/*                 <ul>*/
/*                         {% for manufacturer in manufacturers %}*/
/*                             <li><a href="{{ manufacturer.href }}">{{ manufacturer.name }}</a></li>*/
/*                         {% endfor %}*/
/* */
/*                 </ul>*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*             </li>*/
/*         {% endfor %}*/
/*         </ul>*/
/* */
/*     </div>*/
/* */
/*     <div class="manufacturer__breadcrumb">*/
/*         <ul class="breadcrumb">*/
/*             {% for breadcrumb in breadcrumbs %}*/
/*                 <li class="breadcrumb__item"><a href="{{ breadcrumb.href }}" class="breadcrumb__link">{{ breadcrumb.text }}</a></li>*/
/*             {% endfor %}*/
/*         </ul>*/
/*     </div>*/
/* </main>*/
/* */
/* {{ footer }}*/
