<?php

/* default/template/extension/quickcheckout/payment_method.twig */
class __TwigTemplate_0316fb426e38d7e63ff02599ae1c20b3da0eaef5c06ebe7ed0c3900a9acce18c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["payment_methods"]) ? $context["payment_methods"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["payment_method"]) {
            // line 2
            echo "\t<div class=\"radio\">
        ";
            // line 3
            if (($this->getAttribute($context["payment_method"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                // line 4
                echo "\t\t\t<input type=\"radio\" name=\"payment_method\" value=\"";
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "\" id=\"";
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "\" checked=\"checked\">
\t\t";
            } else {
                // line 6
                echo "\t\t\t<input type=\"radio\" name=\"payment_method\" value=\"";
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "\" id=\"";
                echo $this->getAttribute($context["payment_method"], "code", array());
                echo "\" >
        ";
            }
            // line 8
            echo "
\t\t<label for=\"";
            // line 9
            echo $this->getAttribute($context["payment_method"], "code", array());
            echo "\"><span>";
            echo $this->getAttribute($context["payment_method"], "title", array());
            echo "</span></label>
\t</div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment_method'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "
<div class=\"contact__hidden\">
\t<textarea type=\"text\" name=\"comment\"></textarea>
\t<textarea type=\"text\" name=\"survey\"></textarea>
</div>


";
        // line 20
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 21
            echo "<div class=\"alert alert-danger\">";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "</div>
";
        }
        // line 23
        echo "
<script type=\"text/javascript\"><!--
\$('#payment-method input[name=\\'payment_method\\'], #payment-method select[name=\\'payment_method\\']').on('change', function() {
\t";
        // line 26
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 27
            echo "\t\t\$.ajax({
\t\t\turl: 'index.php?route=extension/quickcheckout/payment_method/set',
\t\t\ttype: 'post',
\t\t\tdata: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-method input[type=\\'text\\'], #payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea'),
\t\t\tdataType: 'html',
\t\t\tcache: false,
\t\t\tsuccess: function(html) {
\t\t\t\t";
            // line 34
            if (((isset($context["cart"]) ? $context["cart"] : null) && (isset($context["payment_reload"]) ? $context["payment_reload"] : null))) {
                // line 35
                echo "\t\t\t\tloadCart();
\t\t\t\t";
            }
            // line 37
            echo "\t\t\t},
\t\t\t";
            // line 38
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 39
                echo "\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t\t";
            }
            // line 43
            echo "\t\t});
\t";
        } else {
            // line 45
            echo "\t\tif (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
\t\t\tvar url = 'index.php?route=extension/quickcheckout/payment_method/set';
\t\t\tvar post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-method input[type=\\'text\\'], #payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea');
\t\t} else {
\t\t\tvar url = 'index.php?route=extension/quickcheckout/payment_method/set&address_id=' + \$('#payment-address select[name=\\'address_id\\']').val();
\t\t\tvar post_data = \$('#payment-method input[type=\\'text\\'], #payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea');
\t\t}

\t\t\$.ajax({
\t\t\turl: url,
\t\t\ttype: 'post',
\t\t\tdata: post_data,
\t\t\tdataType: 'html',
\t\t\tcache: false,
\t\t\tsuccess: function(html) {
\t\t\t\t";
            // line 60
            if (((isset($context["cart"]) ? $context["cart"] : null) && (isset($context["payment_reload"]) ? $context["payment_reload"] : null))) {
                // line 61
                echo "\t\t\t\tloadCart();
\t\t\t\t";
            }
            // line 63
            echo "\t\t\t},
\t\t\t";
            // line 64
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 65
                echo "\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t\t";
            }
            // line 69
            echo "\t\t});
\t";
        }
        // line 71
        echo "});

";
        // line 73
        if ((isset($context["payment_reload"]) ? $context["payment_reload"] : null)) {
            // line 74
            echo "\$(document).ready(function() {
\t\$('#payment-method input[name=\\'payment_method\\']:checked, #payment-method select[name=\\'payment_method\\']').trigger('change');
});
";
        }
        // line 78
        echo "//--></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/payment_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 78,  157 => 74,  155 => 73,  151 => 71,  147 => 69,  141 => 65,  139 => 64,  136 => 63,  132 => 61,  130 => 60,  113 => 45,  109 => 43,  103 => 39,  101 => 38,  98 => 37,  94 => 35,  92 => 34,  83 => 27,  81 => 26,  76 => 23,  70 => 21,  68 => 20,  59 => 13,  47 => 9,  44 => 8,  36 => 6,  28 => 4,  26 => 3,  23 => 2,  19 => 1,);
    }
}
/* {% for payment_method in payment_methods %}*/
/* 	<div class="radio">*/
/*         {% if payment_method.code == code %}*/
/* 			<input type="radio" name="payment_method" value="{{ payment_method.code }}" id="{{ payment_method.code }}" checked="checked">*/
/* 		{% else %}*/
/* 			<input type="radio" name="payment_method" value="{{ payment_method.code }}" id="{{ payment_method.code }}" >*/
/*         {% endif %}*/
/* */
/* 		<label for="{{ payment_method.code }}"><span>{{ payment_method.title }}</span></label>*/
/* 	</div>*/
/* */
/* {% endfor %}*/
/* */
/* <div class="contact__hidden">*/
/* 	<textarea type="text" name="comment"></textarea>*/
/* 	<textarea type="text" name="survey"></textarea>*/
/* </div>*/
/* */
/* */
/* {% if error_warning %}*/
/* <div class="alert alert-danger">{{ error_warning }}</div>*/
/* {% endif %}*/
/* */
/* <script type="text/javascript"><!--*/
/* $('#payment-method input[name=\'payment_method\'], #payment-method select[name=\'payment_method\']').on('change', function() {*/
/* 	{% if not logged %}*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=extension/quickcheckout/payment_method/set',*/
/* 			type: 'post',*/
/* 			data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-method input[type=\'text\'], #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea'),*/
/* 			dataType: 'html',*/
/* 			cache: false,*/
/* 			success: function(html) {*/
/* 				{% if cart and payment_reload %}*/
/* 				loadCart();*/
/* 				{% endif %}*/
/* 			},*/
/* 			{% if debug %}*/
/* 			error: function(xhr, ajaxOptions, thrownError) {*/
/* 				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 			}*/
/* 			{% endif %}*/
/* 		});*/
/* 	{% else %}*/
/* 		if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/* 			var url = 'index.php?route=extension/quickcheckout/payment_method/set';*/
/* 			var post_data = $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-method input[type=\'text\'], #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea');*/
/* 		} else {*/
/* 			var url = 'index.php?route=extension/quickcheckout/payment_method/set&address_id=' + $('#payment-address select[name=\'address_id\']').val();*/
/* 			var post_data = $('#payment-method input[type=\'text\'], #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea');*/
/* 		}*/
/* */
/* 		$.ajax({*/
/* 			url: url,*/
/* 			type: 'post',*/
/* 			data: post_data,*/
/* 			dataType: 'html',*/
/* 			cache: false,*/
/* 			success: function(html) {*/
/* 				{% if cart and payment_reload %}*/
/* 				loadCart();*/
/* 				{% endif %}*/
/* 			},*/
/* 			{% if debug %}*/
/* 			error: function(xhr, ajaxOptions, thrownError) {*/
/* 				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 			}*/
/* 			{% endif %}*/
/* 		});*/
/* 	{% endif %}*/
/* });*/
/* */
/* {% if payment_reload %}*/
/* $(document).ready(function() {*/
/* 	$('#payment-method input[name=\'payment_method\']:checked, #payment-method select[name=\'payment_method\']').trigger('change');*/
/* });*/
/* {% endif %}*/
/* //--></script>*/
/* */
