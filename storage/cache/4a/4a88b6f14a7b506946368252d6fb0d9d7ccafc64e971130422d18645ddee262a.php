<?php

/* default/template/extension/quickcheckout/cart.twig */
class __TwigTemplate_e31da98eaa1c4e5b49a30975596d879e27e56436eb819ac21613ab6c93a4d954 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 2
            echo "\t<div class=\"product__header\">
\t\t<div class=\"product__header-title\">ВАШ ЗАКАЗ</div>
\t\t<div class=\"product__header-edit\"><a class=\"product__edit-link\" data-toggle=\"dialog\" data-target=\"#dialog\" onclick=\"getOCwizardModal_smca(false,'load')\">Изменить</a></div>
\t</div>
\t<div class=\"product__list\">
        ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 8
                echo "\t\t\t<a class=\"product__item ";
                if ( !$this->getAttribute($context["product"], "stock", array())) {
                    echo "error-stock ";
                }
                echo "\" href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">
\t\t\t\t<img class=\"product__image\" src=\"";
                // line 9
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\">
\t\t\t\t<div class=\"product__group\">
\t\t\t\t\t<div class=\"product__title\">";
                // line 11
                echo $this->getAttribute($context["product"], "name", array());
                echo "</div>
\t\t\t\t\t<div class=\"product__details\">
\t\t\t\t\t\t<div class=\"product__count\">";
                // line 13
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "шт.</div>
\t\t\t\t\t\t<div class=\"product__price\">";
                // line 14
                echo $this->getAttribute($context["product"], "total", array());
                echo "</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "\t</div>

\t<div class=\"product__estimation\">
        ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 23
                echo "\t\t\t<div class=\"product__estimation";
                echo $this->getAttribute($context["total"], "class", array());
                echo "\">
\t\t\t\t<div>";
                // line 24
                echo $this->getAttribute($context["total"], "title", array());
                echo ":</div>
\t\t\t\t<div>";
                // line 25
                echo $this->getAttribute($context["total"], "text", array());
                echo "</div>
\t\t\t</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 28,  88 => 25,  84 => 24,  79 => 23,  75 => 22,  70 => 19,  59 => 14,  55 => 13,  50 => 11,  41 => 9,  32 => 8,  28 => 7,  21 => 2,  19 => 1,);
    }
}
/* {% if products or vouchers %}*/
/* 	<div class="product__header">*/
/* 		<div class="product__header-title">ВАШ ЗАКАЗ</div>*/
/* 		<div class="product__header-edit"><a class="product__edit-link" data-toggle="dialog" data-target="#dialog" onclick="getOCwizardModal_smca(false,'load')">Изменить</a></div>*/
/* 	</div>*/
/* 	<div class="product__list">*/
/*         {% for product in products %}*/
/* 			<a class="product__item {% if not product.stock %}error-stock {% endif %}" href="{{ product.href }}">*/
/* 				<img class="product__image" src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}">*/
/* 				<div class="product__group">*/
/* 					<div class="product__title">{{ product.name }}</div>*/
/* 					<div class="product__details">*/
/* 						<div class="product__count">{{ product.quantity }}шт.</div>*/
/* 						<div class="product__price">{{ product.total }}</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</a>*/
/*         {% endfor %}*/
/* 	</div>*/
/* */
/* 	<div class="product__estimation">*/
/*         {% for total in totals %}*/
/* 			<div class="product__estimation{{ total.class }}">*/
/* 				<div>{{ total.title }}:</div>*/
/* 				<div>{{ total.text }}</div>*/
/* 			</div>*/
/*         {% endfor %}*/
/* 	</div>*/
/* {% endif %}*/
