<?php

/* default/template/common/footer.twig */
class __TwigTemplate_561b1aa2c048e9c67c781689fd536b5a8bf15fbbcc3a6159aa0a66d82fc41cb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer class=\"footer\">
    <div class=\"footer__wrap\">
        <ul class=\"footer__menu\">
            ";
        // line 4
        if ((twig_length_filter($this->env, (isset($context["email_list"]) ? $context["email_list"] : null)) > 0)) {
            // line 5
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["email_list"]) ? $context["email_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
                // line 6
                echo "                    <li class=\"footer__item\">
                        <a class=\"footer__link\" href=\"mailto:";
                // line 7
                echo $context["email"];
                echo "\">
                            <i class=\"icon-mail\"></i>
                            <span>";
                // line 9
                echo $context["email"];
                echo "</span>
                        </a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "            ";
        }
        // line 14
        echo "            ";
        if ((twig_length_filter($this->env, (isset($context["telephone_list"]) ? $context["telephone_list"] : null)) > 0)) {
            // line 15
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["telephone_list"]) ? $context["telephone_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                // line 16
                echo "                    <li class=\"footer__item\">
                        <a class=\"footer__link\" href=\"tel:";
                // line 17
                echo $context["phone"];
                echo "\">
                            <i class=\"icon-phone-1\"></i>
                            <span>";
                // line 19
                echo $context["phone"];
                echo "</span>
                        </a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "            ";
        }
        // line 24
        echo "        </ul>

        ";
        // line 26
        if ((isset($context["informations"]) ? $context["informations"] : null)) {
            // line 27
            echo "            <ul class=\"footer__menu\">
                ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 29
                echo "                    <li class=\"footer__item\">
                        <a class=\"footer__link\" href=\"";
                // line 30
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "            </ul>
        ";
        }
        // line 35
        echo "
        <ul class=\"footer__menu\">
            <li class=\"footer__item\"><a class=\"footer__link\" href=\"";
        // line 37
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>
            <li class=\"footer__item\"><a class=\"footer__link\" href=\"";
        // line 38
        echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
        echo "\">";
        echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
        echo "</a></li>
        </ul>

        <ul class=\"footer__menu\">
            <li class=\"footer__item\"><a class=\"footer__link\" href=\"";
        // line 42
        echo (isset($context["special"]) ? $context["special"] : null);
        echo "\">";
        echo (isset($context["text_special"]) ? $context["text_special"] : null);
        echo "</a></li>
            <li class=\"footer__item\"><a class=\"footer__link\" href=\"";
        // line 43
        echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
        echo "\">";
        echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
        echo "</a></li>
        </ul>





    </div>
</footer>
</div>
";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 54
            echo "    <script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "
        <script type=\"text/javascript\">


          function getOCwizardModal_smca(product_id, action) {
            var basket = document.querySelector('.basket');
            quantity = typeof(quantity) != 'undefined' ? quantity : 1;

              var all_keys = [product_id];

              \$('.optional-prods__checkbox:checked').each(function (key, val) {
                  all_keys.push(val.value);
              });

              if(!all_keys.length) return false;

              var prod_ids = all_keys.join(\";\");

            if (action == \"add\") {
              \$.ajax({
                url: 'index.php?route=checkout/cart/add',
                type: 'post',
                data: 'product_id=' + prod_ids + '&quantity=' + quantity,
                dataType: 'json',
                success: function(json) {
                  if (json['redirect']) {
                    location = json['redirect'];
                  }
                  if (json['success']) {
                    buttonManipulate();
                    \$('#dialog').addClass('is-show');
                    getOCwizardModal_smca(product_id, 'load');
                  }
                }
              });
            }

            if (action == \"load\" || action == \"load_option\") {

                \$.ajax({
                    url: 'index.php?route=extension/module/ocdev_smart_cart',
                    type: 'get',
                    success: function(html) {
                        console.log('Корзина загружена')
                        basket.innerHTML = html;
                    }
                });

            }
          }
        </script>
      
</body>
</html>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 56,  156 => 54,  152 => 53,  137 => 43,  131 => 42,  122 => 38,  116 => 37,  112 => 35,  108 => 33,  97 => 30,  94 => 29,  90 => 28,  87 => 27,  85 => 26,  81 => 24,  78 => 23,  68 => 19,  63 => 17,  60 => 16,  55 => 15,  52 => 14,  49 => 13,  39 => 9,  34 => 7,  31 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* <footer class="footer">*/
/*     <div class="footer__wrap">*/
/*         <ul class="footer__menu">*/
/*             {% if  email_list|length > 0%}*/
/*                 {% for email in email_list %}*/
/*                     <li class="footer__item">*/
/*                         <a class="footer__link" href="mailto:{{ email }}">*/
/*                             <i class="icon-mail"></i>*/
/*                             <span>{{ email }}</span>*/
/*                         </a>*/
/*                     </li>*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*             {% if  telephone_list|length > 0%}*/
/*                 {% for phone in telephone_list %}*/
/*                     <li class="footer__item">*/
/*                         <a class="footer__link" href="tel:{{ phone }}">*/
/*                             <i class="icon-phone-1"></i>*/
/*                             <span>{{ phone }}</span>*/
/*                         </a>*/
/*                     </li>*/
/*                 {% endfor %}*/
/*             {% endif %}*/
/*         </ul>*/
/* */
/*         {% if informations %}*/
/*             <ul class="footer__menu">*/
/*                 {% for information in informations %}*/
/*                     <li class="footer__item">*/
/*                         <a class="footer__link" href="{{ information.href }}">{{ information.title }}</a>*/
/*                     </li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         {% endif %}*/
/* */
/*         <ul class="footer__menu">*/
/*             <li class="footer__item"><a class="footer__link" href="{{ contact }}">{{ text_contact }}</a></li>*/
/*             <li class="footer__item"><a class="footer__link" href="{{ sitemap }}">{{ text_sitemap }}</a></li>*/
/*         </ul>*/
/* */
/*         <ul class="footer__menu">*/
/*             <li class="footer__item"><a class="footer__link" href="{{ special }}">{{ text_special }}</a></li>*/
/*             <li class="footer__item"><a class="footer__link" href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>*/
/*         </ul>*/
/* */
/* */
/* */
/* */
/* */
/*     </div>*/
/* </footer>*/
/* </div>*/
/* {% for script in scripts %}*/
/*     <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* */
/*         <script type="text/javascript">*/
/* */
/* */
/*           function getOCwizardModal_smca(product_id, action) {*/
/*             var basket = document.querySelector('.basket');*/
/*             quantity = typeof(quantity) != 'undefined' ? quantity : 1;*/
/* */
/*               var all_keys = [product_id];*/
/* */
/*               $('.optional-prods__checkbox:checked').each(function (key, val) {*/
/*                   all_keys.push(val.value);*/
/*               });*/
/* */
/*               if(!all_keys.length) return false;*/
/* */
/*               var prod_ids = all_keys.join(";");*/
/* */
/*             if (action == "add") {*/
/*               $.ajax({*/
/*                 url: 'index.php?route=checkout/cart/add',*/
/*                 type: 'post',*/
/*                 data: 'product_id=' + prod_ids + '&quantity=' + quantity,*/
/*                 dataType: 'json',*/
/*                 success: function(json) {*/
/*                   if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                   }*/
/*                   if (json['success']) {*/
/*                     buttonManipulate();*/
/*                     $('#dialog').addClass('is-show');*/
/*                     getOCwizardModal_smca(product_id, 'load');*/
/*                   }*/
/*                 }*/
/*               });*/
/*             }*/
/* */
/*             if (action == "load" || action == "load_option") {*/
/* */
/*                 $.ajax({*/
/*                     url: 'index.php?route=extension/module/ocdev_smart_cart',*/
/*                     type: 'get',*/
/*                     success: function(html) {*/
/*                         console.log('Корзина загружена')*/
/*                         basket.innerHTML = html;*/
/*                     }*/
/*                 });*/
/* */
/*             }*/
/*           }*/
/*         </script>*/
/*       */
/* </body>*/
/* </html>*/
/* */
