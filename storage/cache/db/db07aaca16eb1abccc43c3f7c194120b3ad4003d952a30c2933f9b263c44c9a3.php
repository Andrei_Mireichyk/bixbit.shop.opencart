<?php

/* default/template/error/not_found.twig */
class __TwigTemplate_0d8a48acf3efc2cd374c0cb1487218042bda78bf3535076f51d418d0a8f36c49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"err_404\">
  <h1 class=\"err_404__title\">";
        // line 11
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
  <h2 class=\"err_404__subtitle\">Вы можете найти еще много интересного у нас на сайте</h2><img class=\"err_404__bg\" src=\"catalog/view/theme/default/images/bg.svg\">
  <a class=\"btn__default\" href=\"";
        // line 13
        echo (isset($context["continue"]) ? $context["continue"] : null);
        echo "\">";
        echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
        echo "</a>
</main>

";
        // line 16
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "  <link href=\"catalog/view/theme/default/stylesheet/404.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "  <script src=\"catalog/view/theme/default/js/404.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/error/not_found.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 7,  61 => 6,  56 => 4,  53 => 3,  47 => 16,  39 => 13,  34 => 11,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*   <link href="catalog/view/theme/default/stylesheet/404.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*   <script src="catalog/view/theme/default/js/404.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* <main class="err_404">*/
/*   <h1 class="err_404__title">{{ heading_title }}</h1>*/
/*   <h2 class="err_404__subtitle">Вы можете найти еще много интересного у нас на сайте</h2><img class="err_404__bg" src="catalog/view/theme/default/images/bg.svg">*/
/*   <a class="btn__default" href="{{ continue }}">{{ button_continue }}</a>*/
/* </main>*/
/* */
/* {{ footer }}*/
/* */
