<?php

/* extension/module/brainyfilter.twig */
class __TwigTemplate_94a8aa32323ed8670a7d871784d1ab908bd6c6b5b9156ffafe6ca5a5e6d641f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
    ";
        // line 9
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 10
            echo "        <div class=\"alert alert-success\">";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
    ";
        }
        // line 12
        echo "    ";
        if (twig_length_filter($this->env, (isset($context["error_warning"]) ? $context["error_warning"] : null))) {
            // line 13
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error_warning"]) ? $context["error_warning"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["err"]) {
                // line 14
                echo "            <div class=\"warning\">";
                echo $context["err"];
                echo "</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['err'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "    ";
        }
        // line 17
        echo "    <div class=\"box\">
        <div class=\"heading page-header\">
            <div class=\"container-fluid\">
                <h1>";
        // line 20
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
                <ul class=\"breadcrumb\">
                    ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 23
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array(), "array");
            echo "</a></li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "                </ul>
                <div class=\"pull-right\">
                    <a onclick=\"jQuery('[name=action]').val('apply');BF.submitForm();\" class=\"btn btn-success\" data-toggle=\"tooltip\" title=\"";
        // line 27
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "button_save", array());
        echo "\"><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"BF.submitForm();\" class=\"btn btn-primary\" data-toggle=\"tooltip\" title=\"";
        // line 28
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "button_save_n_close", array());
        echo "\"><span class=\"icon\"></span><i class=\"fa fa-save\"></i></a>
                    <a onclick=\"location = '";
        // line 29
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "';\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "button_cancel", array());
        echo "\"><i class=\"fa fa-reply\"></i></a>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 35
        echo "    <form action=\"";
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"application/x-www-form-urlencoded\" id=\"form\">
        <input type=\"hidden\" name=\"action\" value=\"save\" />
        <input type=\"hidden\" name=\"bf\" value=\"\" />
    </form>
    <form action=\"\" id=\"bf-form\" class=\"container-fluid\">
        <input type=\"hidden\" name=\"bf[module_id]\" value=\"";
        // line 40
        echo (((isset($context["isNewInstance"]) ? $context["isNewInstance"] : null)) ? ("new") : ((isset($context["moduleId"]) ? $context["moduleId"] : null)));
        echo "\" />
        <input type=\"hidden\" name=\"bf[current_adm_tab]\" value=\"";
        // line 41
        echo $this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array");
        echo "\" />
        ";
        // line 43
        echo "        <div id=\"bf-adm-main-menu\">
            <ul class=\"clearfix\">
                <li class=\"";
        // line 45
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) === "basic")) {
            echo " selected ";
        }
        echo "\">
                    <div>
                        <a href=\"";
        // line 47
        echo ((isset($context["instanceUrl"]) ? $context["instanceUrl"] : null) . "basic");
        echo "\">
                            <span class=\"icon basic\"></span>
                            ";
        // line 49
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "top_menu_basic_settings", array());
        echo "
                        </a>
                    </div>
                </li>
                <li class=\"";
        // line 53
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) && ((isset($context["moduleId"]) ? $context["moduleId"] : null) != "basic"))) {
            echo " selected ";
        }
        echo "\">
                    <div class=\"dropdown\">
                        <a id=\"dLabel\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            <span class=\"icon layouts\"></span>
                            ";
        // line 57
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "top_menu_module_instances", array());
        echo "
                        </a>
                        <ul class=\"dropdown-menu\" aria-labelledby=\"dLabel\">
                            ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
            // line 61
            echo "                            <li>
                                <a href=\"";
            // line 62
            echo ((isset($context["instanceUrl"]) ? $context["instanceUrl"] : null) . $this->getAttribute($context["module"], "module_id", array(), "array"));
            echo "\" ";
            if (((isset($context["moduleId"]) ? $context["moduleId"] : null) == $this->getAttribute($context["module"], "module_id", array(), "array"))) {
                echo " class=\"bf-selected\" ";
            }
            echo ">
                                    ";
            // line 63
            echo $this->getAttribute($context["module"], "name", array(), "array");
            echo "
                                </a>
                            </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                            <li role=\"separator\" class=\"divider\"></li>
                            <li><a href=\"";
        // line 68
        echo ((isset($context["instanceUrl"]) ? $context["instanceUrl"] : null) . "new");
        echo "\" ";
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) == "new")) {
            echo " class=\"bf-selected\" ";
        }
        echo ">
                                    <i class=\"fa fa-plus\"></i> ";
        // line 69
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "top_menu_add_new_instance", array());
        echo "</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <div class=\"clearfix\"></div>
        </div>
        ";
        // line 78
        echo "        
        <div id=\"bf-adm-main-container\">
            
            ";
        // line 82
        echo "            <div id=\"bf-adm-basic-settings\" class=\"tab-content\" data-group=\"main\" style=\"display:block\">
                ";
        // line 83
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) === "basic")) {
            // line 84
            echo "                <p class=\"bf-info\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_basic_settings_info", array());
            echo "</p>
                ";
        } elseif ((        // line 85
(isset($context["moduleId"]) ? $context["moduleId"] : null) === "new")) {
            // line 86
            echo "                <p class=\"bf-info\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_new_layout_notice", array());
            echo "</p>
                ";
        }
        // line 88
        echo "                <div class=\"bf-panel\">
                    <div id=\"bf-create-instance-alert\" class=\"bf-alert\">
                        ";
        // line 90
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "message_new_instance", array());
        echo "
                    </div>
                    ";
        // line 92
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) != "basic")) {
            // line 93
            echo "                    <div class=\"bf-panel-row clearfix\">
                        <div class=\"bf-notice\"></div>
                    </div>
                    ";
        }
        // line 97
        echo "                    <div class=\"tab-content-inner\">
                        ";
        // line 98
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) != "basic")) {
            // line 99
            echo "                        <div class=\"bf-panel-row bf-local-settings clearfix\">
                            <div class=\"left\">
                                <label for=\"bf-layout-id\">";
            // line 101
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_layout", array());
            echo "</label>
                                <select name=\"bf[layout_id]\" id=\"bf-layout-id\" class=\"bf-layout-select bf-w195\">
                                    <option value=\"0\" selected=\"selected\">";
            // line 103
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "select", array());
            echo "</option>
                                    ";
            // line 104
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["layouts"]) ? $context["layouts"] : null));
            foreach ($context['_seq'] as $context["id"] => $context["layout"]) {
                // line 105
                echo "                                        <option value=\"";
                echo $context["id"];
                echo "\">";
                echo $context["layout"];
                echo "</option>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['id'], $context['layout'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-position\">";
            // line 110
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_position", array());
            echo "</label>
                                <select name=\"bf[layout_position]\" id=\"bf-layout-position\" class=\"bf-layout-position bf-w195\">
                                    <option value=\"content_top\">";
            // line 112
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_content_top", array());
            echo "</option>
                                    <option value=\"content_bottom\">";
            // line 113
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_content_bottom", array());
            echo "</option>
                                    <option value=\"column_left\">";
            // line 114
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_column_left", array());
            echo "</option>
                                    <option value=\"column_right\">";
            // line 115
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_column_right", array());
            echo "</option>
                                </select>
                            </div>
                            <div class=\"left\">
                                <label for=\"bf-layout-sort-order\">";
            // line 119
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "sort_order", array());
            echo "</label>
                                <input type=\"text\" name=\"bf[layout_sort_order]\" id=\"bf-layout-sort-order\" class=\"bf-layout-sort bf-w65\" />
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label center\">";
            // line 123
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</span>
                                <div class=\"bf-layout-enable yesno\">
                                    <span class=\"bf-switcher\">
                                        <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[layout_enabled]\" value=\"0\" />
                                        <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                    </span>
                                </div>
                            </div>
                            <div class=\"left\">
                                <span class=\"bf-label\">&nbsp;</span>
                                <a href=\"";
            // line 133
            echo (isset($context["removeInstanceAction"]) ? $context["removeInstanceAction"] : null);
            echo "\" class=\"bf-remove-layout\" onclick=\"if (!window.confirm(BF.lang.confirm_remove_layout)) return false;\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_remove", array());
            echo "</a>
                            </div>

                        </div>
                        ";
        }
        // line 138
        echo "                        ";
        // line 139
        echo "                        <ul class=\"tabs vertical clearfix\">
                            ";
        // line 140
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) != "basic")) {
            // line 141
            echo "                            <li class=\"tab cat-tab
                                ";
            // line 142
            if (( !$this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "layout_id", array(), "array", true, true) || !twig_in_filter($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "layout_id", array(), "array"), array(0 => $this->getAttribute((isset($context["category_layouts"]) ? $context["category_layouts"] : null), 0, array(), "array"))))) {
                echo " hidden ";
            }
            // line 143
            echo "                                ";
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "categories")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"categories\" data-target=\"#bf-categories\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_categories", array());
            echo "</li>
                            ";
        }
        // line 145
        echo "                            <li class=\"tab ";
        if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "embedding")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"embedding\" data-target=\"#bf-filter-embedding\">";
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_embedding", array());
        echo "</li>
                            ";
        // line 146
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) === "basic")) {
            // line 147
            echo "                            <li class=\"tab ";
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "blocks")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"blocks\" data-target=\"#bf-filter-blocks-display\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_attributes_display", array());
            echo "</li>
                            <li class=\"tab ";
            // line 148
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "layout")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"layout\" data-target=\"#bf-filter-layout-view\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_filter_layout_view", array());
            echo "</li>
                            <li class=\"tab ";
            // line 149
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "submission")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"submission\" data-target=\"#bf-data-submission\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_data_submission", array());
            echo "</li>
                            <li class=\"tab ";
            // line 150
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "attributes")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"attributes\" data-target=\"#bf-attributes\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_attributes", array());
            echo "</li>
                            <li class=\"tab ";
            // line 151
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "options")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"options\" data-target=\"#bf-options\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_options", array());
            echo "</li>
                            <li class=\"tab ";
            // line 152
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "filters")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"filters\" data-target=\"#bf-filters\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_filters", array());
            echo "</li>
                            <li class=\"tab ";
            // line 153
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "responsive")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"responsive\" data-target=\"#bf-responsive\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_responsive_view", array());
            echo "</li>
                            <li class=\"tab ";
            // line 154
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "style")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"style\" data-target=\"#bf-style\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_style", array());
            echo "</li>
                            <li class=\"tab ";
            // line 155
            if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "global")) {
                echo " selected ";
            }
            echo "\" data-tab-name=\"global\" data-target=\"#bf-global-settings\">";
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_global_settings", array());
            echo "</li>
                            ";
        }
        // line 157
        echo "                            <li class=\"tab ";
        if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "attr_values")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"attr_values\" data-target=\"#bf-attr-values\">";
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_attr_values", array());
        echo "</li>
                            <li class=\"tab ";
        // line 158
        if (($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "current_adm_tab", array(), "array") === "help")) {
            echo " selected ";
        }
        echo "\" data-tab-name=\"help\" data-target=\"#bf-help\">";
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tab_help", array());
        echo "</li>
                            <li id=\"refresh-btn-wrapper\">
                                <button onclick=\"BF.refreshDB();return false;\" class=\"bf-button\" id=\"bf-refresh-db\">
                                    <span class=\"icon bf-update\"></span><span class=\"lbl\">";
        // line 161
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "update_cache", array());
        echo "</span>
                                </button>
                            </li>
                        </ul>
                        ";
        // line 166
        echo "                        <div id=\"bf-filter-embedding\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 167
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "embedding_header", array());
        echo "</div>
                            <div class=\"bf-alert\" style=\"display: block;\">";
        // line 168
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "embedding_warning", array());
        echo "</div>
                            <p class=\"bf-info\">";
        // line 169
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "embedding_description", array());
        echo "</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-container-selector\">";
        // line 173
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "embedding_container_selector", array());
        echo "</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][containerSelector]\" value=\"\" id=\"bf-container-selector\" placeholder=\"";
        // line 176
        echo $this->getAttribute($this->getAttribute((isset($context["basicSettings"]) ? $context["basicSettings"] : null), "behaviour", array(), "array"), "containerSelector", array(), "array");
        echo "\" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\"><label for=\"bf-paginator-selector\">";
        // line 181
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "embedding_paginator_selector", array());
        echo "</label></span>
                                    </td>
                                    <td>
                                        <input style=\"width: 290px;\" type=\"text\" name=\"bf[behaviour][paginatorSelector]\" value=\"\" id=\"bf-paginator-selector\" placeholder=\"";
        // line 184
        echo $this->getAttribute($this->getAttribute((isset($context["basicSettings"]) ? $context["basicSettings"] : null), "behaviour", array(), "array"), "paginatorSelector", array(), "array");
        echo "\" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        ";
        // line 189
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) === "basic")) {
            // line 190
            echo "                        ";
            // line 191
            echo "                        <div id=\"bf-filter-blocks-display\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 192
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filter_blocks_header", array());
            echo "</div>
                            <p class=\"bf-info\">";
            // line 193
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filter_blocks_descr", array());
            echo "</p>
                            <table class=\"bf-adm-table\" id=\"bf-filter-sections\">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class=\"center\">";
            // line 198
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                    <th class=\"bf-collapse-td\">";
            // line 199
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "collapse", array());
            echo "</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                ";
            // line 204
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["filterBlocks"]) ? $context["filterBlocks"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                // line 205
                echo "                                <tr class=\"bf-sort\" data-section=\"";
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">";
                // line 207
                echo $this->getAttribute($context["filter"], "label", array(), "array");
                echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-";
                // line 211
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "-filter\" type=\"hidden\" name=\"bf[behaviour][sections][";
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "][enabled]\" value=\"0\" data-disable-adv=\"section-";
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td class=\"center bf-collapse-td\">
                                        <input id=\"bf-";
                // line 216
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "-collapse\" type=\"checkbox\" name=\"bf[behaviour][sections][";
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "][collapsed]\" value=\"1\" data-adv-group=\"section-";
                echo $this->getAttribute($context["filter"], "name", array(), "array");
                echo "\" />
                                    </td>
                                    <td>
                                        ";
                // line 219
                if (($this->getAttribute($context["filter"], "control", array(), "array", true, true) && $this->getAttribute((isset($context["possible_controls"]) ? $context["possible_controls"] : null), $this->getAttribute($context["filter"], "name", array(), "array"), array(), "array", true, true))) {
                    // line 220
                    echo "                                        <select name=\"bf[behaviour][sections][";
                    echo $this->getAttribute($context["filter"], "name", array(), "array");
                    echo "][control]\" data-adv-group=\"section-";
                    echo $this->getAttribute($context["filter"], "name", array(), "array");
                    echo "\">
                                            ";
                    // line 221
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["possible_controls"]) ? $context["possible_controls"] : null), $this->getAttribute($context["filter"], "name", array(), "array"), array(), "array"));
                    foreach ($context['_seq'] as $context["val"] => $context["lbl"]) {
                        // line 222
                        echo "                                            <option value=\"";
                        echo $context["val"];
                        echo "\">";
                        echo $context["lbl"];
                        echo "</option>
                                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['val'], $context['lbl'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 224
                    echo "                                        </select>
                                        ";
                }
                // line 226
                echo "                                    </td>
                                </tr>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 229
            echo "                                </tbody>
                            </table>
                        </div>
                        ";
            // line 233
            echo "                        <div id=\"bf-filter-layout-view\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 234
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_header", array());
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center bf-w165\">";
            // line 239
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                            ";
            // line 247
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_show_attr_groups", array());
            echo "
                                            </span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-attr-group\" type=\"hidden\" name=\"bf[behaviour][attribute_groups]\" value=\"0\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-adv-group-cont\" style=\"margin-bottom:0;\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 262
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_product_count", array());
            echo "</span>
                                        </td>
                                        <td class=\"center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-product-count\" type=\"hidden\" name=\"bf[behaviour][product_count]\" value=\"0\" data-disable-adv=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 274
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_hide_empty_attr", array());
            echo "</span>
                                        </td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-hide-empty\" type=\"hidden\" name=\"bf[behaviour][hide_empty]\" value=\"0\" data-adv-group=\"hide-empty\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"></td>
                                    </tr>
                                </table>
                                <table class=\"bf-adm-table bf-intersect-cont\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 288
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_sliding", array());
            echo "</span>
                                        </td>
                                        <td class=\"bf-intersect center bf-w165\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-sliding\" type=\"hidden\" name=\"bf[behaviour][limit_items][enabled]\" value=\"0\" data-disable-adv=\"sliding\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\" style=\"padding-top: 5px;padding-bottom: 5px;\"> 
                                            <input id=\"bf-number-to-show\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_show]\" value=\"\" data-adv-group=\"sliding\" />
                                            <label for=\"bf-number-to-show\">";
            // line 298
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_sliding_num_to_show", array());
            echo "</label>
                                            <div class=\"bf-suboption\">
                                                <input id=\"bf-number-to-hide\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_items][number_to_hide]\" value=\"\" data-adv-group=\"sliding\" /> 
                                                <label for=\"bf-number-to-hide\">";
            // line 301
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_sliding_min", array());
            echo "</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                            ";
            // line 307
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_height_limit", array());
            echo "</span>
                                        </td>
                                        <td class=\"bf-intersect center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-limit-height\" type=\"hidden\" name=\"bf[behaviour][limit_height][enabled]\" value=\"0\" data-disable-adv=\"limit-height\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td colspan=\"3\"> 
                                            <input id=\"bf-limit-height\" type=\"text\" size=\"4\" name=\"bf[behaviour][limit_height][height]\" value=\"\" data-adv-group=\"limit-height\" /> 
                                            <label for=\"bf-limit-height\">";
            // line 317
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "layout_max_height_limit", array());
            echo "</label>
                                        </td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                        ";
            // line 325
            echo "                        <div id=\"bf-data-submission\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 326
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_header", array());
            echo "</div>
                            <p class=\"bf-info\">";
            // line 327
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_descr", array());
            echo "</p>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">Enabled</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-auto\">";
            // line 337
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_type_auto", array());
            echo "</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-auto\" type=\"radio\" value=\"auto\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        <label for=\"bf-submit-delay\">";
            // line 346
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_delay", array());
            echo "</label></span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-delay\" type=\"radio\" value=\"delay\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td>
                                        <input id=\"bf-submit-delay-time\" type=\"text\" name=\"bf[submission][submit_delay_time]\" value=\"\" size=\"4\" maxlength=\"4\" data-adv-group=\"submit-type\" data-for-val=\"delay\" />
                                        <label for=\"bf-submit-delay-time\">";
            // line 353
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_time_in_sec", array());
            echo "</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\"> 
                                            <label for=\"bf-submit-btn\">";
            // line 358
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_type_button", array());
            echo "</label></span>
                                    </td>
                                    <td class=\"center\"> 
                                        <input id=\"bf-submit-btn\" type=\"radio\" value=\"button\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" />
                                    </td>
                                    <td style=\"padding-top: 5px;padding-bottom: 5px;\">
                                        <input id=\"bf-submit-btn-fixed\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"fix\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                        <label for=\"bf-submit-btn-fixed\">";
            // line 365
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_button_fixed", array());
            echo "</label>
                                        <div class=\"bf-suboption\">
                                            <input id=\"bf-submit-btn-float\" type=\"radio\" name=\"bf[submission][submit_button_type]\" value=\"float\" data-adv-group=\"submit-type\" data-for-val=\"button\" />
                                            <label for=\"bf-submit-btn-float\">";
            // line 368
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_button_float", array());
            echo "</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr class=\"bf-local-settings\">
                                    <td class=\"bf-adm-label-td\">
                                        <span class=\"bf-wrapper\">
                                            <label for=\"bf-submit-default\">";
            // line 375
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_type_default", array());
            echo "</label>
                                        </span>
                                    </td>
                                    <td class=\"center\">
                                        <input id=\"bf-submit-default\" type=\"radio\" value=\"default\" name=\"bf[submission][submit_type]\" data-disable-adv=\"submit-type\" class=\"bf-default\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
            // line 385
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "submission_hide_panel", array());
            echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-layout\" type=\"hidden\" name=\"bf[submission][hide_panel]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        </div>
                        ";
        }
        // line 399
        echo "                        ";
        // line 400
        echo "                        ";
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) != "basic")) {
            // line 401
            echo "                        <div id=\"bf-categories\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 402
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_header", array());
            echo "</div>
                            <p class=\"bf-info\">";
            // line 403
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_info", array());
            echo "</p>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-green-info\">";
            // line 405
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_list_of_enabled", array());
            echo "</p>
                                <div class=\"bf-gray-panel\">
                                    ";
            // line 407
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_filter_hint", array());
            echo "
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-enabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-enabled-categories\">";
            // line 411
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_select_all", array());
            echo "</a> /
                                    <a data-unselect-all=\"#bf-enabled-categories\">";
            // line 412
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_unselect_all", array());
            echo "</a>
                                </div>
                                <div id=\"bf-enabled-categories\" class=\"bf-multi-select\">
                                    ";
            // line 415
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 416
                echo "                                        ";
                if (( !$this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "categories", array(), "array", true, true) ||  !$this->getAttribute($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "categories", array(), "array", false, true), $this->getAttribute($context["category"], "category_id", array(), "array"), array(), "array", true, true))) {
                    // line 417
                    echo "                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][";
                    // line 418
                    echo $this->getAttribute($context["category"], "category_id", array(), "array");
                    echo "]\" value=\"1\" />
                                            ";
                    // line 419
                    echo $this->getAttribute($context["category"], "name", array(), "array");
                    echo "
                                        </div>
                                        ";
                }
                // line 422
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 423
            echo "                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                                ";
            // line 426
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_move_selected", array());
            echo "
                                <br><button class=\"bf-move-right\"></button><br><button class=\"bf-move-left\"></button><br>
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <p class=\"bf-red-info\">";
            // line 430
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_list_of_disabled", array());
            echo "</p>
                                <div class=\"bf-gray-panel\">
                                    ";
            // line 432
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_filter_hint", array());
            echo "
                                    <input type=\"text\" class=\"bf-cat-filter bf-full-width\" data-target=\"#bf-disabled-categories\" />
                                </div>
                                <div style=\"padding-left:15px;\">
                                    <a data-select-all=\"#bf-disabled-categories\">";
            // line 436
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_select_all", array());
            echo "</a> /
                                    <a data-unselect-all=\"#bf-disabled-categories\">";
            // line 437
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories_unselect_all", array());
            echo "</a>
                                </div>
                                <div id=\"bf-disabled-categories\" class=\"bf-multi-select\">
                                    ";
            // line 440
            if ($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "categories", array(), "array", true, true)) {
                // line 441
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["settings"]) ? $context["settings"] : null), "categories", array(), "array"));
                foreach ($context['_seq'] as $context["catId"] => $context["b"]) {
                    // line 442
                    echo "                                        <div class=\"bf-row\">
                                            <input type=\"hidden\" name=\"bf[categories][";
                    // line 443
                    echo $this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), $context["catId"], array(), "array"), "category_id", array(), "array");
                    echo "]\" value=\"1\" />
                                            ";
                    // line 444
                    echo $this->getAttribute($this->getAttribute((isset($context["categories"]) ? $context["categories"] : null), $context["catId"], array(), "array"), "name", array(), "array");
                    echo "
                                        </div>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['catId'], $context['b'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 447
                echo "                                    ";
            }
            // line 448
            echo "                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        ";
        }
        // line 453
        echo "                        ";
        if (((isset($context["moduleId"]) ? $context["moduleId"] : null) === "basic")) {
            // line 454
            echo "                        ";
            // line 455
            echo "                        <div id=\"bf-attributes\" class=\"tab-content with-border\" data-group=\"settings\" data-select-all-group=\"attributes\">
                            <div class=\"bf-th-header-static\">";
            // line 456
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "attributes_header", array());
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                    <tr>
                                        <th></th>
                                        <th class=\"center\">";
            // line 461
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                        <th class=\"bf-w165\">";
            // line 462
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 466
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "attributes_group_setting", array());
            echo "</span></td>
                                        <td class=\"center\">
                                            <span class=\"bf-switcher\">
                                                <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[attributes_default][enable_all]\" value=\"0\" data-disable-adv=\"group-attr-control\" />
                                                <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                            </span>
                                        </td>
                                        <td>
                                            <select name=\"bf[attributes_default][control]\" data-adv-group=\"group-attr-control\">
                                                <option value=\"checkbox\">";
            // line 475
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
            echo "</option>
                                                <option value=\"radio\">";
            // line 476
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
            echo "</option>
                                                <option value=\"select\">";
            // line 477
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
            echo "</option>
                                                <option value=\"slider\">";
            // line 478
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
            echo "</option>
                                                <option value=\"slider_lbl\">";
            // line 479
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
            echo "</option>
                                                <option value=\"slider_lbl_inp\">";
            // line 480
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
            echo "</option>
                                            </select>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 487
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "attributes_individual_set", array());
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 491
            echo sprintf($this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "autocomplete_hint", array()), $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_attribute", array()));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-autocomplete\" data-lookup=\"attributes\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-attr-setting-template\" data-target-tbl=\"#custom-attr-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 494
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_attribute", array());
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 498
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "attributes_custom_set_descr", array());
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"attributes\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                ";
            // line 507
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 509
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "set_all_default", array());
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"0\">";
            // line 511
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "disable_all", array());
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"attributes\" data-select-all-val=\"1\">";
            // line 513
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enable_all", array());
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 516
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-attr-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 527
            echo "                        <div id=\"bf-options\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 528
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_header", array());
            echo "</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"attributes\">
                                <tr>
                                    <th></th>
                                    <th class=\"center bf-w165\">";
            // line 532
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 533
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 534
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode", array());
            echo "</th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 537
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_group_setting", array());
            echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[options_default][enable_all]\" value=\"0\" data-disable-adv=\"group-opt-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[options_default][control]\" data-adv-group=\"group-opt-control\" class=\"bf-w165\">
                                            <option value=\"checkbox\">";
            // line 546
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
            echo "</option>
                                            <option value=\"radio\">";
            // line 547
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
            echo "</option>
                                            <option value=\"select\">";
            // line 548
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
            echo "</option>
                                            <option value=\"slider\">";
            // line 549
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
            echo "</option>
                                            <option value=\"slider_lbl\">";
            // line 550
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 551
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 552
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "grid_of_images", array());
            echo "</option>
                                        </select>
                                    </td>
                                    <td class=\"center\">
                                        <select name=\"bf[options_default][mode]\" class=\"bf-opt-mode bf-w135 d-opt-control\" data-adv-group=\"group-opt-control\" data-bf-role=\"mode\">
                                            <option value=\"label\">";
            // line 557
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_label", array());
            echo "</option>
                                            <option value=\"img_label\">";
            // line 558
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_image_and_label", array());
            echo "</option>
                                            <option value=\"img\">";
            // line 559
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_image", array());
            echo "</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 564
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_individual_set", array());
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 568
            echo sprintf($this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "autocomplete_hint", array()), $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_option", array()));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"options\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-opt-setting-template\" data-target-tbl=\"#custom-opt-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 571
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_option", array());
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 575
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_custom_set_descr", array());
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"options\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">";
            // line 583
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                                ";
            // line 584
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"options\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 586
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "set_all_default", array());
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"0\">";
            // line 588
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "disable_all", array());
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"options\" data-select-all-val=\"1\">";
            // line 590
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enable_all", array());
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 593
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                            <th class=\"bf-w165\">";
            // line 594
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode", array());
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-opt-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 605
            echo "                        <div id=\"bf-filters\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 606
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filters_header", array());
            echo "</div>
                            <table class=\"bf-adm-table\" data-select-all-group=\"filters\">
                                <tr>
                                    <th></th>
                                    <th class=\"center\">";
            // line 610
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "</th>
                                    <th class=\"bf-w165\">";
            // line 611
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 615
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filters_group_setting", array());
            echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-layout-off\" type=\"hidden\" name=\"bf[filters_default][enable_all]\" value=\"0\" data-disable-adv=\"group-filter-control\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <select name=\"bf[filters_default][control]\" data-adv-group=\"group-filter-control\">
                                            <option value=\"checkbox\">";
            // line 624
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
            echo "</option>
                                            <option value=\"radio\">";
            // line 625
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
            echo "</option>
                                            <option value=\"select\">";
            // line 626
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
            echo "</option>
                                            <option value=\"slider\">";
            // line 627
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
            echo "</option>
                                            <option value=\"slider_lbl\">";
            // line 628
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
            echo "</option>
                                            <option value=\"slider_lbl_inp\">";
            // line 629
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
            echo "</option>
                                        </select>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 635
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filters_individual_set", array());
            echo "</div>
                            <div>
                                <div class=\"bf-gray-panel\">
                                    <div class=\"bf-half\">
                                        ";
            // line 639
            echo sprintf($this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "autocomplete_hint", array()), $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_filter", array()));
            echo "
                                        <input type=\"text\" id=\"bf-attr-search\" class=\"bf-w190 bf-autocomplete\" data-lookup=\"filters\" />
                                        <button id=\"bf-attr-add\" class=\"btn btn-success bf-add-row\" data-filter-data=\"\" data-row-tpl=\"#custom-filter-setting-template\" data-target-tbl=\"#custom-filter-settings\">
                                            <i class=\"fa fa-plus\"></i> ";
            // line 642
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "btn_select_filter", array());
            echo "
                                        </button>
                                    </div>
                                    <div class=\"bf-half\">
                                        <p class=\"bf-info\">";
            // line 646
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "filters_custom_set_descr", array());
            echo "</p>
                                    </div>
                                </div>
                                <table class=\"bf-adm-table bf-hide-if-empty\" data-select-all-group=\"filters\">
                                    <thead>
                                        <tr>
                                            <th style=\"width:625px\"></th>
                                            <th class=\"center\" style=\"width:50px\">Collapse</th>
                                            <th class=\"center\" style=\"width:330px\">
                                                ";
            // line 655
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enabled", array());
            echo "
                                                <div class=\"bf-group-actions\">
                                                    <a data-select-all=\"filters\" data-select-all-val=\"2\" class=\"bf-local-settings\">";
            // line 657
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "set_all_default", array());
            echo "</a>
                                                    <span class=\"bf-local-settings\">/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"0\">";
            // line 659
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "disable_all", array());
            echo "</a>
                                                    <span>/</span>
                                                    <a data-select-all=\"filters\" data-select-all-val=\"1\">";
            // line 661
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enable_all", array());
            echo "</a>
                                                </div> 
                                            </th>
                                            <th class=\"bf-w165\">";
            // line 664
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "control", array());
            echo "</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"custom-filter-settings\">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        ";
            // line 675
            echo "                        <div id=\"bf-responsive\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
            // line 676
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_header", array());
            echo "</div>
                            <table class=\"bf-adm-table bf-adv-group-cont\">
                                <tr>
                                    <th></th>
                                    <th class=\"bf-w170\"></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 682
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
            echo "</span></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 686
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_mode_enable", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive\" type=\"hidden\" name=\"bf[style][responsive][enabled]\" value=\"0\" data-disable-adv=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 698
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_collapse_sections", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-responsive-collapse\" type=\"hidden\" name=\"bf[style][responsive][collapsed]\" value=\"0\" data-adv-group=\"responsive\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 710
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_max_width", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 719
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_max_screen_width", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input type=\"text\" name=\"bf[style][responsive][max_screen_width]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 728
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_position", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-position-left\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"left\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-left\">";
            // line 731
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "left", array());
            echo "</label>
                                        <input id=\"bf-responsive-position-right\" type=\"radio\" name=\"bf[style][responsive][position]\" value=\"right\" data-adv-group=\"responsive\" />
                                        <label for=\"bf-responsive-position-right\">";
            // line 733
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "right", array());
            echo "</label>
                                    </td>
                                    <td>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 741
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "responsive_offset", array());
            echo "</span></td> 
                                    <td class=\"center\">
                                        <input id=\"bf-responsive-offset\" type=\"text\" name=\"bf[style][responsive][offset]\" value=\"\" data-adv-group=\"responsive\" class=\"bf-w65\" />
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        ";
            // line 752
            echo "                         <div id=\"bf-style\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 753
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_block_header", array());
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th><span class=\"bf-local-settings\">";
            // line 759
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
            echo "</span></th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 763
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_background", array());
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_background][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_background\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 771
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_text", array());
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input id=\"bf-style-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][block_header_text][val]\" /> 
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"block_header_text\", this)}'/></td>
                                        <td></td>
                                    </tr>
                                    ";
            // line 778
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 779
                echo "                                    <tr>
                                        <td class=\"bf-adm-label-td\">
                                            <span class=\"bf-wrapper\">
                                                ";
                // line 782
                echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_title", array());
                echo " (
                                                <img src=\"";
                // line 783
                echo $this->getAttribute($context["language"], "image_path", array(), "array");
                echo "\" /> 
                                                ";
                // line 784
                echo $this->getAttribute($context["language"], "name", array(), "array");
                echo ")
                                            </span>
                                        </td>
                                        <td colspan=\"2\">
                                            <input type=\"text\" name=\"bf[behaviour][filter_name][";
                // line 788
                echo $this->getAttribute($context["language"], "language_id", array(), "array");
                echo "]\" value=\"\" class=\"bf-w195\" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 793
            echo "                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 795
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_product_quantity", array());
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 801
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 805
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_background", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_background][val]\"/> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 813
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_text", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-product-quantity-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][product_quantity_text][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][product_quantity_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"product_quantity_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                                </div>
                                <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 822
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_price_slider", array());
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 828
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 832
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_background", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 840
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_active_area_background", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-area-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_area_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_area_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_area_background\", this)}' /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 848
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_border", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_border\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 856
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_slider_handle_background", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 864
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_slider_handle_border", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-price-slider-handle-border\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][price_slider_handle_border][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][price_slider_handle_border][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"price_slider_handle_border\", this)}'/></td>
                                    <td></td>
                                </tr>

                            </table>
                            </div>
                             <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 874
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_group_block_header", array());
            echo "</div>
                            <div>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th><span class=\"bf-local-settings\">";
            // line 880
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
            echo "</span></th>
                                    <th></th>
                                </tr>
                                 <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 884
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_background", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-background\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_background][val]\" /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_background][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\"  onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_background\", this)}'/></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 892
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_text", array());
            echo "</span></td>
                                    <td class=\"center bf-w170\">
                                        <input id=\"bf-style-group-block-header-text\" class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][group_block_header_text][val]\"  /> 
                                    </td>
                                    <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][group_block_header_text][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange = 'if (jQuery(this).is(\":checked\")) {BF.changeDefault(\"group_block_header_text\", this)}' /></td>
                                    <td></td>
                                </tr>
                                </table>
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
            // line 901
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_responsive_popup_view", array());
            echo "</div>
                            <div>
                                <table class=\"bf-adm-table\">
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 905
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_show_btn_color", array());
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_show_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_show_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_show_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
            // line 913
            echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "theme_reset_btn_color", array());
            echo "</span></td>
                                        <td class=\"center bf-w170\">
                                            <input class=\"bf-w165 entry color-pick\" type=\"text\" name=\"bf[style][resp_reset_btn_color][val]\" />
                                        </td>
                                        <td class=\"bf-w65 center\"><input type=\"checkbox\" name=\"bf[style][resp_reset_btn_color][default]\" value=\"1\" class=\"bf-chkbox-def bf-local-settings\" onchange=\"if (jQuery(this).is(':checked')) {BF.changeDefault('resp_reset_btn_color', this);}\" /></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        ";
        }
        // line 924
        echo "                        ";
        // line 925
        echo "                        <div id=\"bf-attr-values\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 926
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "ordering_header", array());
        echo "</div>
                            <div class=\"bf-alert\" style=\"display: block;\">";
        // line 927
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "ordering_note", array());
        echo "</div>
                            <p class=\"bf-info\">";
        // line 928
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "ordering_descr", array());
        echo "</p>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    ";
        // line 931
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "ordering_filter_hint", array());
        echo "
                                    <input type=\"text\" class=\"bf-attr-filter bf-full-width\" data-target=\"#bf-attr-list\" />
                                </div>
                                <div id=\"bf-attr-list\" class=\"bf-multi-select\">
                                    ";
        // line 935
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attributes"]) ? $context["attributes"] : null));
        foreach ($context['_seq'] as $context["attrId"] => $context["attribute"]) {
            // line 936
            echo "                                    <div class=\"bf-row\" data-attr-id=\"";
            echo $context["attrId"];
            echo "\">
                                        <b>";
            // line 937
            echo $this->getAttribute($context["attribute"], "group", array(), "array");
            echo "</b> / ";
            echo $this->getAttribute($context["attribute"], "name", array(), "array");
            echo "
                                    </div>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrId'], $context['attribute'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 940
        echo "                                </div>
                            </div>
                            <div class=\"bf-middle-buttons-col\">
                            </div>
                            <div class=\"bf-multi-select-group\">
                                <div class=\"bf-gray-panel\">
                                    <div class=\"buttons\">
                                        <div>";
        // line 947
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "ordering_language", array());
        echo ":</div>
                                        <select id=\"bf-attr-val-language\" class=\"bf-w165\">
                                            ";
        // line 949
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["i"] => $context["language"]) {
            // line 950
            echo "                                            <option value=\"";
            echo $this->getAttribute($context["language"], "language_id", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["language"], "name", array(), "array");
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 952
        echo "                                        </select>
                                        <div class=\"bf-pull-right\">
                                            <button class=\"bf-auto-sort\" data-type=\"number\">0..9</button>
                                            <button class=\"bf-auto-sort\" data-type=\"string\">A..Z</button>
                                            <a class=\"bf-button bf-save-btn\" title=\"";
        // line 956
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "button_save_n_close", array());
        echo "\"><span class=\"icon\"></span></a>
                                        </div>
                                    </div>
                                </div>
                                <div id=\"bf-attr-val-list\" class=\"bf-multi-select\">
                                    
                                </div>
                            </div>
                            <div class=\"clearfix\"></div>
                        </div>
                        ";
        // line 967
        echo "                        <div id=\"bf-global-settings\" class=\"tab-content with-border bf-global-settings\" data-group=\"settings\">
                            <div class=\"bf-th-header-static\">";
        // line 968
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_header", array());
        echo "</div>
                            <p class=\"bf-info\">";
        // line 969
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_settings_descr", array());
        echo "</p>
                            <table class=\"bf-adm-table\">
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
        // line 972
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_hide_empty_stock", array());
        echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-hide-out-of-stock\" type=\"hidden\" name=\"bf[global][hide_out_of_stock]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr class=\"bf-global-settings\">
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 983
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_postponed_count", array());
        echo "</span>
                                    </td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-postponed-count\" type=\"hidden\" name=\"bf[global][postponed_count]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">";
        // line 994
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_enable_multiple_attributes", array());
        echo "</span></td>
                                    <td class=\"center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-multiple-attributes\" type=\"hidden\" name=\"bf[global][multiple_attributes]\" value=\"0\" data-disable-adv=\"attr-separator\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td>
                                        <label for=\"bf-attr-separator\">";
        // line 1002
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_separator", array());
        echo "</label>
                                        <input id=\"bf-attr-separator\" type=\"text\" name=\"bf[global][attribute_separator]\" value=\"\" size=\"4\" data-adv-group=\"attr-separator\" />
                                        ";
        // line 1004
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_multiple_attr_separator", array());
        echo "
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1009
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_in_stock_status_id", array());
        echo "</span>
                                    </td>
                                    <td class=\"\" colspan=\"2\">
                                        <select name=\"bf[global][instock_status_id]\" class=\"bf-w165\">
                                            ";
        // line 1013
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["stockStatuses"]) ? $context["stockStatuses"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 1014
            echo "                                                    <option value=\"";
            echo $this->getAttribute($context["status"], "stock_status_id", array(), "array");
            echo "\">";
            echo $this->getAttribute($context["status"], "name", array(), "array");
            echo "</option>
                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1016
        echo "                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1021
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "global_subcats_fix", array());
        echo "</span>
                                    </td>
                                    <td class=\" center\">
                                        <span class=\"bf-switcher\">
                                            <input id=\"bf-subcategories\" type=\"hidden\" name=\"bf[global][subcategories_fix]\" value=\"0\" />
                                            <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                                        </span>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper\">
                                        ";
        // line 1033
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "entry_cron_secret_key", array());
        echo "
                                            <span class=\"bf-link-highlight\" style=\"padding-top:5px;\">";
        // line 1034
        echo (isset($context["catalogUrl"]) ? $context["catalogUrl"] : null);
        echo "index.php?route=extension/module/brainyfilter/cron&key=<b>cron secret key</b></span>
                                        </span>
                                    </td>
                                    <td>
                                        <input id=\"bf-cron-key\" class=\"bf-w165\" type=\"text\" name=\"bf[global][cron_secret_key]\" value=\"\" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan=\"3\">
                                    </td>
                                </tr>
                            </table>                    
                        </div>
                        ";
        // line 1049
        echo "                        <div id=\"bf-help\" class=\"tab-content with-border\" data-group=\"settings\">
                            <div class=\"bf-th-header\" id=\"bf-faq-n-troubleshooting\"><span class=\"icon bf-arrow\"></span>";
        // line 1050
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "help_faq_n_trouleshooting", array());
        echo "</div>
                            <div style=\"display:none;\">
                                
                            </div>
                            <div class=\"bf-th-header expanded\"><span class=\"icon bf-arrow\"></span>";
        // line 1054
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "help_about", array());
        echo "</div>
                            <div id=\"bf-about\">
                                <div class=\"bf-about-text\">
                                    ";
        // line 1057
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "help_about_content", array());
        echo "
                                    <hr />
                                    <p>";
        // line 1059
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "bf_signature", array());
        echo "</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class=\"bf-signature\">";
        // line 1069
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "bf_signature", array());
        echo "</div>
</div>

<table style=\"display:none;\">
    <tbody id=\"custom-attr-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][attribute][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[attributes][{i}][enabled]\" value=\"0\" data-disable-adv=\"attr-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[attributes][{i}][control]\" data-adv-group=\"attr-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">";
        // line 1084
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
        echo "</option>
                    <option value=\"radio\">";
        // line 1085
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
        echo "</option>
                    <option value=\"select\">";
        // line 1086
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
        echo "</option>
                    <option value=\"slider\">";
        // line 1087
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1088
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1089
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
        echo "</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-opt-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][options][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[options][{i}][enabled]\" value=\"0\" data-disable-adv=\"opt-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][control]\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"control\" class=\"bf-w135\">
                    <option value=\"checkbox\">";
        // line 1108
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
        echo "</option>
                    <option value=\"radio\">";
        // line 1109
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
        echo "</option>
                    <option value=\"select\">";
        // line 1110
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
        echo "</option>
                    <option value=\"slider\">";
        // line 1111
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1112
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1113
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
        echo "</option>
                    <option value=\"grid\">";
        // line 1114
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "grid_of_images", array());
        echo "</option>
                </select>
            </td>
            <td class=\"center\">
                <select name=\"bf[options][{i}][mode]\" class=\"bf-opt-mode bf-w135\" data-adv-group=\"opt-control-{i}\" data-bf-role=\"mode\">
                    <option value=\"label\">";
        // line 1119
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_label", array());
        echo "</option>
                    <option value=\"img_label\">";
        // line 1120
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_image_and_label", array());
        echo "</option>
                    <option value=\"img\">";
        // line 1121
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "options_view_mode_image", array());
        echo "</option>
                </select>
            </td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>
<table style=\"display:none;\">
    <tbody id=\"custom-filter-setting-template\">
        <tr>
            <td class=\"bf-adm-label-td\"><span class=\"bf-wrapper bf-attr-name\" data-bf-role=\"name\"></span></td>
            <td class=\"bf-adm-label-td center\"><input type=\"checkbox\" name=\"bf[behaviour][sections][filter][{i}][collapsed]\" data-bf-role=\"collapsed\"></td>
            <td class=\"center\">
                <span class=\"bf-switcher\">
                    <input class=\"bf-attr-enable\" type=\"hidden\" name=\"bf[filters][{i}][enabled]\" value=\"0\" data-disable-adv=\"filter-control-{i}\" data-bf-role=\"enabled\" />
                    <span class=\"bf-def\"></span><span class=\"bf-no\"></span><span class=\"bf-yes\"></span>
                </span>
            </td>
            <td><select name=\"bf[filters][{i}][control]\" data-adv-group=\"filter-control-{i}\" data-bf-role=\"control\">
                    <option value=\"checkbox\">";
        // line 1140
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "checkbox", array());
        echo "</option>
                    <option value=\"radio\">";
        // line 1141
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "radio", array());
        echo "</option>
                    <option value=\"select\">";
        // line 1142
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "selectbox", array());
        echo "</option>
                    <option value=\"slider\">";
        // line 1143
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider", array());
        echo "</option>
                    <option value=\"slider_lbl\">";
        // line 1144
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_only", array());
        echo "</option>
                    <option value=\"slider_lbl_inp\">";
        // line 1145
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "slider_labels_and_inputs", array());
        echo "</option>
                </select></td>
            <td><a class=\"bf-remove-row\"><i class=\"fa fa-times\"></i></a></td>
        </tr>
    </tbody>
</table>

";
        // line 1153
        echo "
<div id=\"bf-category-list-tpl\" class=\"bf-category-list\" style=\"display: none;\">
    <div class=\"bf-label\">
        ";
        // line 1156
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "categories", array());
        echo "
        (<a onclick=\"jQuery(this).closest('.bf-category-list').find('input').removeAttr('checked')\">";
        // line 1157
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "disable_all", array());
        echo "</a>
        <span>/</span>
        <a onclick=\"jQuery(this).closest('.bf-category-list').find('input').attr('checked', 'checked')\">";
        // line 1159
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enable_all", array());
        echo "</a>)
    </div>
    <div class=\"bf-cat-list-cont\">
        <ul data-select-all-group=\"categories\">
            ";
        // line 1163
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["cat"]) {
            // line 1164
            echo "            <li>
                <label>
                    <input type=\"checkbox\" name=\"bf[categories][";
            // line 1166
            echo $this->getAttribute($context["cat"], "category_id", array(), "array");
            echo "]\" value=\"1\" />
                    ";
            // line 1167
            echo $this->getAttribute($context["cat"], "name", array(), "array");
            echo "
                </label>
            </li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cat'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1171
        echo "        </ul>
    </div>
</div>

";
        // line 1176
        echo "<script>
BF.lang = {
        'default' : '";
        // line 1178
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
        echo "',
        'error_layout_not_set' : '";
        // line 1179
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "message_error_layout_not_set", array());
        echo "',
        'error_cant_remove_default' : '";
        // line 1180
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_remove_default_layout", array());
        echo "',
        'default_layout' : '";
        // line 1181
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_default_layout", array());
        echo "',
        'confirm_remove_layout' : '";
        // line 1182
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_remove_confirmation", array());
        echo "',
        'confirm_unsaved_changes' : '";
        // line 1183
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "message_unsaved_changes", array());
        echo "',
        'updating' : '";
        // line 1184
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "updating", array());
        echo "',
        'empty_table' : '";
        // line 1185
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "message_empty_table", array());
        echo "',
        'content_top' : '";
        // line 1186
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_content_top", array());
        echo "',
        'column_left' : '";
        // line 1187
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_column_left", array());
        echo "',
        'column_right' : '";
        // line 1188
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_column_right", array());
        echo "',
        'content_bottom' : '";
        // line 1189
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "instance_content_bottom", array());
        echo "'
    };
BF.moduleId = '";
        // line 1191
        echo (((isset($context["isNewInstance"]) ? $context["isNewInstance"] : null)) ? ("new") : ((isset($context["moduleId"]) ? $context["moduleId"] : null)));
        echo "';
BF.settings = ";
        // line 1192
        echo twig_jsonencode_filter((isset($context["settings"]) ? $context["settings"] : null));
        echo ";
BF.attributes = ";
        // line 1193
        echo twig_jsonencode_filter((isset($context["attributes"]) ? $context["attributes"] : null));
        echo ";
BF.options = ";
        // line 1194
        echo twig_jsonencode_filter((isset($context["options"]) ? $context["options"] : null));
        echo ";
BF.filters = ";
        // line 1195
        echo twig_jsonencode_filter((isset($context["filters"]) ? $context["filters"] : null));
        echo ";
BF.refreshActionUrl = '";
        // line 1196
        echo twig_replace_filter((isset($context["refreshAction"]) ? $context["refreshAction"] : null), array("&amp;" => "&"));
        echo "';
BF.modRefreshActionUrl = '";
        // line 1197
        echo twig_replace_filter((isset($context["modRefreshAction"]) ? $context["modRefreshAction"] : null), array("&amp;" => "&"));
        echo "';
BF.attrValActionUrl = '";
        // line 1198
        echo twig_replace_filter((isset($context["attributeValuesAction"]) ? $context["attributeValuesAction"] : null), array("&amp;" => "&"));
        echo "';
BF.isFirstLaunch = ";
        // line 1199
        echo (isset($context["isFirstLaunch"]) ? $context["isFirstLaunch"] : null);
        echo ";
BF.categoryLayouts = ";
        // line 1200
        echo twig_jsonencode_filter((isset($context["category_layouts"]) ? $context["category_layouts"] : null));
        echo ";
jQuery(document).ready(BF.init());
</script>

<style>
    .bf-def:before {
        content: '";
        // line 1206
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "default", array());
        echo "';
    }
    .bf-no:before {
        content: '";
        // line 1209
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "no", array());
        echo "';
    }
    .bf-yes:before {
        content: '";
        // line 1212
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "yes", array());
        echo "';
    }
    .bf-disable-enable .bf-no:before {
        content: '";
        // line 1215
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "disable_all", array());
        echo "';
    }
    .bf-disable-enable .bf-yes:before {
        content: '";
        // line 1218
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "enable_all", array());
        echo "';
    }
</style>
";
        // line 1221
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "extension/module/brainyfilter.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2364 => 1221,  2358 => 1218,  2352 => 1215,  2346 => 1212,  2340 => 1209,  2334 => 1206,  2325 => 1200,  2321 => 1199,  2317 => 1198,  2313 => 1197,  2309 => 1196,  2305 => 1195,  2301 => 1194,  2297 => 1193,  2293 => 1192,  2289 => 1191,  2284 => 1189,  2280 => 1188,  2276 => 1187,  2272 => 1186,  2268 => 1185,  2264 => 1184,  2260 => 1183,  2256 => 1182,  2252 => 1181,  2248 => 1180,  2244 => 1179,  2240 => 1178,  2236 => 1176,  2230 => 1171,  2220 => 1167,  2216 => 1166,  2212 => 1164,  2208 => 1163,  2201 => 1159,  2196 => 1157,  2192 => 1156,  2187 => 1153,  2177 => 1145,  2173 => 1144,  2169 => 1143,  2165 => 1142,  2161 => 1141,  2157 => 1140,  2135 => 1121,  2131 => 1120,  2127 => 1119,  2119 => 1114,  2115 => 1113,  2111 => 1112,  2107 => 1111,  2103 => 1110,  2099 => 1109,  2095 => 1108,  2073 => 1089,  2069 => 1088,  2065 => 1087,  2061 => 1086,  2057 => 1085,  2053 => 1084,  2035 => 1069,  2022 => 1059,  2017 => 1057,  2011 => 1054,  2004 => 1050,  2001 => 1049,  1984 => 1034,  1980 => 1033,  1965 => 1021,  1958 => 1016,  1947 => 1014,  1943 => 1013,  1936 => 1009,  1928 => 1004,  1923 => 1002,  1912 => 994,  1898 => 983,  1884 => 972,  1878 => 969,  1874 => 968,  1871 => 967,  1858 => 956,  1852 => 952,  1841 => 950,  1837 => 949,  1832 => 947,  1823 => 940,  1812 => 937,  1807 => 936,  1803 => 935,  1796 => 931,  1790 => 928,  1786 => 927,  1782 => 926,  1779 => 925,  1777 => 924,  1763 => 913,  1752 => 905,  1745 => 901,  1733 => 892,  1722 => 884,  1715 => 880,  1706 => 874,  1693 => 864,  1682 => 856,  1671 => 848,  1660 => 840,  1649 => 832,  1642 => 828,  1633 => 822,  1621 => 813,  1610 => 805,  1603 => 801,  1594 => 795,  1590 => 793,  1579 => 788,  1572 => 784,  1568 => 783,  1564 => 782,  1559 => 779,  1555 => 778,  1545 => 771,  1534 => 763,  1527 => 759,  1518 => 753,  1515 => 752,  1502 => 741,  1491 => 733,  1486 => 731,  1480 => 728,  1468 => 719,  1456 => 710,  1441 => 698,  1426 => 686,  1419 => 682,  1410 => 676,  1407 => 675,  1394 => 664,  1388 => 661,  1383 => 659,  1378 => 657,  1373 => 655,  1361 => 646,  1354 => 642,  1348 => 639,  1341 => 635,  1332 => 629,  1328 => 628,  1324 => 627,  1320 => 626,  1316 => 625,  1312 => 624,  1300 => 615,  1293 => 611,  1289 => 610,  1282 => 606,  1279 => 605,  1266 => 594,  1262 => 593,  1256 => 590,  1251 => 588,  1246 => 586,  1241 => 584,  1237 => 583,  1226 => 575,  1219 => 571,  1213 => 568,  1206 => 564,  1198 => 559,  1194 => 558,  1190 => 557,  1182 => 552,  1178 => 551,  1174 => 550,  1170 => 549,  1166 => 548,  1162 => 547,  1158 => 546,  1146 => 537,  1140 => 534,  1136 => 533,  1132 => 532,  1125 => 528,  1122 => 527,  1109 => 516,  1103 => 513,  1098 => 511,  1093 => 509,  1088 => 507,  1076 => 498,  1069 => 494,  1063 => 491,  1056 => 487,  1046 => 480,  1042 => 479,  1038 => 478,  1034 => 477,  1030 => 476,  1026 => 475,  1014 => 466,  1007 => 462,  1003 => 461,  995 => 456,  992 => 455,  990 => 454,  987 => 453,  980 => 448,  977 => 447,  968 => 444,  964 => 443,  961 => 442,  956 => 441,  954 => 440,  948 => 437,  944 => 436,  937 => 432,  932 => 430,  925 => 426,  920 => 423,  914 => 422,  908 => 419,  904 => 418,  901 => 417,  898 => 416,  894 => 415,  888 => 412,  884 => 411,  877 => 407,  872 => 405,  867 => 403,  863 => 402,  860 => 401,  857 => 400,  855 => 399,  838 => 385,  825 => 375,  815 => 368,  809 => 365,  799 => 358,  791 => 353,  781 => 346,  769 => 337,  756 => 327,  752 => 326,  749 => 325,  739 => 317,  726 => 307,  717 => 301,  711 => 298,  698 => 288,  681 => 274,  666 => 262,  648 => 247,  637 => 239,  629 => 234,  626 => 233,  621 => 229,  613 => 226,  609 => 224,  598 => 222,  594 => 221,  587 => 220,  585 => 219,  575 => 216,  563 => 211,  556 => 207,  550 => 205,  546 => 204,  538 => 199,  534 => 198,  526 => 193,  522 => 192,  519 => 191,  517 => 190,  515 => 189,  507 => 184,  501 => 181,  493 => 176,  487 => 173,  480 => 169,  476 => 168,  472 => 167,  469 => 166,  462 => 161,  452 => 158,  443 => 157,  434 => 155,  426 => 154,  418 => 153,  410 => 152,  402 => 151,  394 => 150,  386 => 149,  378 => 148,  369 => 147,  367 => 146,  358 => 145,  348 => 143,  344 => 142,  341 => 141,  339 => 140,  336 => 139,  334 => 138,  324 => 133,  311 => 123,  304 => 119,  297 => 115,  293 => 114,  289 => 113,  285 => 112,  280 => 110,  275 => 107,  264 => 105,  260 => 104,  256 => 103,  251 => 101,  247 => 99,  245 => 98,  242 => 97,  236 => 93,  234 => 92,  229 => 90,  225 => 88,  219 => 86,  217 => 85,  212 => 84,  210 => 83,  207 => 82,  202 => 78,  191 => 69,  183 => 68,  180 => 67,  170 => 63,  162 => 62,  159 => 61,  155 => 60,  149 => 57,  140 => 53,  133 => 49,  128 => 47,  121 => 45,  117 => 43,  113 => 41,  109 => 40,  100 => 35,  90 => 29,  86 => 28,  82 => 27,  78 => 25,  67 => 23,  63 => 22,  58 => 20,  53 => 17,  50 => 16,  41 => 14,  36 => 13,  33 => 12,  27 => 10,  25 => 9,  19 => 7,);
    }
}
/* {#*/
/*  * Brainy Filter Pro 5.1.3 OC3, September 18, 2017 / brainyfilter.com */
/*  * Copyright 2015-2017 Giant Leap Lab / www.giantleaplab.com */
/*  * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store. */
/*  * Support: http://support.giantleaplab.com */
/* #}*/
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*     {% if success %}*/
/*         <div class="alert alert-success">{{ success }}</div>*/
/*     {% endif %}*/
/*     {% if error_warning|length %}*/
/*         {% for err in error_warning %}*/
/*             <div class="warning">{{ err }}</div>*/
/*         {% endfor %}*/
/*     {% endif %}*/
/*     <div class="box">*/
/*         <div class="heading page-header">*/
/*             <div class="container-fluid">*/
/*                 <h1>{{ heading_title }}</h1>*/
/*                 <ul class="breadcrumb">*/
/*                     {% for breadcrumb in breadcrumbs %}*/
/*                     <li><a href="{{ breadcrumb['href'] }}">{{ breadcrumb['text'] }}</a></li>*/
/*                     {% endfor %}*/
/*                 </ul>*/
/*                 <div class="pull-right">*/
/*                     <a onclick="jQuery('[name=action]').val('apply');BF.submitForm();" class="btn btn-success" data-toggle="tooltip" title="{{ lang.button_save }}"><i class="fa fa-save"></i></a>*/
/*                     <a onclick="BF.submitForm();" class="btn btn-primary" data-toggle="tooltip" title="{{ lang.button_save_n_close }}"><span class="icon"></span><i class="fa fa-save"></i></a>*/
/*                     <a onclick="location = '{{ cancel }}';" class="btn btn-default" data-toggle="tooltip" title="{{ lang.button_cancel }}"><i class="fa fa-reply"></i></a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/*     {# settings block #}*/
/*     <form action="{{ action }}" method="post" enctype="application/x-www-form-urlencoded" id="form">*/
/*         <input type="hidden" name="action" value="save" />*/
/*         <input type="hidden" name="bf" value="" />*/
/*     </form>*/
/*     <form action="" id="bf-form" class="container-fluid">*/
/*         <input type="hidden" name="bf[module_id]" value="{{ isNewInstance ? 'new' : moduleId }}" />*/
/*         <input type="hidden" name="bf[current_adm_tab]" value="{{ settings['current_adm_tab'] }}" />*/
/*         {# main menu #}*/
/*         <div id="bf-adm-main-menu">*/
/*             <ul class="clearfix">*/
/*                 <li class="{% if moduleId is same as ('basic') %} selected {% endif %}">*/
/*                     <div>*/
/*                         <a href="{{ instanceUrl ~ 'basic' }}">*/
/*                             <span class="icon basic"></span>*/
/*                             {{ lang.top_menu_basic_settings }}*/
/*                         </a>*/
/*                     </div>*/
/*                 </li>*/
/*                 <li class="{% if moduleId and moduleId != 'basic' %} selected {% endif %}">*/
/*                     <div class="dropdown">*/
/*                         <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/
/*                             <span class="icon layouts"></span>*/
/*                             {{ lang.top_menu_module_instances }}*/
/*                         </a>*/
/*                         <ul class="dropdown-menu" aria-labelledby="dLabel">*/
/*                             {% for module in modules %}*/
/*                             <li>*/
/*                                 <a href="{{ instanceUrl ~ module['module_id'] }}" {% if moduleId == module['module_id'] %} class="bf-selected" {% endif %}>*/
/*                                     {{ module['name'] }}*/
/*                                 </a>*/
/*                             </li>*/
/*                             {% endfor %}*/
/*                             <li role="separator" class="divider"></li>*/
/*                             <li><a href="{{ instanceUrl ~ 'new' }}" {% if moduleId == 'new' %} class="bf-selected" {% endif %}>*/
/*                                     <i class="fa fa-plus"></i> {{ lang.top_menu_add_new_instance }}</a>*/
/*                             </li>*/
/*                         </ul>*/
/*                     </div>*/
/*                 </li>*/
/*             </ul>*/
/*             <div class="clearfix"></div>*/
/*         </div>*/
/*         {# main menu block end #}*/
/*         */
/*         <div id="bf-adm-main-container">*/
/*             */
/*             {# basic settings container #}*/
/*             <div id="bf-adm-basic-settings" class="tab-content" data-group="main" style="display:block">*/
/*                 {% if moduleId is same as ('basic') %}*/
/*                 <p class="bf-info">{{ lang.instance_basic_settings_info }}</p>*/
/*                 {% elseif moduleId is same as ('new') %}*/
/*                 <p class="bf-info">{{ lang.instance_new_layout_notice }}</p>*/
/*                 {% endif %}*/
/*                 <div class="bf-panel">*/
/*                     <div id="bf-create-instance-alert" class="bf-alert">*/
/*                         {{ lang.message_new_instance }}*/
/*                     </div>*/
/*                     {% if moduleId != 'basic' %}*/
/*                     <div class="bf-panel-row clearfix">*/
/*                         <div class="bf-notice"></div>*/
/*                     </div>*/
/*                     {% endif %}*/
/*                     <div class="tab-content-inner">*/
/*                         {% if moduleId != 'basic' %}*/
/*                         <div class="bf-panel-row bf-local-settings clearfix">*/
/*                             <div class="left">*/
/*                                 <label for="bf-layout-id">{{ lang.instance_layout }}</label>*/
/*                                 <select name="bf[layout_id]" id="bf-layout-id" class="bf-layout-select bf-w195">*/
/*                                     <option value="0" selected="selected">{{ lang.select }}</option>*/
/*                                     {% for  id, layout in layouts %}*/
/*                                         <option value="{{ id }}">{{ layout }}</option>*/
/*                                     {% endfor %}*/
/*                                 </select>*/
/*                             </div>*/
/*                             <div class="left">*/
/*                                 <label for="bf-layout-position">{{ lang.instance_position }}</label>*/
/*                                 <select name="bf[layout_position]" id="bf-layout-position" class="bf-layout-position bf-w195">*/
/*                                     <option value="content_top">{{ lang.instance_content_top }}</option>*/
/*                                     <option value="content_bottom">{{ lang.instance_content_bottom }}</option>*/
/*                                     <option value="column_left">{{ lang.instance_column_left }}</option>*/
/*                                     <option value="column_right">{{ lang.instance_column_right }}</option>*/
/*                                 </select>*/
/*                             </div>*/
/*                             <div class="left">*/
/*                                 <label for="bf-layout-sort-order">{{ lang.sort_order }}</label>*/
/*                                 <input type="text" name="bf[layout_sort_order]" id="bf-layout-sort-order" class="bf-layout-sort bf-w65" />*/
/*                             </div>*/
/*                             <div class="left">*/
/*                                 <span class="bf-label center">{{ lang.enabled }}</span>*/
/*                                 <div class="bf-layout-enable yesno">*/
/*                                     <span class="bf-switcher">*/
/*                                         <input id="bf-layout-off" type="hidden" name="bf[layout_enabled]" value="0" />*/
/*                                         <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                     </span>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="left">*/
/*                                 <span class="bf-label">&nbsp;</span>*/
/*                                 <a href="{{ removeInstanceAction }}" class="bf-remove-layout" onclick="if (!window.confirm(BF.lang.confirm_remove_layout)) return false;">{{ lang.instance_remove }}</a>*/
/*                             </div>*/
/* */
/*                         </div>*/
/*                         {% endif %}*/
/*                         {# Basic section tabs #}*/
/*                         <ul class="tabs vertical clearfix">*/
/*                             {% if moduleId != 'basic' %}*/
/*                             <li class="tab cat-tab*/
/*                                 {% if settings['layout_id'] is not defined or settings['layout_id'] not in [category_layouts[0]] %} hidden {% endif %}*/
/*                                 {% if settings['current_adm_tab'] is same as ('categories') %} selected {% endif %}" data-tab-name="categories" data-target="#bf-categories">{{ lang.tab_categories }}</li>*/
/*                             {% endif %}*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('embedding') %} selected {% endif %}" data-tab-name="embedding" data-target="#bf-filter-embedding">{{ lang.tab_embedding }}</li>*/
/*                             {% if moduleId is same as ('basic') %}*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('blocks') %} selected {% endif %}" data-tab-name="blocks" data-target="#bf-filter-blocks-display">{{ lang.tab_attributes_display }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('layout') %} selected {% endif %}" data-tab-name="layout" data-target="#bf-filter-layout-view">{{ lang.tab_filter_layout_view }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('submission') %} selected {% endif %}" data-tab-name="submission" data-target="#bf-data-submission">{{ lang.tab_data_submission }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('attributes') %} selected {% endif %}" data-tab-name="attributes" data-target="#bf-attributes">{{ lang.tab_attributes }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('options') %} selected {% endif %}" data-tab-name="options" data-target="#bf-options">{{ lang.tab_options }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('filters') %} selected {% endif %}" data-tab-name="filters" data-target="#bf-filters">{{ lang.tab_filters }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('responsive') %} selected {% endif %}" data-tab-name="responsive" data-target="#bf-responsive">{{ lang.tab_responsive_view }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('style') %} selected {% endif %}" data-tab-name="style" data-target="#bf-style">{{ lang.tab_style }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('global') %} selected {% endif %}" data-tab-name="global" data-target="#bf-global-settings">{{ lang.tab_global_settings }}</li>*/
/*                             {% endif %}*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('attr_values') %} selected {% endif %}" data-tab-name="attr_values" data-target="#bf-attr-values">{{ lang.tab_attr_values }}</li>*/
/*                             <li class="tab {% if settings['current_adm_tab'] is same as ('help') %} selected {% endif %}" data-tab-name="help" data-target="#bf-help">{{ lang.tab_help }}</li>*/
/*                             <li id="refresh-btn-wrapper">*/
/*                                 <button onclick="BF.refreshDB();return false;" class="bf-button" id="bf-refresh-db">*/
/*                                     <span class="icon bf-update"></span><span class="lbl">{{ lang.update_cache }}</span>*/
/*                                 </button>*/
/*                             </li>*/
/*                         </ul>*/
/*                         {# Containers for Filter Embedding #}*/
/*                         <div id="bf-filter-embedding" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.embedding_header }}</div>*/
/*                             <div class="bf-alert" style="display: block;">{{ lang.embedding_warning }}</div>*/
/*                             <p class="bf-info">{{ lang.embedding_description }}</p>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td">*/
/*                                         <span class="bf-wrapper"><label for="bf-container-selector">{{ lang.embedding_container_selector }}</label></span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <input style="width: 290px;" type="text" name="bf[behaviour][containerSelector]" value="" id="bf-container-selector" placeholder="{{ basicSettings['behaviour']['containerSelector'] }}" />*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td">*/
/*                                         <span class="bf-wrapper"><label for="bf-paginator-selector">{{ lang.embedding_paginator_selector }}</label></span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <input style="width: 290px;" type="text" name="bf[behaviour][paginatorSelector]" value="" id="bf-paginator-selector" placeholder="{{ basicSettings['behaviour']['paginatorSelector'] }}" />*/
/*                                     </td>*/
/*                                 </tr>*/
/*                             </table>*/
/*                         </div>*/
/*                         {% if moduleId is same as ('basic') %}*/
/*                         {# Filter Blocks Display #}*/
/*                         <div id="bf-filter-blocks-display" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.filter_blocks_header }}</div>*/
/*                             <p class="bf-info">{{ lang.filter_blocks_descr }}</p>*/
/*                             <table class="bf-adm-table" id="bf-filter-sections">*/
/*                                 <thead>*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th class="center">{{ lang.enabled }}</th>*/
/*                                     <th class="bf-collapse-td">{{ lang.collapse }}</th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                 </thead>*/
/*                                 <tbody>*/
/*                                 {% for filter in filterBlocks %}*/
/*                                 <tr class="bf-sort" data-section="{{ filter['name'] }}">*/
/*                                     <td class="bf-adm-label-td">*/
/*                                         <span class="bf-wrapper">{{ filter['label'] }}</span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-{{ filter['name'] }}-filter" type="hidden" name="bf[behaviour][sections][{{ filter['name'] }}][enabled]" value="0" data-disable-adv="section-{{ filter['name'] }}" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td class="center bf-collapse-td">*/
/*                                         <input id="bf-{{ filter['name'] }}-collapse" type="checkbox" name="bf[behaviour][sections][{{ filter['name'] }}][collapsed]" value="1" data-adv-group="section-{{ filter['name'] }}" />*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         {% if filter['control'] is defined and possible_controls[filter['name']] is defined %}*/
/*                                         <select name="bf[behaviour][sections][{{ filter['name'] }}][control]" data-adv-group="section-{{ filter['name'] }}">*/
/*                                             {% for val, lbl in possible_controls[filter['name']] %}*/
/*                                             <option value="{{ val }}">{{ lbl }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                         {% endif %}*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 {% endfor %}*/
/*                                 </tbody>*/
/*                             </table>*/
/*                         </div>*/
/*                         {# Filter Layout View #}*/
/*                         <div id="bf-filter-layout-view" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.layout_header }}</div>*/
/*                             <div>*/
/*                                 <table class="bf-adm-table" style="margin-bottom:0;">*/
/*                                     <tr>*/
/*                                         <th></th>*/
/*                                         <th class="center bf-w165">{{ lang.enabled }}</th>*/
/*                                         <th></th>*/
/*                                         <th></th>*/
/*                                         <th></th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td">*/
/*                                             <span class="bf-wrapper">*/
/*                                             {{ lang.layout_show_attr_groups }}*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td class="center bf-w165">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-attr-group" type="hidden" name="bf[behaviour][attribute_groups]" value="0" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="3"></td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                                 <table class="bf-adm-table bf-adv-group-cont" style="margin-bottom:0;">*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                             {{ lang.layout_product_count }}</span>*/
/*                                         </td>*/
/*                                         <td class="center bf-w165">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-product-count" type="hidden" name="bf[behaviour][product_count]" value="0" data-disable-adv="hide-empty" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="3"></td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                             {{ lang.layout_hide_empty_attr }}</span>*/
/*                                         </td>*/
/*                                         <td class="center">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-hide-empty" type="hidden" name="bf[behaviour][hide_empty]" value="0" data-adv-group="hide-empty" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="3"></td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                                 <table class="bf-adm-table bf-intersect-cont">*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                             {{ lang.layout_sliding }}</span>*/
/*                                         </td>*/
/*                                         <td class="bf-intersect center bf-w165">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-sliding" type="hidden" name="bf[behaviour][limit_items][enabled]" value="0" data-disable-adv="sliding" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="3" style="padding-top: 5px;padding-bottom: 5px;"> */
/*                                             <input id="bf-number-to-show" type="text" size="4" name="bf[behaviour][limit_items][number_to_show]" value="" data-adv-group="sliding" />*/
/*                                             <label for="bf-number-to-show">{{ lang.layout_sliding_num_to_show }}</label>*/
/*                                             <div class="bf-suboption">*/
/*                                                 <input id="bf-number-to-hide" type="text" size="4" name="bf[behaviour][limit_items][number_to_hide]" value="" data-adv-group="sliding" /> */
/*                                                 <label for="bf-number-to-hide">{{ lang.layout_sliding_min }}</label>*/
/*                                             </div>*/
/*                                         </td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                             {{ lang.layout_height_limit }}</span>*/
/*                                         </td>*/
/*                                         <td class="bf-intersect center">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-limit-height" type="hidden" name="bf[behaviour][limit_height][enabled]" value="0" data-disable-adv="limit-height" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="3"> */
/*                                             <input id="bf-limit-height" type="text" size="4" name="bf[behaviour][limit_height][height]" value="" data-adv-group="limit-height" /> */
/*                                             <label for="bf-limit-height">{{ lang.layout_max_height_limit }}</label>*/
/*                                         </td>*/
/*                                     </tr>*/
/* */
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                         {# Data Submission Tab #}*/
/*                         <div id="bf-data-submission" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.submission_header }}</div>*/
/*                             <p class="bf-info">{{ lang.submission_descr }}</p>*/
/*                             <div>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th class="center">Enabled</th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         <label for="bf-submit-auto">{{ lang.submission_type_auto }}</label></span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <input id="bf-submit-auto" type="radio" value="auto" name="bf[submission][submit_type]" data-disable-adv="submit-type" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         <label for="bf-submit-delay">{{ lang.submission_delay }}</label></span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <input id="bf-submit-delay" type="radio" value="delay" name="bf[submission][submit_type]" data-disable-adv="submit-type" />*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <input id="bf-submit-delay-time" type="text" name="bf[submission][submit_delay_time]" value="" size="4" maxlength="4" data-adv-group="submit-type" data-for-val="delay" />*/
/*                                         <label for="bf-submit-delay-time">{{ lang.submission_time_in_sec }}</label>*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper"> */
/*                                             <label for="bf-submit-btn">{{ lang.submission_type_button }}</label></span>*/
/*                                     </td>*/
/*                                     <td class="center"> */
/*                                         <input id="bf-submit-btn" type="radio" value="button" name="bf[submission][submit_type]" data-disable-adv="submit-type" />*/
/*                                     </td>*/
/*                                     <td style="padding-top: 5px;padding-bottom: 5px;">*/
/*                                         <input id="bf-submit-btn-fixed" type="radio" name="bf[submission][submit_button_type]" value="fix" data-adv-group="submit-type" data-for-val="button" />*/
/*                                         <label for="bf-submit-btn-fixed">{{ lang.submission_button_fixed }}</label>*/
/*                                         <div class="bf-suboption">*/
/*                                             <input id="bf-submit-btn-float" type="radio" name="bf[submission][submit_button_type]" value="float" data-adv-group="submit-type" data-for-val="button" />*/
/*                                             <label for="bf-submit-btn-float">{{ lang.submission_button_float }}</label>*/
/*                                         </div>*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 <tr class="bf-local-settings">*/
/*                                     <td class="bf-adm-label-td">*/
/*                                         <span class="bf-wrapper">*/
/*                                             <label for="bf-submit-default">{{ lang.submission_type_default }}</label>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <input id="bf-submit-default" type="radio" value="default" name="bf[submission][submit_type]" data-disable-adv="submit-type" class="bf-default" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         {{ lang.submission_hide_panel }}</span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-hide-layout" type="hidden" name="bf[submission][hide_panel]" value="0" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                             </table>*/
/*                         </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {# Categories #}*/
/*                         {% if moduleId != 'basic' %}*/
/*                         <div id="bf-categories" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.categories_header }}</div>*/
/*                             <p class="bf-info">{{ lang.categories_info }}</p>*/
/*                             <div class="bf-multi-select-group">*/
/*                                 <p class="bf-green-info">{{ lang.categories_list_of_enabled }}</p>*/
/*                                 <div class="bf-gray-panel">*/
/*                                     {{ lang.categories_filter_hint }}*/
/*                                     <input type="text" class="bf-cat-filter bf-full-width" data-target="#bf-enabled-categories" />*/
/*                                 </div>*/
/*                                 <div style="padding-left:15px;">*/
/*                                     <a data-select-all="#bf-enabled-categories">{{ lang.categories_select_all }}</a> /*/
/*                                     <a data-unselect-all="#bf-enabled-categories">{{ lang.categories_unselect_all }}</a>*/
/*                                 </div>*/
/*                                 <div id="bf-enabled-categories" class="bf-multi-select">*/
/*                                     {% for category in categories %}*/
/*                                         {% if not settings['categories'] is defined or settings['categories'][category['category_id']] is not defined %}*/
/*                                         <div class="bf-row">*/
/*                                             <input type="hidden" name="bf[categories][{{ category['category_id'] }}]" value="1" />*/
/*                                             {{ category['name'] }}*/
/*                                         </div>*/
/*                                         {% endif %}*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="bf-middle-buttons-col">*/
/*                                 {{ lang.categories_move_selected }}*/
/*                                 <br><button class="bf-move-right"></button><br><button class="bf-move-left"></button><br>*/
/*                             </div>*/
/*                             <div class="bf-multi-select-group">*/
/*                                 <p class="bf-red-info">{{ lang.categories_list_of_disabled }}</p>*/
/*                                 <div class="bf-gray-panel">*/
/*                                     {{ lang.categories_filter_hint }}*/
/*                                     <input type="text" class="bf-cat-filter bf-full-width" data-target="#bf-disabled-categories" />*/
/*                                 </div>*/
/*                                 <div style="padding-left:15px;">*/
/*                                     <a data-select-all="#bf-disabled-categories">{{ lang.categories_select_all }}</a> /*/
/*                                     <a data-unselect-all="#bf-disabled-categories">{{ lang.categories_unselect_all }}</a>*/
/*                                 </div>*/
/*                                 <div id="bf-disabled-categories" class="bf-multi-select">*/
/*                                     {% if settings['categories'] is defined %}*/
/*                                         {% for catId, b in settings['categories'] %}*/
/*                                         <div class="bf-row">*/
/*                                             <input type="hidden" name="bf[categories][{{ categories[catId]['category_id'] }}]" value="1" />*/
/*                                             {{ categories[catId]['name'] }}*/
/*                                         </div>*/
/*                                         {% endfor %}*/
/*                                     {% endif %}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="clearfix"></div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {% if moduleId is same as ('basic') %}*/
/*                         {# Attributes Tab #}*/
/*                         <div id="bf-attributes" class="tab-content with-border" data-group="settings" data-select-all-group="attributes">*/
/*                             <div class="bf-th-header-static">{{ lang.attributes_header }}</div>*/
/*                             <div>*/
/*                                 <table class="bf-adm-table" data-select-all-group="attributes">*/
/*                                     <tr>*/
/*                                         <th></th>*/
/*                                         <th class="center">{{ lang.enabled }}</th>*/
/*                                         <th class="bf-w165">{{ lang.control }}</th>*/
/*                                         <th></th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.attributes_group_setting }}</span></td>*/
/*                                         <td class="center">*/
/*                                             <span class="bf-switcher">*/
/*                                                 <input id="bf-layout-off" type="hidden" name="bf[attributes_default][enable_all]" value="0" data-disable-adv="group-attr-control" />*/
/*                                                 <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td>*/
/*                                             <select name="bf[attributes_default][control]" data-adv-group="group-attr-control">*/
/*                                                 <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                                                 <option value="radio">{{ lang.radio }}</option>*/
/*                                                 <option value="select">{{ lang.selectbox }}</option>*/
/*                                                 <option value="slider">{{ lang.slider }}</option>*/
/*                                                 <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                                                 <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                                             </select>*/
/*                                         </td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                             </div>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.attributes_individual_set }}</div>*/
/*                             <div>*/
/*                                 <div class="bf-gray-panel">*/
/*                                     <div class="bf-half">*/
/*                                         {{ lang.autocomplete_hint|format(lang.btn_select_attribute) }}*/
/*                                         <input type="text" id="bf-attr-search" class="bf-autocomplete" data-lookup="attributes" />*/
/*                                         <button id="bf-attr-add" class="btn btn-success bf-add-row" data-filter-data="" data-row-tpl="#custom-attr-setting-template" data-target-tbl="#custom-attr-settings">*/
/*                                             <i class="fa fa-plus"></i> {{ lang.btn_select_attribute }}*/
/*                                         </button>*/
/*                                     </div>*/
/*                                     <div class="bf-half">*/
/*                                         <p class="bf-info">{{ lang.attributes_custom_set_descr }}</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <table class="bf-adm-table bf-hide-if-empty" data-select-all-group="attributes">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th style="width:625px"></th>*/
/*                                             <th class="center" style="width:50px">Collapse</th>*/
/*                                             <th class="center" style="width:330px">*/
/*                                                 {{ lang.enabled }}*/
/*                                                 <div class="bf-group-actions">*/
/*                                                     <a data-select-all="attributes" data-select-all-val="2" class="bf-local-settings">{{ lang.set_all_default }}</a>*/
/*                                                     <span class="bf-local-settings">/</span>*/
/*                                                     <a data-select-all="attributes" data-select-all-val="0">{{ lang.disable_all }}</a>*/
/*                                                     <span>/</span>*/
/*                                                     <a data-select-all="attributes" data-select-all-val="1">{{ lang.enable_all }}</a>*/
/*                                                 </div> */
/*                                             </th>*/
/*                                             <th class="bf-w165">{{ lang.control }}</th>*/
/*                                             <th></th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody id="custom-attr-settings">*/
/*                                         */
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                         {# Options Tab #}*/
/*                         <div id="bf-options" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.options_header }}</div>*/
/*                             <table class="bf-adm-table" data-select-all-group="attributes">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th class="center bf-w165">{{ lang.enabled }}</th>*/
/*                                     <th class="bf-w165">{{ lang.control }}</th>*/
/*                                     <th class="bf-w165">{{ lang.options_view_mode }}</th>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.options_group_setting }}</span></td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-layout-off" type="hidden" name="bf[options_default][enable_all]" value="0" data-disable-adv="group-opt-control" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <select name="bf[options_default][control]" data-adv-group="group-opt-control" class="bf-w165">*/
/*                                             <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                                             <option value="radio">{{ lang.radio }}</option>*/
/*                                             <option value="select">{{ lang.selectbox }}</option>*/
/*                                             <option value="slider">{{ lang.slider }}</option>*/
/*                                             <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                                             <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                                             <option value="slider_lbl_inp">{{ lang.grid_of_images }}</option>*/
/*                                         </select>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <select name="bf[options_default][mode]" class="bf-opt-mode bf-w135 d-opt-control" data-adv-group="group-opt-control" data-bf-role="mode">*/
/*                                             <option value="label">{{ lang.options_view_mode_label }}</option>*/
/*                                             <option value="img_label">{{ lang.options_view_mode_image_and_label }}</option>*/
/*                                             <option value="img">{{ lang.options_view_mode_image }}</option>*/
/*                                         </select>*/
/*                                     </td>*/
/*                                 </tr>*/
/*                             </table>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.options_individual_set }}</div>*/
/*                             <div>*/
/*                                 <div class="bf-gray-panel">*/
/*                                     <div class="bf-half">*/
/*                                         {{ lang.autocomplete_hint|format(lang.btn_select_option) }}*/
/*                                         <input type="text" id="bf-attr-search" class="bf-w190 bf-autocomplete" data-lookup="options" />*/
/*                                         <button id="bf-attr-add" class="btn btn-success bf-add-row" data-filter-data="" data-row-tpl="#custom-opt-setting-template" data-target-tbl="#custom-opt-settings">*/
/*                                             <i class="fa fa-plus"></i> {{ lang.btn_select_option }}*/
/*                                         </button>*/
/*                                     </div>*/
/*                                     <div class="bf-half">*/
/*                                         <p class="bf-info">{{ lang.options_custom_set_descr }}</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <table class="bf-adm-table bf-hide-if-empty" data-select-all-group="options">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th style="width:625px"></th>*/
/*                                             <th class="center" style="width:50px">Collapse</th>*/
/*                                             <th class="center" style="width:330px">{{ lang.enabled }}</th>*/
/*                                                 {{ lang.enabled }}*/
/*                                                 <div class="bf-group-actions">*/
/*                                                     <a data-select-all="options" data-select-all-val="2" class="bf-local-settings">{{ lang.set_all_default }}</a>*/
/*                                                     <span class="bf-local-settings">/</span>*/
/*                                                     <a data-select-all="options" data-select-all-val="0">{{ lang.disable_all }}</a>*/
/*                                                     <span>/</span>*/
/*                                                     <a data-select-all="options" data-select-all-val="1">{{ lang.enable_all }}</a>*/
/*                                                 </div> */
/*                                             </th>*/
/*                                             <th class="bf-w165">{{ lang.control }}</th>*/
/*                                             <th class="bf-w165">{{ lang.options_view_mode }}</th>*/
/*                                             <th></th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody id="custom-opt-settings">*/
/*                                         */
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                         {# Filters Tab #}*/
/*                         <div id="bf-filters" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.filters_header }}</div>*/
/*                             <table class="bf-adm-table" data-select-all-group="filters">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th class="center">{{ lang.enabled }}</th>*/
/*                                     <th class="bf-w165">{{ lang.control }}</th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.filters_group_setting }}</span></td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-layout-off" type="hidden" name="bf[filters_default][enable_all]" value="0" data-disable-adv="group-filter-control" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <select name="bf[filters_default][control]" data-adv-group="group-filter-control">*/
/*                                             <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                                             <option value="radio">{{ lang.radio }}</option>*/
/*                                             <option value="select">{{ lang.selectbox }}</option>*/
/*                                             <option value="slider">{{ lang.slider }}</option>*/
/*                                             <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                                             <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                                         </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                             </table>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.filters_individual_set }}</div>*/
/*                             <div>*/
/*                                 <div class="bf-gray-panel">*/
/*                                     <div class="bf-half">*/
/*                                         {{ lang.autocomplete_hint|format(lang.btn_select_filter) }}*/
/*                                         <input type="text" id="bf-attr-search" class="bf-w190 bf-autocomplete" data-lookup="filters" />*/
/*                                         <button id="bf-attr-add" class="btn btn-success bf-add-row" data-filter-data="" data-row-tpl="#custom-filter-setting-template" data-target-tbl="#custom-filter-settings">*/
/*                                             <i class="fa fa-plus"></i> {{ lang.btn_select_filter }}*/
/*                                         </button>*/
/*                                     </div>*/
/*                                     <div class="bf-half">*/
/*                                         <p class="bf-info">{{ lang.filters_custom_set_descr }}</p>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <table class="bf-adm-table bf-hide-if-empty" data-select-all-group="filters">*/
/*                                     <thead>*/
/*                                         <tr>*/
/*                                             <th style="width:625px"></th>*/
/*                                             <th class="center" style="width:50px">Collapse</th>*/
/*                                             <th class="center" style="width:330px">*/
/*                                                 {{ lang.enabled }}*/
/*                                                 <div class="bf-group-actions">*/
/*                                                     <a data-select-all="filters" data-select-all-val="2" class="bf-local-settings">{{ lang.set_all_default }}</a>*/
/*                                                     <span class="bf-local-settings">/</span>*/
/*                                                     <a data-select-all="filters" data-select-all-val="0">{{ lang.disable_all }}</a>*/
/*                                                     <span>/</span>*/
/*                                                     <a data-select-all="filters" data-select-all-val="1">{{ lang.enable_all }}</a>*/
/*                                                 </div> */
/*                                             </th>*/
/*                                             <th class="bf-w165">{{ lang.control }}</th>*/
/*                                             <th></th>*/
/*                                         </tr>*/
/*                                     </thead>*/
/*                                     <tbody id="custom-filter-settings">*/
/*                                         */
/*                                     </tbody>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                         {# Responsive View #}*/
/*                         <div id="bf-responsive" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.responsive_header }}</div>*/
/*                             <table class="bf-adm-table bf-adv-group-cont">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th class="bf-w170"></th>*/
/*                                     <th></th>*/
/*                                     <th><span class="bf-local-settings">{{ lang.default }}</span></th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_mode_enable }}</span></td> */
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-responsive" type="hidden" name="bf[style][responsive][enabled]" value="0" data-disable-adv="responsive" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_collapse_sections }}</span></td> */
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-responsive-collapse" type="hidden" name="bf[style][responsive][collapsed]" value="0" data-adv-group="responsive" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_max_width }}</span></td> */
/*                                     <td class="center">*/
/*                                         <input type="text" name="bf[style][responsive][max_width]" value="" data-adv-group="responsive" class="bf-w65" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_max_screen_width }}</span></td> */
/*                                     <td class="center">*/
/*                                         <input type="text" name="bf[style][responsive][max_screen_width]" value="" data-adv-group="responsive" class="bf-w65" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_position }}</span></td> */
/*                                     <td class="center">*/
/*                                         <input id="bf-responsive-position-left" type="radio" name="bf[style][responsive][position]" value="left" data-adv-group="responsive" />*/
/*                                         <label for="bf-responsive-position-left">{{ lang.left }}</label>*/
/*                                         <input id="bf-responsive-position-right" type="radio" name="bf[style][responsive][position]" value="right" data-adv-group="responsive" />*/
/*                                         <label for="bf-responsive-position-right">{{ lang.right }}</label>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.responsive_offset }}</span></td> */
/*                                     <td class="center">*/
/*                                         <input id="bf-responsive-offset" type="text" name="bf[style][responsive][offset]" value="" data-adv-group="responsive" class="bf-w65" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                             </table>*/
/*                         </div>*/
/*                         {# Theme Tab #}*/
/*                          <div id="bf-style" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.theme_block_header }}</div>*/
/*                             <div>*/
/*                                 <table class="bf-adm-table">*/
/*                                     <tr>*/
/*                                         <th></th>*/
/*                                         <th></th>*/
/*                                         <th><span class="bf-local-settings">{{ lang.default }}</span></th>*/
/*                                         <th></th>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_background }}</span></td>*/
/*                                         <td class="center bf-w170">*/
/*                                             <input id="bf-style-block-header-background" class="bf-w165 entry color-pick" type="text" name="bf[style][block_header_background][val]" />*/
/*                                         </td>*/
/*                                         <td class="bf-w65 center"><input type="checkbox" name="bf[style][block_header_background][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("block_header_background", this)}'/></td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_text }}</span></td>*/
/*                                         <td class="center bf-w170">*/
/*                                             <input id="bf-style-block-header-text" class="bf-w165 entry color-pick" type="text" name="bf[style][block_header_text][val]" /> */
/*                                         </td>*/
/*                                         <td class="bf-w65 center"><input type="checkbox" name="bf[style][block_header_text][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("block_header_text", this)}'/></td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                     {% for language in languages %}*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td">*/
/*                                             <span class="bf-wrapper">*/
/*                                                 {{ lang.theme_title }} (*/
/*                                                 <img src="{{ language['image_path'] }}" /> */
/*                                                 {{ language['name'] }})*/
/*                                             </span>*/
/*                                         </td>*/
/*                                         <td colspan="2">*/
/*                                             <input type="text" name="bf[behaviour][filter_name][{{ language['language_id'] }}]" value="" class="bf-w195" />*/
/*                                         </td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </table>*/
/*                             </div>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.theme_product_quantity }}</div>*/
/*                             <div>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th></th>*/
/*                                     <th><span class="bf-local-settings">{{ lang.default }}</span></th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                  <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_background }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-product-quantity-background" class="bf-w165 entry color-pick" type="text" name="bf[style][product_quantity_background][val]"/> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][product_quantity_background][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("product_quantity_background", this)}'/></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_text }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-product-quantity-text" class="bf-w165 entry color-pick" type="text" name="bf[style][product_quantity_text][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][product_quantity_text][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("product_quantity_text", this)}' /></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 </table>*/
/*                                 </div>*/
/*                                 <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.theme_price_slider }}</div>*/
/*                             <div>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th></th>*/
/*                                     <th><span class="bf-local-settings">{{ lang.default }}</span></th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                  <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_background }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-price-slider-background" class="bf-w165 entry color-pick" type="text" name="bf[style][price_slider_background][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][price_slider_background][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("price_slider_background", this)}' /></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_active_area_background }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-price-slider-area-background" class="bf-w165 entry color-pick" type="text" name="bf[style][price_slider_area_background][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][price_slider_area_background][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("price_slider_area_background", this)}' /></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_border }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-price-slider-border" class="bf-w165 entry color-pick" type="text" name="bf[style][price_slider_border][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][price_slider_border][default]" value="1" class="bf-chkbox-def bf-local-settings"  onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("price_slider_border", this)}'/></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_slider_handle_background }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-price-slider-handle-background" class="bf-w165 entry color-pick" type="text" name="bf[style][price_slider_handle_background][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][price_slider_handle_background][default]" value="1" class="bf-chkbox-def bf-local-settings"  onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("price_slider_handle_background", this)}'/></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_slider_handle_border }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-price-slider-handle-border" class="bf-w165 entry color-pick" type="text" name="bf[style][price_slider_handle_border][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][price_slider_handle_border][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("price_slider_handle_border", this)}'/></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/* */
/*                             </table>*/
/*                             </div>*/
/*                              <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.theme_group_block_header }}</div>*/
/*                             <div>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <th></th>*/
/*                                     <th></th>*/
/*                                     <th><span class="bf-local-settings">{{ lang.default }}</span></th>*/
/*                                     <th></th>*/
/*                                 </tr>*/
/*                                  <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_background }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-group-block-header-background" class="bf-w165 entry color-pick" type="text" name="bf[style][group_block_header_background][val]" /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][group_block_header_background][default]" value="1" class="bf-chkbox-def bf-local-settings"  onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("group_block_header_background", this)}'/></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_text }}</span></td>*/
/*                                     <td class="center bf-w170">*/
/*                                         <input id="bf-style-group-block-header-text" class="bf-w165 entry color-pick" type="text" name="bf[style][group_block_header_text][val]"  /> */
/*                                     </td>*/
/*                                     <td class="bf-w65 center"><input type="checkbox" name="bf[style][group_block_header_text][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange = 'if (jQuery(this).is(":checked")) {BF.changeDefault("group_block_header_text", this)}' /></td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 </table>*/
/*                             </div>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.theme_responsive_popup_view }}</div>*/
/*                             <div>*/
/*                                 <table class="bf-adm-table">*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_show_btn_color }}</span></td>*/
/*                                         <td class="center bf-w170">*/
/*                                             <input class="bf-w165 entry color-pick" type="text" name="bf[style][resp_show_btn_color][val]" />*/
/*                                         </td>*/
/*                                         <td class="bf-w65 center"><input type="checkbox" name="bf[style][resp_show_btn_color][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange="if (jQuery(this).is(':checked')) {BF.changeDefault('resp_show_btn_color', this);}" /></td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                     <tr>*/
/*                                         <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.theme_reset_btn_color }}</span></td>*/
/*                                         <td class="center bf-w170">*/
/*                                             <input class="bf-w165 entry color-pick" type="text" name="bf[style][resp_reset_btn_color][val]" />*/
/*                                         </td>*/
/*                                         <td class="bf-w65 center"><input type="checkbox" name="bf[style][resp_reset_btn_color][default]" value="1" class="bf-chkbox-def bf-local-settings" onchange="if (jQuery(this).is(':checked')) {BF.changeDefault('resp_reset_btn_color', this);}" /></td>*/
/*                                         <td></td>*/
/*                                     </tr>*/
/*                                 </table>*/
/*                             </div>*/
/*                         </div>*/
/*                         {% endif %}*/
/*                         {# Attribute Values Ordering #}*/
/*                         <div id="bf-attr-values" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.ordering_header }}</div>*/
/*                             <div class="bf-alert" style="display: block;">{{ lang.ordering_note }}</div>*/
/*                             <p class="bf-info">{{ lang.ordering_descr }}</p>*/
/*                             <div class="bf-multi-select-group">*/
/*                                 <div class="bf-gray-panel">*/
/*                                     {{ lang.ordering_filter_hint }}*/
/*                                     <input type="text" class="bf-attr-filter bf-full-width" data-target="#bf-attr-list" />*/
/*                                 </div>*/
/*                                 <div id="bf-attr-list" class="bf-multi-select">*/
/*                                     {% for attrId, attribute in attributes %}*/
/*                                     <div class="bf-row" data-attr-id="{{ attrId }}">*/
/*                                         <b>{{ attribute['group'] }}</b> / {{ attribute['name'] }}*/
/*                                     </div>*/
/*                                     {% endfor %}*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="bf-middle-buttons-col">*/
/*                             </div>*/
/*                             <div class="bf-multi-select-group">*/
/*                                 <div class="bf-gray-panel">*/
/*                                     <div class="buttons">*/
/*                                         <div>{{ lang.ordering_language }}:</div>*/
/*                                         <select id="bf-attr-val-language" class="bf-w165">*/
/*                                             {% for i, language in languages %}*/
/*                                             <option value="{{ language['language_id'] }}">{{ language['name'] }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                         <div class="bf-pull-right">*/
/*                                             <button class="bf-auto-sort" data-type="number">0..9</button>*/
/*                                             <button class="bf-auto-sort" data-type="string">A..Z</button>*/
/*                                             <a class="bf-button bf-save-btn" title="{{ lang.button_save_n_close }}"><span class="icon"></span></a>*/
/*                                         </div>*/
/*                                     </div>*/
/*                                 </div>*/
/*                                 <div id="bf-attr-val-list" class="bf-multi-select">*/
/*                                     */
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="clearfix"></div>*/
/*                         </div>*/
/*                         {# Global Settings #}*/
/*                         <div id="bf-global-settings" class="tab-content with-border bf-global-settings" data-group="settings">*/
/*                             <div class="bf-th-header-static">{{ lang.global_header }}</div>*/
/*                             <p class="bf-info">{{ lang.global_settings_descr }}</p>*/
/*                             <table class="bf-adm-table">*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.global_hide_empty_stock }}</span></td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-hide-out-of-stock" type="hidden" name="bf[global][hide_out_of_stock]" value="0" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr class="bf-global-settings">*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         {{ lang.global_postponed_count }}</span>*/
/*                                     </td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-postponed-count" type="hidden" name="bf[global][postponed_count]" value="0" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">{{ lang.global_enable_multiple_attributes }}</span></td>*/
/*                                     <td class="center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-multiple-attributes" type="hidden" name="bf[global][multiple_attributes]" value="0" data-disable-adv="attr-separator" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <label for="bf-attr-separator">{{ lang.global_separator }}</label>*/
/*                                         <input id="bf-attr-separator" type="text" name="bf[global][attribute_separator]" value="" size="4" data-adv-group="attr-separator" />*/
/*                                         {{ lang.global_multiple_attr_separator }}*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         {{ lang.global_in_stock_status_id }}</span>*/
/*                                     </td>*/
/*                                     <td class="" colspan="2">*/
/*                                         <select name="bf[global][instock_status_id]" class="bf-w165">*/
/*                                             {% for status in stockStatuses %}*/
/*                                                     <option value="{{ status['stock_status_id'] }}">{{ status['name'] }}</option>*/
/*                                             {% endfor %}*/
/*                                         </select>*/
/*                                     </td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         {{ lang.global_subcats_fix }}</span>*/
/*                                     </td>*/
/*                                     <td class=" center">*/
/*                                         <span class="bf-switcher">*/
/*                                             <input id="bf-subcategories" type="hidden" name="bf[global][subcategories_fix]" value="0" />*/
/*                                             <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td class="bf-adm-label-td"><span class="bf-wrapper">*/
/*                                         {{ lang.entry_cron_secret_key }}*/
/*                                             <span class="bf-link-highlight" style="padding-top:5px;">{{ catalogUrl }}index.php?route=extension/module/brainyfilter/cron&key=<b>cron secret key</b></span>*/
/*                                         </span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                         <input id="bf-cron-key" class="bf-w165" type="text" name="bf[global][cron_secret_key]" value="" />*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                 </tr>*/
/*                                 <tr>*/
/*                                     <td colspan="3">*/
/*                                     </td>*/
/*                                 </tr>*/
/*                             </table>                    */
/*                         </div>*/
/*                         {# FAQ container #}*/
/*                         <div id="bf-help" class="tab-content with-border" data-group="settings">*/
/*                             <div class="bf-th-header" id="bf-faq-n-troubleshooting"><span class="icon bf-arrow"></span>{{ lang.help_faq_n_trouleshooting }}</div>*/
/*                             <div style="display:none;">*/
/*                                 */
/*                             </div>*/
/*                             <div class="bf-th-header expanded"><span class="icon bf-arrow"></span>{{ lang.help_about }}</div>*/
/*                             <div id="bf-about">*/
/*                                 <div class="bf-about-text">*/
/*                                     {{ lang.help_about_content }}*/
/*                                     <hr />*/
/*                                     <p>{{ lang.bf_signature }}</p>*/
/*                                 </div>*/
/*                             </div>*/
/*                         </div>*/
/* */
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </form>*/
/*     <div class="bf-signature">{{ lang.bf_signature }}</div>*/
/* </div>*/
/* */
/* <table style="display:none;">*/
/*     <tbody id="custom-attr-setting-template">*/
/*         <tr>*/
/*             <td class="bf-adm-label-td"><span class="bf-wrapper bf-attr-name" data-bf-role="name"></span></td>*/
/*             <td class="bf-adm-label-td center"><input type="checkbox" name="bf[behaviour][sections][attribute][{i}][collapsed]" data-bf-role="collapsed"></td>*/
/*             <td class="center">*/
/*                 <span class="bf-switcher">*/
/*                     <input class="bf-attr-enable" type="hidden" name="bf[attributes][{i}][enabled]" value="0" data-disable-adv="attr-control-{i}" data-bf-role="enabled" />*/
/*                     <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                 </span>*/
/*             </td>*/
/*             <td><select name="bf[attributes][{i}][control]" data-adv-group="attr-control-{i}" data-bf-role="control">*/
/*                     <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                     <option value="radio">{{ lang.radio }}</option>*/
/*                     <option value="select">{{ lang.selectbox }}</option>*/
/*                     <option value="slider">{{ lang.slider }}</option>*/
/*                     <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                     <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                 </select></td>*/
/*             <td><a class="bf-remove-row"><i class="fa fa-times"></i></a></td>*/
/*         </tr>*/
/*     </tbody>*/
/* </table>*/
/* <table style="display:none;">*/
/*     <tbody id="custom-opt-setting-template">*/
/*         <tr>*/
/*             <td class="bf-adm-label-td"><span class="bf-wrapper bf-attr-name" data-bf-role="name"></span></td>*/
/*             <td class="bf-adm-label-td center"><input type="checkbox" name="bf[behaviour][sections][options][{i}][collapsed]" data-bf-role="collapsed"></td>*/
/*             <td class="center">*/
/*                 <span class="bf-switcher">*/
/*                     <input class="bf-attr-enable" type="hidden" name="bf[options][{i}][enabled]" value="0" data-disable-adv="opt-control-{i}" data-bf-role="enabled" />*/
/*                     <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                 </span>*/
/*             </td>*/
/*             <td class="center">*/
/*                 <select name="bf[options][{i}][control]" data-adv-group="opt-control-{i}" data-bf-role="control" class="bf-w135">*/
/*                     <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                     <option value="radio">{{ lang.radio }}</option>*/
/*                     <option value="select">{{ lang.selectbox }}</option>*/
/*                     <option value="slider">{{ lang.slider }}</option>*/
/*                     <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                     <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                     <option value="grid">{{ lang.grid_of_images }}</option>*/
/*                 </select>*/
/*             </td>*/
/*             <td class="center">*/
/*                 <select name="bf[options][{i}][mode]" class="bf-opt-mode bf-w135" data-adv-group="opt-control-{i}" data-bf-role="mode">*/
/*                     <option value="label">{{ lang.options_view_mode_label }}</option>*/
/*                     <option value="img_label">{{ lang.options_view_mode_image_and_label }}</option>*/
/*                     <option value="img">{{ lang.options_view_mode_image }}</option>*/
/*                 </select>*/
/*             </td>*/
/*             <td><a class="bf-remove-row"><i class="fa fa-times"></i></a></td>*/
/*         </tr>*/
/*     </tbody>*/
/* </table>*/
/* <table style="display:none;">*/
/*     <tbody id="custom-filter-setting-template">*/
/*         <tr>*/
/*             <td class="bf-adm-label-td"><span class="bf-wrapper bf-attr-name" data-bf-role="name"></span></td>*/
/*             <td class="bf-adm-label-td center"><input type="checkbox" name="bf[behaviour][sections][filter][{i}][collapsed]" data-bf-role="collapsed"></td>*/
/*             <td class="center">*/
/*                 <span class="bf-switcher">*/
/*                     <input class="bf-attr-enable" type="hidden" name="bf[filters][{i}][enabled]" value="0" data-disable-adv="filter-control-{i}" data-bf-role="enabled" />*/
/*                     <span class="bf-def"></span><span class="bf-no"></span><span class="bf-yes"></span>*/
/*                 </span>*/
/*             </td>*/
/*             <td><select name="bf[filters][{i}][control]" data-adv-group="filter-control-{i}" data-bf-role="control">*/
/*                     <option value="checkbox">{{ lang.checkbox }}</option>*/
/*                     <option value="radio">{{ lang.radio }}</option>*/
/*                     <option value="select">{{ lang.selectbox }}</option>*/
/*                     <option value="slider">{{ lang.slider }}</option>*/
/*                     <option value="slider_lbl">{{ lang.slider_labels_only }}</option>*/
/*                     <option value="slider_lbl_inp">{{ lang.slider_labels_and_inputs }}</option>*/
/*                 </select></td>*/
/*             <td><a class="bf-remove-row"><i class="fa fa-times"></i></a></td>*/
/*         </tr>*/
/*     </tbody>*/
/* </table>*/
/* */
/* {# Category list template #}*/
/* */
/* <div id="bf-category-list-tpl" class="bf-category-list" style="display: none;">*/
/*     <div class="bf-label">*/
/*         {{ lang.categories }}*/
/*         (<a onclick="jQuery(this).closest('.bf-category-list').find('input').removeAttr('checked')">{{ lang.disable_all }}</a>*/
/*         <span>/</span>*/
/*         <a onclick="jQuery(this).closest('.bf-category-list').find('input').attr('checked', 'checked')">{{ lang.enable_all }}</a>)*/
/*     </div>*/
/*     <div class="bf-cat-list-cont">*/
/*         <ul data-select-all-group="categories">*/
/*             {% for cat in categories %}*/
/*             <li>*/
/*                 <label>*/
/*                     <input type="checkbox" name="bf[categories][{{ cat['category_id'] }}]" value="1" />*/
/*                     {{ cat['name'] }}*/
/*                 </label>*/
/*             </li>*/
/*             {% endfor %}*/
/*         </ul>*/
/*     </div>*/
/* </div>*/
/* */
/* {# End of Category list template #}*/
/* <script>*/
/* BF.lang = {*/
/*         'default' : '{{ lang.default }}',*/
/*         'error_layout_not_set' : '{{ lang.message_error_layout_not_set }}',*/
/*         'error_cant_remove_default' : '{{ lang.instance_remove_default_layout }}',*/
/*         'default_layout' : '{{ lang.instance_default_layout }}',*/
/*         'confirm_remove_layout' : '{{ lang.instance_remove_confirmation }}',*/
/*         'confirm_unsaved_changes' : '{{ lang.message_unsaved_changes }}',*/
/*         'updating' : '{{ lang.updating }}',*/
/*         'empty_table' : '{{ lang.message_empty_table }}',*/
/*         'content_top' : '{{ lang.instance_content_top }}',*/
/*         'column_left' : '{{ lang.instance_column_left }}',*/
/*         'column_right' : '{{ lang.instance_column_right }}',*/
/*         'content_bottom' : '{{ lang.instance_content_bottom }}'*/
/*     };*/
/* BF.moduleId = '{{ isNewInstance ? 'new' : moduleId }}';*/
/* BF.settings = {{ settings|json_encode() }};*/
/* BF.attributes = {{ attributes|json_encode() }};*/
/* BF.options = {{ options|json_encode() }};*/
/* BF.filters = {{ filters|json_encode() }};*/
/* BF.refreshActionUrl = '{{ refreshAction|replace({'&amp;': '&' }) }}';*/
/* BF.modRefreshActionUrl = '{{ modRefreshAction|replace({'&amp;': '&' }) }}';*/
/* BF.attrValActionUrl = '{{ attributeValuesAction|replace({'&amp;': '&' }) }}';*/
/* BF.isFirstLaunch = {{ isFirstLaunch }};*/
/* BF.categoryLayouts = {{ category_layouts|json_encode() }};*/
/* jQuery(document).ready(BF.init());*/
/* </script>*/
/* */
/* <style>*/
/*     .bf-def:before {*/
/*         content: '{{ lang.default }}';*/
/*     }*/
/*     .bf-no:before {*/
/*         content: '{{ lang.no }}';*/
/*     }*/
/*     .bf-yes:before {*/
/*         content: '{{ lang.yes }}';*/
/*     }*/
/*     .bf-disable-enable .bf-no:before {*/
/*         content: '{{ lang.disable_all }}';*/
/*     }*/
/*     .bf-disable-enable .bf-yes:before {*/
/*         content: '{{ lang.enable_all }}';*/
/*     }*/
/* </style>*/
/* {{ footer }}*/
