<?php

/* default/template/extension/quickcheckout/shipping_method.twig */
class __TwigTemplate_0412223caa80fc15a0a126348ca93ba1d04f3e8f75eb8e067dadede109507ec9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 2
            echo "    <div class=\"alert alert-danger\">";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "</div>
";
        }
        // line 4
        echo "
  ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["shipping_methods"]) ? $context["shipping_methods"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["shipping_method"]) {
            // line 6
            echo "      ";
            if ( !$this->getAttribute($context["shipping_method"], "error", array())) {
                // line 7
                echo "          ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["shipping_method"], "quote", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["quote"]) {
                    // line 8
                    echo "              <div class=\"radio\">
                  ";
                    // line 9
                    if (($this->getAttribute($context["quote"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                        // line 10
                        echo "                  <input type=\"radio\" name=\"shipping_method\" id=\"";
                        echo $this->getAttribute($context["quote"], "code", array());
                        echo "\" value=\"";
                        echo $this->getAttribute($context["quote"], "code", array());
                        echo "\" checked=\"checked\">
                  ";
                    } else {
                        // line 12
                        echo "                  <input type=\"radio\" name=\"shipping_method\" id=\"";
                        echo $this->getAttribute($context["quote"], "code", array());
                        echo "\" value=\"";
                        echo $this->getAttribute($context["quote"], "code", array());
                        echo "\">
                  ";
                    }
                    // line 13
                    echo "</td>
                  <label for=\"";
                    // line 14
                    echo $this->getAttribute($context["quote"], "code", array());
                    echo "\"><span>";
                    echo $this->getAttribute($context["quote"], "title", array());
                    echo " ";
                    echo $this->getAttribute($context["quote"], "text", array());
                    echo "</span></label>
              </div>
          ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['quote'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 17
                echo "      ";
            } else {
                // line 18
                echo "          ";
                echo $this->getAttribute($context["shipping_method"], "error", array());
                echo "
      ";
            }
            // line 20
            echo "  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['shipping_method'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "<div class=\"contact__hidden\">
    <input type=\"text\" name=\"delivery_date\" value=\"\" class=\"hide\"/>
    <select name=\"delivery_time\" class=\"hide\">
        <option value=\"\"></option>
    </select>
</div>









<script type=\"text/javascript\"><!--
    \$('[name=shipping_method]').change(function(){
        var than = \$(this).val();
        if(than === 'flat.flat'){
            \$('#entered_address').show();
        }else{
            \$('#entered_address').hide();
        }
    });

    \$('#shipping-method input[name=\\'shipping_method\\'], #shipping-method select[name=\\'shipping_method\\']').on('change', function () {
        ";
        // line 47
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 48
            echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/set',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function (html) {
                ";
            // line 61
            if ((isset($context["cart"]) ? $context["cart"] : null)) {
                // line 62
                echo "                loadCart();
                ";
            }
            // line 64
            echo "
                ";
            // line 65
            if ((isset($context["shipping_reload"]) ? $context["shipping_reload"] : null)) {
                // line 66
                echo "                reloadPaymentMethod();
                ";
            }
            // line 68
            echo "            },
            ";
            // line 69
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 70
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 74
            echo "        });
        ";
        } else {
            // line 76
            echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set';
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + \$('#shipping-address select[name=\\'address_id\\']').val();
            var post_data = \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: url,
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            success: function (html) {
                ";
            // line 91
            if ((isset($context["cart"]) ? $context["cart"] : null)) {
                // line 92
                echo "                loadCart();
                ";
            }
            // line 94
            echo "
                ";
            // line 95
            if ((isset($context["shipping_reload"]) ? $context["shipping_reload"] : null)) {
                // line 96
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }
                ";
            }
            // line 102
            echo "            },
            ";
            // line 103
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 104
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 108
            echo "        });
        ";
        }
        // line 110
        echo "    });

    \$(document).ready(function () {
        \$('#shipping-method input[name=\\'shipping_method\\']:checked, #shipping-method select[name=\\'shipping_method\\']').trigger('change');
    });

    ";
        // line 116
        if (((isset($context["delivery"]) ? $context["delivery"] : null) && ((isset($context["delivery_delivery_time"]) ? $context["delivery_delivery_time"] : null) == "1"))) {
            // line 117
            echo "    \$(document).ready(function () {
        \$('input[name=\\'delivery_date\\']').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            minDate: '";
            // line 120
            echo (isset($context["delivery_min"]) ? $context["delivery_min"] : null);
            echo "',
            maxDate: '";
            // line 121
            echo (isset($context["delivery_max"]) ? $context["delivery_max"] : null);
            echo "',
            disabledDates: [";
            // line 122
            echo (isset($context["delivery_unavailable"]) ? $context["delivery_unavailable"] : null);
            echo "],
            enabledHours: [";
            // line 123
            echo (isset($context["hours"]) ? $context["hours"] : null);
            echo "],
            ignoreReadonly: true,
            ";
            // line 125
            if (((isset($context["delivery_days_of_week"]) ? $context["delivery_days_of_week"] : null) != "")) {
                // line 126
                echo "            daysOfWeekDisabled: [";
                echo (isset($context["delivery_days_of_week"]) ? $context["delivery_days_of_week"] : null);
                echo "]
            ";
            }
            // line 128
            echo "        });
    });
    ";
        } elseif ((        // line 130
(isset($context["delivery"]) ? $context["delivery"] : null) && (((isset($context["delivery_delivery_time"]) ? $context["delivery_delivery_time"] : null) == "3") || ((isset($context["delivery_delivery_time"]) ? $context["delivery_delivery_time"] : null) == "0")))) {
            // line 131
            echo "    \$('input[name=\\'delivery_date\\']').datetimepicker({
        format: 'YYYY-MM-DD',
        minDate: '";
            // line 133
            echo (isset($context["delivery_min"]) ? $context["delivery_min"] : null);
            echo "',
        maxDate: '";
            // line 134
            echo (isset($context["delivery_max"]) ? $context["delivery_max"] : null);
            echo "',
        disabledDates: [";
            // line 135
            echo (isset($context["delivery_unavailable"]) ? $context["delivery_unavailable"] : null);
            echo "],
        ignoreReadonly: true,
        ";
            // line 137
            if (((isset($context["delivery_days_of_week"]) ? $context["delivery_days_of_week"] : null) != "")) {
                // line 138
                echo "        daysOfWeekDisabled: [";
                echo (isset($context["delivery_days_of_week"]) ? $context["delivery_days_of_week"] : null);
                echo "]
        ";
            }
            // line 140
            echo "    });
    ";
        }
        // line 142
        echo "    //--></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/shipping_method.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  295 => 142,  291 => 140,  285 => 138,  283 => 137,  278 => 135,  274 => 134,  270 => 133,  266 => 131,  264 => 130,  260 => 128,  254 => 126,  252 => 125,  247 => 123,  243 => 122,  239 => 121,  235 => 120,  230 => 117,  228 => 116,  220 => 110,  216 => 108,  210 => 104,  208 => 103,  205 => 102,  197 => 96,  195 => 95,  192 => 94,  188 => 92,  186 => 91,  169 => 76,  165 => 74,  159 => 70,  157 => 69,  154 => 68,  150 => 66,  148 => 65,  145 => 64,  141 => 62,  139 => 61,  124 => 48,  122 => 47,  94 => 21,  88 => 20,  82 => 18,  79 => 17,  66 => 14,  63 => 13,  55 => 12,  47 => 10,  45 => 9,  42 => 8,  37 => 7,  34 => 6,  30 => 5,  27 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if error_warning %}*/
/*     <div class="alert alert-danger">{{ error_warning }}</div>*/
/* {% endif %}*/
/* */
/*   {% for key,shipping_method in shipping_methods %}*/
/*       {% if not shipping_method.error %}*/
/*           {% for quote in shipping_method.quote %}*/
/*               <div class="radio">*/
/*                   {% if quote.code == code %}*/
/*                   <input type="radio" name="shipping_method" id="{{ quote.code }}" value="{{ quote.code }}" checked="checked">*/
/*                   {% else %}*/
/*                   <input type="radio" name="shipping_method" id="{{ quote.code }}" value="{{ quote.code }}">*/
/*                   {% endif %}</td>*/
/*                   <label for="{{ quote.code }}"><span>{{ quote.title }} {{ quote.text }}</span></label>*/
/*               </div>*/
/*           {% endfor %}*/
/*       {% else %}*/
/*           {{ shipping_method.error }}*/
/*       {% endif %}*/
/*   {% endfor %}*/
/* <div class="contact__hidden">*/
/*     <input type="text" name="delivery_date" value="" class="hide"/>*/
/*     <select name="delivery_time" class="hide">*/
/*         <option value=""></option>*/
/*     </select>*/
/* </div>*/
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* */
/* <script type="text/javascript"><!--*/
/*     $('[name=shipping_method]').change(function(){*/
/*         var than = $(this).val();*/
/*         if(than === 'flat.flat'){*/
/*             $('#entered_address').show();*/
/*         }else{*/
/*             $('#entered_address').hide();*/
/*         }*/
/*     });*/
/* */
/*     $('#shipping-method input[name=\'shipping_method\'], #shipping-method select[name=\'shipping_method\']').on('change', function () {*/
/*         {% if not logged %}*/
/*         if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*             var post_data = $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         } else {*/
/*             var post_data = $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method/set',*/
/*             type: 'post',*/
/*             data: post_data,*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             success: function (html) {*/
/*                 {% if cart %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/* */
/*                 {% if shipping_reload %}*/
/*                 reloadPaymentMethod();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*         {% else %}*/
/*         if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*             var url = 'index.php?route=extension/quickcheckout/shipping_method/set';*/
/*             var post_data = $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         } else {*/
/*             var url = 'index.php?route=extension/quickcheckout/shipping_method/set&address_id=' + $('#shipping-address select[name=\'address_id\']').val();*/
/*             var post_data = $('#shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: url,*/
/*             type: 'post',*/
/*             data: post_data,*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             success: function (html) {*/
/*                 {% if cart %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/* */
/*                 {% if shipping_reload %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*         {% endif %}*/
/*     });*/
/* */
/*     $(document).ready(function () {*/
/*         $('#shipping-method input[name=\'shipping_method\']:checked, #shipping-method select[name=\'shipping_method\']').trigger('change');*/
/*     });*/
/* */
/*     {% if delivery and delivery_delivery_time == '1' %}*/
/*     $(document).ready(function () {*/
/*         $('input[name=\'delivery_date\']').datetimepicker({*/
/*             format: 'YYYY-MM-DD HH:mm',*/
/*             minDate: '{{ delivery_min }}',*/
/*             maxDate: '{{ delivery_max }}',*/
/*             disabledDates: [{{ delivery_unavailable }}],*/
/*             enabledHours: [{{ hours }}],*/
/*             ignoreReadonly: true,*/
/*             {% if delivery_days_of_week != '' %}*/
/*             daysOfWeekDisabled: [{{ delivery_days_of_week }}]*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% elseif delivery and (delivery_delivery_time == '3' or delivery_delivery_time == '0') %}*/
/*     $('input[name=\'delivery_date\']').datetimepicker({*/
/*         format: 'YYYY-MM-DD',*/
/*         minDate: '{{ delivery_min }}',*/
/*         maxDate: '{{ delivery_max }}',*/
/*         disabledDates: [{{ delivery_unavailable }}],*/
/*         ignoreReadonly: true,*/
/*         {% if delivery_days_of_week != '' %}*/
/*         daysOfWeekDisabled: [{{ delivery_days_of_week }}]*/
/*         {% endif %}*/
/*     });*/
/*     {% endif %}*/
/*     //--></script>*/
/* */
