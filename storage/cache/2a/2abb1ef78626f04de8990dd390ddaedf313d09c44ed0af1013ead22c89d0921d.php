<?php

/* default/template/information/information.twig */
class __TwigTemplate_677f93b889f7a560a3a04d3492fc3fc9cc8c3d8a89d990478c967694d22823ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"information\">
  <div class=\"information__wrap\">
    <h1 class=\"information__title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
    <p class=\"information__desc\">";
        // line 13
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "</p>
  </div>
  <div class=\"information__breadcrumb-block\">
    <ul class=\"breadcrumb\">
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 18
            echo "          <li class=\"breadcrumb__item\">
            <a href=\"";
            // line 19
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\" class=\"breadcrumb__link\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a>
          </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    </ul>
  </div>
</main>
";
        // line 25
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "  <link href=\"catalog/view/theme/default/stylesheet/information.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "  <script src=\"catalog/view/theme/default/js/information.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/information/information.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 7,  81 => 6,  76 => 4,  73 => 3,  69 => 25,  64 => 22,  53 => 19,  50 => 18,  46 => 17,  39 => 13,  35 => 12,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*   <link href="catalog/view/theme/default/stylesheet/information.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*   <script src="catalog/view/theme/default/js/information.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* <main class="information">*/
/*   <div class="information__wrap">*/
/*     <h1 class="information__title">{{ heading_title }}</h1>*/
/*     <p class="information__desc">{{ description }}</p>*/
/*   </div>*/
/*   <div class="information__breadcrumb-block">*/
/*     <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*           <li class="breadcrumb__item">*/
/*             <a href="{{ breadcrumb.href }}" class="breadcrumb__link">{{ breadcrumb.text }}</a>*/
/*           </li>*/
/*         {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/* </main>*/
/* {{ footer }}*/
