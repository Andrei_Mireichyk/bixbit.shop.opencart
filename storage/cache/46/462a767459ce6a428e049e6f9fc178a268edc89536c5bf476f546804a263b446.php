<?php

/* default/template/extension/quickcheckout/checkout.twig */
class __TwigTemplate_77026c15757566990e3d943cd1e066d915a137fbfeefb5cd5df1b2ad4d373fa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "


<main id=\"quickcheckoutconfirm\" class=\"order\">
    <div class=\"order__wrap\">
        <section class=\"order__contact-block contact__block--city contact\">

            <div id=\"warning-messages\">
                ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "                    <div class=\"order__alert\">
                        ";
            // line 19
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
                    </div>
                ";
        }
        // line 22
        echo "            </div>

            <div id=\"success-messages\"></div>

            ";
        // line 26
        echo (isset($context["guest"]) ? $context["guest"] : null);
        echo "

            <div class=\"contact__block contact__block-address\">
                <div class=\"contact__title\">";
        // line 29
        echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
        echo "</div>
                <div id=\"shipping-method\"></div>
            </div>


            <div id='entered_address' class=\"contact__block contact__block--address\">
                <div class=\"contact__title\">АДРЕС ДОСТАВКИ</div>
                <div class=\"input__group\">
                    <div class=\"input\">
                    <input type=\"text\" required=\"required\" id=\"str\">
                    <label for=\"str\">Населенный пункт</label>
                    <small>Текст ошибки</small>
                    </div>
                </div>
                <div class=\"input__group\">
                    <div class=\"input\">
                        <input type=\"text\" required=\"required\" id=\"str\">
                        <label for=\"str\">Улица</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"house\">
                        <label for=\"house\">Дом</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"apartments\">
                        <label for=\"apartments\">Кв</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"index\">
                        <label for=\"index\">Индекс</label>
                        <small>Текст ошибки</small>
                    </div>
                </div>
            </div>



            <div class=\"contact__block\">
                <div class=\"contact__title\">";
        // line 70
        echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
        echo "</div>
                <div id=\"payment-method\"></div>
            </div>




            <div id=\"terms\" class=\"contact__action\">
                ";
        // line 78
        echo (isset($context["terms"]) ? $context["terms"] : null);
        echo "
            </div>
        </section>
        <section class=\"order__product-block\">
            <div id=\"cart1\" class=\"product\"></div>
        </section>
    </div>
</main>

<script type=\"text/javascript\"><!--

    \$('#entered_address input').change(function(){
        var str = '';
        \$('#entered_address input').each(function (k,v) {
            str += \" \"+\$(v).val();
        });
        \$('[name=address_1]').val(str);
    });


    ";
        // line 98
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 99
            echo "    \$(window).load(function () {
        \$.blockUI({
            message: '<h1 style=\"color:#ffffff;\">";
            // line 101
            echo (isset($context["text_please_wait"]) ? $context["text_please_wait"] : null);
            echo "</h1>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-khtml-border-radius': '10px',
                'border-radius': '10px',
                opacity: .8,
                color: '#ffffff'
            }
        });

        setTimeout(function () {
            \$.unblockUI();
        }, 2000);
    });
    ";
        }
        // line 120
        echo "
    ";
        // line 121
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 122
            echo "    ";
            if ((isset($context["save_data"]) ? $context["save_data"] : null)) {
                // line 123
                echo "    // Save form data
    \$(document).on('change', '#payment-address input[type=\\'text\\'], #payment-address select', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/checkout/save&type=payment',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                // No action needed
            },
            ";
                // line 134
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 135
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 139
                echo "        });
    });

    \$(document).on('change', '#shipping-address input[type=\\'text\\'], #shipping-address select', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/checkout/save&type=shipping',
            type: 'post',
            data: \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'password\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                // No action needed
            },
            ";
                // line 152
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 153
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 157
                echo "        });
    });
    ";
            }
            // line 160
            echo "
    ";
            // line 161
            if ((isset($context["login_module"]) ? $context["login_module"] : null)) {
                // line 162
                echo "    // Login Form Clicked
    \$(document).on('click', '#button-login', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/login/validate',
            type: 'post',
            data: \$('#checkout #login :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (json) {
                \$('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    \$('.alert-danger').fadeIn('slow');
                }
            },
            ";
                // line 189
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 190
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 194
                echo "        });
    });
    ";
            }
            // line 197
            echo "
    // Validate Register
    function validateRegister() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/register/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 224
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 225
                echo "                    if (json['error']['password']) {
                        \$('#payment-address input[name=\\'password\\']').after('<div class=\"text-danger\">' + json['error']['password'] + '</div>');
                    }

                    if (json['error']['confirm']) {
                        \$('#payment-address input[name=\\'confirm\\']').after('<div class=\"text-danger\">' + json['error']['confirm'] + '</div>');
                    }
                    ";
            }
            // line 233
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 234
                echo "                    if (json['error']['password']) {
                        \$('#payment-address input[name=\\'password\\']').css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }

                    if (json['error']['confirm']) {
                        \$('#payment-address input[name=\\'confirm\\']').css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 242
            echo "                } else {
                    ";
            // line 243
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 244
                echo "                    var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').val();

                    if (shipping_address) {
                        validateShippingMethod();
                    } else {
                        validateGuestShippingAddress();
                    }
                    ";
            } else {
                // line 252
                echo "                    validatePaymentMethod();
                    ";
            }
            // line 254
            echo "                }
            },
            ";
            // line 256
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 257
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 261
            echo "        });
    }

    // Validate Guest Payment Address
    function validateGuestAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/guest/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));
                        var wrap = element.closest('.input');
                        \$('small', wrap).remove();
                        wrap.addClass('has-error');
                        wrap.append('<small>' + json['error'][i] + '</small>');
                    }
                } else {
                    var create_account = \$('#payment-address input[name=\\'create_account\\']:checked').val();

                    ";
            // line 290
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 291
                echo "                    var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').val();

                    if (create_account) {
                        validateRegister();
                    } else {
                        if (shipping_address) {
                            validateShippingMethod();
                        } else {
                            validateGuestShippingAddress();
                        }
                    }
                    ";
            } else {
                // line 303
                echo "                    if (create_account) {
                        validateRegister();
                    } else {
                        validatePaymentMethod();
                    }
                    ";
            }
            // line 309
            echo "                }
            },
            ";
            // line 311
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 312
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 316
            echo "        });
    }

    // Validate Guest Shipping Address
    function validateGuestShippingAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/guest_shipping/validate',
            type: 'post',
            data: \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address select, #shipping-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 344
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 345
                echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
            }
            // line 355
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 356
                echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 362
            echo "                } else {
                    validateShippingMethod();
                }
            },
            ";
            // line 366
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 367
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 371
            echo "        });
    }

    // Confirm Payment
    \$(document).on('click', '#button-payment-method', function () {

        validateGuestAddress();
    });
    ";
        } else {
            // line 380
            echo "    // Validate Payment Address
    function validatePaymentAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_address/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 405
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 406
                echo "                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
            }
            // line 416
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 417
                echo "                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 423
            echo "                } else {
                    ";
            // line 424
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 425
                echo "                    validateShippingAddress();
                    ";
            } else {
                // line 427
                echo "                    validatePaymentMethod();
                    ";
            }
            // line 429
            echo "                }
            },
            ";
            // line 431
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 432
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 436
            echo "        });
    }

    ";
            // line 439
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 440
                echo "    // Validate Shipping Address
    function validateShippingAddress() {
        var payment_mode = \$('input[name=\"payment_address\"]:checked').val();
        if (payment_mode == 'new') {
            ";
                // line 444
                if (((isset($context["logged"]) ? $context["logged"] : null) &&  !(isset($context["show_shipping_address"]) ? $context["show_shipping_address"] : null))) {
                    // line 445
                    echo "            var addressType = '#payment-address';
            var shipping_mode = 'input[name=\"shipping_address\"]:checked, ';
            ";
                } else {
                    // line 448
                    echo "            var addressType = '#shipping-address';
            var shipping_mode = '';
            ";
                }
                // line 451
                echo "        } else {
            var addressType = '#shipping-address';
            var shipping_mode = '';
        }
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_address/validate',
            type: 'post',
            data: \$(shipping_mode + addressType + ' input[type=\\'text\\'], ' + addressType + ' input[type=\\'password\\'], ' + addressType + ' input[type=\\'checkbox\\']:checked, ' + addressType + ' input[type=\\'radio\\']:checked, ' + addressType + ' select, ' + addressType + ' textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
                // line 479
                if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                    // line 480
                    echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
                }
                // line 490
                echo "                    ";
                if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                    // line 491
                    echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
                }
                // line 497
                echo "                } else {
                    validateShippingMethod();
                }
            },
            ";
                // line 501
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 502
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 506
                echo "        });
    }
    ";
            }
            // line 509
            echo "
    // Confirm payment
    \$(document).on('click', '#button-payment-method', function () {

        validatePaymentAddress();
    });
    ";
        }
        // line 515
        echo "// Close if logged php

    // Payment Method
    function reloadPaymentMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea, #payment-method input[type=\\'text\\'], #payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#payment-method').html(html);

                ";
        // line 531
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 532
            echo "                \$.unblockUI();
                ";
        }
        // line 534
        echo "            },
            ";
        // line 535
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 536
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 540
        echo "        });
    }

    function reloadPaymentMethodById(address_id) {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method&address_id=' + address_id,
            type: 'post',
            data: \$('#payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#payment-method').html(html);

                ";
        // line 556
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 557
            echo "                \$.unblockUI();
                ";
        }
        // line 559
        echo "            },
            ";
        // line 560
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 561
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 565
        echo "        });
    }

    // Validate Payment Method
    function validatePaymentMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method/validate',
            type: 'post',
            data: \$('#payment-method select, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'checkbox\\']:checked, #payment-method textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }
                } else {
                    validateTerms();
                }
            },
            ";
        // line 597
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 598
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 602
        echo "        });
    }

    // Shipping Method
    ";
        // line 606
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 607
            echo "    function reloadShippingMethod(type) {
        if (type == 'payment') {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-address textarea, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#shipping-method').html(html);
            },
            ";
            // line 626
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 627
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 631
            echo "        });
    }

    function reloadShippingMethodById(address_id) {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method&address_id=' + address_id,
            type: 'post',
            data: \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#shipping-method').html(html);
            },
            ";
            // line 647
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 648
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 652
            echo "        });
    }

    // Validate Shipping Method
    function validateShippingMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/validate',
            type: 'post',
            data: \$('#shipping-method select, #shipping-method input[type=\\'radio\\']:checked, #shipping-method textarea, #shipping-method input[type=\\'text\\']'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }
                } else {
                    validatePaymentMethod();
                }
            },
            ";
            // line 684
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 685
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 689
            echo "        });
    }
    ";
        }
        // line 692
        echo "
    // Validate confirm button
    function validateTerms() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/terms/validate',
            type: 'post',
            data: \$('#terms input[type=\\'checkbox\\']:checked'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                     if (json['error']['warning']) {
                         var wrap = \$('#terms .checkbox');
                         wrap.addClass('has-error');
                         wrap.append(\"<small>\"+json['error']['warning']+\"</small>\");
                    }
                } else {
                    loadConfirm();
                }
            },
            ";
        // line 716
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 717
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 721
        echo "        });
    }

    // Load confirm
    function loadConfirm() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/confirm',
            dataType: 'html',
            cache: false,
            beforeSend: function () {
                ";
        // line 731
        if ((isset($context["confirmation_page"]) ? $context["confirmation_page"] : null)) {
            // line 732
            echo "                \$('html, body').animate({scrollTop: 0}, 'slow');

                ";
            // line 734
            if ((isset($context["slide_effect"]) ? $context["slide_effect"] : null)) {
                // line 735
                echo "                \$('#quickcheckoutconfirm').slideUp('slow');
                ";
            } else {
                // line 737
                echo "                \$('#quickcheckoutconfirm').html('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 739
            echo "
                ";
            // line 740
            if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
                // line 741
                echo "                \$.blockUI({
                    message: '<h1 style=\"color:#ffffff;\">";
                // line 742
                echo (isset($context["text_please_wait"]) ? $context["text_please_wait"] : null);
                echo "</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        '-khtml-border-radius': '10px',
                        'border-radius': '10px',
                        opacity: .8,
                        color: '#ffffff'
                    }
                });
                ";
            }
            // line 756
            echo "                ";
        }
        // line 757
        echo "            },
            success: function (html) {
                ";
        // line 759
        if ((isset($context["confirmation_page"]) ? $context["confirmation_page"] : null)) {
            // line 760
            echo "                ";
            if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
                // line 761
                echo "                \$.unblockUI();
                ";
            }
            // line 763
            echo "
                \$('#quickcheckoutconfirm').hide().html(html);

                ";
            // line 766
            if ( !(isset($context["auto_submit"]) ? $context["auto_submit"] : null)) {
                // line 767
                echo "                ";
                if ((isset($context["slide_effect"]) ? $context["slide_effect"] : null)) {
                    // line 768
                    echo "                \$('#quickcheckoutconfirm').slideDown('slow');
                ";
                } else {
                    // line 770
                    echo "                \$('#quickcheckoutconfirm').show();
                ";
                }
                // line 772
                echo "                ";
            } else {
                // line 773
                echo "                \$('#quickcheckoutconfirm').after('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 775
            echo "                ";
        } else {
            // line 776
            echo "                \$('#terms .terms').hide();
                \$('#payment').html(html).slideDown('fast');

                ";
            // line 779
            if ((isset($context["auto_submit"]) ? $context["auto_submit"] : null)) {
                // line 780
                echo "                \$('#payment').hide().after('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 782
            echo "
                \$('html, body').animate({scrollTop: \$('#terms').offset().top}, 'slow');

                disableCheckout();
                ";
        }
        // line 787
        echo "            },
            ";
        // line 788
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 789
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 793
        echo "        });
    }

    // Load cart
    ";
        // line 797
        if ((isset($context["cart_module"]) ? $context["cart_module"] : null)) {
            // line 798
            echo "    function loadCart() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart',
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {
                \$('#cart1').html(html);
            }
        });
    }

    ";
            // line 811
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 812
                echo "    \$(document).ready(function () {
        loadCart();
    });
    ";
            }
            // line 816
            echo "    ";
        }
        // line 817
        echo "
    ";
        // line 818
        if ((((isset($context["voucher_module"]) ? $context["voucher_module"] : null) || (isset($context["coupon_module"]) ? $context["coupon_module"] : null)) || (isset($context["reward_module"]) ? $context["reward_module"] : null))) {
            // line 819
            echo "    // Validate Coupon
    \$(document).on('click', '#button-coupon', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateCoupon',
            type: 'post',
            data: \$('#coupon-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-coupon').prop('disabled', true);
                \$('#button-coupon').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-coupon').prop('disabled', false);
                \$('#coupon-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 850
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 851
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 854
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 855
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 857
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 860
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 861
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 863
                echo "                }
                ";
            } else {
                // line 865
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 871
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 872
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 878
                echo "                ";
            }
            // line 879
            echo "
                ";
            // line 880
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 881
                echo "                loadCart();
                ";
            }
            // line 883
            echo "            },
            ";
            // line 884
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 885
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 889
            echo "        });
    });

    \$(document).on('click', '#button-voucher', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateVoucher',
            type: 'post',
            data: \$('#voucher-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-voucher').prop('disabled', true);
                \$('#button-voucher').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-voucher').prop('disabled', false);
                \$('#voucher-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 922
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 923
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 926
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 927
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 929
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 932
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 933
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 935
                echo "                }
                ";
            } else {
                // line 937
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 943
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 944
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 950
                echo "                ";
            }
            // line 951
            echo "
                ";
            // line 952
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 953
                echo "                loadCart();
                ";
            }
            // line 955
            echo "            },
            ";
            // line 956
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 957
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 961
            echo "        });
    });

    \$(document).on('click', '#button-reward', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateReward',
            type: 'post',
            data: \$('#reward-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-reward').prop('disabled', true);
                \$('#button-reward').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-reward').prop('disabled', false);
                \$('#reward-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 994
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 995
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 998
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 999
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 1001
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 1004
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1005
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 1007
                echo "                }
                ";
            } else {
                // line 1009
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 1015
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1016
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 1022
                echo "                ";
            }
            // line 1023
            echo "
                ";
            // line 1024
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1025
                echo "                loadCart();
                ";
            }
            // line 1027
            echo "            },
            ";
            // line 1028
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1029
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1033
            echo "        });
    });
    ";
        }
        // line 1036
        echo "
    ";
        // line 1037
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 1038
            echo "    \$(document).on('focusout', 'input[name=\\'postcode\\']', function () {
        ";
            // line 1039
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1040
                echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            reloadShippingMethod('payment');
        } else {
            reloadShippingMethod('shipping');
        }
        ";
            } else {
                // line 1046
                echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            reloadShippingMethod('shipping');
        } else {
            reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
        }
        ";
            }
            // line 1052
            echo "    });
    ";
        }
        // line 1054
        echo "
    ";
        // line 1055
        if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
            // line 1056
            echo "    \$(document).on('keydown', 'input', function () {
       var wrap = \$(this).closest('.input');
        wrap.removeClass('has-error');
        \$('small' , wrap).remove();
    });
    \$(document).on('change', 'select', function () {
        \$(this).css('background', '').css('border', '');

        \$(this).siblings('.text-danger').remove();
    });
    ";
        }
        // line 1067
        echo "
    ";
        // line 1068
        if ((isset($context["edit_cart"]) ? $context["edit_cart"] : null)) {
            // line 1069
            echo "    \$(document).on('click', '.button-update', function () {

        var quantity = \$(this).parents('.quantity').find('input.qc-product-qantity');
        if (quantity.length) {
            if (\$(this).data('type') == 'increase') {
                quantity.val(parseInt(quantity.val()) + 1);
            } else if (\$(this).data('type') == 'decrease') {
                quantity.val(parseInt(quantity.val()) - 1);
            }
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart/update',
            type: 'post',
            data: \$('#cart1 :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                //\$('#cart1 .button-update').prop('disabled', true);
            },
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    if (json['error']['stock']) {
                        \$('#button-payment-method').attr(\"disabled\", true);
                    } else if (json['error']['warning']) {
                        \$('#warning-messages').html('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');
                        \$('.alert-danger').fadeIn('slow');
                        \$('#button-payment-method').attr(\"disabled\", true);
                    } else {
                        \$('#warning-messages').html('');
                        \$('#button-payment-method').removeAttr(\"disabled\");
                    }

                    ";
            // line 1104
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1105
                echo "                    if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                        reloadPaymentMethod();

                        ";
                // line 1108
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1109
                    echo "                        reloadShippingMethod('payment');
                        ";
                }
                // line 1111
                echo "                    } else {
                        reloadPaymentMethod();

                        ";
                // line 1114
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1115
                    echo "                        reloadShippingMethod('shipping');
                        ";
                }
                // line 1117
                echo "                    }
                    ";
            } else {
                // line 1119
                echo "                    if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                        reloadPaymentMethod();
                    } else {
                        reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                    }

                    ";
                // line 1125
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1126
                    echo "                    if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                        reloadShippingMethod('shipping');
                    } else {
                        reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                    }
                    ";
                }
                // line 1132
                echo "                    ";
            }
            // line 1133
            echo "
                    ";
            // line 1134
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1135
                echo "                    loadCart();
                    ";
            }
            // line 1137
            echo "                }
            },
            ";
            // line 1139
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1140
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1144
            echo "        });
    });

    \$(document).on('click', '.button-remove', function () {
        var remove_id = \$(this).attr('data-remove');

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart/update&remove=' + remove_id,
            type: 'get',
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#cart1 .button-remove').prop('disabled', true);
            },
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    ";
            // line 1162
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1163
                echo "                    if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                        reloadPaymentMethod();

                        ";
                // line 1166
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1167
                    echo "                        reloadShippingMethod('payment');
                        ";
                }
                // line 1169
                echo "                    } else {
                        reloadPaymentMethod();

                        ";
                // line 1172
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1173
                    echo "                        reloadShippingMethod('shipping');
                        ";
                }
                // line 1175
                echo "                    }
                    ";
            } else {
                // line 1177
                echo "                    if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                        reloadPaymentMethod();
                    } else {
                        reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                    }

                    ";
                // line 1183
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1184
                    echo "                    if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                        reloadShippingMethod('shipping');
                    } else {
                        reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                    }
                    ";
                }
                // line 1190
                echo "                    ";
            }
            // line 1191
            echo "
                    ";
            // line 1192
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1193
                echo "                    loadCart();
                    ";
            }
            // line 1195
            echo "                }
            },
            ";
            // line 1197
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1198
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1202
            echo "        });

        return false;
    });
    ";
        }
        // line 1207
        echo "

    //--></script>
";
        // line 1210
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 1211
            echo "    <script type=\"text/javascript\"><!--
        \$('#button-payment-method').attr(\"disabled\", true);
        //--></script>
";
        }
        // line 1215
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "    <link href=\"catalog/view/theme/default/stylesheet/order.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"catalog/view/theme/default/js/order.bundle.js\" type=\"text/javascript\"></script>
    <script src=\"catalog/view/javascript/jquery/quickcheckout/quickcheckout.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1712 => 6,  1709 => 5,  1704 => 3,  1701 => 2,  1695 => 1215,  1689 => 1211,  1687 => 1210,  1682 => 1207,  1675 => 1202,  1669 => 1198,  1667 => 1197,  1663 => 1195,  1659 => 1193,  1657 => 1192,  1654 => 1191,  1651 => 1190,  1643 => 1184,  1641 => 1183,  1633 => 1177,  1629 => 1175,  1625 => 1173,  1623 => 1172,  1618 => 1169,  1614 => 1167,  1612 => 1166,  1607 => 1163,  1605 => 1162,  1585 => 1144,  1579 => 1140,  1577 => 1139,  1573 => 1137,  1569 => 1135,  1567 => 1134,  1564 => 1133,  1561 => 1132,  1553 => 1126,  1551 => 1125,  1543 => 1119,  1539 => 1117,  1535 => 1115,  1533 => 1114,  1528 => 1111,  1524 => 1109,  1522 => 1108,  1517 => 1105,  1515 => 1104,  1478 => 1069,  1476 => 1068,  1473 => 1067,  1460 => 1056,  1458 => 1055,  1455 => 1054,  1451 => 1052,  1443 => 1046,  1435 => 1040,  1433 => 1039,  1430 => 1038,  1428 => 1037,  1425 => 1036,  1420 => 1033,  1414 => 1029,  1412 => 1028,  1409 => 1027,  1405 => 1025,  1403 => 1024,  1400 => 1023,  1397 => 1022,  1389 => 1016,  1387 => 1015,  1379 => 1009,  1375 => 1007,  1371 => 1005,  1369 => 1004,  1364 => 1001,  1360 => 999,  1358 => 998,  1353 => 995,  1351 => 994,  1316 => 961,  1310 => 957,  1308 => 956,  1305 => 955,  1301 => 953,  1299 => 952,  1296 => 951,  1293 => 950,  1285 => 944,  1283 => 943,  1275 => 937,  1271 => 935,  1267 => 933,  1265 => 932,  1260 => 929,  1256 => 927,  1254 => 926,  1249 => 923,  1247 => 922,  1212 => 889,  1206 => 885,  1204 => 884,  1201 => 883,  1197 => 881,  1195 => 880,  1192 => 879,  1189 => 878,  1181 => 872,  1179 => 871,  1171 => 865,  1167 => 863,  1163 => 861,  1161 => 860,  1156 => 857,  1152 => 855,  1150 => 854,  1145 => 851,  1143 => 850,  1110 => 819,  1108 => 818,  1105 => 817,  1102 => 816,  1096 => 812,  1094 => 811,  1079 => 798,  1077 => 797,  1071 => 793,  1065 => 789,  1063 => 788,  1060 => 787,  1053 => 782,  1049 => 780,  1047 => 779,  1042 => 776,  1039 => 775,  1035 => 773,  1032 => 772,  1028 => 770,  1024 => 768,  1021 => 767,  1019 => 766,  1014 => 763,  1010 => 761,  1007 => 760,  1005 => 759,  1001 => 757,  998 => 756,  981 => 742,  978 => 741,  976 => 740,  973 => 739,  969 => 737,  965 => 735,  963 => 734,  959 => 732,  957 => 731,  945 => 721,  939 => 717,  937 => 716,  911 => 692,  906 => 689,  900 => 685,  898 => 684,  864 => 652,  858 => 648,  856 => 647,  838 => 631,  832 => 627,  830 => 626,  809 => 607,  807 => 606,  801 => 602,  795 => 598,  793 => 597,  759 => 565,  753 => 561,  751 => 560,  748 => 559,  744 => 557,  742 => 556,  724 => 540,  718 => 536,  716 => 535,  713 => 534,  709 => 532,  707 => 531,  689 => 515,  680 => 509,  675 => 506,  669 => 502,  667 => 501,  661 => 497,  653 => 491,  650 => 490,  638 => 480,  636 => 479,  606 => 451,  601 => 448,  596 => 445,  594 => 444,  588 => 440,  586 => 439,  581 => 436,  575 => 432,  573 => 431,  569 => 429,  565 => 427,  561 => 425,  559 => 424,  556 => 423,  548 => 417,  545 => 416,  533 => 406,  531 => 405,  504 => 380,  493 => 371,  487 => 367,  485 => 366,  479 => 362,  471 => 356,  468 => 355,  456 => 345,  454 => 344,  424 => 316,  418 => 312,  416 => 311,  412 => 309,  404 => 303,  390 => 291,  388 => 290,  357 => 261,  351 => 257,  349 => 256,  345 => 254,  341 => 252,  331 => 244,  329 => 243,  326 => 242,  316 => 234,  313 => 233,  303 => 225,  301 => 224,  272 => 197,  267 => 194,  261 => 190,  259 => 189,  230 => 162,  228 => 161,  225 => 160,  220 => 157,  214 => 153,  212 => 152,  197 => 139,  191 => 135,  189 => 134,  176 => 123,  173 => 122,  171 => 121,  168 => 120,  146 => 101,  142 => 99,  140 => 98,  117 => 78,  106 => 70,  62 => 29,  56 => 26,  50 => 22,  44 => 19,  41 => 18,  39 => 17,  29 => 9,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/order.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/order.bundle.js" type="text/javascript"></script>*/
/*     <script src="catalog/view/javascript/jquery/quickcheckout/quickcheckout.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* <main id="quickcheckoutconfirm" class="order">*/
/*     <div class="order__wrap">*/
/*         <section class="order__contact-block contact__block--city contact">*/
/* */
/*             <div id="warning-messages">*/
/*                 {% if error_warning %}*/
/*                     <div class="order__alert">*/
/*                         {{ error_warning }}*/
/*                     </div>*/
/*                 {% endif %}*/
/*             </div>*/
/* */
/*             <div id="success-messages"></div>*/
/* */
/*             {{ guest }}*/
/* */
/*             <div class="contact__block contact__block-address">*/
/*                 <div class="contact__title">{{ text_checkout_shipping_method }}</div>*/
/*                 <div id="shipping-method"></div>*/
/*             </div>*/
/* */
/* */
/*             <div id='entered_address' class="contact__block contact__block--address">*/
/*                 <div class="contact__title">АДРЕС ДОСТАВКИ</div>*/
/*                 <div class="input__group">*/
/*                     <div class="input">*/
/*                     <input type="text" required="required" id="str">*/
/*                     <label for="str">Населенный пункт</label>*/
/*                     <small>Текст ошибки</small>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="input__group">*/
/*                     <div class="input">*/
/*                         <input type="text" required="required" id="str">*/
/*                         <label for="str">Улица</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="house">*/
/*                         <label for="house">Дом</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="apartments">*/
/*                         <label for="apartments">Кв</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="index">*/
/*                         <label for="index">Индекс</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/* */
/* */
/*             <div class="contact__block">*/
/*                 <div class="contact__title">{{ text_checkout_payment_method }}</div>*/
/*                 <div id="payment-method"></div>*/
/*             </div>*/
/* */
/* */
/* */
/* */
/*             <div id="terms" class="contact__action">*/
/*                 {{ terms }}*/
/*             </div>*/
/*         </section>*/
/*         <section class="order__product-block">*/
/*             <div id="cart1" class="product"></div>*/
/*         </section>*/
/*     </div>*/
/* </main>*/
/* */
/* <script type="text/javascript"><!--*/
/* */
/*     $('#entered_address input').change(function(){*/
/*         var str = '';*/
/*         $('#entered_address input').each(function (k,v) {*/
/*             str += " "+$(v).val();*/
/*         });*/
/*         $('[name=address_1]').val(str);*/
/*     });*/
/* */
/* */
/*     {% if load_screen %}*/
/*     $(window).load(function () {*/
/*         $.blockUI({*/
/*             message: '<h1 style="color:#ffffff;">{{ text_please_wait }}</h1>',*/
/*             css: {*/
/*                 border: 'none',*/
/*                 padding: '15px',*/
/*                 backgroundColor: '#000000',*/
/*                 '-webkit-border-radius': '10px',*/
/*                 '-moz-border-radius': '10px',*/
/*                 '-khtml-border-radius': '10px',*/
/*                 'border-radius': '10px',*/
/*                 opacity: .8,*/
/*                 color: '#ffffff'*/
/*             }*/
/*         });*/
/* */
/*         setTimeout(function () {*/
/*             $.unblockUI();*/
/*         }, 2000);*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if not logged %}*/
/*     {% if save_data %}*/
/*     // Save form data*/
/*     $(document).on('change', '#payment-address input[type=\'text\'], #payment-address select', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/checkout/save&type=payment',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 // No action needed*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('change', '#shipping-address input[type=\'text\'], #shipping-address select', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/checkout/save&type=shipping',*/
/*             type: 'post',*/
/*             data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 // No action needed*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if login_module %}*/
/*     // Login Form Clicked*/
/*     $(document).on('click', '#button-login', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/login/validate',*/
/*             type: 'post',*/
/*             data: $('#checkout #login :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/* */
/*             },*/
/*             complete: function () {*/
/* */
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     // Validate Register*/
/*     function validateRegister() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/register/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     if (json['error']['password']) {*/
/*                         $('#payment-address input[name=\'password\']').after('<div class="text-danger">' + json['error']['password'] + '</div>');*/
/*                     }*/
/* */
/*                     if (json['error']['confirm']) {*/
/*                         $('#payment-address input[name=\'confirm\']').after('<div class="text-danger">' + json['error']['confirm'] + '</div>');*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     if (json['error']['password']) {*/
/*                         $('#payment-address input[name=\'password\']').css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/* */
/*                     if (json['error']['confirm']) {*/
/*                         $('#payment-address input[name=\'confirm\']').css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     {% if shipping_required %}*/
/*                     var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').val();*/
/* */
/*                     if (shipping_address) {*/
/*                         validateShippingMethod();*/
/*                     } else {*/
/*                         validateGuestShippingAddress();*/
/*                     }*/
/*                     {% else %}*/
/*                     validatePaymentMethod();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Guest Payment Address*/
/*     function validateGuestAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/guest/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/*                         var wrap = element.closest('.input');*/
/*                         $('small', wrap).remove();*/
/*                         wrap.addClass('has-error');*/
/*                         wrap.append('<small>' + json['error'][i] + '</small>');*/
/*                     }*/
/*                 } else {*/
/*                     var create_account = $('#payment-address input[name=\'create_account\']:checked').val();*/
/* */
/*                     {% if shipping_required %}*/
/*                     var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').val();*/
/* */
/*                     if (create_account) {*/
/*                         validateRegister();*/
/*                     } else {*/
/*                         if (shipping_address) {*/
/*                             validateShippingMethod();*/
/*                         } else {*/
/*                             validateGuestShippingAddress();*/
/*                         }*/
/*                     }*/
/*                     {% else %}*/
/*                     if (create_account) {*/
/*                         validateRegister();*/
/*                     } else {*/
/*                         validatePaymentMethod();*/
/*                     }*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Guest Shipping Address*/
/*     function validateGuestShippingAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/guest_shipping/validate',*/
/*             type: 'post',*/
/*             data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address select, #shipping-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     validateShippingMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Confirm Payment*/
/*     $(document).on('click', '#button-payment-method', function () {*/
/* */
/*         validateGuestAddress();*/
/*     });*/
/*     {% else %}*/
/*     // Validate Payment Address*/
/*     function validatePaymentAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_address/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     {% if shipping_required %}*/
/*                     validateShippingAddress();*/
/*                     {% else %}*/
/*                     validatePaymentMethod();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     {% if shipping_required %}*/
/*     // Validate Shipping Address*/
/*     function validateShippingAddress() {*/
/*         var payment_mode = $('input[name="payment_address"]:checked').val();*/
/*         if (payment_mode == 'new') {*/
/*             {% if logged and not show_shipping_address %}*/
/*             var addressType = '#payment-address';*/
/*             var shipping_mode = 'input[name="shipping_address"]:checked, ';*/
/*             {% else %}*/
/*             var addressType = '#shipping-address';*/
/*             var shipping_mode = '';*/
/*             {% endif %}*/
/*         } else {*/
/*             var addressType = '#shipping-address';*/
/*             var shipping_mode = '';*/
/*         }*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_address/validate',*/
/*             type: 'post',*/
/*             data: $(shipping_mode + addressType + ' input[type=\'text\'], ' + addressType + ' input[type=\'password\'], ' + addressType + ' input[type=\'checkbox\']:checked, ' + addressType + ' input[type=\'radio\']:checked, ' + addressType + ' select, ' + addressType + ' textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     validateShippingMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/*     {% endif %}*/
/* */
/*     // Confirm payment*/
/*     $(document).on('click', '#button-payment-method', function () {*/
/* */
/*         validatePaymentAddress();*/
/*     });*/
/*     {% endif %}// Close if logged php*/
/* */
/*     // Payment Method*/
/*     function reloadPaymentMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea, #payment-method input[type=\'text\'], #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#payment-method').html(html);*/
/* */
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     function reloadPaymentMethodById(address_id) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method&address_id=' + address_id,*/
/*             type: 'post',*/
/*             data: $('#payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#payment-method').html(html);*/
/* */
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Payment Method*/
/*     function validatePaymentMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-method select, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/*                 } else {*/
/*                     validateTerms();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Shipping Method*/
/*     {% if shipping_required %}*/
/*     function reloadShippingMethod(type) {*/
/*         if (type == 'payment') {*/
/*             var post_data = $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         } else {*/
/*             var post_data = $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-address textarea, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method',*/
/*             type: 'post',*/
/*             data: post_data,*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#shipping-method').html(html);*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     function reloadShippingMethodById(address_id) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method&address_id=' + address_id,*/
/*             type: 'post',*/
/*             data: $('#shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#shipping-method').html(html);*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Shipping Method*/
/*     function validateShippingMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method/validate',*/
/*             type: 'post',*/
/*             data: $('#shipping-method select, #shipping-method input[type=\'radio\']:checked, #shipping-method textarea, #shipping-method input[type=\'text\']'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/*                 } else {*/
/*                     validatePaymentMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/*     {% endif %}*/
/* */
/*     // Validate confirm button*/
/*     function validateTerms() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/terms/validate',*/
/*             type: 'post',*/
/*             data: $('#terms input[type=\'checkbox\']:checked'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 }*/
/* */
/*                 if (json['error']) {*/
/*                      if (json['error']['warning']) {*/
/*                          var wrap = $('#terms .checkbox');*/
/*                          wrap.addClass('has-error');*/
/*                          wrap.append("<small>"+json['error']['warning']+"</small>");*/
/*                     }*/
/*                 } else {*/
/*                     loadConfirm();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Load confirm*/
/*     function loadConfirm() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/confirm',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 {% if confirmation_page %}*/
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 {% if slide_effect %}*/
/*                 $('#quickcheckoutconfirm').slideUp('slow');*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/* */
/*                 {% if load_screen %}*/
/*                 $.blockUI({*/
/*                     message: '<h1 style="color:#ffffff;">{{ text_please_wait }}</h1>',*/
/*                     css: {*/
/*                         border: 'none',*/
/*                         padding: '15px',*/
/*                         backgroundColor: '#000000',*/
/*                         '-webkit-border-radius': '10px',*/
/*                         '-moz-border-radius': '10px',*/
/*                         '-khtml-border-radius': '10px',*/
/*                         'border-radius': '10px',*/
/*                         opacity: .8,*/
/*                         color: '#ffffff'*/
/*                     }*/
/*                 });*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/*             },*/
/*             success: function (html) {*/
/*                 {% if confirmation_page %}*/
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/* */
/*                 $('#quickcheckoutconfirm').hide().html(html);*/
/* */
/*                 {% if not auto_submit %}*/
/*                 {% if slide_effect %}*/
/*                 $('#quickcheckoutconfirm').slideDown('slow');*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').show();*/
/*                 {% endif %}*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').after('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/*                 {% else %}*/
/*                 $('#terms .terms').hide();*/
/*                 $('#payment').html(html).slideDown('fast');*/
/* */
/*                 {% if auto_submit %}*/
/*                 $('#payment').hide().after('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/* */
/*                 $('html, body').animate({scrollTop: $('#terms').offset().top}, 'slow');*/
/* */
/*                 disableCheckout();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Load cart*/
/*     {% if cart_module %}*/
/*     function loadCart() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/*                 $('#cart1').html(html);*/
/*             }*/
/*         });*/
/*     }*/
/* */
/*     {% if not shipping_required %}*/
/*     $(document).ready(function () {*/
/*         loadCart();*/
/*     });*/
/*     {% endif %}*/
/*     {% endif %}*/
/* */
/*     {% if voucher_module or coupon_module or reward_module %}*/
/*     // Validate Coupon*/
/*     $(document).on('click', '#button-coupon', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateCoupon',*/
/*             type: 'post',*/
/*             data: $('#coupon-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-coupon').prop('disabled', true);*/
/*                 $('#button-coupon').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-coupon').prop('disabled', false);*/
/*                 $('#coupon-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '#button-voucher', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateVoucher',*/
/*             type: 'post',*/
/*             data: $('#voucher-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-voucher').prop('disabled', true);*/
/*                 $('#button-voucher').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-voucher').prop('disabled', false);*/
/*                 $('#voucher-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '#button-reward', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateReward',*/
/*             type: 'post',*/
/*             data: $('#reward-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-reward').prop('disabled', true);*/
/*                 $('#button-reward').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-reward').prop('disabled', false);*/
/*                 $('#reward-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if shipping_required %}*/
/*     $(document).on('focusout', 'input[name=\'postcode\']', function () {*/
/*         {% if not logged %}*/
/*         if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*             reloadShippingMethod('payment');*/
/*         } else {*/
/*             reloadShippingMethod('shipping');*/
/*         }*/
/*         {% else %}*/
/*         if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*             reloadShippingMethod('shipping');*/
/*         } else {*/
/*             reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*         }*/
/*         {% endif %}*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if highlight_error %}*/
/*     $(document).on('keydown', 'input', function () {*/
/*        var wrap = $(this).closest('.input');*/
/*         wrap.removeClass('has-error');*/
/*         $('small' , wrap).remove();*/
/*     });*/
/*     $(document).on('change', 'select', function () {*/
/*         $(this).css('background', '').css('border', '');*/
/* */
/*         $(this).siblings('.text-danger').remove();*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if edit_cart %}*/
/*     $(document).on('click', '.button-update', function () {*/
/* */
/*         var quantity = $(this).parents('.quantity').find('input.qc-product-qantity');*/
/*         if (quantity.length) {*/
/*             if ($(this).data('type') == 'increase') {*/
/*                 quantity.val(parseInt(quantity.val()) + 1);*/
/*             } else if ($(this).data('type') == 'decrease') {*/
/*                 quantity.val(parseInt(quantity.val()) - 1);*/
/*             }*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart/update',*/
/*             type: 'post',*/
/*             data: $('#cart1 :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 //$('#cart1 .button-update').prop('disabled', true);*/
/*             },*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     if (json['error']['stock']) {*/
/*                         $('#button-payment-method').attr("disabled", true);*/
/*                     } else if (json['error']['warning']) {*/
/*                         $('#warning-messages').html('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/*                         $('.alert-danger').fadeIn('slow');*/
/*                         $('#button-payment-method').attr("disabled", true);*/
/*                     } else {*/
/*                         $('#warning-messages').html('');*/
/*                         $('#button-payment-method').removeAttr("disabled");*/
/*                     }*/
/* */
/*                     {% if not logged %}*/
/*                     if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('payment');*/
/*                         {% endif %}*/
/*                     } else {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('shipping');*/
/*                         {% endif %}*/
/*                     }*/
/*                     {% else %}*/
/*                     if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                         reloadPaymentMethod();*/
/*                     } else {*/
/*                         reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                     }*/
/* */
/*                     {% if shipping_required %}*/
/*                     if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                         reloadShippingMethod('shipping');*/
/*                     } else {*/
/*                         reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% endif %}*/
/* */
/*                     {% if not shipping_required %}*/
/*                     loadCart();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '.button-remove', function () {*/
/*         var remove_id = $(this).attr('data-remove');*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart/update&remove=' + remove_id,*/
/*             type: 'get',*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#cart1 .button-remove').prop('disabled', true);*/
/*             },*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     {% if not logged %}*/
/*                     if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('payment');*/
/*                         {% endif %}*/
/*                     } else {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('shipping');*/
/*                         {% endif %}*/
/*                     }*/
/*                     {% else %}*/
/*                     if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                         reloadPaymentMethod();*/
/*                     } else {*/
/*                         reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                     }*/
/* */
/*                     {% if shipping_required %}*/
/*                     if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                         reloadShippingMethod('shipping');*/
/*                     } else {*/
/*                         reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% endif %}*/
/* */
/*                     {% if not shipping_required %}*/
/*                     loadCart();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/* */
/*         return false;*/
/*     });*/
/*     {% endif %}*/
/* */
/* */
/*     //--></script>*/
/* {% if error_warning %}*/
/*     <script type="text/javascript"><!--*/
/*         $('#button-payment-method').attr("disabled", true);*/
/*         //--></script>*/
/* {% endif %}*/
/* {{ footer }}*/
/* */
