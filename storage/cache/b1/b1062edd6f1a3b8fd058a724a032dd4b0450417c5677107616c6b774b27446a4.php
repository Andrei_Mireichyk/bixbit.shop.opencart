<?php

/* default/template/extension/module/bestseller_product.twig */
class __TwigTemplate_f26609d7e08d71f1d848649586420e57bba8e28a8346435cc86684c7854c2c8b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"product__bestsellers\">

  <div class=\"bestsellers__block-title\">";
        // line 3
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</div>
  <div class=\"bestsellers\">
    <div class=\"bestsellers__wrap bestsellers__wrap--prod\">
      ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 7
            echo "
        <a class=\"bestsellers__item\" href=\"";
            // line 8
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">
          <img class=\"bestsellers__img\" src=\"";
            // line 9
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\">
          <div class=\"bestsellers__title\">";
            // line 10
            echo $this->getAttribute($context["product"], "name", array());
            echo "</div>
          <div class=\"bestsellers__desc\">";
            // line 11
            echo $this->getAttribute($context["product"], "description", array());
            echo "</div>
          <div class=\"bestsellers__price\">";
            // line 12
            echo $this->getAttribute($context["product"], "price", array());
            echo "</div>
        </a>

      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    </div>
  </div>
</section>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/bestseller_product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  56 => 12,  52 => 11,  48 => 10,  40 => 9,  36 => 8,  33 => 7,  29 => 6,  23 => 3,  19 => 1,);
    }
}
/* <section class="product__bestsellers">*/
/* */
/*   <div class="bestsellers__block-title">{{ heading_title }}</div>*/
/*   <div class="bestsellers">*/
/*     <div class="bestsellers__wrap bestsellers__wrap--prod">*/
/*       {% for product in products %}*/
/* */
/*         <a class="bestsellers__item" href="{{ product.href }}">*/
/*           <img class="bestsellers__img" src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}">*/
/*           <div class="bestsellers__title">{{ product.name }}</div>*/
/*           <div class="bestsellers__desc">{{ product.description }}</div>*/
/*           <div class="bestsellers__price">{{ product.price }}</div>*/
/*         </a>*/
/* */
/*       {% endfor %}*/
/*     </div>*/
/*   </div>*/
/* </section>*/
