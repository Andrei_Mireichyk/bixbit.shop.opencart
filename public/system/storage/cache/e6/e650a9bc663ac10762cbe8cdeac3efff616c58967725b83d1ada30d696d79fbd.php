<?php

/* default/template/product/product.twig */
class __TwigTemplate_40c209a0f140b09a38fda94dca9902771cc5956ca2ae7033b5b8703bb1b8bec6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 8
        echo "

<main class=\"product\">
    <section class=\"product__wrap\">
        <div class=\"product__header\">
            <div class=\"product__header-wrap\">
                <div class=\"product__header-align\">
                    <div class=\"product__images\">
                        <ul class=\"slider\">
                            ";
        // line 17
        if ((isset($context["images"]) ? $context["images"] : null)) {
            // line 18
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["images"]) ? $context["images"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 19
                echo "                                    <li class=\"slider__item\">
                                        <img src=\"";
                // line 20
                echo $this->getAttribute($context["image"], "popup", array());
                echo "\" title=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" alt=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\">
                                    </li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "                            ";
        }
        // line 24
        echo "                        </ul>
                    </div>
                    <a class=\"product__back-link\" href=\"";
        // line 26
        echo (isset($context["cat_href"]) ? $context["cat_href"] : null);
        echo "\"><i class=\"icon-arrow-l\"></i><span>";
        echo (isset($context["cat_name"]) ? $context["cat_name"] : null);
        echo "</span></a>
                    <div class=\"product__title\">
                        <h1 class=\"product__name\">";
        // line 28
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
                        <div class=\"product__price\">";
        // line 29
        echo (isset($context["price"]) ? $context["price"] : null);
        echo "</div>
                    </div>
                    <div class=\"product__min-desc\">";
        // line 31
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "</div>
                    <ul class=\"product__basic-opts basic-opts\">
                        ";
        // line 33
        if ((isset($context["manufacturer"]) ? $context["manufacturer"] : null)) {
            // line 34
            echo "                            <li class=\"basic-opts__item\">
                                <div class=\"basic-opts__name\">";
            // line 35
            echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
            echo "</div>
                                <div class=\"basic-opts__value\">";
            // line 36
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo "</div>
                            </li>
                        ";
        }
        // line 39
        echo "


                        ";
        // line 42
        $context["break"] = true;
        // line 43
        echo "                        ";
        $context["i"] = 0;
        // line 44
        echo "
                        ";
        // line 45
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
            // line 46
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                if ((isset($context["break"]) ? $context["break"] : null)) {
                    // line 47
                    echo "                                <li class=\"basic-opts__item\">
                                    <div class=\"basic-opts__name\">";
                    // line 48
                    echo $this->getAttribute($context["attribute"], "name", array());
                    echo "</div>
                                    <div class=\"basic-opts__value\">";
                    // line 49
                    echo $this->getAttribute($context["attribute"], "text", array());
                    echo "</div>
                                </li>

                                ";
                    // line 52
                    $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
                    // line 53
                    echo "
                                ";
                    // line 54
                    if (((isset($context["i"]) ? $context["i"] : null) >= 4)) {
                        // line 55
                        echo "                                    ";
                        $context["break"] = false;
                        // line 56
                        echo "                                ";
                    }
                    // line 57
                    echo "
                            ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "
                    </ul>
                    ";
        // line 62
        if ((twig_length_filter($this->env, (isset($context["products"]) ? $context["products"] : null)) > 0)) {
            // line 63
            echo "                    <ul class=\"product__optional optional-prods\">
                        <li class=\"optional-prods__item-title\">Дополнительное оборудование:</li>
                        ";
            // line 65
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 66
                echo "                            <li>
                                <input class=\"optional-prods__checkbox\" type=\"checkbox\" id=\"c-";
                // line 67
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\" value=\"";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\">
                                <label class=\"optional-prods__item\" for=\"c-";
                // line 68
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\">
                                    <img class=\"optional-prods__image\" src=\"";
                // line 69
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\">
                                    <span class=\"optional-prods__link\">";
                // line 70
                echo $this->getAttribute($context["product"], "name", array());
                echo "</span>
                                    <span class=\"optional-prods__price\">";
                // line 71
                echo $this->getAttribute($context["product"], "price", array());
                echo "</span>
                                </label>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 75
            echo "                    </ul>
                    ";
        }
        // line 77
        echo "                    <a class=\"product__order btn btn__primary-order\" href=\"javascript:void(0)\" onclick=\"getOCwizardModal_smca(";
        echo (isset($context["product_id"]) ? $context["product_id"] : null);
        echo ",'add')\">
                        <i class=\"icon-bucket-2\"></i><span>add to cart</span>
                    </a>
                </div>
            </div>
        </div>
        <div class=\"product__nav prod-nav\">
            ";
        // line 84
        if (((isset($context["prev_link"]) ? $context["prev_link"] : null) != "")) {
            // line 85
            echo "            <a class=\"prod-nav__item\" href=\"";
            echo (isset($context["prev_link"]) ? $context["prev_link"] : null);
            echo "\">
                <i class=\"icon-arrow-l\"></i>
                <span>назад</span>
            </a>
            ";
        } else {
            // line 90
            echo "                <span></span>
            ";
        }
        // line 92
        echo "
            <a class=\"prod-nav__item\" href=\"";
        // line 93
        echo (isset($context["cat_href"]) ? $context["cat_href"] : null);
        echo "\"><i class=\"icon-catalog\"></i><span>вернуться в каталог</span></a>

            ";
        // line 95
        if (((isset($context["next_link"]) ? $context["next_link"] : null) != "")) {
            // line 96
            echo "                 <a class=\"prod-nav__item\" href=\"";
            echo (isset($context["next_link"]) ? $context["next_link"] : null);
            echo "\"><span>далее</span><i class=\"icon-arrow-r\"></i></a>
             ";
        } else {
            // line 98
            echo "                <span></span>
            ";
        }
        // line 100
        echo "        </div>
        <h2 class=\"product__subtitle\">Все характеристики</h2>
        <ul class=\"product__full-opts full-opts\">

            ";
        // line 104
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["attribute_groups"]) ? $context["attribute_groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["attribute_group"]) {
            // line 105
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["attribute_group"], "attribute", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                // line 106
                echo "
                    <li class=\"full-opts__item\">
                        <div class=\"full-opts__name\">";
                // line 108
                echo $this->getAttribute($context["attribute"], "name", array());
                echo "</div>
                        <div class=\"full-opts__value\">";
                // line 109
                echo $this->getAttribute($context["attribute"], "text", array());
                echo "</div>
                    </li>

                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 114
        echo "
            <li class=\"full-opts__item\">
                <div class=\"full-opts__name\">";
        // line 116
        echo (isset($context["text_size"]) ? $context["text_size"] : null);
        echo "</div>
                <div class=\"full-opts__value\">";
        // line 117
        echo (isset($context["size"]) ? $context["size"] : null);
        echo "</div>
            </li>

            ";
        // line 120
        if ((isset($context["weight"]) ? $context["weight"] : null)) {
            // line 121
            echo "                <li class=\"full-opts__item\">
                    <div class=\"full-opts__name\">";
            // line 122
            echo (isset($context["text_weight"]) ? $context["text_weight"] : null);
            echo "</div>
                    <div class=\"full-opts__value\">";
            // line 123
            echo (isset($context["weight"]) ? $context["weight"] : null);
            echo "</div>
                </li>
            ";
        }
        // line 126
        echo "        </ul>
        <div class=\"product__max-desc\">";
        // line 127
        echo (isset($context["full_description"]) ? $context["full_description"] : null);
        echo "</div>
    </section>
    ";
        // line 129
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "
</main>


<script type=\"text/javascript\"><!--
\$('select[name=\\'recurring_id\\'], input[name=\"quantity\"]').change(function(){
\t\$.ajax({
\t\turl: 'index.php?route=product/product/getRecurringDescription',
\t\ttype: 'post',
\t\tdata: \$('input[name=\\'product_id\\'], input[name=\\'quantity\\'], select[name=\\'recurring_id\\']'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#recurring-description').html('');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();

\t\t\tif (json['success']) {
\t\t\t\t\$('#recurring-description').html(json['success']);
\t\t\t}
\t\t}
\t});
});
//--></script> 
<script type=\"text/javascript\"><!--
\$('#button-cart').on('click', function() {

    //products id


\t\$.ajax({
\t\turl: 'index.php?route=checkout/cart/add',
\t\ttype: 'post',
\t\tdata: \$('#product input[type=\\'text\\'], #product input[type=\\'hidden\\'], #product input[type=\\'radio\\']:checked, #product input[type=\\'checkbox\\']:checked, #product select, #product textarea'),
\t\tdataType: 'json',
\t\tbeforeSend: function() {
\t\t\t\$('#button-cart').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-cart').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible, .text-danger').remove();
\t\t\t\$('.form-group').removeClass('has-error');

\t\t\tif (json['error']) {
\t\t\t\tif (json['error']['option']) {
\t\t\t\t\tfor (i in json['error']['option']) {
\t\t\t\t\t\tvar element = \$('#input-option' + i.replace('_', '-'));

\t\t\t\t\t\tif (element.parent().hasClass('input-group')) {
\t\t\t\t\t\t\telement.parent().after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\telement.after('<div class=\"text-danger\">' + json['error']['option'][i] + '</div>');
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tif (json['error']['recurring']) {
\t\t\t\t\t\$('select[name=\\'recurring_id\\']').after('<div class=\"text-danger\">' + json['error']['recurring'] + '</div>');
\t\t\t\t}

\t\t\t\t// Highlight any found errors
\t\t\t\t\$('.text-danger').parent().addClass('has-error');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('.breadcrumb').after('<div class=\"alert alert-success alert-dismissible\">' + json['success'] + '<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

\t\t\t\t\$('#cart > button').html('<span id=\"cart-total\"><i class=\"fa fa-shopping-cart\"></i> ' + json['total'] + '</span>');

\t\t\t\t\$('html, body').animate({ scrollTop: 0 }, 'slow');

\t\t\t\t\$('#cart > ul').load('index.php?route=common/cart/info ul li');
\t\t\t}
\t\t},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
\t});
});
//--></script>


";
        // line 213
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "    <link href=\"catalog/view/theme/default/stylesheet/product.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"catalog/view/theme/default/js/product.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/product/product.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  444 => 6,  441 => 5,  436 => 3,  433 => 2,  427 => 213,  340 => 129,  335 => 127,  332 => 126,  326 => 123,  322 => 122,  319 => 121,  317 => 120,  311 => 117,  307 => 116,  303 => 114,  297 => 113,  287 => 109,  283 => 108,  279 => 106,  274 => 105,  270 => 104,  264 => 100,  260 => 98,  254 => 96,  252 => 95,  247 => 93,  244 => 92,  240 => 90,  231 => 85,  229 => 84,  218 => 77,  214 => 75,  204 => 71,  200 => 70,  196 => 69,  192 => 68,  186 => 67,  183 => 66,  179 => 65,  175 => 63,  173 => 62,  169 => 60,  163 => 59,  155 => 57,  152 => 56,  149 => 55,  147 => 54,  144 => 53,  142 => 52,  136 => 49,  132 => 48,  129 => 47,  123 => 46,  119 => 45,  116 => 44,  113 => 43,  111 => 42,  106 => 39,  100 => 36,  96 => 35,  93 => 34,  91 => 33,  86 => 31,  81 => 29,  77 => 28,  70 => 26,  66 => 24,  63 => 23,  50 => 20,  47 => 19,  42 => 18,  40 => 17,  29 => 8,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/product.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/product.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* */
/* <main class="product">*/
/*     <section class="product__wrap">*/
/*         <div class="product__header">*/
/*             <div class="product__header-wrap">*/
/*                 <div class="product__header-align">*/
/*                     <div class="product__images">*/
/*                         <ul class="slider">*/
/*                             {% if images %}*/
/*                                 {% for image in images %}*/
/*                                     <li class="slider__item">*/
/*                                         <img src="{{ image.popup }}" title="{{ heading_title }}" alt="{{ heading_title }}">*/
/*                                     </li>*/
/*                                 {% endfor %}*/
/*                             {% endif %}*/
/*                         </ul>*/
/*                     </div>*/
/*                     <a class="product__back-link" href="{{ cat_href }}"><i class="icon-arrow-l"></i><span>{{ cat_name }}</span></a>*/
/*                     <div class="product__title">*/
/*                         <h1 class="product__name">{{ heading_title }}</h1>*/
/*                         <div class="product__price">{{ price }}</div>*/
/*                     </div>*/
/*                     <div class="product__min-desc">{{ description }}</div>*/
/*                     <ul class="product__basic-opts basic-opts">*/
/*                         {% if manufacturer %}*/
/*                             <li class="basic-opts__item">*/
/*                                 <div class="basic-opts__name">{{ text_manufacturer }}</div>*/
/*                                 <div class="basic-opts__value">{{ manufacturer }}</div>*/
/*                             </li>*/
/*                         {% endif %}*/
/* */
/* */
/* */
/*                         {% set break = true %}*/
/*                         {% set i = 0 %}*/
/* */
/*                         {% for attribute_group in attribute_groups  %}*/
/*                             {% for attribute in attribute_group.attribute if  break %}*/
/*                                 <li class="basic-opts__item">*/
/*                                     <div class="basic-opts__name">{{ attribute.name }}</div>*/
/*                                     <div class="basic-opts__value">{{ attribute.text }}</div>*/
/*                                 </li>*/
/* */
/*                                 {% set i = i+1 %}*/
/* */
/*                                 {% if i >= 4 %}*/
/*                                     {% set break = false %}*/
/*                                 {% endif %}*/
/* */
/*                             {% endfor %}*/
/*                         {% endfor %}*/
/* */
/*                     </ul>*/
/*                     {% if products|length > 0 %}*/
/*                     <ul class="product__optional optional-prods">*/
/*                         <li class="optional-prods__item-title">Дополнительное оборудование:</li>*/
/*                         {% for product in products %}*/
/*                             <li>*/
/*                                 <input class="optional-prods__checkbox" type="checkbox" id="c-{{ product.product_id }}" value="{{ product.product_id }}">*/
/*                                 <label class="optional-prods__item" for="c-{{ product.product_id }}">*/
/*                                     <img class="optional-prods__image" src="{{ product.thumb }}">*/
/*                                     <span class="optional-prods__link">{{ product.name }}</span>*/
/*                                     <span class="optional-prods__price">{{ product.price }}</span>*/
/*                                 </label>*/
/*                             </li>*/
/*                         {% endfor %}*/
/*                     </ul>*/
/*                     {% endif %}*/
/*                     <a class="product__order btn btn__primary-order" href="javascript:void(0)" onclick="getOCwizardModal_smca({{ product_id }},'add')">*/
/*                         <i class="icon-bucket-2"></i><span>add to cart</span>*/
/*                     </a>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         <div class="product__nav prod-nav">*/
/*             {% if prev_link != '' %}*/
/*             <a class="prod-nav__item" href="{{ prev_link }}">*/
/*                 <i class="icon-arrow-l"></i>*/
/*                 <span>назад</span>*/
/*             </a>*/
/*             {% else %}*/
/*                 <span></span>*/
/*             {% endif %}*/
/* */
/*             <a class="prod-nav__item" href="{{ cat_href }}"><i class="icon-catalog"></i><span>вернуться в каталог</span></a>*/
/* */
/*             {% if next_link != '' %}*/
/*                  <a class="prod-nav__item" href="{{ next_link }}"><span>далее</span><i class="icon-arrow-r"></i></a>*/
/*              {% else %}*/
/*                 <span></span>*/
/*             {% endif %}*/
/*         </div>*/
/*         <h2 class="product__subtitle">Все характеристики</h2>*/
/*         <ul class="product__full-opts full-opts">*/
/* */
/*             {% for attribute_group in attribute_groups%}*/
/*                 {% for attribute in attribute_group.attribute %}*/
/* */
/*                     <li class="full-opts__item">*/
/*                         <div class="full-opts__name">{{ attribute.name }}</div>*/
/*                         <div class="full-opts__value">{{ attribute.text }}</div>*/
/*                     </li>*/
/* */
/*                 {% endfor %}*/
/*             {% endfor %}*/
/* */
/*             <li class="full-opts__item">*/
/*                 <div class="full-opts__name">{{ text_size }}</div>*/
/*                 <div class="full-opts__value">{{ size }}</div>*/
/*             </li>*/
/* */
/*             {% if weight %}*/
/*                 <li class="full-opts__item">*/
/*                     <div class="full-opts__name">{{ text_weight }}</div>*/
/*                     <div class="full-opts__value">{{ weight }}</div>*/
/*                 </li>*/
/*             {% endif %}*/
/*         </ul>*/
/*         <div class="product__max-desc">{{ full_description }}</div>*/
/*     </section>*/
/*     {{ content_bottom }}*/
/* </main>*/
/* */
/* */
/* <script type="text/javascript"><!--*/
/* $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=product/product/getRecurringDescription',*/
/* 		type: 'post',*/
/* 		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#recurring-description').html('');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* */
/* 			if (json['success']) {*/
/* 				$('#recurring-description').html(json['success']);*/
/* 			}*/
/* 		}*/
/* 	});*/
/* });*/
/* //--></script> */
/* <script type="text/javascript"><!--*/
/* $('#button-cart').on('click', function() {*/
/* */
/*     //products id*/
/* */
/* */
/* 	$.ajax({*/
/* 		url: 'index.php?route=checkout/cart/add',*/
/* 		type: 'post',*/
/* 		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),*/
/* 		dataType: 'json',*/
/* 		beforeSend: function() {*/
/* 			$('#button-cart').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-cart').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible, .text-danger').remove();*/
/* 			$('.form-group').removeClass('has-error');*/
/* */
/* 			if (json['error']) {*/
/* 				if (json['error']['option']) {*/
/* 					for (i in json['error']['option']) {*/
/* 						var element = $('#input-option' + i.replace('_', '-'));*/
/* */
/* 						if (element.parent().hasClass('input-group')) {*/
/* 							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						} else {*/
/* 							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');*/
/* 						}*/
/* 					}*/
/* 				}*/
/* */
/* 				if (json['error']['recurring']) {*/
/* 					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');*/
/* 				}*/
/* */
/* 				// Highlight any found errors*/
/* 				$('.text-danger').parent().addClass('has-error');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('.breadcrumb').after('<div class="alert alert-success alert-dismissible">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* */
/* 				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');*/
/* */
/* 				$('html, body').animate({ scrollTop: 0 }, 'slow');*/
/* */
/* 				$('#cart > ul').load('index.php?route=common/cart/info ul li');*/
/* 			}*/
/* 		},*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/* 	});*/
/* });*/
/* //--></script>*/
/* */
/* */
/* {{ footer }} */
/* */
