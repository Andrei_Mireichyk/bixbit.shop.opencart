<?php

/* default/template/common/header.twig */
class __TwigTemplate_2a3ad977fc2f837d8d213e7fb36f76a7695bec6e0ff759262ef1c57f25336815 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]>
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 8
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\">
    <title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>

    ";
        // line 14
        if ((isset($context["robots"]) ? $context["robots"] : null)) {
            // line 15
            echo "        <meta name=\"robots\" content=\"";
            echo (isset($context["robots"]) ? $context["robots"] : null);
            echo "\"/>
    ";
        }
        // line 17
        echo "    <base href=\"";
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\"/>
    ";
        // line 18
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 19
            echo "        <meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\"/>
    ";
        }
        // line 21
        echo "    ";
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 22
            echo "        <meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\"/>
    ";
        }
        // line 24
        echo "    <meta property=\"og:title\" content=\"";
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "\"/>
    <meta property=\"og:type\" content=\"website\"/>
    <meta property=\"og:url\" content=\"";
        // line 26
        echo (isset($context["og_url"]) ? $context["og_url"] : null);
        echo "\"/>
    ";
        // line 27
        if ((isset($context["og_image"]) ? $context["og_image"] : null)) {
            // line 28
            echo "        <meta property=\"og:image\" content=\"";
            echo (isset($context["og_image"]) ? $context["og_image"] : null);
            echo "\"/>
    ";
        } else {
            // line 30
            echo "        <meta property=\"og:image\" content=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\"/>
    ";
        }
        // line 32
        echo "    <meta property=\"og:site_name\" content=\"";
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\"/>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js\"></script>
    ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 35
            echo "        <link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\"/>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "
    ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 39
            echo "        <link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\"/>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 42
            echo "        ";
            echo $context["analytic"];
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "    <link href=\"catalog/view/theme/default/stylesheet/common.css\" rel=\"stylesheet\">
    ";
        // line 45
        $this->displayBlock('link', $context, $blocks);
        // line 48
        echo "    <script src=\"catalog/view/theme/default/js/common.bundle.js\" type=\"text/javascript\"></script>

    ";
        // line 50
        $this->displayBlock('scripts', $context, $blocks);
        // line 53
        echo "

        ";
        // line 55
        if ($this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "customer_groups", array(), "array")) {
            // line 56
            echo "            ";
            $context["customer_groups"] = $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "customer_groups", array(), "array");
            // line 57
            echo "        ";
        } else {
            // line 58
            echo "            ";
            $context["customer_groups"] = array();
            // line 59
            echo "        ";
        }
        // line 60
        echo "        ";
        if ($this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "stores", array(), "array")) {
            // line 61
            echo "            ";
            $context["stores"] = $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "stores", array(), "array");
            // line 62
            echo "        ";
        } else {
            // line 63
            echo "            ";
            $context["stores"] = array();
            // line 64
            echo "        ";
        }
        // line 65
        echo "
        ";
        // line 66
        if ((($this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "activate", array(), "array") && !twig_in_filter((isset($context["smca_customer_group_id"]) ? $context["smca_customer_group_id"] : null), (isset($context["customer_groups"]) ? $context["customer_groups"] : null))) && !twig_in_filter((isset($context["smca_store_id"]) ? $context["smca_store_id"] : null), (isset($context["stores"]) ? $context["stores"] : null)))) {
            // line 67
            echo "        <script src=\"catalog/view/javascript/ocdev_smart_cart/jquery.magnific-popup.min.js?v=";
            echo $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "front_module_version", array(), "array");
            echo "\" type=\"text/javascript\"></script>
        <link href=\"catalog/view/javascript/ocdev_smart_cart/magnific-popup.css?v=";
            // line 68
            echo $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "front_module_version", array(), "array");
            echo "\" rel=\"stylesheet\" media=\"screen\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/default/stylesheet/ocdev_smart_cart/stylesheet.css?v=";
            // line 69
            echo $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "front_module_version", array(), "array");
            echo "\"/>
        <script type=\"text/javascript\" src=\"catalog/view/javascript/ocdev_smart_cart/ocdev_smart_cart.js?v=";
            // line 70
            echo $this->getAttribute((isset($context["smca_form_data"]) ? $context["smca_form_data"] : null), "front_module_version", array(), "array");
            echo "\"></script>
        ";
        }
        // line 72
        echo "      
</head>
<body>
<div onclick=\"void(0)\">

    <div class=\"dialog dialog--basket\" id=\"dialog\">
        <div class=\"dialog__content\">
            <div class=\"basket\"></div>
        </div>
    </div>

    <header class=\"header\">
        <div class=\"header__logo\"><a href=\"/\"><img src=\"catalog/view/theme/default/images/logo.svg\"></a></div>
        <div class=\"header__nav\">
            <div class=\"header__search\"><a href=\"javascript:void(0)\"><i class=\"icon-search search__toggle\"></i></a>
                <div class=\"search\">
                    <div class=\"search__input\">
                        <input placeholder=\"Поиск\" name=\"live_search\"><i class=\"icon-search\"></i>
                    </div>
                    <div class=\"search__list\">
                        <div class=\"search__status\">Что ищите ?</div>
                        <div class=\"search__results\">

                        </div>
                    </div>
                </div>
            </div>
            ";
        // line 99
        if ((twig_length_filter($this->env, (isset($context["telephone_list"]) ? $context["telephone_list"] : null)) > 0)) {
            // line 100
            echo "                <div class=\"header__contacts contact-dropdown\">
                    <a href=\"javascript:void(0)\">
                        <i class=\"icon-phone-1 contact-dropdown__phone-icon\"></i>
                        <span class=\"contact-dropdown__title contact-dropdown__title--first\">";
            // line 103
            echo $this->getAttribute((isset($context["telephone_list"]) ? $context["telephone_list"] : null), 0, array(), "array");
            echo "</span>
                        <i class=\"icon-arrow-b contact-dropdown__arrow-icon\"></i></a>
                    <div class=\"contact-dropdown__list\">
                        <div class=\"contact-dropdown__header\">Контакты</div>

                        ";
            // line 108
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["telephone_list"]) ? $context["telephone_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
                // line 109
                echo "                            <a class=\"contact-dropdown__item\" href=\"tel:";
                echo $context["phone"];
                echo "\">
                                <i class=\"icon-phone-1 contact-dropdown__icon\"></i>
                                <span class=\"contact-dropdown__title\">";
                // line 111
                echo $context["phone"];
                echo "</span>
                            </a>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 114
            echo "
                    </div>
                </div>
            ";
        }
        // line 118
        echo "
            <div class=\"header__bucket\" data-toggle=\"dialog\" data-target=\"#dialog\" onclick=\"getOCwizardModal_smca(false,'load')\">

                <i class=\"icon-bucket header__bucket-icon\">
                    <span class=\"header__bucket-badge\">1</span>
                </i>
                <span class=\"header__bucket-title\">Корзина</span>
            </div>
            <a class=\"header__toggler\" href=\"javascript:void(0)\"><i class=\"icon-menu header__toggler-icon\"></i></a>
        </div>
    </header>

    ";
        // line 130
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "


";
    }

    // line 45
    public function block_link($context, array $blocks = array())
    {
        // line 46
        echo "
    ";
    }

    // line 50
    public function block_scripts($context, array $blocks = array())
    {
        // line 51
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "default/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  332 => 51,  329 => 50,  324 => 46,  321 => 45,  313 => 130,  299 => 118,  293 => 114,  284 => 111,  278 => 109,  274 => 108,  266 => 103,  261 => 100,  259 => 99,  230 => 72,  225 => 70,  221 => 69,  217 => 68,  212 => 67,  210 => 66,  207 => 65,  204 => 64,  201 => 63,  198 => 62,  195 => 61,  192 => 60,  189 => 59,  186 => 58,  183 => 57,  180 => 56,  178 => 55,  174 => 53,  172 => 50,  168 => 48,  166 => 45,  163 => 44,  154 => 42,  149 => 41,  138 => 39,  134 => 38,  131 => 37,  118 => 35,  114 => 34,  108 => 32,  102 => 30,  96 => 28,  94 => 27,  90 => 26,  84 => 24,  78 => 22,  75 => 21,  69 => 19,  67 => 18,  62 => 17,  56 => 15,  54 => 14,  49 => 12,  40 => 8,  33 => 6,  26 => 4,  21 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]>*/
/* <html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]>*/
/* <html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <head>*/
/*     <meta charset="UTF-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">*/
/*     <title>{{ title }}</title>*/
/* */
/*     {% if robots %}*/
/*         <meta name="robots" content="{{ robots }}"/>*/
/*     {% endif %}*/
/*     <base href="{{ base }}"/>*/
/*     {% if description %}*/
/*         <meta name="description" content="{{ description }}"/>*/
/*     {% endif %}*/
/*     {% if keywords %}*/
/*         <meta name="keywords" content="{{ keywords }}"/>*/
/*     {% endif %}*/
/*     <meta property="og:title" content="{{ title }}"/>*/
/*     <meta property="og:type" content="website"/>*/
/*     <meta property="og:url" content="{{ og_url }}"/>*/
/*     {% if og_image %}*/
/*         <meta property="og:image" content="{{ og_image }}"/>*/
/*     {% else %}*/
/*         <meta property="og:image" content="{{ logo }}"/>*/
/*     {% endif %}*/
/*     <meta property="og:site_name" content="{{ name }}"/>*/
/*     <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.1/jquery.min.js"></script>*/
/*     {% for style in styles %}*/
/*         <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}"/>*/
/*     {% endfor %}*/
/* */
/*     {% for link in links %}*/
/*         <link href="{{ link.href }}" rel="{{ link.rel }}"/>*/
/*     {% endfor %}*/
/*     {% for analytic in analytics %}*/
/*         {{ analytic }}*/
/*     {% endfor %}*/
/*     <link href="catalog/view/theme/default/stylesheet/common.css" rel="stylesheet">*/
/*     {% block link %}*/
/* */
/*     {% endblock %}*/
/*     <script src="catalog/view/theme/default/js/common.bundle.js" type="text/javascript"></script>*/
/* */
/*     {% block scripts %}*/
/* */
/*     {% endblock %}*/
/* */
/* */
/*         {% if smca_form_data['customer_groups'] %}*/
/*             {% set customer_groups = smca_form_data['customer_groups'] %}*/
/*         {% else %}*/
/*             {% set customer_groups = {} %}*/
/*         {% endif %}*/
/*         {% if smca_form_data['stores'] %}*/
/*             {% set stores = smca_form_data['stores'] %}*/
/*         {% else %}*/
/*             {% set stores = {} %}*/
/*         {% endif %}*/
/* */
/*         {% if smca_form_data['activate'] and (smca_customer_group_id not in customer_groups) and (smca_store_id not in stores) %}*/
/*         <script src="catalog/view/javascript/ocdev_smart_cart/jquery.magnific-popup.min.js?v={{ smca_form_data['front_module_version'] }}" type="text/javascript"></script>*/
/*         <link href="catalog/view/javascript/ocdev_smart_cart/magnific-popup.css?v={{ smca_form_data['front_module_version'] }}" rel="stylesheet" media="screen" />*/
/*         <link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ocdev_smart_cart/stylesheet.css?v={{ smca_form_data['front_module_version'] }}"/>*/
/*         <script type="text/javascript" src="catalog/view/javascript/ocdev_smart_cart/ocdev_smart_cart.js?v={{ smca_form_data['front_module_version'] }}"></script>*/
/*         {% endif %}*/
/*       */
/* </head>*/
/* <body>*/
/* <div onclick="void(0)">*/
/* */
/*     <div class="dialog dialog--basket" id="dialog">*/
/*         <div class="dialog__content">*/
/*             <div class="basket"></div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <header class="header">*/
/*         <div class="header__logo"><a href="/"><img src="catalog/view/theme/default/images/logo.svg"></a></div>*/
/*         <div class="header__nav">*/
/*             <div class="header__search"><a href="javascript:void(0)"><i class="icon-search search__toggle"></i></a>*/
/*                 <div class="search">*/
/*                     <div class="search__input">*/
/*                         <input placeholder="Поиск" name="live_search"><i class="icon-search"></i>*/
/*                     </div>*/
/*                     <div class="search__list">*/
/*                         <div class="search__status">Что ищите ?</div>*/
/*                         <div class="search__results">*/
/* */
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             {% if  telephone_list|length > 0 %}*/
/*                 <div class="header__contacts contact-dropdown">*/
/*                     <a href="javascript:void(0)">*/
/*                         <i class="icon-phone-1 contact-dropdown__phone-icon"></i>*/
/*                         <span class="contact-dropdown__title contact-dropdown__title--first">{{ telephone_list[0] }}</span>*/
/*                         <i class="icon-arrow-b contact-dropdown__arrow-icon"></i></a>*/
/*                     <div class="contact-dropdown__list">*/
/*                         <div class="contact-dropdown__header">Контакты</div>*/
/* */
/*                         {% for phone in telephone_list %}*/
/*                             <a class="contact-dropdown__item" href="tel:{{ phone }}">*/
/*                                 <i class="icon-phone-1 contact-dropdown__icon"></i>*/
/*                                 <span class="contact-dropdown__title">{{ phone }}</span>*/
/*                             </a>*/
/*                         {% endfor %}*/
/* */
/*                     </div>*/
/*                 </div>*/
/*             {% endif %}*/
/* */
/*             <div class="header__bucket" data-toggle="dialog" data-target="#dialog" onclick="getOCwizardModal_smca(false,'load')">*/
/* */
/*                 <i class="icon-bucket header__bucket-icon">*/
/*                     <span class="header__bucket-badge">1</span>*/
/*                 </i>*/
/*                 <span class="header__bucket-title">Корзина</span>*/
/*             </div>*/
/*             <a class="header__toggler" href="javascript:void(0)"><i class="icon-menu header__toggler-icon"></i></a>*/
/*         </div>*/
/*     </header>*/
/* */
/*     {{ menu }}*/
/* */
/* */
/* */
