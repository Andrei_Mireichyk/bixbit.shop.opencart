<?php

/* default/template/extension/quickcheckout/cart.twig */
class __TwigTemplate_20e3816da9665cecf09350a7d7fe8a18379c48fd06aaf9ca8db1e9884572c0b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"stock_warning\">
    ";
        // line 2
        if ((isset($context["error_warning_stock"]) ? $context["error_warning_stock"] : null)) {
            // line 3
            echo "\t\t<div class=\"alert alert-danger\" style=\"\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_stock"]) ? $context["error_stock"] : null);
            echo "</div>
    ";
        }
        // line 5
        echo "</div>

";
        // line 7
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 8
            echo "\t<div class=\"product__header\">
\t\t<div class=\"product__header-title\">ВАШ ЗАКАЗ</div>
\t\t<div class=\"product__header-edit\"><a class=\"product__edit-link\" data-toggle=\"dialog\" data-target=\"#dialog\" onclick=\"getOCwizardModal_smca(false,'load')\">Изменить</a></div>
\t</div>
\t<div class=\"product__list\">
        ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 14
                echo "\t\t\t<a class=\"product__item\" href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">
\t\t\t\t<img class=\"product__image\" src=\"";
                // line 15
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\">
\t\t\t\t<div class=\"product__group\">
\t\t\t\t\t<div class=\"product__title\">";
                // line 17
                echo $this->getAttribute($context["product"], "name", array());
                echo "</div>
\t\t\t\t\t<div class=\"product__details\">
\t\t\t\t\t\t<div class=\"product__count\">";
                // line 19
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "шт.</div>
\t\t\t\t\t\t<div class=\"product__price\">";
                // line 20
                echo $this->getAttribute($context["product"], "total", array());
                echo "</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</a>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "\t</div>

\t<div class=\"product__estimation\">
        ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 29
                echo "\t\t\t<div class=\"product__estimation";
                echo $this->getAttribute($context["total"], "class", array());
                echo "\">
\t\t\t\t<div>";
                // line 30
                echo $this->getAttribute($context["total"], "title", array());
                echo ":</div>
\t\t\t\t<div>";
                // line 31
                echo $this->getAttribute($context["total"], "text", array());
                echo "</div>
\t\t\t</div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 34,  99 => 31,  95 => 30,  90 => 29,  86 => 28,  81 => 25,  70 => 20,  66 => 19,  61 => 17,  52 => 15,  47 => 14,  43 => 13,  36 => 8,  34 => 7,  30 => 5,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div id="stock_warning">*/
/*     {% if error_warning_stock %}*/
/* 		<div class="alert alert-danger" style=""><i class="fa fa-exclamation-circle"></i> {{ error_stock }}</div>*/
/*     {% endif %}*/
/* </div>*/
/* */
/* {% if products or vouchers %}*/
/* 	<div class="product__header">*/
/* 		<div class="product__header-title">ВАШ ЗАКАЗ</div>*/
/* 		<div class="product__header-edit"><a class="product__edit-link" data-toggle="dialog" data-target="#dialog" onclick="getOCwizardModal_smca(false,'load')">Изменить</a></div>*/
/* 	</div>*/
/* 	<div class="product__list">*/
/*         {% for product in products %}*/
/* 			<a class="product__item" href="{{ product.href }}">*/
/* 				<img class="product__image" src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}">*/
/* 				<div class="product__group">*/
/* 					<div class="product__title">{{ product.name }}</div>*/
/* 					<div class="product__details">*/
/* 						<div class="product__count">{{ product.quantity }}шт.</div>*/
/* 						<div class="product__price">{{ product.total }}</div>*/
/* 					</div>*/
/* 				</div>*/
/* 			</a>*/
/*         {% endfor %}*/
/* 	</div>*/
/* */
/* 	<div class="product__estimation">*/
/*         {% for total in totals %}*/
/* 			<div class="product__estimation{{ total.class }}">*/
/* 				<div>{{ total.title }}:</div>*/
/* 				<div>{{ total.text }}</div>*/
/* 			</div>*/
/*         {% endfor %}*/
/* 	</div>*/
/* {% endif %}*/
