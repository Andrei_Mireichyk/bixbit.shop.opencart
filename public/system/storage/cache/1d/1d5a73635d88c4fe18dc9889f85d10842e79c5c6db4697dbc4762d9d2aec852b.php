<?php

/* default/template/product/manufacturer_info.twig */
class __TwigTemplate_254810dcfd6f6ea8b19765e78cf80d5a0347f34dbe0e1e27de2c7e9d99095208 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"products\">
  <section class=\"products__wrap\">
    <div class=\"products__filter-block\">
        ";
        // line 13
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    </div>
    <div class=\"products__products-block\">
      <div class=\"products__description-block cat_description\">
        <h1 class=\"cat_description__title\">";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
        <div class=\"cat_description__description\">
            ";
        // line 19
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "
        </div>
      </div>
      <div class=\"products__sort-block sort\">

        <div class=\"sort__title\">";
        // line 24
        echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
        echo "</div>

          ";
        // line 26
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 27
            echo "            <a class=\"sort__item sort__item--active\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "href", array());
            echo "\">
              <span>";
            // line 28
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "text", array());
            echo "</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        } else {
            // line 32
            echo "            <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "href", array());
            echo "\">
              <span>";
            // line 33
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 0, array(), "array"), "text", array());
            echo "</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        }
        // line 37
        echo "
          ";
        // line 38
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 39
            echo "            <a class=\"sort__item sort__item--desc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 2, array(), "array"), "href", array());
            echo "\">
              <span>Имя</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 43
(isset($context["sorts"]) ? $context["sorts"] : null), 2, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 44
            echo "            <a class=\"sort__item sort__item--asc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "href", array());
            echo "\">
              <span>Имя</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        } else {
            // line 49
            echo "            <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 1, array(), "array"), "href", array());
            echo "\">
              <span>Имя</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        }
        // line 54
        echo "

          ";
        // line 56
        if (($this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 57
            echo "            <a class=\"sort__item sort__item--desc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 4, array(), "array"), "href", array());
            echo "\">
              <span>Цена</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        } elseif (($this->getAttribute($this->getAttribute(        // line 61
(isset($context["sorts"]) ? $context["sorts"] : null), 4, array(), "array"), "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
            // line 62
            echo "            <a class=\"sort__item sort__item--asc\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "href", array());
            echo "\">
              <span>Цена</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        } else {
            // line 67
            echo "            <a class=\"sort__item\" href=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["sorts"]) ? $context["sorts"] : null), 3, array(), "array"), "href", array());
            echo "\">
              <span>Цена</span>
              <i class=\"icon-arrow-b\"></i>
            </a>
          ";
        }
        // line 72
        echo "
      </div>
      <a class=\"sort__toggle\" href=\"javascript:void(0)\">";
        // line 74
        echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
        echo "</a>
      <div class=\"products__list product\">
          ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 77
            echo "            <div class=\"product__item\">
              <a class=\"product__wrap\" href=\"";
            // line 78
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">
                <img class=\"product__image\" src=\"";
            // line 79
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\">
                <div class=\"product__group\">
                  <div class=\"product__title\">";
            // line 81
            echo $this->getAttribute($context["product"], "name", array());
            echo "</div>
                  <div class=\"product__desc\">";
            // line 82
            echo $this->getAttribute($context["product"], "description", array());
            echo "</div>
                  <div class=\"product__details\">
                    <div class=\"product__price\">";
            // line 84
            echo $this->getAttribute($context["product"], "price", array());
            echo "</div>

                      ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "tags", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 87
                echo "                        <div class=\"product__label product__label--new\">";
                echo $this->getAttribute($context["tag"], "tag", array());
                echo "</div>
                      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "
                  </div>
                </div>
              </a>
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "
          ";
        // line 96
        if ( !(isset($context["products"]) ? $context["products"] : null)) {
            // line 97
            echo "            <p class=\"products__empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
          ";
        }
        // line 99
        echo "      </div>
      <div class=\"products__pagination_wrap\">
          ";
        // line 101
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
      </div>

    </div>
  </section>
</main>

";
        // line 108
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "  <link href=\"catalog/view/theme/default/stylesheet/products.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "  <script src=\"catalog/view/theme/default/js/products.bundle.js\" type=\"text/javascript\"></script>
  <script src=\"catalog/view/javascript/brainyfilter.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/product/manufacturer_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  258 => 6,  255 => 5,  250 => 3,  247 => 2,  243 => 108,  233 => 101,  229 => 99,  223 => 97,  221 => 96,  218 => 95,  207 => 89,  198 => 87,  194 => 86,  189 => 84,  184 => 82,  180 => 81,  173 => 79,  169 => 78,  166 => 77,  162 => 76,  157 => 74,  153 => 72,  144 => 67,  135 => 62,  133 => 61,  125 => 57,  123 => 56,  119 => 54,  110 => 49,  101 => 44,  99 => 43,  91 => 39,  89 => 38,  86 => 37,  79 => 33,  74 => 32,  67 => 28,  62 => 27,  60 => 26,  55 => 24,  47 => 19,  42 => 17,  35 => 13,  29 => 9,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*   <link href="catalog/view/theme/default/stylesheet/products.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*   <script src="catalog/view/theme/default/js/products.bundle.js" type="text/javascript"></script>*/
/*   <script src="catalog/view/javascript/brainyfilter.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* <main class="products">*/
/*   <section class="products__wrap">*/
/*     <div class="products__filter-block">*/
/*         {{ column_left }}*/
/*     </div>*/
/*     <div class="products__products-block">*/
/*       <div class="products__description-block cat_description">*/
/*         <h1 class="cat_description__title">{{ heading_title }}</h1>*/
/*         <div class="cat_description__description">*/
/*             {{ description }}*/
/*         </div>*/
/*       </div>*/
/*       <div class="products__sort-block sort">*/
/* */
/*         <div class="sort__title">{{ text_sort }}</div>*/
/* */
/*           {% if sorts[0].value == '%s-%s'|format(sort, order) %}*/
/*             <a class="sort__item sort__item--active" href="{{ sorts[0].href }}">*/
/*               <span>{{ sorts[0].text }}</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% else %}*/
/*             <a class="sort__item" href="{{ sorts[0].href }}">*/
/*               <span>{{ sorts[0].text }}</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% endif %}*/
/* */
/*           {% if sorts[1].value == '%s-%s'|format(sort, order) %}*/
/*             <a class="sort__item sort__item--desc" href="{{ sorts[2].href }}">*/
/*               <span>Имя</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% elseif sorts[2].value == '%s-%s'|format(sort, order) %}*/
/*             <a class="sort__item sort__item--asc" href="{{ sorts[1].href }}">*/
/*               <span>Имя</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% else %}*/
/*             <a class="sort__item" href="{{ sorts[1].href }}">*/
/*               <span>Имя</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% endif %}*/
/* */
/* */
/*           {% if sorts[3].value == '%s-%s'|format(sort, order) %}*/
/*             <a class="sort__item sort__item--desc" href="{{ sorts[4].href }}">*/
/*               <span>Цена</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% elseif sorts[4].value == '%s-%s'|format(sort, order) %}*/
/*             <a class="sort__item sort__item--asc" href="{{ sorts[3].href }}">*/
/*               <span>Цена</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% else %}*/
/*             <a class="sort__item" href="{{ sorts[3].href }}">*/
/*               <span>Цена</span>*/
/*               <i class="icon-arrow-b"></i>*/
/*             </a>*/
/*           {% endif %}*/
/* */
/*       </div>*/
/*       <a class="sort__toggle" href="javascript:void(0)">{{ text_sort }}</a>*/
/*       <div class="products__list product">*/
/*           {% for product in products %}*/
/*             <div class="product__item">*/
/*               <a class="product__wrap" href="{{ product.href }}">*/
/*                 <img class="product__image" src="{{ product.thumb }}" alt="{{ product.name }}">*/
/*                 <div class="product__group">*/
/*                   <div class="product__title">{{ product.name }}</div>*/
/*                   <div class="product__desc">{{ product.description }}</div>*/
/*                   <div class="product__details">*/
/*                     <div class="product__price">{{ product.price }}</div>*/
/* */
/*                       {% for tag in product.tags %}*/
/*                         <div class="product__label product__label--new">{{ tag.tag }}</div>*/
/*                       {% endfor %}*/
/* */
/*                   </div>*/
/*                 </div>*/
/*               </a>*/
/*             </div>*/
/*           {% endfor %}*/
/* */
/*           {% if not products %}*/
/*             <p class="products__empty">{{ text_empty }}</p>*/
/*           {% endif %}*/
/*       </div>*/
/*       <div class="products__pagination_wrap">*/
/*           {{ pagination }}*/
/*       </div>*/
/* */
/*     </div>*/
/*   </section>*/
/* </main>*/
/* */
/* {{ footer }}*/
