<?php

/* default/template/extension/quickcheckout/guest.twig */
class __TwigTemplate_e28b2f89fe767bcb7b945f23020b7453a95cb14ae7165452d0f312a0ab066c17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"contact__block contact__block--name\">
    <div id=\"payment-address\">
        <div class=\"contact__hidden\">

            <select name=\"country_id\" class=\"hide\" id=\"input-payment-country\" class=\"hide\">
                <option value=\"20\" selected></option>
            </select>

            <select name=\"zone_id\" class=\"hide\" id=\"input-payment-zone\">
                <option value=\"339\" selected></option>
            </select>

            <input type=\"text\" name=\"company\" value=\"\" class=\"hide\">
            <input type=\"text\" name=\"customer_group\" value=\"\" class=\"hide\">
            <input type=\"text\" name=\"address_1\" value=\"\" class=\"hide\">
            <input type=\"text\" name=\"address_2\" value=\"\" class=\"hide\">
            <input type=\"text\" name=\"city\" value=\"\" class=\"hide\">
            <input type=\"text\" name=\"postcode\" value=\"\" class=\"hide\">
            <input type=\"checkbox\" name=\"shipping_address\" value=\"1\" id=\"shipping\" checked=\"checked\" class=\"hide\"/>
        </div>
        <div class=\"contact__title\">КОНТАКТНАЯ ИНФОРМАЦИЯ</div>
        <div class=\"input__group\">
            <div class=\"input\">
                <input type=\"text\" name=\"firstname\" required=\"required\" id=\"input-payment-firstname\">
                <label for=\"input-payment-firstname\">Фамилия</label>
                <small>Текст ошибки</small>
            </div>
            <div class=\"input\">
                <input type=\"text\" name=\"lastname\" required=\"required\" id=\"input-payment-lastname\">
                <label for=\"input-payment-lastname\">Имя</label>
                <small>Текст ошибки</small>
            </div>
            <div class=\"input\">
                <input type=\"text\" required=\"required\" id=\"patronymic_name\">
                <label for=\"patronymic_name\">Отчество</label>
                <small>Текст ошибки</small>
            </div>
        </div>
        <div class=\"input__group\">
            <div class=\"input\">
                <input type=\"text\" name=\"telephone\" required=\"required\" id=\"input-payment-telephone\">
                <label for=\"input-payment-telephone\">Телефон</label>
                <small>Текст ошибки</small>
            </div>
            <div class=\"input\">
                <input type=\"text\" name=\"email\" required=\"required\" id=\"input-payment-email\">
                <label for=\"input-payment-email\">E-mail</label>
                <small>Текст ошибки</small>
            </div>
        </div>
    </div>
</div>


<script type=\"text/javascript\"><!--
    \$(document).ready(function () {
        // Sort the custom fields
        \$('#custom-field-payment .custom-field[data-sort]').detach().each(function () {
            if (\$(this).attr('data-sort') >= 0 && \$(this).attr('data-sort') <= \$('#payment-address .col-sm-6').length) {
                \$('#payment-address .col-sm-6').eq(\$(this).attr('data-sort')).before(this);
            }

            if (\$(this).attr('data-sort') > \$('#payment-address .col-sm-6').length) {
                \$('#payment-address .col-sm-6:last').after(this);
            }

            if (\$(this).attr('data-sort') < -\$('#payment-address .col-sm-6').length) {
                \$('#payment-address .col-sm-6:first').before(this);
            }
        });

        \$('#payment-address select[name=\\'customer_group_id\\']').on('change', function () {
            \$.ajax({
                url: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,
                dataType: 'json',
                success: function (json) {
                    \$('#payment-address .custom-field').hide();
                    \$('#payment-address .custom-field').removeClass('required');

                    for (i = 0; i < json.length; i++) {
                        custom_field = json[i];

                        \$('#payment-custom-field' + custom_field['custom_field_id']).show();

                        if (custom_field['required']) {
                            \$('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');
                        } else {
                            \$('#payment-custom-field' + custom_field['custom_field_id']).removeClass('required');
                        }
                    }

                    ";
        // line 92
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 93
            echo "                    \$('#shipping-address .custom-field').hide();
                    \$('#shipping-address .custom-field').removeClass('required');

                    for (i = 0; i < json.length; i++) {
                        custom_field = json[i];

                        \$('#shipping-custom-field' + custom_field['custom_field_id']).show();

                        if (custom_field['required']) {
                            \$('#shipping-custom-field' + custom_field['custom_field_id']).addClass('required');
                        } else {
                            \$('#shipping-custom-field' + custom_field['custom_field_id']).removeClass('required');
                        }
                    }
                    ";
        }
        // line 108
        echo "                },
                ";
        // line 109
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 110
            echo "                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
                ";
        }
        // line 114
        echo "            });
        });

        \$('#payment-address select[name=\\'customer_group_id\\']').trigger('change');

        \$('#payment-address button[id^=\\'button-payment-custom-field\\']').on('click', function () {
            var node = this;

            \$('#form-upload').remove();

            \$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

            \$('#form-upload input[name=\\'file\\']').trigger('click');

            timer = setInterval(function () {
                if (\$('#form-upload input[name=\\'file\\']').val() != '') {
                    clearInterval(timer);

                    \$.ajax({
                        url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData(\$('#form-upload')[0]),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function () {
                            \$(node).button('loading');
                        },
                        complete: function () {
                            \$(node).button('reset');
                        },
                        success: function (json) {
                            \$('.text-danger').remove();

                            if (json['error']) {
                                \$(node).parent().find('input[name^=\\'custom_field\\']').after('<div class=\"text-danger\">' + json['error'] + '</div>');
                            }

                            if (json['success']) {
                                alert(json['success']);

                                \$(node).parent().find('input[name^=\\'custom_field\\']').attr('value', json['file']);
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                        }
                    });
                }
            }, 500);
        });

        \$('#payment-address select[name=\\'country_id\\']').on('change', function () {
            \$.ajax({
                url: 'index.php?route=extension/quickcheckout/checkout/country&country_id=' + this.value,
                dataType: 'json',
                cache: false,
                beforeSend: function () {
                    \$('#payment-address select[name=\\'country_id\\']').after('<i class=\"fa fa-spinner fa-spin\"></i>');
                },
                complete: function () {
                    \$('.fa-spinner').remove();
                },
                success: function (json) {
                    if (json['postcode_required'] == '1') {
                        \$('#payment-postcode-required').addClass('required');
                    } else {
                        \$('#payment-postcode-required').removeClass('required');
                    }

                    var html = '';

                    if (json['zone'] != '') {
                        for (i = 0; i < json['zone'].length; i++) {
                            html += '<option value=\"' + json['zone'][i]['zone_id'] + '\"';

                            if (json['zone'][i]['zone_id'] == '";
        // line 191
        echo (isset($context["zone_id"]) ? $context["zone_id"] : null);
        echo "') {
                                html += ' selected=\"selected\"';
                            }

                            html += '>' + json['zone'][i]['name'] + '</option>';
                        }
                    } else {
                        html += '<option value=\"0\" selected=\"selected\">";
        // line 198
        echo (isset($context["text_none"]) ? $context["text_none"] : null);
        echo "</option>';
                    }

                    \$('#payment-address select[name=\\'zone_id\\']').html(html).trigger('change');
                },
                ";
        // line 203
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 204
            echo "                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                }
                ";
        }
        // line 208
        echo "            });
        });

        \$('#payment-address select[name=\\'country_id\\']').trigger('change');

        ";
        // line 213
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 214
            echo "        // Guest Shipping Form
        \$('#payment-address input[name=\\'shipping_address\\']').on('change', function () {
            if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                \$('#shipping-address').slideUp('slow');

                ";
            // line 219
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 220
                echo "                reloadShippingMethod('payment');
                ";
            }
            // line 222
            echo "            } else {
                \$.ajax({
                    url: 'index.php?route=extension/quickcheckout/guest_shipping&customer_group_id=' + \$('#payment-address select[name=\\'customer_group_id\\']').val(),
                    dataType: 'html',
                    cache: false,
                    beforeSend: function () {
                        // Nothing at the moment
                    },
                    success: function (html) {
                        \$('#shipping-address .quickcheckout-content').html(html);

                        \$('#shipping-address').slideDown('slow');
                    },
                    ";
            // line 235
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 236
                echo "                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
                    }
                    ";
            }
            // line 240
            echo "                });
            }
        });

        ";
            // line 244
            if ((isset($context["shipping_address"]) ? $context["shipping_address"] : null)) {
                // line 245
                echo "        \$('#shipping-address').hide();
        ";
            } else {
                // line 247
                echo "        \$('#payment-address input[name=\\'shipping_address\\']').trigger('change');
        ";
            }
            // line 249
            echo "        ";
        }
        // line 250
        echo "
        \$('#payment-address select[name=\\'zone_id\\']').on('change', function () {
            reloadPaymentMethod();

            ";
        // line 254
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 255
            echo "            if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                reloadShippingMethod('payment');
            }
            ";
        }
        // line 259
        echo "        });

        // Create account
        \$('#payment-address input[name=\\'create_account\\']').on('change', function () {
            if (\$('#payment-address input[name=\\'create_account\\']:checked').val()) {
                \$('#create_account').slideDown('slow');
            } else {
                \$('#create_account').slideUp('slow');
            }
        });

        ";
        // line 270
        if ((((isset($context["create_account"]) ? $context["create_account"] : null) ||  !(isset($context["guest_checkout"]) ? $context["guest_checkout"] : null)) || $this->getAttribute((isset($context["field_register"]) ? $context["field_register"] : null), "required", array()))) {
            // line 271
            echo "        \$('#create_account').show();
        ";
        } else {
            // line 273
            echo "        \$('#create_account').hide();
        ";
        }
        // line 275
        echo "    });
    //--></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/guest.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  348 => 275,  344 => 273,  340 => 271,  338 => 270,  325 => 259,  319 => 255,  317 => 254,  311 => 250,  308 => 249,  304 => 247,  300 => 245,  298 => 244,  292 => 240,  286 => 236,  284 => 235,  269 => 222,  265 => 220,  263 => 219,  256 => 214,  254 => 213,  247 => 208,  241 => 204,  239 => 203,  231 => 198,  221 => 191,  142 => 114,  136 => 110,  134 => 109,  131 => 108,  114 => 93,  112 => 92,  19 => 1,);
    }
}
/* <div class="contact__block contact__block--name">*/
/*     <div id="payment-address">*/
/*         <div class="contact__hidden">*/
/* */
/*             <select name="country_id" class="hide" id="input-payment-country" class="hide">*/
/*                 <option value="20" selected></option>*/
/*             </select>*/
/* */
/*             <select name="zone_id" class="hide" id="input-payment-zone">*/
/*                 <option value="339" selected></option>*/
/*             </select>*/
/* */
/*             <input type="text" name="company" value="" class="hide">*/
/*             <input type="text" name="customer_group" value="" class="hide">*/
/*             <input type="text" name="address_1" value="" class="hide">*/
/*             <input type="text" name="address_2" value="" class="hide">*/
/*             <input type="text" name="city" value="" class="hide">*/
/*             <input type="text" name="postcode" value="" class="hide">*/
/*             <input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" class="hide"/>*/
/*         </div>*/
/*         <div class="contact__title">КОНТАКТНАЯ ИНФОРМАЦИЯ</div>*/
/*         <div class="input__group">*/
/*             <div class="input">*/
/*                 <input type="text" name="firstname" required="required" id="input-payment-firstname">*/
/*                 <label for="input-payment-firstname">Фамилия</label>*/
/*                 <small>Текст ошибки</small>*/
/*             </div>*/
/*             <div class="input">*/
/*                 <input type="text" name="lastname" required="required" id="input-payment-lastname">*/
/*                 <label for="input-payment-lastname">Имя</label>*/
/*                 <small>Текст ошибки</small>*/
/*             </div>*/
/*             <div class="input">*/
/*                 <input type="text" required="required" id="patronymic_name">*/
/*                 <label for="patronymic_name">Отчество</label>*/
/*                 <small>Текст ошибки</small>*/
/*             </div>*/
/*         </div>*/
/*         <div class="input__group">*/
/*             <div class="input">*/
/*                 <input type="text" name="telephone" required="required" id="input-payment-telephone">*/
/*                 <label for="input-payment-telephone">Телефон</label>*/
/*                 <small>Текст ошибки</small>*/
/*             </div>*/
/*             <div class="input">*/
/*                 <input type="text" name="email" required="required" id="input-payment-email">*/
/*                 <label for="input-payment-email">E-mail</label>*/
/*                 <small>Текст ошибки</small>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* */
/* <script type="text/javascript"><!--*/
/*     $(document).ready(function () {*/
/*         // Sort the custom fields*/
/*         $('#custom-field-payment .custom-field[data-sort]').detach().each(function () {*/
/*             if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#payment-address .col-sm-6').length) {*/
/*                 $('#payment-address .col-sm-6').eq($(this).attr('data-sort')).before(this);*/
/*             }*/
/* */
/*             if ($(this).attr('data-sort') > $('#payment-address .col-sm-6').length) {*/
/*                 $('#payment-address .col-sm-6:last').after(this);*/
/*             }*/
/* */
/*             if ($(this).attr('data-sort') < -$('#payment-address .col-sm-6').length) {*/
/*                 $('#payment-address .col-sm-6:first').before(this);*/
/*             }*/
/*         });*/
/* */
/*         $('#payment-address select[name=\'customer_group_id\']').on('change', function () {*/
/*             $.ajax({*/
/*                 url: 'index.php?route=checkout/checkout/customfield&customer_group_id=' + this.value,*/
/*                 dataType: 'json',*/
/*                 success: function (json) {*/
/*                     $('#payment-address .custom-field').hide();*/
/*                     $('#payment-address .custom-field').removeClass('required');*/
/* */
/*                     for (i = 0; i < json.length; i++) {*/
/*                         custom_field = json[i];*/
/* */
/*                         $('#payment-custom-field' + custom_field['custom_field_id']).show();*/
/* */
/*                         if (custom_field['required']) {*/
/*                             $('#payment-custom-field' + custom_field['custom_field_id']).addClass('required');*/
/*                         } else {*/
/*                             $('#payment-custom-field' + custom_field['custom_field_id']).removeClass('required');*/
/*                         }*/
/*                     }*/
/* */
/*                     {% if shipping_required %}*/
/*                     $('#shipping-address .custom-field').hide();*/
/*                     $('#shipping-address .custom-field').removeClass('required');*/
/* */
/*                     for (i = 0; i < json.length; i++) {*/
/*                         custom_field = json[i];*/
/* */
/*                         $('#shipping-custom-field' + custom_field['custom_field_id']).show();*/
/* */
/*                         if (custom_field['required']) {*/
/*                             $('#shipping-custom-field' + custom_field['custom_field_id']).addClass('required');*/
/*                         } else {*/
/*                             $('#shipping-custom-field' + custom_field['custom_field_id']).removeClass('required');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                 },*/
/*                 {% if debug %}*/
/*                 error: function (xhr, ajaxOptions, thrownError) {*/
/*                     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                 }*/
/*                 {% endif %}*/
/*             });*/
/*         });*/
/* */
/*         $('#payment-address select[name=\'customer_group_id\']').trigger('change');*/
/* */
/*         $('#payment-address button[id^=\'button-payment-custom-field\']').on('click', function () {*/
/*             var node = this;*/
/* */
/*             $('#form-upload').remove();*/
/* */
/*             $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* */
/*             $('#form-upload input[name=\'file\']').trigger('click');*/
/* */
/*             timer = setInterval(function () {*/
/*                 if ($('#form-upload input[name=\'file\']').val() != '') {*/
/*                     clearInterval(timer);*/
/* */
/*                     $.ajax({*/
/*                         url: 'index.php?route=tool/upload',*/
/*                         type: 'post',*/
/*                         dataType: 'json',*/
/*                         data: new FormData($('#form-upload')[0]),*/
/*                         cache: false,*/
/*                         contentType: false,*/
/*                         processData: false,*/
/*                         beforeSend: function () {*/
/*                             $(node).button('loading');*/
/*                         },*/
/*                         complete: function () {*/
/*                             $(node).button('reset');*/
/*                         },*/
/*                         success: function (json) {*/
/*                             $('.text-danger').remove();*/
/* */
/*                             if (json['error']) {*/
/*                                 $(node).parent().find('input[name^=\'custom_field\']').after('<div class="text-danger">' + json['error'] + '</div>');*/
/*                             }*/
/* */
/*                             if (json['success']) {*/
/*                                 alert(json['success']);*/
/* */
/*                                 $(node).parent().find('input[name^=\'custom_field\']').attr('value', json['file']);*/
/*                             }*/
/*                         },*/
/*                         error: function (xhr, ajaxOptions, thrownError) {*/
/*                             alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                         }*/
/*                     });*/
/*                 }*/
/*             }, 500);*/
/*         });*/
/* */
/*         $('#payment-address select[name=\'country_id\']').on('change', function () {*/
/*             $.ajax({*/
/*                 url: 'index.php?route=extension/quickcheckout/checkout/country&country_id=' + this.value,*/
/*                 dataType: 'json',*/
/*                 cache: false,*/
/*                 beforeSend: function () {*/
/*                     $('#payment-address select[name=\'country_id\']').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*                 },*/
/*                 complete: function () {*/
/*                     $('.fa-spinner').remove();*/
/*                 },*/
/*                 success: function (json) {*/
/*                     if (json['postcode_required'] == '1') {*/
/*                         $('#payment-postcode-required').addClass('required');*/
/*                     } else {*/
/*                         $('#payment-postcode-required').removeClass('required');*/
/*                     }*/
/* */
/*                     var html = '';*/
/* */
/*                     if (json['zone'] != '') {*/
/*                         for (i = 0; i < json['zone'].length; i++) {*/
/*                             html += '<option value="' + json['zone'][i]['zone_id'] + '"';*/
/* */
/*                             if (json['zone'][i]['zone_id'] == '{{ zone_id }}') {*/
/*                                 html += ' selected="selected"';*/
/*                             }*/
/* */
/*                             html += '>' + json['zone'][i]['name'] + '</option>';*/
/*                         }*/
/*                     } else {*/
/*                         html += '<option value="0" selected="selected">{{ text_none }}</option>';*/
/*                     }*/
/* */
/*                     $('#payment-address select[name=\'zone_id\']').html(html).trigger('change');*/
/*                 },*/
/*                 {% if debug %}*/
/*                 error: function (xhr, ajaxOptions, thrownError) {*/
/*                     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                 }*/
/*                 {% endif %}*/
/*             });*/
/*         });*/
/* */
/*         $('#payment-address select[name=\'country_id\']').trigger('change');*/
/* */
/*         {% if shipping_required %}*/
/*         // Guest Shipping Form*/
/*         $('#payment-address input[name=\'shipping_address\']').on('change', function () {*/
/*             if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                 $('#shipping-address').slideUp('slow');*/
/* */
/*                 {% if shipping_required %}*/
/*                 reloadShippingMethod('payment');*/
/*                 {% endif %}*/
/*             } else {*/
/*                 $.ajax({*/
/*                     url: 'index.php?route=extension/quickcheckout/guest_shipping&customer_group_id=' + $('#payment-address select[name=\'customer_group_id\']').val(),*/
/*                     dataType: 'html',*/
/*                     cache: false,*/
/*                     beforeSend: function () {*/
/*                         // Nothing at the moment*/
/*                     },*/
/*                     success: function (html) {*/
/*                         $('#shipping-address .quickcheckout-content').html(html);*/
/* */
/*                         $('#shipping-address').slideDown('slow');*/
/*                     },*/
/*                     {% if debug %}*/
/*                     error: function (xhr, ajaxOptions, thrownError) {*/
/*                         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*                     }*/
/*                     {% endif %}*/
/*                 });*/
/*             }*/
/*         });*/
/* */
/*         {% if shipping_address %}*/
/*         $('#shipping-address').hide();*/
/*         {% else %}*/
/*         $('#payment-address input[name=\'shipping_address\']').trigger('change');*/
/*         {% endif %}*/
/*         {% endif %}*/
/* */
/*         $('#payment-address select[name=\'zone_id\']').on('change', function () {*/
/*             reloadPaymentMethod();*/
/* */
/*             {% if shipping_required %}*/
/*             if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                 reloadShippingMethod('payment');*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/* */
/*         // Create account*/
/*         $('#payment-address input[name=\'create_account\']').on('change', function () {*/
/*             if ($('#payment-address input[name=\'create_account\']:checked').val()) {*/
/*                 $('#create_account').slideDown('slow');*/
/*             } else {*/
/*                 $('#create_account').slideUp('slow');*/
/*             }*/
/*         });*/
/* */
/*         {% if create_account or not guest_checkout or field_register.required %}*/
/*         $('#create_account').show();*/
/*         {% else %}*/
/*         $('#create_account').hide();*/
/*         {% endif %}*/
/*     });*/
/*     //--></script>*/
/* */
