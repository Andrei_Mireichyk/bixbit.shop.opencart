<?php

/* default/template/extension/module/ocdev_smart_cart/ocdev_smart_cart_voucher.twig */
class __TwigTemplate_998dd2ce805dbe2c953a2955c5a5f0f8fdc69af4995fe3f14a76970a357a95fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<input type=\"text\" name=\"smca_voucher\" value=\"";
        echo (isset($context["voucher"]) ? $context["voucher"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["entry_voucher"]) ? $context["entry_voucher"] : null);
        echo "\" id=\"smca-input-voucher\" />
<input type=\"submit\" value=\"";
        // line 2
        echo (isset($context["button_voucher"]) ? $context["button_voucher"] : null);
        echo "\" id=\"smca-button-voucher\" data-loading-text=\"";
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" class=\"next-step-button\" />
<script type=\"text/javascript\"><!--
\$('#smca-button-voucher').on('click', function() {
  maskElement('#check-data', true);
  \$.ajax({
    url: 'index.php?route=extension/module/ocdev_smart_cart/voucher',
    type: 'post',
    data: 'smca_voucher=' + encodeURIComponent(\$('input[name=\\'smca_voucher\\']').val()),
    dataType: 'json',
    beforeSend: function() {
      \$('#smca-button-voucher').button('loading');
    },
    complete: function() {
      \$('#smca-button-voucher').button('reset');
    },
    success: function(json) {
      \$('.field-error').remove();
      if (json['error']) {
        maskElement('#check-data', false);
        \$('input[name=\\'smca_voucher\\']').addClass('error-style').after('<span class=\"error-text field-error\">' + json['error'] + '</span>');
      } else {
        maskElement('#check-data', false);
        \$('input[name=\\'smca_voucher\\']').removeClass('error-style').after('<span id=\"smca-voucher-success\">' + json['success'] + '</span>').fadeIn();
        \$('#smca-voucher-success').delay(3000).fadeOut();
      }
    }
  });
});
//-->
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/ocdev_smart_cart/ocdev_smart_cart_voucher.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  19 => 1,);
    }
}
/* <input type="text" name="smca_voucher" value="{{ voucher }}" placeholder="{{ entry_voucher }}" id="smca-input-voucher" />*/
/* <input type="submit" value="{{ button_voucher }}" id="smca-button-voucher" data-loading-text="{{ text_loading }}" class="next-step-button" />*/
/* <script type="text/javascript"><!--*/
/* $('#smca-button-voucher').on('click', function() {*/
/*   maskElement('#check-data', true);*/
/*   $.ajax({*/
/*     url: 'index.php?route=extension/module/ocdev_smart_cart/voucher',*/
/*     type: 'post',*/
/*     data: 'smca_voucher=' + encodeURIComponent($('input[name=\'smca_voucher\']').val()),*/
/*     dataType: 'json',*/
/*     beforeSend: function() {*/
/*       $('#smca-button-voucher').button('loading');*/
/*     },*/
/*     complete: function() {*/
/*       $('#smca-button-voucher').button('reset');*/
/*     },*/
/*     success: function(json) {*/
/*       $('.field-error').remove();*/
/*       if (json['error']) {*/
/*         maskElement('#check-data', false);*/
/*         $('input[name=\'smca_voucher\']').addClass('error-style').after('<span class="error-text field-error">' + json['error'] + '</span>');*/
/*       } else {*/
/*         maskElement('#check-data', false);*/
/*         $('input[name=\'smca_voucher\']').removeClass('error-style').after('<span id="smca-voucher-success">' + json['success'] + '</span>').fadeIn();*/
/*         $('#smca-voucher-success').delay(3000).fadeOut();*/
/*       }*/
/*     }*/
/*   });*/
/* });*/
/* //-->*/
/* </script>*/
