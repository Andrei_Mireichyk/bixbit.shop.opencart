<?php

/* default/template/extension/quickcheckout/terms.twig */
class __TwigTemplate_c6f8f0d09d9864369c5af89190ac1ca5efd5c41408e39630985cc7c081b7b53a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"payment\" class=\"text-left\" style=\"display:none;\"></div>


<div class=\"checkbox contact__accept\">
  <input type=\"checkbox\" name=\"agree\"  value=\"1\" id=\"agree_checkbox\">
  <label for=\"agree_checkbox\">";
        // line 6
        echo (isset($context["text_agree"]) ? $context["text_agree"] : null);
        echo "</label>
</div>
<a id=\"button-payment-method\" class=\"btn__primary\" href=\"javascript:void(0)\">";
        // line 8
        echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/terms.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 8,  26 => 6,  19 => 1,);
    }
}
/* <div id="payment" class="text-left" style="display:none;"></div>*/
/* */
/* */
/* <div class="checkbox contact__accept">*/
/*   <input type="checkbox" name="agree"  value="1" id="agree_checkbox">*/
/*   <label for="agree_checkbox">{{ text_agree }}</label>*/
/* </div>*/
/* <a id="button-payment-method" class="btn__primary" href="javascript:void(0)">{{ button_continue }}</a>*/
/* */
