<?php

/* default/template/common/menu.twig */
class __TwigTemplate_c6cdadf0fddd76a6da37c317d7f83c57322b78b1b986d34ad627e80836b67273 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["categories"]) ? $context["categories"] : null)) {
            // line 2
            echo "    <ul class=\"category\">
        ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 4
                echo "
            ";
                // line 5
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 6
                    echo "                <li>
                    <a class=\"category__item\" href=\"";
                    // line 7
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
                    <ul class=\"subcategory\">


                        ";
                    // line 11
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 12
                        echo "                            <li class=\"subcategory__item\"><a class=\"subcategory__link\" href=\"";
                        echo $this->getAttribute($context["child"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["child"], "name", array());
                        echo "</a></li>
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 14
                    echo "
                    </ul>
                </li>
            ";
                } else {
                    // line 18
                    echo "                <li><a class=\"category__item\" href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
            ";
                }
                // line 20
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    </ul>
    <div class=\"category__bg-mobile\"></div>
";
        }
    }

    public function getTemplateName()
    {
        return "default/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 21,  74 => 20,  66 => 18,  60 => 14,  49 => 12,  45 => 11,  36 => 7,  33 => 6,  31 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if categories %}*/
/*     <ul class="category">*/
/*         {% for category in categories %}*/
/* */
/*             {% if category.children %}*/
/*                 <li>*/
/*                     <a class="category__item" href="{{ category.href }}">{{ category.name }}</a>*/
/*                     <ul class="subcategory">*/
/* */
/* */
/*                         {% for child in category.children %}*/
/*                             <li class="subcategory__item"><a class="subcategory__link" href="{{ child.href }}">{{ child.name }}</a></li>*/
/*                         {% endfor %}*/
/* */
/*                     </ul>*/
/*                 </li>*/
/*             {% else %}*/
/*                 <li><a class="category__item" href="{{ category.href }}">{{ category.name }}</a></li>*/
/*             {% endif %}*/
/*         {% endfor %}*/
/*     </ul>*/
/*     <div class="category__bg-mobile"></div>*/
/* {% endif %}*/
/* */
