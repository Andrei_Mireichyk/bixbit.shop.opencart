<?php

/* default/template/extension/module/d_ajax_search.twig */
class __TwigTemplate_410243457400d7172e8382454e875f07a462851344a65e8c6c1dc09397835513 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script>
\tfunction text_complite(ev, keywords) {
\t\tif (ev.keyCode == 38 || ev.keyCode == 40) {
\t\t\treturn false;
\t\t}
\t\tif (keywords == '' || keywords.length < 1 || keywords.length < ";
        // line 6
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "max_symbols", array(), "array");
        echo " ) {
\t\t\treturn false;
\t\t}

\t\t\$.ajax({
\t\t\turl: \$('base').attr('href') + 'index.php?route=extension/module/d_ajax_search/getAutocomplite&keyword=' + keywords,
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function () {
\t\t\t},
\t\t\tsuccess: function (autocomplite) {
\t\t\t\t\$('#search-autocomplite').text('');
\t\t\t\t\$('#help').hide();
\t\t\t\tif (typeof autocomplite != 'undefined' && autocomplite != null) {
\t\t\t\t\t\$(\"";
        // line 19
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "\").first().val().toLowerCase();
\t\t\t\t\tif (autocomplite != '' && autocomplite.indexOf(\$(\"";
        // line 20
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "\").first().val()) !== -1) {
\t\t\t\t\t\t\$('#search-autocomplite').text(autocomplite.toLowerCase());
\t\t\t\t\t}

\t\t\t\t\t\$(\"";
        // line 24
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "\").keydown(function (event) {
\t\t\t\t\t\tif (event.keyCode == 39) {
\t\t\t\t\t\t\t\$(\"";
        // line 26
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "\").val(autocomplite);
\t\t\t\t\t\t\t\$('#search-autocomplite').text('');
\t\t\t\t\t\t} else if (event.keyCode == 08){
\t\t\t\t\t\t\t\$('#search-autocomplite').text('');
\t\t\t\t\t\t}
\t\t\t\t\t});

\t\t\t\t}
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\tconsole.log(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t}
\t\t});
\t}

\tfunction doquick_search(ev, keywords) {

\t\tif (ev.keyCode == 38 || ev.keyCode == 40) {
\t\t\treturn false;
\t\t}

        var search_status = document.querySelector('.search__status');
        var search_result = document.querySelector('.search__results');

\t\tupdown = -1;

\t\tif (keywords == '' || keywords.length < 1 || keywords.length < ";
        // line 52
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "max_symbols", array(), "array");
        echo " ) {
\t\t\treturn false;
\t\t}
\t\tkeywords = encodeURI(keywords);

\t\t\$.ajax({
\t\t\turl: \$('base').attr('href') + 'index.php?route=extension/module/d_ajax_search/searchresults&keyword=' + keywords,
\t\t\tdataType: 'json',
\t\t\tbeforeSend: function () {
                search_status.innerHTML = 'Загрузка...';
\t\t\t},

\t\t\tsuccess: function (results) {

                search_result.innerHTML = '';

\t\t\t\tvar result = \$.map(results, function (value, index) {
\t\t\t\t\treturn [value];
\t\t\t\t});

\t\t\t\tif (typeof result !== 'undefined' && result.length > 0) {

                    search_status.innerHTML = 'Результатов: '+result.length;

                    for (var item in result) {

                        if(result.hasOwnProperty(item)){
                            var product = result[item];
                        }else{
                            console.error('Продукт не найден');
                        }

                        var html_item = '<a class=\"search__item\" href=\"' + product.href + '\">';
                        html_item += '<img class=\"search__img\"   src=\"' + product.image + '\">';
                        html_item += '<span class=\"search__title\">' + product.name + '</span>';
                        html_item += '<span class=\"search__price\">' + product.price + '</span>';
                        html_item += '</a>';

                        search_result.innerHTML += html_item;
                    }

\t\t\t\t} else {
                    search_status.innerHTML = 'Нет результатов ;(';
\t\t\t\t}
\t\t\t},
\t\t\terror: function (xhr, ajaxOptions, thrownError) {
\t\t\t\tconsole.log('error');
\t\t\t}
\t\t});
\t\treturn true;
\t}

\tvar delay = (function () {
\t\tvar timer = 0;
\t\treturn function (callback, ms) {
\t\t\tclearTimeout(timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();


\t\$(document).ready(function () {

\t\t\$(document).on('blur', '";
        // line 115
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "', function (event) {
\t\t\tsetTimeout(function () {
\t\t\t\t\$('body').click(function (event) {
\t\t\t\t\tif (\$(event.target).attr('class') != 'all_results hidden') {
\t\t\t\t\t\t\$('#d_ajax_search_results').remove();
\t\t\t\t\t}
\t\t\t\t});
\t\t\t\t\$('#help').show();
\t\t\t\tif (";
        // line 123
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "block_result", array(), "array");
        echo ") {
\t\t\t\t\tupdown = 1;
\t\t\t\t} else {
\t\t\t\t\tupdown = 0;
\t\t\t\t}
\t\t\t}, 500);
\t\t});

\t\t\$(document).on('keyup', '";
        // line 131
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "', function (ev) {
\t\t\tvar a = ev;
\t\t\tvar b = this.value;
\t\t\ttext_complite(a, b);
\t\t\tdelay(function () {
\t\t\t\tdoquick_search(a, b);
\t\t\t}, 500);
\t\t});

\t\t\$(document).on('focus', '";
        // line 140
        echo $this->getAttribute((isset($context["setting"]) ? $context["setting"] : null), "class", array(), "array");
        echo "', function (ev) {
\t\t\tvar a = ev;
\t\t\tvar b = this.value;
\t\t\ttext_complite(a, b);
\t\t\tdelay(function () {
\t\t\t\tdoquick_search(a, b);
\t\t\t}, 500);
\t\t});
\t});
</script>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/d_ajax_search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 140,  175 => 131,  164 => 123,  153 => 115,  87 => 52,  58 => 26,  53 => 24,  46 => 20,  42 => 19,  26 => 6,  19 => 1,);
    }
}
/* <script>*/
/* 	function text_complite(ev, keywords) {*/
/* 		if (ev.keyCode == 38 || ev.keyCode == 40) {*/
/* 			return false;*/
/* 		}*/
/* 		if (keywords == '' || keywords.length < 1 || keywords.length < {{ setting['max_symbols'] }} ) {*/
/* 			return false;*/
/* 		}*/
/* */
/* 		$.ajax({*/
/* 			url: $('base').attr('href') + 'index.php?route=extension/module/d_ajax_search/getAutocomplite&keyword=' + keywords,*/
/* 			dataType: 'json',*/
/* 			beforeSend: function () {*/
/* 			},*/
/* 			success: function (autocomplite) {*/
/* 				$('#search-autocomplite').text('');*/
/* 				$('#help').hide();*/
/* 				if (typeof autocomplite != 'undefined' && autocomplite != null) {*/
/* 					$("{{ setting['class'] }}").first().val().toLowerCase();*/
/* 					if (autocomplite != '' && autocomplite.indexOf($("{{ setting['class'] }}").first().val()) !== -1) {*/
/* 						$('#search-autocomplite').text(autocomplite.toLowerCase());*/
/* 					}*/
/* */
/* 					$("{{ setting['class'] }}").keydown(function (event) {*/
/* 						if (event.keyCode == 39) {*/
/* 							$("{{ setting['class'] }}").val(autocomplite);*/
/* 							$('#search-autocomplite').text('');*/
/* 						} else if (event.keyCode == 08){*/
/* 							$('#search-autocomplite').text('');*/
/* 						}*/
/* 					});*/
/* */
/* 				}*/
/* 			},*/
/* 			error: function (xhr, ajaxOptions, thrownError) {*/
/* 				console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 			}*/
/* 		});*/
/* 	}*/
/* */
/* 	function doquick_search(ev, keywords) {*/
/* */
/* 		if (ev.keyCode == 38 || ev.keyCode == 40) {*/
/* 			return false;*/
/* 		}*/
/* */
/*         var search_status = document.querySelector('.search__status');*/
/*         var search_result = document.querySelector('.search__results');*/
/* */
/* 		updown = -1;*/
/* */
/* 		if (keywords == '' || keywords.length < 1 || keywords.length < {{ setting['max_symbols'] }} ) {*/
/* 			return false;*/
/* 		}*/
/* 		keywords = encodeURI(keywords);*/
/* */
/* 		$.ajax({*/
/* 			url: $('base').attr('href') + 'index.php?route=extension/module/d_ajax_search/searchresults&keyword=' + keywords,*/
/* 			dataType: 'json',*/
/* 			beforeSend: function () {*/
/*                 search_status.innerHTML = 'Загрузка...';*/
/* 			},*/
/* */
/* 			success: function (results) {*/
/* */
/*                 search_result.innerHTML = '';*/
/* */
/* 				var result = $.map(results, function (value, index) {*/
/* 					return [value];*/
/* 				});*/
/* */
/* 				if (typeof result !== 'undefined' && result.length > 0) {*/
/* */
/*                     search_status.innerHTML = 'Результатов: '+result.length;*/
/* */
/*                     for (var item in result) {*/
/* */
/*                         if(result.hasOwnProperty(item)){*/
/*                             var product = result[item];*/
/*                         }else{*/
/*                             console.error('Продукт не найден');*/
/*                         }*/
/* */
/*                         var html_item = '<a class="search__item" href="' + product.href + '">';*/
/*                         html_item += '<img class="search__img"   src="' + product.image + '">';*/
/*                         html_item += '<span class="search__title">' + product.name + '</span>';*/
/*                         html_item += '<span class="search__price">' + product.price + '</span>';*/
/*                         html_item += '</a>';*/
/* */
/*                         search_result.innerHTML += html_item;*/
/*                     }*/
/* */
/* 				} else {*/
/*                     search_status.innerHTML = 'Нет результатов ;(';*/
/* 				}*/
/* 			},*/
/* 			error: function (xhr, ajaxOptions, thrownError) {*/
/* 				console.log('error');*/
/* 			}*/
/* 		});*/
/* 		return true;*/
/* 	}*/
/* */
/* 	var delay = (function () {*/
/* 		var timer = 0;*/
/* 		return function (callback, ms) {*/
/* 			clearTimeout(timer);*/
/* 			timer = setTimeout(callback, ms);*/
/* 		};*/
/* 	})();*/
/* */
/* */
/* 	$(document).ready(function () {*/
/* */
/* 		$(document).on('blur', '{{ setting['class'] }}', function (event) {*/
/* 			setTimeout(function () {*/
/* 				$('body').click(function (event) {*/
/* 					if ($(event.target).attr('class') != 'all_results hidden') {*/
/* 						$('#d_ajax_search_results').remove();*/
/* 					}*/
/* 				});*/
/* 				$('#help').show();*/
/* 				if ({{ setting['block_result'] }}) {*/
/* 					updown = 1;*/
/* 				} else {*/
/* 					updown = 0;*/
/* 				}*/
/* 			}, 500);*/
/* 		});*/
/* */
/* 		$(document).on('keyup', '{{ setting['class'] }}', function (ev) {*/
/* 			var a = ev;*/
/* 			var b = this.value;*/
/* 			text_complite(a, b);*/
/* 			delay(function () {*/
/* 				doquick_search(a, b);*/
/* 			}, 500);*/
/* 		});*/
/* */
/* 		$(document).on('focus', '{{ setting['class'] }}', function (ev) {*/
/* 			var a = ev;*/
/* 			var b = this.value;*/
/* 			text_complite(a, b);*/
/* 			delay(function () {*/
/* 				doquick_search(a, b);*/
/* 			}, 500);*/
/* 		});*/
/* 	});*/
/* </script>*/
