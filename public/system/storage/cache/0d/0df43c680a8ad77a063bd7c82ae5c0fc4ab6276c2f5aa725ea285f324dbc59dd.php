<?php

/* default/template/information/contact.twig */
class __TwigTemplate_67410959ca0d822bf97ec6d99dcdde44f149c7f233d19c7fcd804911caa9b110 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"contacts\">
    <div class=\"contacts__wrap\">
        <h1 class=\"contacts__title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
        <h2 class=\"contacts__subtitle\">Вопросы, замечания, комментарии?</h2>
        <div class=\"contacts__group\">
            <ul class=\"contacts__list\">
                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["emails"]) ? $context["emails"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 17
            echo "                    <li class=\"contacts__item\"><a class=\"contacts__link\" href=\"mailto:";
            echo $context["email"];
            echo "\"><i
                                    class=\"icon-email-c\"></i><span>";
            // line 18
            echo $context["email"];
            echo "</span></a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["telephones"]) ? $context["telephones"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["phone"]) {
            // line 21
            echo "                    <li class=\"contacts__item\"><a class=\"contacts__link\" href=\"tel:";
            echo $context["phone"];
            echo "\"><i
                                    class=\"icon-phone-c\"></i><span>";
            // line 22
            echo $context["phone"];
            echo "</span></a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['phone'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "            </ul>
            <form action=\"";
        // line 25
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" class=\"contacts__form\">
                <div class=\"input__group\">
                    <div class=\"input ";
        // line 27
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            echo " has-error ";
        }
        echo "\">
                        <input type=\"text\" required=\"required\" id=\"name\" value=\"";
        // line 28
        echo (isset($context["name"]) ? $context["name"] : null);
        echo "\" name=\"name\">
                        <label for=\"name\">";
        // line 29
        echo (isset($context["entry_name"]) ? $context["entry_name"] : null);
        echo "</label>
                        ";
        // line 30
        if ((isset($context["error_name"]) ? $context["error_name"] : null)) {
            // line 31
            echo "                            <small>";
            echo (isset($context["error_name"]) ? $context["error_name"] : null);
            echo "</small>
                        ";
        }
        // line 33
        echo "                    </div>
                    <div class=\"input ";
        // line 34
        if ((isset($context["error_email"]) ? $context["error_email"] : null)) {
            echo " has-error ";
        }
        echo "\">
                        <input type=\"text\" required=\"required\" id=\"email\" value=\"";
        // line 35
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "\" name=\"email\">
                        <label for=\"email\">";
        // line 36
        echo (isset($context["entry_email"]) ? $context["entry_email"] : null);
        echo "</label>
                        ";
        // line 37
        if ((isset($context["error_email"]) ? $context["error_email"] : null)) {
            // line 38
            echo "                            <small>";
            echo (isset($context["error_email"]) ? $context["error_email"] : null);
            echo "</small>
                        ";
        }
        // line 40
        echo "                    </div>
                </div>
                <div class=\"textarea__group\">
                    <div class=\"textarea ";
        // line 43
        if ((isset($context["error_enquiry"]) ? $context["error_enquiry"] : null)) {
            echo " has-error ";
        }
        echo "\">
                        <textarea name=\"enquiry\"required=\"required\" id=\"enquiry\">";
        // line 44
        echo (isset($context["enquiry"]) ? $context["enquiry"] : null);
        echo "</textarea>
                        <label for=\"enquiry\">";
        // line 45
        echo (isset($context["entry_enquiry"]) ? $context["entry_enquiry"] : null);
        echo "</label>
                        ";
        // line 46
        if ((isset($context["error_enquiry"]) ? $context["error_enquiry"] : null)) {
            // line 47
            echo "                            <small>";
            echo (isset($context["error_enquiry"]) ? $context["error_enquiry"] : null);
            echo "</small>
                        ";
        }
        // line 49
        echo "                    </div>
                </div>
                <div class=\"contacts__action\">
                    ";
        // line 52
        echo (isset($context["captcha"]) ? $context["captcha"] : null);
        echo "
                    <button class=\"btn btn__dark\">";
        // line 53
        echo (isset($context["button_submit"]) ? $context["button_submit"] : null);
        echo "</button>
                </div>
            </form>
        </div>
    </div>
</main>
";
        // line 59
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"catalog/view/theme/default/stylesheet/contacts.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "    <script src=\"catalog/view/theme/default/js/contacts.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/information/contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 7,  191 => 6,  186 => 4,  183 => 3,  177 => 59,  168 => 53,  164 => 52,  159 => 49,  153 => 47,  151 => 46,  147 => 45,  143 => 44,  137 => 43,  132 => 40,  126 => 38,  124 => 37,  120 => 36,  116 => 35,  110 => 34,  107 => 33,  101 => 31,  99 => 30,  95 => 29,  91 => 28,  85 => 27,  80 => 25,  77 => 24,  69 => 22,  64 => 21,  59 => 20,  51 => 18,  46 => 17,  42 => 16,  35 => 12,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/contacts.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/contacts.bundle.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* <main class="contacts">*/
/*     <div class="contacts__wrap">*/
/*         <h1 class="contacts__title">{{ heading_title }}</h1>*/
/*         <h2 class="contacts__subtitle">Вопросы, замечания, комментарии?</h2>*/
/*         <div class="contacts__group">*/
/*             <ul class="contacts__list">*/
/*                 {% for email in emails %}*/
/*                     <li class="contacts__item"><a class="contacts__link" href="mailto:{{ email }}"><i*/
/*                                     class="icon-email-c"></i><span>{{ email }}</span></a></li>*/
/*                 {% endfor %}*/
/*                 {% for phone in telephones %}*/
/*                     <li class="contacts__item"><a class="contacts__link" href="tel:{{ phone }}"><i*/
/*                                     class="icon-phone-c"></i><span>{{ phone }}</span></a></li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*             <form action="{{ action }}" method="post" enctype="multipart/form-data" class="contacts__form">*/
/*                 <div class="input__group">*/
/*                     <div class="input {% if error_name %} has-error {% endif %}">*/
/*                         <input type="text" required="required" id="name" value="{{ name }}" name="name">*/
/*                         <label for="name">{{ entry_name }}</label>*/
/*                         {% if error_name %}*/
/*                             <small>{{ error_name }}</small>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                     <div class="input {% if error_email %} has-error {% endif %}">*/
/*                         <input type="text" required="required" id="email" value="{{ email }}" name="email">*/
/*                         <label for="email">{{ entry_email }}</label>*/
/*                         {% if error_email %}*/
/*                             <small>{{ error_email }}</small>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="textarea__group">*/
/*                     <div class="textarea {% if error_enquiry %} has-error {% endif %}">*/
/*                         <textarea name="enquiry"required="required" id="enquiry">{{ enquiry }}</textarea>*/
/*                         <label for="enquiry">{{ entry_enquiry }}</label>*/
/*                         {% if error_enquiry %}*/
/*                             <small>{{ error_enquiry }}</small>*/
/*                         {% endif %}*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="contacts__action">*/
/*                     {{ captcha }}*/
/*                     <button class="btn btn__dark">{{ button_submit }}</button>*/
/*                 </div>*/
/*             </form>*/
/*         </div>*/
/*     </div>*/
/* </main>*/
/* {{ footer }}*/
/* */
