<?php

/* default/template/extension/module/banner.twig */
class __TwigTemplate_91cb7105eedb8247e4486e2281c2d663bd65f1d9656d1c752dd6749f9cbf541f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        if (($this->getAttribute($this->getAttribute((isset($context["banners"]) ? $context["banners"] : null), 0, array(), "array"), "name", array()) == "HOME_TOP")) {
            // line 3
            echo "    ";
            $context["class"] = "home__one";
        } elseif (($this->getAttribute($this->getAttribute(        // line 4
(isset($context["banners"]) ? $context["banners"] : null), 0, array(), "array"), "name", array()) == "HOME_CENTER")) {
            // line 5
            echo "    ";
            $context["class"] = "home__two";
        } elseif (($this->getAttribute($this->getAttribute(        // line 6
(isset($context["banners"]) ? $context["banners"] : null), 0, array(), "array"), "name", array()) == "HOME_BOTTOM")) {
            // line 7
            echo "    ";
            $context["class"] = "home__three";
        } else {
            // line 9
            echo "    ";
            $context["class"] = "";
        }
        // line 11
        echo "
<section class=\"";
        // line 12
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
    ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) ? $context["banners"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 14
            echo "        ";
            if (twig_in_filter($this->getAttribute($context["banner"], "name", array()), array(0 => "HOME_TOP", 1 => "HOME_BOTTOM"))) {
                // line 15
                echo "            <a href=\"";
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">
                <img class=\"home__img\" src=\"";
                // line 16
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\">
            </a>
            <div class=\"home__title\">";
                // line 18
                echo $this->getAttribute($context["banner"], "title", array());
                echo "</div>
            <p class=\"home__desc\">";
                // line 19
                echo $this->getAttribute($context["banner"], "description", array());
                echo "</p>
            <div class=\"home__action\">
                <a class=\"btn btn__primary\" href=\"";
                // line 21
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">КУПИТЬ</a>
                <a class=\"btn btn__default\" href=\"";
                // line 22
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">ПОДРОБНЕЕ</a>
            </div>
        ";
            }
            // line 25
            echo "
        ";
            // line 26
            if (($this->getAttribute($context["banner"], "name", array()) == "HOME_CENTER")) {
                // line 27
                echo "            <div class=\"home__poster\">
                <a href=\"";
                // line 28
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">
                    <img class=\"home__img\" src=\"";
                // line 29
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\">
                </a>

                <div class=\"home__title\">";
                // line 32
                echo $this->getAttribute($context["banner"], "title", array());
                echo "</div>

                <p class=\"home__desc\">";
                // line 34
                echo $this->getAttribute($context["banner"], "description", array());
                echo "</p>

                <div class=\"home__action\">
                    <a class=\"btn btn__primary-outline\" href=\"";
                // line 37
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\">КУПИТЬ</a>
                </div>
            </div>
        ";
            }
            // line 41
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "</section>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/banner.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 42,  125 => 41,  118 => 37,  112 => 34,  107 => 32,  99 => 29,  95 => 28,  92 => 27,  90 => 26,  87 => 25,  81 => 22,  77 => 21,  72 => 19,  68 => 18,  61 => 16,  56 => 15,  53 => 14,  49 => 13,  45 => 12,  42 => 11,  38 => 9,  34 => 7,  32 => 6,  29 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* */
/* {% if  banners[0].name == 'HOME_TOP' %}*/
/*     {% set class = 'home__one' %}*/
/* {% elseif  banners[0].name == 'HOME_CENTER' %}*/
/*     {% set class = 'home__two' %}*/
/* {% elseif  banners[0].name == 'HOME_BOTTOM' %}*/
/*     {% set class = 'home__three' %}*/
/* {% else %}*/
/*     {% set class = '' %}*/
/* {% endif %}*/
/* */
/* <section class="{{ class }}">*/
/*     {% for banner in banners %}*/
/*         {% if  banner.name in ['HOME_TOP','HOME_BOTTOM']%}*/
/*             <a href="{{ banner.link }}">*/
/*                 <img class="home__img" src="{{ banner.image }}" alt="{{ banner.title }}">*/
/*             </a>*/
/*             <div class="home__title">{{ banner.title }}</div>*/
/*             <p class="home__desc">{{ banner.description }}</p>*/
/*             <div class="home__action">*/
/*                 <a class="btn btn__primary" href="{{ banner.link }}">КУПИТЬ</a>*/
/*                 <a class="btn btn__default" href="{{ banner.link }}">ПОДРОБНЕЕ</a>*/
/*             </div>*/
/*         {% endif %}*/
/* */
/*         {% if banner.name == 'HOME_CENTER' %}*/
/*             <div class="home__poster">*/
/*                 <a href="{{ banner.link }}">*/
/*                     <img class="home__img" src="{{ banner.image }}" alt="{{ banner.title }}">*/
/*                 </a>*/
/* */
/*                 <div class="home__title">{{ banner.title }}</div>*/
/* */
/*                 <p class="home__desc">{{ banner.description }}</p>*/
/* */
/*                 <div class="home__action">*/
/*                     <a class="btn btn__primary-outline" href="{{ banner.link }}">КУПИТЬ</a>*/
/*                 </div>*/
/*             </div>*/
/*         {% endif %}*/
/*     {% endfor %}*/
/* </section>*/
