<?php

/* default/template/information/sitemap.twig */
class __TwigTemplate_350603cf431290ae6a05dd4fb70290f755c7da8d3c38aa6c930d74b30672047d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"site_map\">
    <div class=\"site_map__wrap\">
        <h1 class=\"site_map__title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
        <div class=\"site_map__col\">

            <ul class=\"site_map__list\">
                ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category_1"]) {
            // line 17
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["category_1"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["category_1"], "name", array());
            echo "</a>
                        ";
            // line 18
            if ($this->getAttribute($context["category_1"], "children", array())) {
                // line 19
                echo "                            <ul class=\"site_map__list\">
                                ";
                // line 20
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category_1"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["category_2"]) {
                    // line 21
                    echo "                                    <li><a href=\"";
                    echo $this->getAttribute($context["category_2"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category_2"], "name", array());
                    echo "</a>
                                        ";
                    // line 22
                    if ($this->getAttribute($context["category_2"], "children", array())) {
                        // line 23
                        echo "                                            <ul>
                                                ";
                        // line 24
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category_2"], "children", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["category_3"]) {
                            // line 25
                            echo "                                                    <li><a href=\"";
                            echo $this->getAttribute($context["category_3"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["category_3"], "name", array());
                            echo "</a></li>
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_3'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 27
                        echo "                                            </ul>
                                        ";
                    }
                    // line 29
                    echo "                                    </li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_2'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "                            </ul>
                        ";
            }
            // line 33
            echo "                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category_1'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            </ul>
        </div>
        <div class=\"site_map__col\">
            <ul class=\"site_map__list\">
                ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 40
            echo "                    <li><a href=\"";
            echo $this->getAttribute($context["information"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["information"], "title", array());
            echo "</a></li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "                <li><a href=\"";
        echo (isset($context["checkout"]) ? $context["checkout"] : null);
        echo "\">";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</a></li>
                <li><a href=\"";
        // line 43
        echo (isset($context["special"]) ? $context["special"] : null);
        echo "\">";
        echo (isset($context["text_special"]) ? $context["text_special"] : null);
        echo "</a></li>
                <li><a href=\"";
        // line 44
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>

            </ul>
        </div>
    </div>
</main>
";
        // line 50
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"catalog/view/theme/default/stylesheet/site_map.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "    <script src=\"catalog/view/theme/default/js/site_map.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/information/sitemap.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 7,  168 => 6,  163 => 4,  160 => 3,  156 => 50,  145 => 44,  139 => 43,  132 => 42,  121 => 40,  117 => 39,  111 => 35,  104 => 33,  100 => 31,  93 => 29,  89 => 27,  78 => 25,  74 => 24,  71 => 23,  69 => 22,  62 => 21,  58 => 20,  55 => 19,  53 => 18,  46 => 17,  42 => 16,  35 => 12,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/site_map.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/site_map.bundle.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* <main class="site_map">*/
/*     <div class="site_map__wrap">*/
/*         <h1 class="site_map__title">{{ heading_title }}</h1>*/
/*         <div class="site_map__col">*/
/* */
/*             <ul class="site_map__list">*/
/*                 {% for category_1 in categories %}*/
/*                     <li><a href="{{ category_1.href }}">{{ category_1.name }}</a>*/
/*                         {% if category_1.children %}*/
/*                             <ul class="site_map__list">*/
/*                                 {% for category_2 in category_1.children %}*/
/*                                     <li><a href="{{ category_2.href }}">{{ category_2.name }}</a>*/
/*                                         {% if category_2.children %}*/
/*                                             <ul>*/
/*                                                 {% for category_3 in category_2.children %}*/
/*                                                     <li><a href="{{ category_3.href }}">{{ category_3.name }}</a></li>*/
/*                                                 {% endfor %}*/
/*                                             </ul>*/
/*                                         {% endif %}*/
/*                                     </li>*/
/*                                 {% endfor %}*/
/*                             </ul>*/
/*                         {% endif %}*/
/*                     </li>*/
/*                 {% endfor %}*/
/*             </ul>*/
/*         </div>*/
/*         <div class="site_map__col">*/
/*             <ul class="site_map__list">*/
/*                 {% for information in informations %}*/
/*                     <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*                 {% endfor %}*/
/*                 <li><a href="{{ checkout }}">{{ text_checkout }}</a></li>*/
/*                 <li><a href="{{ special }}">{{ text_special }}</a></li>*/
/*                 <li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/* */
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/* </main>*/
/* {{ footer }}*/
