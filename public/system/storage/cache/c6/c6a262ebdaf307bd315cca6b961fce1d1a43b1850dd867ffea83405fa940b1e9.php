<?php

/* extension/feed/google_base_category.twig */
class __TwigTemplate_5a3e21c6ccb8a4fb5e24062084666b2563a2f34bcdd34be89d4afa52d2adf01d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table table-bordered\">
  <thead>
    <tr>
      <td class=\"text-left\">";
        // line 4
        echo (isset($context["column_google_category"]) ? $context["column_google_category"] : null);
        echo "</td>
      <td class=\"text-left\">";
        // line 5
        echo (isset($context["column_category"]) ? $context["column_category"] : null);
        echo "</td>
      <td class=\"text-right\">";
        // line 6
        echo (isset($context["column_action"]) ? $context["column_action"] : null);
        echo "</td>
    </tr>
  </thead>
  <tbody>
    ";
        // line 10
        if ((isset($context["google_base_categories"]) ? $context["google_base_categories"] : null)) {
            // line 11
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["google_base_categories"]) ? $context["google_base_categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["google_base_category"]) {
                // line 12
                echo "    <tr>
      <td class=\"text-left\">";
                // line 13
                echo $this->getAttribute($context["google_base_category"], "google_base_category", array());
                echo "</td>
      <td class=\"text-left\">";
                // line 14
                echo $this->getAttribute($context["google_base_category"], "category", array());
                echo "</td>
      <td class=\"text-right\"><button type=\"button\" value=\"";
                // line 15
                echo $this->getAttribute($context["google_base_category"], "category_id", array());
                echo "\" data-loading-text=\"";
                echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger\"><i class=\"fa fa-minus-circle\"></i></button></td>
    </tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['google_base_category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "    ";
        } else {
            // line 19
            echo "    <tr>
      <td class=\"text-center\" colspan=\"3\">";
            // line 20
            echo (isset($context["text_no_results"]) ? $context["text_no_results"] : null);
            echo "</td>
    </tr>
    ";
        }
        // line 23
        echo "  </tbody>
</table>
<div class=\"row\">
  <div class=\"col-sm-6 text-left\">";
        // line 26
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "</div>
  <div class=\"col-sm-6 text-right\">";
        // line 27
        echo (isset($context["results"]) ? $context["results"] : null);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "extension/feed/google_base_category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 27,  87 => 26,  82 => 23,  76 => 20,  73 => 19,  70 => 18,  57 => 15,  53 => 14,  49 => 13,  46 => 12,  41 => 11,  39 => 10,  32 => 6,  28 => 5,  24 => 4,  19 => 1,);
    }
}
/* <table class="table table-bordered">*/
/*   <thead>*/
/*     <tr>*/
/*       <td class="text-left">{{ column_google_category }}</td>*/
/*       <td class="text-left">{{ column_category }}</td>*/
/*       <td class="text-right">{{ column_action }}</td>*/
/*     </tr>*/
/*   </thead>*/
/*   <tbody>*/
/*     {% if google_base_categories %}*/
/*     {% for google_base_category in google_base_categories %}*/
/*     <tr>*/
/*       <td class="text-left">{{ google_base_category.google_base_category }}</td>*/
/*       <td class="text-left">{{ google_base_category.category }}</td>*/
/*       <td class="text-right"><button type="button" value="{{ google_base_category.category_id }}" data-loading-text="{{ text_loading }}" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>*/
/*     </tr>*/
/*     {% endfor %}*/
/*     {% else %}*/
/*     <tr>*/
/*       <td class="text-center" colspan="3">{{ text_no_results }}</td>*/
/*     </tr>*/
/*     {% endif %}*/
/*   </tbody>*/
/* </table>*/
/* <div class="row">*/
/*   <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*   <div class="col-sm-6 text-right">{{ results }}</div>*/
/* </div>*/
/* */
