<?php

/* default/template/extension/module/bestseller_home.twig */
class __TwigTemplate_6555b4177e93f6084bee819e08e20d09699d03c701425928367b0fb168780991 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"home__bestsellers\">
  <div class=\"home__title home__title--bestsellers\">";
        // line 2
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</div>
  <div class=\"bestsellers\">
    <div class=\"bestsellers__wrap\">
        ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 6
            echo "          <a class=\"bestsellers__item\" href=\"";
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">
            <img class=\"bestsellers__img\" src=\"";
            // line 7
            echo $this->getAttribute($context["product"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["product"], "name", array());
            echo "\">
            <div class=\"bestsellers__title\">";
            // line 8
            echo $this->getAttribute($context["product"], "name", array());
            echo "</div>
            <div class=\"bestsellers__desc\">";
            // line 9
            echo $this->getAttribute($context["product"], "description", array());
            echo "</div>
            <div class=\"bestsellers__price\">";
            // line 10
            echo $this->getAttribute($context["product"], "price", array());
            echo "</div>
          </a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "    </div>
  </div>
</section>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/bestseller_home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 13,  53 => 10,  49 => 9,  45 => 8,  37 => 7,  32 => 6,  28 => 5,  22 => 2,  19 => 1,);
    }
}
/* <section class="home__bestsellers">*/
/*   <div class="home__title home__title--bestsellers">{{ heading_title }}</div>*/
/*   <div class="bestsellers">*/
/*     <div class="bestsellers__wrap">*/
/*         {% for product in products %}*/
/*           <a class="bestsellers__item" href="{{ product.href }}">*/
/*             <img class="bestsellers__img" src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}">*/
/*             <div class="bestsellers__title">{{ product.name }}</div>*/
/*             <div class="bestsellers__desc">{{ product.description }}</div>*/
/*             <div class="bestsellers__price">{{ product.price }}</div>*/
/*           </a>*/
/*         {% endfor %}*/
/*     </div>*/
/*   </div>*/
/* </section>*/
