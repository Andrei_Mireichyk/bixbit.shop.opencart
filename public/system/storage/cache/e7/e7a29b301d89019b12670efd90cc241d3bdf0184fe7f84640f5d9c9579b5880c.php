<?php

/* default/template/information/information.twig */
class __TwigTemplate_b87a218650460f7d6cbefe15be8c8fe4c307c94aeb2f5544e4bcb3168ac8f153 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"information\">
  <div class=\"information__wrap\">
    <h1 class=\"information__title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
    <p class=\"information__desc\">";
        // line 13
        echo (isset($context["description"]) ? $context["description"] : null);
        echo "</p>
  </div>
</main>
";
        // line 16
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "  <link href=\"catalog/view/theme/default/stylesheet/information.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "  <script src=\"catalog/view/theme/default/js/information.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/information/information.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 7,  57 => 6,  52 => 4,  49 => 3,  45 => 16,  39 => 13,  35 => 12,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*   <link href="catalog/view/theme/default/stylesheet/information.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*   <script src="catalog/view/theme/default/js/information.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* <main class="information">*/
/*   <div class="information__wrap">*/
/*     <h1 class="information__title">{{ heading_title }}</h1>*/
/*     <p class="information__desc">{{ description }}</p>*/
/*   </div>*/
/* </main>*/
/* {{ footer }}*/
