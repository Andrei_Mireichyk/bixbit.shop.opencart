<?php

/* default/template/common/home.twig */
class __TwigTemplate_c12be84348b8c76ececd10a2a1349bfbb44e6a7ce5a05f4e7ad9e004e2468e4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"home\">
    ";
        // line 11
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
    ";
        // line 12
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "
</main>

";
        // line 15
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"catalog/view/theme/default/stylesheet/index.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "    <script src=\"catalog/view/theme/default/js/index.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 7,  56 => 6,  51 => 4,  48 => 3,  44 => 15,  38 => 12,  34 => 11,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/index.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/index.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* <main class="home">*/
/*     {{ content_top }}*/
/*     {{ content_bottom }}*/
/* </main>*/
/* */
/* {{ footer }}*/
