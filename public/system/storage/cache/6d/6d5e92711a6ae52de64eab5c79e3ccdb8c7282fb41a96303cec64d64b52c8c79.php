<?php

/* default/template/extension/quickcheckout/checkout.twig */
class __TwigTemplate_b32c5e1324d30672a050b4695f86908ee4ea3eac67fe08cdf43fbeac4cff6b1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
";
        // line 2
        $this->displayBlock('link', $context, $blocks);
        // line 5
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "


<main id=\"quickcheckoutconfirm\" class=\"order\">
    <div class=\"order__wrap\">
        <section class=\"order__contact-block contact__block--city contact\">

            <div id=\"warning-messages\">
                ";
        // line 17
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 18
            echo "                    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
                    </div>
                ";
        }
        // line 22
        echo "            </div>

            <div id=\"success-messages\"></div>

            ";
        // line 26
        echo (isset($context["guest"]) ? $context["guest"] : null);
        echo "

            <div class=\"contact__block contact__block-address\">
                <div class=\"contact__title\">";
        // line 29
        echo (isset($context["text_checkout_shipping_method"]) ? $context["text_checkout_shipping_method"] : null);
        echo "</div>
                <div id=\"shipping-method\"></div>
            </div>


            <div id='entered_address' class=\"contact__block contact__block--address\">
                <div class=\"contact__title\">АДРЕС ДОСТАВКИ</div>
                <div class=\"input__group\">
                    <div class=\"input\">
                    <input type=\"text\" required=\"required\" id=\"str\">
                    <label for=\"str\">Населенный пункт</label>
                    <small>Текст ошибки</small>
                    </div>
                </div>
                <div class=\"input__group\">
                    <div class=\"input\">
                        <input type=\"text\" required=\"required\" id=\"str\">
                        <label for=\"str\">Улица</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"house\">
                        <label for=\"house\">Дом</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"apartments\">
                        <label for=\"apartments\">Кв</label>
                        <small>Текст ошибки</small>
                    </div>
                    <div class=\"input\">
                        <input type=\"number\" required=\"required\" id=\"index\">
                        <label for=\"index\">Индекс</label>
                        <small>Текст ошибки</small>
                    </div>
                </div>
            </div>



            <div class=\"contact__block\">
                <div class=\"contact__title\">";
        // line 70
        echo (isset($context["text_checkout_payment_method"]) ? $context["text_checkout_payment_method"] : null);
        echo "</div>
                <div id=\"payment-method\"></div>
            </div>




            <div id=\"terms\" class=\"contact__action\">
                ";
        // line 78
        echo (isset($context["terms"]) ? $context["terms"] : null);
        echo "
            </div>
        </section>
        <section class=\"order__product-block\">
            <div id=\"cart1\" class=\"product\"></div>
        </section>
    </div>
</main>

<script type=\"text/javascript\"><!--

    \$('#entered_address input').change(function(){
        var str = '';
        \$('#entered_address input').each(function (k,v) {
            str += \" \"+\$(v).val();
        });
        \$('[name=address_1]').val(str);
    });


    ";
        // line 98
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 99
            echo "    \$(window).load(function () {
        \$.blockUI({
            message: '<h1 style=\"color:#ffffff;\">";
            // line 101
            echo (isset($context["text_please_wait"]) ? $context["text_please_wait"] : null);
            echo "</h1>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                '-khtml-border-radius': '10px',
                'border-radius': '10px',
                opacity: .8,
                color: '#ffffff'
            }
        });

        setTimeout(function () {
            \$.unblockUI();
        }, 2000);
    });
    ";
        }
        // line 120
        echo "
    ";
        // line 121
        if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
            // line 122
            echo "    ";
            if ((isset($context["save_data"]) ? $context["save_data"] : null)) {
                // line 123
                echo "    // Save form data
    \$(document).on('change', '#payment-address input[type=\\'text\\'], #payment-address select', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/checkout/save&type=payment',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                // No action needed
            },
            ";
                // line 134
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 135
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 139
                echo "        });
    });

    \$(document).on('change', '#shipping-address input[type=\\'text\\'], #shipping-address select', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/checkout/save&type=shipping',
            type: 'post',
            data: \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'password\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                // No action needed
            },
            ";
                // line 152
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 153
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 157
                echo "        });
    });
    ";
            }
            // line 160
            echo "
    ";
            // line 161
            if ((isset($context["login_module"]) ? $context["login_module"] : null)) {
                // line 162
                echo "    // Login Form Clicked
    \$(document).on('click', '#button-login', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/login/validate',
            type: 'post',
            data: \$('#checkout #login :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (json) {
                \$('.alert').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    \$('.alert-danger').fadeIn('slow');
                }
            },
            ";
                // line 189
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 190
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 194
                echo "        });
    });
    ";
            }
            // line 197
            echo "
    // Validate Register
    function validateRegister() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/register/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 224
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 225
                echo "                    if (json['error']['password']) {
                        \$('#payment-address input[name=\\'password\\']').after('<div class=\"text-danger\">' + json['error']['password'] + '</div>');
                    }

                    if (json['error']['confirm']) {
                        \$('#payment-address input[name=\\'confirm\\']').after('<div class=\"text-danger\">' + json['error']['confirm'] + '</div>');
                    }
                    ";
            }
            // line 233
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 234
                echo "                    if (json['error']['password']) {
                        \$('#payment-address input[name=\\'password\\']').css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }

                    if (json['error']['confirm']) {
                        \$('#payment-address input[name=\\'confirm\\']').css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 242
            echo "                } else {
                    ";
            // line 243
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 244
                echo "                    var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').val();

                    if (shipping_address) {
                        validateShippingMethod();
                    } else {
                        validateGuestShippingAddress();
                    }
                    ";
            } else {
                // line 252
                echo "                    validatePaymentMethod();
                    ";
            }
            // line 254
            echo "                }
            },
            ";
            // line 256
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 257
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 261
            echo "        });
    }

    // Validate Guest Payment Address
    function validateGuestAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/guest/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {
                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));
                        var wrap = element.closest('.input');
                        \$('small', wrap).remove();
                        wrap.addClass('has-error');
                        wrap.append('<small>' + json['error'][i] + '</small>');
                    }
                } else {
                    var create_account = \$('#payment-address input[name=\\'create_account\\']:checked').val();

                    ";
            // line 290
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 291
                echo "                    var shipping_address = \$('#payment-address input[name=\\'shipping_address\\']:checked').val();

                    if (create_account) {
                        validateRegister();
                    } else {
                        if (shipping_address) {
                            validateShippingMethod();
                        } else {
                            validateGuestShippingAddress();
                        }
                    }
                    ";
            } else {
                // line 303
                echo "                    if (create_account) {
                        validateRegister();
                    } else {
                        validatePaymentMethod();
                    }
                    ";
            }
            // line 309
            echo "                }
            },
            ";
            // line 311
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 312
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 316
            echo "        });
    }

    // Validate Guest Shipping Address
    function validateGuestShippingAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/guest_shipping/validate',
            type: 'post',
            data: \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address select, #shipping-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 344
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 345
                echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
            }
            // line 355
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 356
                echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 362
            echo "                } else {
                    validateShippingMethod();
                }
            },
            ";
            // line 366
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 367
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 371
            echo "        });
    }

    // Confirm Payment
    \$(document).on('click', '#button-payment-method', function () {

        validateGuestAddress();
    });
    ";
        } else {
            // line 380
            echo "    // Validate Payment Address
    function validatePaymentAddress() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_address/validate',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'password\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {

                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
            // line 405
            if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                // line 406
                echo "                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
            }
            // line 416
            echo "                    ";
            if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                // line 417
                echo "                    for (i in json['error']) {
                        var element = \$('#input-payment-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
            }
            // line 423
            echo "                } else {
                    ";
            // line 424
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 425
                echo "                    validateShippingAddress();
                    ";
            } else {
                // line 427
                echo "                    validatePaymentMethod();
                    ";
            }
            // line 429
            echo "                }
            },
            ";
            // line 431
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 432
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 436
            echo "        });
    }

    ";
            // line 439
            if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 440
                echo "    // Validate Shipping Address
    function validateShippingAddress() {
        var payment_mode = \$('input[name=\"payment_address\"]:checked').val();
        if (payment_mode == 'new') {
            ";
                // line 444
                if (((isset($context["logged"]) ? $context["logged"] : null) &&  !(isset($context["show_shipping_address"]) ? $context["show_shipping_address"] : null))) {
                    // line 445
                    echo "            var addressType = '#payment-address';
            var shipping_mode = 'input[name=\"shipping_address\"]:checked, ';
            ";
                } else {
                    // line 448
                    echo "            var addressType = '#shipping-address';
            var shipping_mode = '';
            ";
                }
                // line 451
                echo "        } else {
            var addressType = '#shipping-address';
            var shipping_mode = '';
        }
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_address/validate',
            type: 'post',
            data: \$(shipping_mode + addressType + ' input[type=\\'text\\'], ' + addressType + ' input[type=\\'password\\'], ' + addressType + ' input[type=\\'checkbox\\']:checked, ' + addressType + ' input[type=\\'radio\\']:checked, ' + addressType + ' select, ' + addressType + ' textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }

                    ";
                // line 479
                if ((isset($context["text_error"]) ? $context["text_error"] : null)) {
                    // line 480
                    echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        if (\$(element).parent().hasClass('input-group')) {
                            \$(element).parent().after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        } else {
                            \$(element).after('<div class=\"text-danger\">' + json['error'][i] + '</div>');
                        }
                    }
                    ";
                }
                // line 490
                echo "                    ";
                if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
                    // line 491
                    echo "                    for (i in json['error']) {
                        var element = \$('#input-shipping-' + i.replace('_', '-'));

                        \$(element).css('border', '1px solid #f00').css('background', '#F8ACAC');
                    }
                    ";
                }
                // line 497
                echo "                } else {
                    validateShippingMethod();
                }
            },
            ";
                // line 501
                if ((isset($context["debug"]) ? $context["debug"] : null)) {
                    // line 502
                    echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
                }
                // line 506
                echo "        });
    }
    ";
            }
            // line 509
            echo "
    // Confirm payment
    \$(document).on('click', '#button-payment-method', function () {

        validatePaymentAddress();
    });
    ";
        }
        // line 515
        echo "// Close if logged php

    // Payment Method
    function reloadPaymentMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method',
            type: 'post',
            data: \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea, #payment-method input[type=\\'text\\'], #payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#payment-method').html(html);

                ";
        // line 531
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 532
            echo "                \$.unblockUI();
                ";
        }
        // line 534
        echo "            },
            ";
        // line 535
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 536
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 540
        echo "        });
    }

    function reloadPaymentMethodById(address_id) {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method&address_id=' + address_id,
            type: 'post',
            data: \$('#payment-method input[type=\\'checkbox\\']:checked, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'hidden\\'], #payment-method select, #payment-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#payment-method').html(html);

                ";
        // line 556
        if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
            // line 557
            echo "                \$.unblockUI();
                ";
        }
        // line 559
        echo "            },
            ";
        // line 560
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 561
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 565
        echo "        });
    }

    // Validate Payment Method
    function validatePaymentMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/payment_method/validate',
            type: 'post',
            data: \$('#payment-method select, #payment-method input[type=\\'radio\\']:checked, #payment-method input[type=\\'checkbox\\']:checked, #payment-method textarea'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }
                } else {
                    validateTerms();
                }
            },
            ";
        // line 597
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 598
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 602
        echo "        });
    }

    // Shipping Method
    ";
        // line 606
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 607
            echo "    function reloadShippingMethod(type) {
        if (type == 'payment') {
            var post_data = \$('#payment-address input[type=\\'text\\'], #payment-address input[type=\\'checkbox\\']:checked, #payment-address input[type=\\'radio\\']:checked, #payment-address input[type=\\'hidden\\'], #payment-address select, #payment-address textarea, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        } else {
            var post_data = \$('#shipping-address input[type=\\'text\\'], #shipping-address input[type=\\'checkbox\\']:checked, #shipping-address input[type=\\'radio\\']:checked, #shipping-address input[type=\\'hidden\\'], #shipping-address select, #shipping-address textarea, #shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea');
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method',
            type: 'post',
            data: post_data,
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#shipping-method').html(html);
            },
            ";
            // line 626
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 627
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 631
            echo "        });
    }

    function reloadShippingMethodById(address_id) {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method&address_id=' + address_id,
            type: 'post',
            data: \$('#shipping-method input[type=\\'text\\'], #shipping-method input[type=\\'checkbox\\']:checked, #shipping-method input[type=\\'radio\\']:checked, #shipping-method input[type=\\'hidden\\'], #shipping-method select, #shipping-method textarea'),
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {

                \$('#shipping-method').html(html);
            },
            ";
            // line 647
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 648
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 652
            echo "        });
    }

    // Validate Shipping Method
    function validateShippingMethod() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/shipping_method/validate',
            type: 'post',
            data: \$('#shipping-method select, #shipping-method input[type=\\'radio\\']:checked, #shipping-method textarea, #shipping-method input[type=\\'text\\']'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                \$('.alert, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                } else if (json['error']) {


                    \$('.fa-spinner').remove();

                    \$('html, body').animate({scrollTop: 0}, 'slow');

                    if (json['error']['warning']) {
                        \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                        \$('.alert-danger').fadeIn('slow');
                    }
                } else {
                    validatePaymentMethod();
                }
            },
            ";
            // line 684
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 685
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 689
            echo "        });
    }
    ";
        }
        // line 692
        echo "
    // Validate confirm button
    function validateTerms() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/terms/validate',
            type: 'post',
            data: \$('#terms input[type=\\'checkbox\\']:checked'),
            dataType: 'json',
            cache: false,
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['error']) {
                     if (json['error']['warning']) {
                         var wrap = \$('#terms .checkbox');
                         wrap.addClass('has-error');
                         wrap.append(\"<small>\"+json['error']['warning']+\"</small>\");
                    }
                } else {
                    loadConfirm();
                }
            },
            ";
        // line 716
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 717
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 721
        echo "        });
    }

    // Load confirm
    function loadConfirm() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/confirm',
            dataType: 'html',
            cache: false,
            beforeSend: function () {
                ";
        // line 731
        if ((isset($context["confirmation_page"]) ? $context["confirmation_page"] : null)) {
            // line 732
            echo "                \$('html, body').animate({scrollTop: 0}, 'slow');

                ";
            // line 734
            if ((isset($context["slide_effect"]) ? $context["slide_effect"] : null)) {
                // line 735
                echo "                \$('#quickcheckoutconfirm').slideUp('slow');
                ";
            } else {
                // line 737
                echo "                \$('#quickcheckoutconfirm').html('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 739
            echo "
                ";
            // line 740
            if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
                // line 741
                echo "                \$.blockUI({
                    message: '<h1 style=\"color:#ffffff;\">";
                // line 742
                echo (isset($context["text_please_wait"]) ? $context["text_please_wait"] : null);
                echo "</h1>',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        '-khtml-border-radius': '10px',
                        'border-radius': '10px',
                        opacity: .8,
                        color: '#ffffff'
                    }
                });
                ";
            }
            // line 756
            echo "                ";
        }
        // line 757
        echo "            },
            success: function (html) {
                ";
        // line 759
        if ((isset($context["confirmation_page"]) ? $context["confirmation_page"] : null)) {
            // line 760
            echo "                ";
            if ((isset($context["load_screen"]) ? $context["load_screen"] : null)) {
                // line 761
                echo "                \$.unblockUI();
                ";
            }
            // line 763
            echo "
                \$('#quickcheckoutconfirm').hide().html(html);

                ";
            // line 766
            if ( !(isset($context["auto_submit"]) ? $context["auto_submit"] : null)) {
                // line 767
                echo "                ";
                if ((isset($context["slide_effect"]) ? $context["slide_effect"] : null)) {
                    // line 768
                    echo "                \$('#quickcheckoutconfirm').slideDown('slow');
                ";
                } else {
                    // line 770
                    echo "                \$('#quickcheckoutconfirm').show();
                ";
                }
                // line 772
                echo "                ";
            } else {
                // line 773
                echo "                \$('#quickcheckoutconfirm').after('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 775
            echo "                ";
        } else {
            // line 776
            echo "                \$('#terms .terms').hide();
                \$('#payment').html(html).slideDown('fast');

                ";
            // line 779
            if ((isset($context["auto_submit"]) ? $context["auto_submit"] : null)) {
                // line 780
                echo "                \$('#payment').hide().after('<div class=\"text-center\"><i class=\"fa fa-spinner fa-spin fa-5x\"></i></div>');
                ";
            }
            // line 782
            echo "
                \$('html, body').animate({scrollTop: \$('#terms').offset().top}, 'slow');

                disableCheckout();
                ";
        }
        // line 787
        echo "            },
            ";
        // line 788
        if ((isset($context["debug"]) ? $context["debug"] : null)) {
            // line 789
            echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
        }
        // line 793
        echo "        });
    }

    // Load cart
    ";
        // line 797
        if ((isset($context["cart_module"]) ? $context["cart_module"] : null)) {
            // line 798
            echo "    function loadCart() {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart',
            dataType: 'html',
            cache: false,
            beforeSend: function () {
            },
            success: function (html) {
                \$('#cart1').html(html);
            }
        });
    }

    ";
            // line 811
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 812
                echo "    \$(document).ready(function () {
        loadCart();
    });
    ";
            }
            // line 816
            echo "    ";
        }
        // line 817
        echo "
    ";
        // line 818
        if ((((isset($context["voucher_module"]) ? $context["voucher_module"] : null) || (isset($context["coupon_module"]) ? $context["coupon_module"] : null)) || (isset($context["reward_module"]) ? $context["reward_module"] : null))) {
            // line 819
            echo "    // Validate Coupon
    \$(document).on('click', '#button-coupon', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateCoupon',
            type: 'post',
            data: \$('#coupon-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-coupon').prop('disabled', true);
                \$('#button-coupon').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-coupon').prop('disabled', false);
                \$('#coupon-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 850
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 851
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 854
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 855
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 857
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 860
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 861
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 863
                echo "                }
                ";
            } else {
                // line 865
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 871
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 872
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 878
                echo "                ";
            }
            // line 879
            echo "
                ";
            // line 880
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 881
                echo "                loadCart();
                ";
            }
            // line 883
            echo "            },
            ";
            // line 884
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 885
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 889
            echo "        });
    });

    \$(document).on('click', '#button-voucher', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateVoucher',
            type: 'post',
            data: \$('#voucher-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-voucher').prop('disabled', true);
                \$('#button-voucher').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-voucher').prop('disabled', false);
                \$('#voucher-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 922
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 923
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 926
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 927
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 929
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 932
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 933
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 935
                echo "                }
                ";
            } else {
                // line 937
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 943
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 944
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 950
                echo "                ";
            }
            // line 951
            echo "
                ";
            // line 952
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 953
                echo "                loadCart();
                ";
            }
            // line 955
            echo "            },
            ";
            // line 956
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 957
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 961
            echo "        });
    });

    \$(document).on('click', '#button-reward', function () {
        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/voucher/validateReward',
            type: 'post',
            data: \$('#reward-content :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#button-reward').prop('disabled', true);
                \$('#button-reward').after('<i class=\"fa fa-spinner fa-spin\"></i>');
            },
            complete: function () {
                \$('#button-reward').prop('disabled', false);
                \$('#reward-content .fa-spinner').remove();
            },
            success: function (json) {
                \$('.alert').remove();

                \$('html, body').animate({scrollTop: 0}, 'slow');

                if (json['success']) {
                    \$('#success-messages').prepend('<div class=\"alert alert-success\" style=\"display:none;\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + '</div>');

                    \$('.alert-success').fadeIn('slow');
                } else if (json['error']) {
                    \$('#warning-messages').prepend('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');

                    \$('.alert-danger').fadeIn('slow');
                }

                ";
            // line 994
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 995
                echo "                if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                    reloadPaymentMethod();

                    ";
                // line 998
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 999
                    echo "                    reloadShippingMethod('payment');
                    ";
                }
                // line 1001
                echo "                } else {
                    reloadPaymentMethod();

                    ";
                // line 1004
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1005
                    echo "                    reloadShippingMethod('shipping');
                    ";
                }
                // line 1007
                echo "                }
                ";
            } else {
                // line 1009
                echo "                if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                    reloadPaymentMethod();
                } else {
                    reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                }

                ";
                // line 1015
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1016
                    echo "                if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                    reloadShippingMethod('shipping');
                } else {
                    reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                }
                ";
                }
                // line 1022
                echo "                ";
            }
            // line 1023
            echo "
                ";
            // line 1024
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1025
                echo "                loadCart();
                ";
            }
            // line 1027
            echo "            },
            ";
            // line 1028
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1029
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1033
            echo "        });
    });
    ";
        }
        // line 1036
        echo "
    ";
        // line 1037
        if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
            // line 1038
            echo "    \$(document).on('focusout', 'input[name=\\'postcode\\']', function () {
        ";
            // line 1039
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1040
                echo "        if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
            reloadShippingMethod('payment');
        } else {
            reloadShippingMethod('shipping');
        }
        ";
            } else {
                // line 1046
                echo "        if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
            reloadShippingMethod('shipping');
        } else {
            reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
        }
        ";
            }
            // line 1052
            echo "    });
    ";
        }
        // line 1054
        echo "
    ";
        // line 1055
        if ((isset($context["highlight_error"]) ? $context["highlight_error"] : null)) {
            // line 1056
            echo "    \$(document).on('keydown', 'input', function () {
       var wrap = \$(this).closest('.input');
        wrap.removeClass('has-error');
        \$('small' , wrap).remove();
    });
    \$(document).on('change', 'select', function () {
        \$(this).css('background', '').css('border', '');

        \$(this).siblings('.text-danger').remove();
    });
    ";
        }
        // line 1067
        echo "
    ";
        // line 1068
        if ((isset($context["edit_cart"]) ? $context["edit_cart"] : null)) {
            // line 1069
            echo "    \$(document).on('click', '.button-update', function () {

        var quantity = \$(this).parents('.quantity').find('input.qc-product-qantity');
        if (quantity.length) {
            if (\$(this).data('type') == 'increase') {
                quantity.val(parseInt(quantity.val()) + 1);
            } else if (\$(this).data('type') == 'decrease') {
                quantity.val(parseInt(quantity.val()) - 1);
            }
        }

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart/update',
            type: 'post',
            data: \$('#cart1 :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                //\$('#cart1 .button-update').prop('disabled', true);
            },
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    if (json['error']['stock']) {
                        \$('#button-payment-method').attr(\"disabled\", true);
                    } else if (json['error']['warning']) {
                        \$('#warning-messages').html('<div class=\"alert alert-danger\" style=\"display: none;\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error']['warning'] + '</div>');
                        \$('.alert-danger').fadeIn('slow');
                        \$('#button-payment-method').attr(\"disabled\", true);
                    } else {
                        \$('#warning-messages').html('');
                        \$('#button-payment-method').removeAttr(\"disabled\");
                    }

                    ";
            // line 1104
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1105
                echo "                    if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                        reloadPaymentMethod();

                        ";
                // line 1108
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1109
                    echo "                        reloadShippingMethod('payment');
                        ";
                }
                // line 1111
                echo "                    } else {
                        reloadPaymentMethod();

                        ";
                // line 1114
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1115
                    echo "                        reloadShippingMethod('shipping');
                        ";
                }
                // line 1117
                echo "                    }
                    ";
            } else {
                // line 1119
                echo "                    if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                        reloadPaymentMethod();
                    } else {
                        reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                    }

                    ";
                // line 1125
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1126
                    echo "                    if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                        reloadShippingMethod('shipping');
                    } else {
                        reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                    }
                    ";
                }
                // line 1132
                echo "                    ";
            }
            // line 1133
            echo "
                    ";
            // line 1134
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1135
                echo "                    loadCart();
                    ";
            }
            // line 1137
            echo "                }
            },
            ";
            // line 1139
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1140
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1144
            echo "        });
    });

    \$(document).on('click', '.button-remove', function () {
        var remove_id = \$(this).attr('data-remove');

        \$.ajax({
            url: 'index.php?route=extension/quickcheckout/cart/update&remove=' + remove_id,
            type: 'get',
            dataType: 'json',
            cache: false,
            beforeSend: function () {
                \$('#cart1 .button-remove').prop('disabled', true);
            },
            success: function (json) {
                if (json['redirect']) {
                    location = json['redirect'];
                } else {
                    ";
            // line 1162
            if ( !(isset($context["logged"]) ? $context["logged"] : null)) {
                // line 1163
                echo "                    if (\$('#payment-address input[name=\\'shipping_address\\']:checked').val()) {
                        reloadPaymentMethod();

                        ";
                // line 1166
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1167
                    echo "                        reloadShippingMethod('payment');
                        ";
                }
                // line 1169
                echo "                    } else {
                        reloadPaymentMethod();

                        ";
                // line 1172
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1173
                    echo "                        reloadShippingMethod('shipping');
                        ";
                }
                // line 1175
                echo "                    }
                    ";
            } else {
                // line 1177
                echo "                    if (\$('#payment-address input[name=\\'payment_address\\']:checked').val() == 'new') {
                        reloadPaymentMethod();
                    } else {
                        reloadPaymentMethodById(\$('#payment-address select[name=\\'address_id\\']').val());
                    }

                    ";
                // line 1183
                if ((isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                    // line 1184
                    echo "                    if (\$('#shipping-address input[name=\\'shipping_address\\']:checked').val() == 'new') {
                        reloadShippingMethod('shipping');
                    } else {
                        reloadShippingMethodById(\$('#shipping-address select[name=\\'address_id\\']').val());
                    }
                    ";
                }
                // line 1190
                echo "                    ";
            }
            // line 1191
            echo "
                    ";
            // line 1192
            if ( !(isset($context["shipping_required"]) ? $context["shipping_required"] : null)) {
                // line 1193
                echo "                    loadCart();
                    ";
            }
            // line 1195
            echo "                }
            },
            ";
            // line 1197
            if ((isset($context["debug"]) ? $context["debug"] : null)) {
                // line 1198
                echo "            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
            }
            ";
            }
            // line 1202
            echo "        });

        return false;
    });
    ";
        }
        // line 1207
        echo "

    //--></script>
";
        // line 1210
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 1211
            echo "    <script type=\"text/javascript\"><!--
        \$('#button-payment-method').attr(\"disabled\", true);
        //--></script>
";
        }
        // line 1215
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    // line 2
    public function block_link($context, array $blocks = array())
    {
        // line 3
        echo "    <link href=\"catalog/view/theme/default/stylesheet/order.css\" rel=\"stylesheet\">
";
    }

    // line 5
    public function block_scripts($context, array $blocks = array())
    {
        // line 6
        echo "    <script src=\"catalog/view/theme/default/js/order.bundle.js\" type=\"text/javascript\"></script>
    <script src=\"catalog/view/javascript/jquery/quickcheckout/quickcheckout.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/extension/quickcheckout/checkout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1711 => 6,  1708 => 5,  1703 => 3,  1700 => 2,  1694 => 1215,  1688 => 1211,  1686 => 1210,  1681 => 1207,  1674 => 1202,  1668 => 1198,  1666 => 1197,  1662 => 1195,  1658 => 1193,  1656 => 1192,  1653 => 1191,  1650 => 1190,  1642 => 1184,  1640 => 1183,  1632 => 1177,  1628 => 1175,  1624 => 1173,  1622 => 1172,  1617 => 1169,  1613 => 1167,  1611 => 1166,  1606 => 1163,  1604 => 1162,  1584 => 1144,  1578 => 1140,  1576 => 1139,  1572 => 1137,  1568 => 1135,  1566 => 1134,  1563 => 1133,  1560 => 1132,  1552 => 1126,  1550 => 1125,  1542 => 1119,  1538 => 1117,  1534 => 1115,  1532 => 1114,  1527 => 1111,  1523 => 1109,  1521 => 1108,  1516 => 1105,  1514 => 1104,  1477 => 1069,  1475 => 1068,  1472 => 1067,  1459 => 1056,  1457 => 1055,  1454 => 1054,  1450 => 1052,  1442 => 1046,  1434 => 1040,  1432 => 1039,  1429 => 1038,  1427 => 1037,  1424 => 1036,  1419 => 1033,  1413 => 1029,  1411 => 1028,  1408 => 1027,  1404 => 1025,  1402 => 1024,  1399 => 1023,  1396 => 1022,  1388 => 1016,  1386 => 1015,  1378 => 1009,  1374 => 1007,  1370 => 1005,  1368 => 1004,  1363 => 1001,  1359 => 999,  1357 => 998,  1352 => 995,  1350 => 994,  1315 => 961,  1309 => 957,  1307 => 956,  1304 => 955,  1300 => 953,  1298 => 952,  1295 => 951,  1292 => 950,  1284 => 944,  1282 => 943,  1274 => 937,  1270 => 935,  1266 => 933,  1264 => 932,  1259 => 929,  1255 => 927,  1253 => 926,  1248 => 923,  1246 => 922,  1211 => 889,  1205 => 885,  1203 => 884,  1200 => 883,  1196 => 881,  1194 => 880,  1191 => 879,  1188 => 878,  1180 => 872,  1178 => 871,  1170 => 865,  1166 => 863,  1162 => 861,  1160 => 860,  1155 => 857,  1151 => 855,  1149 => 854,  1144 => 851,  1142 => 850,  1109 => 819,  1107 => 818,  1104 => 817,  1101 => 816,  1095 => 812,  1093 => 811,  1078 => 798,  1076 => 797,  1070 => 793,  1064 => 789,  1062 => 788,  1059 => 787,  1052 => 782,  1048 => 780,  1046 => 779,  1041 => 776,  1038 => 775,  1034 => 773,  1031 => 772,  1027 => 770,  1023 => 768,  1020 => 767,  1018 => 766,  1013 => 763,  1009 => 761,  1006 => 760,  1004 => 759,  1000 => 757,  997 => 756,  980 => 742,  977 => 741,  975 => 740,  972 => 739,  968 => 737,  964 => 735,  962 => 734,  958 => 732,  956 => 731,  944 => 721,  938 => 717,  936 => 716,  910 => 692,  905 => 689,  899 => 685,  897 => 684,  863 => 652,  857 => 648,  855 => 647,  837 => 631,  831 => 627,  829 => 626,  808 => 607,  806 => 606,  800 => 602,  794 => 598,  792 => 597,  758 => 565,  752 => 561,  750 => 560,  747 => 559,  743 => 557,  741 => 556,  723 => 540,  717 => 536,  715 => 535,  712 => 534,  708 => 532,  706 => 531,  688 => 515,  679 => 509,  674 => 506,  668 => 502,  666 => 501,  660 => 497,  652 => 491,  649 => 490,  637 => 480,  635 => 479,  605 => 451,  600 => 448,  595 => 445,  593 => 444,  587 => 440,  585 => 439,  580 => 436,  574 => 432,  572 => 431,  568 => 429,  564 => 427,  560 => 425,  558 => 424,  555 => 423,  547 => 417,  544 => 416,  532 => 406,  530 => 405,  503 => 380,  492 => 371,  486 => 367,  484 => 366,  478 => 362,  470 => 356,  467 => 355,  455 => 345,  453 => 344,  423 => 316,  417 => 312,  415 => 311,  411 => 309,  403 => 303,  389 => 291,  387 => 290,  356 => 261,  350 => 257,  348 => 256,  344 => 254,  340 => 252,  330 => 244,  328 => 243,  325 => 242,  315 => 234,  312 => 233,  302 => 225,  300 => 224,  271 => 197,  266 => 194,  260 => 190,  258 => 189,  229 => 162,  227 => 161,  224 => 160,  219 => 157,  213 => 153,  211 => 152,  196 => 139,  190 => 135,  188 => 134,  175 => 123,  172 => 122,  170 => 121,  167 => 120,  145 => 101,  141 => 99,  139 => 98,  116 => 78,  105 => 70,  61 => 29,  55 => 26,  49 => 22,  41 => 18,  39 => 17,  29 => 9,  27 => 5,  25 => 2,  21 => 1,);
    }
}
/* {{ header }}*/
/* {% block link %}*/
/*     <link href="catalog/view/theme/default/stylesheet/order.css" rel="stylesheet">*/
/* {% endblock %}*/
/* {% block scripts %}*/
/*     <script src="catalog/view/theme/default/js/order.bundle.js" type="text/javascript"></script>*/
/*     <script src="catalog/view/javascript/jquery/quickcheckout/quickcheckout.js" type="text/javascript"></script>*/
/* {% endblock %}*/
/* */
/* */
/* */
/* <main id="quickcheckoutconfirm" class="order">*/
/*     <div class="order__wrap">*/
/*         <section class="order__contact-block contact__block--city contact">*/
/* */
/*             <div id="warning-messages">*/
/*                 {% if error_warning %}*/
/*                     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*                         <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*                     </div>*/
/*                 {% endif %}*/
/*             </div>*/
/* */
/*             <div id="success-messages"></div>*/
/* */
/*             {{ guest }}*/
/* */
/*             <div class="contact__block contact__block-address">*/
/*                 <div class="contact__title">{{ text_checkout_shipping_method }}</div>*/
/*                 <div id="shipping-method"></div>*/
/*             </div>*/
/* */
/* */
/*             <div id='entered_address' class="contact__block contact__block--address">*/
/*                 <div class="contact__title">АДРЕС ДОСТАВКИ</div>*/
/*                 <div class="input__group">*/
/*                     <div class="input">*/
/*                     <input type="text" required="required" id="str">*/
/*                     <label for="str">Населенный пункт</label>*/
/*                     <small>Текст ошибки</small>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="input__group">*/
/*                     <div class="input">*/
/*                         <input type="text" required="required" id="str">*/
/*                         <label for="str">Улица</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="house">*/
/*                         <label for="house">Дом</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="apartments">*/
/*                         <label for="apartments">Кв</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                     <div class="input">*/
/*                         <input type="number" required="required" id="index">*/
/*                         <label for="index">Индекс</label>*/
/*                         <small>Текст ошибки</small>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/* */
/* */
/*             <div class="contact__block">*/
/*                 <div class="contact__title">{{ text_checkout_payment_method }}</div>*/
/*                 <div id="payment-method"></div>*/
/*             </div>*/
/* */
/* */
/* */
/* */
/*             <div id="terms" class="contact__action">*/
/*                 {{ terms }}*/
/*             </div>*/
/*         </section>*/
/*         <section class="order__product-block">*/
/*             <div id="cart1" class="product"></div>*/
/*         </section>*/
/*     </div>*/
/* </main>*/
/* */
/* <script type="text/javascript"><!--*/
/* */
/*     $('#entered_address input').change(function(){*/
/*         var str = '';*/
/*         $('#entered_address input').each(function (k,v) {*/
/*             str += " "+$(v).val();*/
/*         });*/
/*         $('[name=address_1]').val(str);*/
/*     });*/
/* */
/* */
/*     {% if load_screen %}*/
/*     $(window).load(function () {*/
/*         $.blockUI({*/
/*             message: '<h1 style="color:#ffffff;">{{ text_please_wait }}</h1>',*/
/*             css: {*/
/*                 border: 'none',*/
/*                 padding: '15px',*/
/*                 backgroundColor: '#000000',*/
/*                 '-webkit-border-radius': '10px',*/
/*                 '-moz-border-radius': '10px',*/
/*                 '-khtml-border-radius': '10px',*/
/*                 'border-radius': '10px',*/
/*                 opacity: .8,*/
/*                 color: '#ffffff'*/
/*             }*/
/*         });*/
/* */
/*         setTimeout(function () {*/
/*             $.unblockUI();*/
/*         }, 2000);*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if not logged %}*/
/*     {% if save_data %}*/
/*     // Save form data*/
/*     $(document).on('change', '#payment-address input[type=\'text\'], #payment-address select', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/checkout/save&type=payment',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 // No action needed*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('change', '#shipping-address input[type=\'text\'], #shipping-address select', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/checkout/save&type=shipping',*/
/*             type: 'post',*/
/*             data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'password\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 // No action needed*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if login_module %}*/
/*     // Login Form Clicked*/
/*     $(document).on('click', '#button-login', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/login/validate',*/
/*             type: 'post',*/
/*             data: $('#checkout #login :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/* */
/*             },*/
/*             complete: function () {*/
/* */
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     // Validate Register*/
/*     function validateRegister() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/register/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     if (json['error']['password']) {*/
/*                         $('#payment-address input[name=\'password\']').after('<div class="text-danger">' + json['error']['password'] + '</div>');*/
/*                     }*/
/* */
/*                     if (json['error']['confirm']) {*/
/*                         $('#payment-address input[name=\'confirm\']').after('<div class="text-danger">' + json['error']['confirm'] + '</div>');*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     if (json['error']['password']) {*/
/*                         $('#payment-address input[name=\'password\']').css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/* */
/*                     if (json['error']['confirm']) {*/
/*                         $('#payment-address input[name=\'confirm\']').css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     {% if shipping_required %}*/
/*                     var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').val();*/
/* */
/*                     if (shipping_address) {*/
/*                         validateShippingMethod();*/
/*                     } else {*/
/*                         validateGuestShippingAddress();*/
/*                     }*/
/*                     {% else %}*/
/*                     validatePaymentMethod();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Guest Payment Address*/
/*     function validateGuestAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/guest/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/*                         var wrap = element.closest('.input');*/
/*                         $('small', wrap).remove();*/
/*                         wrap.addClass('has-error');*/
/*                         wrap.append('<small>' + json['error'][i] + '</small>');*/
/*                     }*/
/*                 } else {*/
/*                     var create_account = $('#payment-address input[name=\'create_account\']:checked').val();*/
/* */
/*                     {% if shipping_required %}*/
/*                     var shipping_address = $('#payment-address input[name=\'shipping_address\']:checked').val();*/
/* */
/*                     if (create_account) {*/
/*                         validateRegister();*/
/*                     } else {*/
/*                         if (shipping_address) {*/
/*                             validateShippingMethod();*/
/*                         } else {*/
/*                             validateGuestShippingAddress();*/
/*                         }*/
/*                     }*/
/*                     {% else %}*/
/*                     if (create_account) {*/
/*                         validateRegister();*/
/*                     } else {*/
/*                         validatePaymentMethod();*/
/*                     }*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Guest Shipping Address*/
/*     function validateGuestShippingAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/guest_shipping/validate',*/
/*             type: 'post',*/
/*             data: $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address select, #shipping-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     validateShippingMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Confirm Payment*/
/*     $(document).on('click', '#button-payment-method', function () {*/
/* */
/*         validateGuestAddress();*/
/*     });*/
/*     {% else %}*/
/*     // Validate Payment Address*/
/*     function validatePaymentAddress() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_address/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'password\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-payment-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     {% if shipping_required %}*/
/*                     validateShippingAddress();*/
/*                     {% else %}*/
/*                     validatePaymentMethod();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     {% if shipping_required %}*/
/*     // Validate Shipping Address*/
/*     function validateShippingAddress() {*/
/*         var payment_mode = $('input[name="payment_address"]:checked').val();*/
/*         if (payment_mode == 'new') {*/
/*             {% if logged and not show_shipping_address %}*/
/*             var addressType = '#payment-address';*/
/*             var shipping_mode = 'input[name="shipping_address"]:checked, ';*/
/*             {% else %}*/
/*             var addressType = '#shipping-address';*/
/*             var shipping_mode = '';*/
/*             {% endif %}*/
/*         } else {*/
/*             var addressType = '#shipping-address';*/
/*             var shipping_mode = '';*/
/*         }*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_address/validate',*/
/*             type: 'post',*/
/*             data: $(shipping_mode + addressType + ' input[type=\'text\'], ' + addressType + ' input[type=\'password\'], ' + addressType + ' input[type=\'checkbox\']:checked, ' + addressType + ' input[type=\'radio\']:checked, ' + addressType + ' select, ' + addressType + ' textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/* */
/*                     {% if text_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         if ($(element).parent().hasClass('input-group')) {*/
/*                             $(element).parent().after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         } else {*/
/*                             $(element).after('<div class="text-danger">' + json['error'][i] + '</div>');*/
/*                         }*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% if highlight_error %}*/
/*                     for (i in json['error']) {*/
/*                         var element = $('#input-shipping-' + i.replace('_', '-'));*/
/* */
/*                         $(element).css('border', '1px solid #f00').css('background', '#F8ACAC');*/
/*                     }*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     validateShippingMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/*     {% endif %}*/
/* */
/*     // Confirm payment*/
/*     $(document).on('click', '#button-payment-method', function () {*/
/* */
/*         validatePaymentAddress();*/
/*     });*/
/*     {% endif %}// Close if logged php*/
/* */
/*     // Payment Method*/
/*     function reloadPaymentMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method',*/
/*             type: 'post',*/
/*             data: $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea, #payment-method input[type=\'text\'], #payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#payment-method').html(html);*/
/* */
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     function reloadPaymentMethodById(address_id) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method&address_id=' + address_id,*/
/*             type: 'post',*/
/*             data: $('#payment-method input[type=\'checkbox\']:checked, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'hidden\'], #payment-method select, #payment-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#payment-method').html(html);*/
/* */
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Payment Method*/
/*     function validatePaymentMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/payment_method/validate',*/
/*             type: 'post',*/
/*             data: $('#payment-method select, #payment-method input[type=\'radio\']:checked, #payment-method input[type=\'checkbox\']:checked, #payment-method textarea'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/*                 } else {*/
/*                     validateTerms();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Shipping Method*/
/*     {% if shipping_required %}*/
/*     function reloadShippingMethod(type) {*/
/*         if (type == 'payment') {*/
/*             var post_data = $('#payment-address input[type=\'text\'], #payment-address input[type=\'checkbox\']:checked, #payment-address input[type=\'radio\']:checked, #payment-address input[type=\'hidden\'], #payment-address select, #payment-address textarea, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         } else {*/
/*             var post_data = $('#shipping-address input[type=\'text\'], #shipping-address input[type=\'checkbox\']:checked, #shipping-address input[type=\'radio\']:checked, #shipping-address input[type=\'hidden\'], #shipping-address select, #shipping-address textarea, #shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea');*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method',*/
/*             type: 'post',*/
/*             data: post_data,*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#shipping-method').html(html);*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     function reloadShippingMethodById(address_id) {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method&address_id=' + address_id,*/
/*             type: 'post',*/
/*             data: $('#shipping-method input[type=\'text\'], #shipping-method input[type=\'checkbox\']:checked, #shipping-method input[type=\'radio\']:checked, #shipping-method input[type=\'hidden\'], #shipping-method select, #shipping-method textarea'),*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/* */
/*                 $('#shipping-method').html(html);*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Validate Shipping Method*/
/*     function validateShippingMethod() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/shipping_method/validate',*/
/*             type: 'post',*/
/*             data: $('#shipping-method select, #shipping-method input[type=\'radio\']:checked, #shipping-method textarea, #shipping-method input[type=\'text\']'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 $('.alert, .text-danger').remove();*/
/* */
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else if (json['error']) {*/
/* */
/* */
/*                     $('.fa-spinner').remove();*/
/* */
/*                     $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                     if (json['error']['warning']) {*/
/*                         $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                         $('.alert-danger').fadeIn('slow');*/
/*                     }*/
/*                 } else {*/
/*                     validatePaymentMethod();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/*     {% endif %}*/
/* */
/*     // Validate confirm button*/
/*     function validateTerms() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/terms/validate',*/
/*             type: 'post',*/
/*             data: $('#terms input[type=\'checkbox\']:checked'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 }*/
/* */
/*                 if (json['error']) {*/
/*                      if (json['error']['warning']) {*/
/*                          var wrap = $('#terms .checkbox');*/
/*                          wrap.addClass('has-error');*/
/*                          wrap.append("<small>"+json['error']['warning']+"</small>");*/
/*                     }*/
/*                 } else {*/
/*                     loadConfirm();*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Load confirm*/
/*     function loadConfirm() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/confirm',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 {% if confirmation_page %}*/
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 {% if slide_effect %}*/
/*                 $('#quickcheckoutconfirm').slideUp('slow');*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').html('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/* */
/*                 {% if load_screen %}*/
/*                 $.blockUI({*/
/*                     message: '<h1 style="color:#ffffff;">{{ text_please_wait }}</h1>',*/
/*                     css: {*/
/*                         border: 'none',*/
/*                         padding: '15px',*/
/*                         backgroundColor: '#000000',*/
/*                         '-webkit-border-radius': '10px',*/
/*                         '-moz-border-radius': '10px',*/
/*                         '-khtml-border-radius': '10px',*/
/*                         'border-radius': '10px',*/
/*                         opacity: .8,*/
/*                         color: '#ffffff'*/
/*                     }*/
/*                 });*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/*             },*/
/*             success: function (html) {*/
/*                 {% if confirmation_page %}*/
/*                 {% if load_screen %}*/
/*                 $.unblockUI();*/
/*                 {% endif %}*/
/* */
/*                 $('#quickcheckoutconfirm').hide().html(html);*/
/* */
/*                 {% if not auto_submit %}*/
/*                 {% if slide_effect %}*/
/*                 $('#quickcheckoutconfirm').slideDown('slow');*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').show();*/
/*                 {% endif %}*/
/*                 {% else %}*/
/*                 $('#quickcheckoutconfirm').after('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/*                 {% else %}*/
/*                 $('#terms .terms').hide();*/
/*                 $('#payment').html(html).slideDown('fast');*/
/* */
/*                 {% if auto_submit %}*/
/*                 $('#payment').hide().after('<div class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></div>');*/
/*                 {% endif %}*/
/* */
/*                 $('html, body').animate({scrollTop: $('#terms').offset().top}, 'slow');*/
/* */
/*                 disableCheckout();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     }*/
/* */
/*     // Load cart*/
/*     {% if cart_module %}*/
/*     function loadCart() {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart',*/
/*             dataType: 'html',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*             },*/
/*             success: function (html) {*/
/*                 $('#cart1').html(html);*/
/*             }*/
/*         });*/
/*     }*/
/* */
/*     {% if not shipping_required %}*/
/*     $(document).ready(function () {*/
/*         loadCart();*/
/*     });*/
/*     {% endif %}*/
/*     {% endif %}*/
/* */
/*     {% if voucher_module or coupon_module or reward_module %}*/
/*     // Validate Coupon*/
/*     $(document).on('click', '#button-coupon', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateCoupon',*/
/*             type: 'post',*/
/*             data: $('#coupon-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-coupon').prop('disabled', true);*/
/*                 $('#button-coupon').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-coupon').prop('disabled', false);*/
/*                 $('#coupon-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '#button-voucher', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateVoucher',*/
/*             type: 'post',*/
/*             data: $('#voucher-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-voucher').prop('disabled', true);*/
/*                 $('#button-voucher').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-voucher').prop('disabled', false);*/
/*                 $('#voucher-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '#button-reward', function () {*/
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/voucher/validateReward',*/
/*             type: 'post',*/
/*             data: $('#reward-content :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#button-reward').prop('disabled', true);*/
/*                 $('#button-reward').after('<i class="fa fa-spinner fa-spin"></i>');*/
/*             },*/
/*             complete: function () {*/
/*                 $('#button-reward').prop('disabled', false);*/
/*                 $('#reward-content .fa-spinner').remove();*/
/*             },*/
/*             success: function (json) {*/
/*                 $('.alert').remove();*/
/* */
/*                 $('html, body').animate({scrollTop: 0}, 'slow');*/
/* */
/*                 if (json['success']) {*/
/*                     $('#success-messages').prepend('<div class="alert alert-success" style="display:none;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');*/
/* */
/*                     $('.alert-success').fadeIn('slow');*/
/*                 } else if (json['error']) {*/
/*                     $('#warning-messages').prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/* */
/*                     $('.alert-danger').fadeIn('slow');*/
/*                 }*/
/* */
/*                 {% if not logged %}*/
/*                 if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('payment');*/
/*                     {% endif %}*/
/*                 } else {*/
/*                     reloadPaymentMethod();*/
/* */
/*                     {% if shipping_required %}*/
/*                     reloadShippingMethod('shipping');*/
/*                     {% endif %}*/
/*                 }*/
/*                 {% else %}*/
/*                 if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                     reloadPaymentMethod();*/
/*                 } else {*/
/*                     reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                 }*/
/* */
/*                 {% if shipping_required %}*/
/*                 if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                     reloadShippingMethod('shipping');*/
/*                 } else {*/
/*                     reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                 }*/
/*                 {% endif %}*/
/*                 {% endif %}*/
/* */
/*                 {% if not shipping_required %}*/
/*                 loadCart();*/
/*                 {% endif %}*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if shipping_required %}*/
/*     $(document).on('focusout', 'input[name=\'postcode\']', function () {*/
/*         {% if not logged %}*/
/*         if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*             reloadShippingMethod('payment');*/
/*         } else {*/
/*             reloadShippingMethod('shipping');*/
/*         }*/
/*         {% else %}*/
/*         if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*             reloadShippingMethod('shipping');*/
/*         } else {*/
/*             reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*         }*/
/*         {% endif %}*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if highlight_error %}*/
/*     $(document).on('keydown', 'input', function () {*/
/*        var wrap = $(this).closest('.input');*/
/*         wrap.removeClass('has-error');*/
/*         $('small' , wrap).remove();*/
/*     });*/
/*     $(document).on('change', 'select', function () {*/
/*         $(this).css('background', '').css('border', '');*/
/* */
/*         $(this).siblings('.text-danger').remove();*/
/*     });*/
/*     {% endif %}*/
/* */
/*     {% if edit_cart %}*/
/*     $(document).on('click', '.button-update', function () {*/
/* */
/*         var quantity = $(this).parents('.quantity').find('input.qc-product-qantity');*/
/*         if (quantity.length) {*/
/*             if ($(this).data('type') == 'increase') {*/
/*                 quantity.val(parseInt(quantity.val()) + 1);*/
/*             } else if ($(this).data('type') == 'decrease') {*/
/*                 quantity.val(parseInt(quantity.val()) - 1);*/
/*             }*/
/*         }*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart/update',*/
/*             type: 'post',*/
/*             data: $('#cart1 :input'),*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 //$('#cart1 .button-update').prop('disabled', true);*/
/*             },*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     if (json['error']['stock']) {*/
/*                         $('#button-payment-method').attr("disabled", true);*/
/*                     } else if (json['error']['warning']) {*/
/*                         $('#warning-messages').html('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '</div>');*/
/*                         $('.alert-danger').fadeIn('slow');*/
/*                         $('#button-payment-method').attr("disabled", true);*/
/*                     } else {*/
/*                         $('#warning-messages').html('');*/
/*                         $('#button-payment-method').removeAttr("disabled");*/
/*                     }*/
/* */
/*                     {% if not logged %}*/
/*                     if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('payment');*/
/*                         {% endif %}*/
/*                     } else {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('shipping');*/
/*                         {% endif %}*/
/*                     }*/
/*                     {% else %}*/
/*                     if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                         reloadPaymentMethod();*/
/*                     } else {*/
/*                         reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                     }*/
/* */
/*                     {% if shipping_required %}*/
/*                     if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                         reloadShippingMethod('shipping');*/
/*                     } else {*/
/*                         reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% endif %}*/
/* */
/*                     {% if not shipping_required %}*/
/*                     loadCart();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/*     });*/
/* */
/*     $(document).on('click', '.button-remove', function () {*/
/*         var remove_id = $(this).attr('data-remove');*/
/* */
/*         $.ajax({*/
/*             url: 'index.php?route=extension/quickcheckout/cart/update&remove=' + remove_id,*/
/*             type: 'get',*/
/*             dataType: 'json',*/
/*             cache: false,*/
/*             beforeSend: function () {*/
/*                 $('#cart1 .button-remove').prop('disabled', true);*/
/*             },*/
/*             success: function (json) {*/
/*                 if (json['redirect']) {*/
/*                     location = json['redirect'];*/
/*                 } else {*/
/*                     {% if not logged %}*/
/*                     if ($('#payment-address input[name=\'shipping_address\']:checked').val()) {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('payment');*/
/*                         {% endif %}*/
/*                     } else {*/
/*                         reloadPaymentMethod();*/
/* */
/*                         {% if shipping_required %}*/
/*                         reloadShippingMethod('shipping');*/
/*                         {% endif %}*/
/*                     }*/
/*                     {% else %}*/
/*                     if ($('#payment-address input[name=\'payment_address\']:checked').val() == 'new') {*/
/*                         reloadPaymentMethod();*/
/*                     } else {*/
/*                         reloadPaymentMethodById($('#payment-address select[name=\'address_id\']').val());*/
/*                     }*/
/* */
/*                     {% if shipping_required %}*/
/*                     if ($('#shipping-address input[name=\'shipping_address\']:checked').val() == 'new') {*/
/*                         reloadShippingMethod('shipping');*/
/*                     } else {*/
/*                         reloadShippingMethodById($('#shipping-address select[name=\'address_id\']').val());*/
/*                     }*/
/*                     {% endif %}*/
/*                     {% endif %}*/
/* */
/*                     {% if not shipping_required %}*/
/*                     loadCart();*/
/*                     {% endif %}*/
/*                 }*/
/*             },*/
/*             {% if debug %}*/
/*             error: function (xhr, ajaxOptions, thrownError) {*/
/*                 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*             }*/
/*             {% endif %}*/
/*         });*/
/* */
/*         return false;*/
/*     });*/
/*     {% endif %}*/
/* */
/* */
/*     //--></script>*/
/* {% if error_warning %}*/
/*     <script type="text/javascript"><!--*/
/*         $('#button-payment-method').attr("disabled", true);*/
/*         //--></script>*/
/* {% endif %}*/
/* {{ footer }}*/
/* */
