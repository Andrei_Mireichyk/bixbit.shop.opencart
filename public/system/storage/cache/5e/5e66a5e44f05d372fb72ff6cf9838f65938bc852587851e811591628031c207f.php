<?php

/* extension/feed/google_base.twig */
class __TwigTemplate_8612203340b7fd85c0ec441e2ca40011c965467deb71aba30118406e52e210e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
<div id=\"content\">
  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"button\" id=\"button-import\" data-toggle=\"tooltip\" title=\"";
        // line 6
        echo (isset($context["button_import"]) ? $context["button_import"] : null);
        echo "\" class=\"btn btn-success\"><i class=\"fa fa fa-upload\"></i></button>
        <button type=\"submit\" form=\"form-feed\" data-toggle=\"tooltip\" title=\"";
        // line 7
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 8
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 9
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 12
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 14
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
    ";
        // line 18
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 19
            echo "    <div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
    </div>
    ";
        }
        // line 23
        echo "    <div class=\"panel panel-default\">
      <div class=\"panel-heading\">
        <h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 25
        echo (isset($context["text_edit"]) ? $context["text_edit"] : null);
        echo "</h3>
      </div>
      <div class=\"panel-body\">
        <div class=\"alert alert-info\"><i class=\"fa fa-info-circle\"></i> ";
        // line 28
        echo (isset($context["text_import"]) ? $context["text_import"] : null);
        echo " <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button></div>
        <form action=\"";
        // line 29
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-feed\" class=\"form-horizontal\">
            <div id=\"category\"></div>
            <br />
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">";
        // line 33
        echo (isset($context["entry_google_category"]) ? $context["entry_google_category"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
                  <input type=\"text\" name=\"google_base_category\" value=\"\" placeholder=\"";
        // line 35
        echo (isset($context["entry_google_category"]) ? $context["entry_google_category"] : null);
        echo "\" id=\"input-google-category\" class=\"form-control\" />
                  <input type=\"hidden\" name=\"google_base_category_id\" value=\"\" />
                  <div class=\"input-group\">
                    <input type=\"text\" name=\"category\" value=\"\" placeholder=\"";
        // line 38
        echo (isset($context["entry_category"]) ? $context["entry_category"] : null);
        echo "\" id=\"input-category\" class=\"form-control\" />
                    <input type=\"hidden\" name=\"category_id\" value=\"\" />
                    <span class=\"input-group-btn\"><button type=\"button\" id=\"button-category-add\" data-toggle=\"tooltip\" title=\"";
        // line 40
        echo (isset($context["button_category_add"]) ? $context["button_category_add"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i></button></span>
                  </div>
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-data-feed\">";
        // line 45
        echo (isset($context["entry_data_feed"]) ? $context["entry_data_feed"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
                <textarea rows=\"5\" id=\"input-data-feed\" class=\"form-control\" readonly>";
        // line 47
        echo (isset($context["data_feed"]) ? $context["data_feed"] : null);
        echo "</textarea>
              </div>
            </div>
            <div class=\"form-group\">
              <label class=\"col-sm-2 control-label\" for=\"input-status\">";
        // line 51
        echo (isset($context["entry_status"]) ? $context["entry_status"] : null);
        echo "</label>
              <div class=\"col-sm-10\">
                <select name=\"feed_google_base_status\" id=\"input-status\" class=\"form-control\">
                  ";
        // line 54
        if ((isset($context["feed_google_base_status"]) ? $context["feed_google_base_status"] : null)) {
            // line 55
            echo "                  <option value=\"1\" selected=\"selected\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                  <option value=\"0\">";
            // line 56
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                  ";
        } else {
            // line 58
            echo "                  <option value=\"1\">";
            echo (isset($context["text_enabled"]) ? $context["text_enabled"] : null);
            echo "</option>
                  <option value=\"0\" selected=\"selected\">";
            // line 59
            echo (isset($context["text_disabled"]) ? $context["text_disabled"] : null);
            echo "</option>
                  ";
        }
        // line 61
        echo "                </select>
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type=\"text/javascript\"><!--
// Google Category
\$('input[name=\\'google_base_category\\']').autocomplete({
    'source': function(request, response) {
      \$.ajax({
        url: 'index.php?route=extension/feed/google_base/autocomplete&user_token=";
        // line 73
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
        dataType: 'json',
        success: function(json) {
          response(\$.map(json, function(item) {
            return {
              label: item['name'],
              value: item['google_base_category_id']
            }
          }));
        },
        error: function(xhr, ajaxOptions, thrownError) {
          alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
        }
      });
    },
    'select': function(item) {
      \$(this).val(item['label']);
      \$('input[name=\\'google_base_category_id\\']').val(item['value']);
  }
});

// Category
\$('input[name=\\'category\\']').autocomplete({
\t'source': function(request, response) {
\t\t\$.ajax({
\t\t\turl: 'index.php?route=catalog/category/autocomplete&user_token=";
        // line 98
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "&filter_name=' +  encodeURIComponent(request),
\t\t\tdataType: 'json',
\t\t\tsuccess: function(json) {
\t\t\t\tresponse(\$.map(json, function(item) {
\t\t\t\t\treturn {
\t\t\t\t\t\tlabel: item['name'],
\t\t\t\t\t\tvalue: item['category_id']
\t\t\t\t\t}
\t\t\t\t}));
\t\t\t},
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
      }
    });
\t},
\t'select': function(item) {
      \$(this).val(item['label']);
      \$('input[name=\"category_id\"]').val(item['value']);
    }
});

\$('#category').delegate('.pagination a', 'click', function(e) {
\te.preventDefault();

\t\$('#category').load(this.href);
});

\$('#category').load('index.php?route=extension/feed/google_base/category&user_token=";
        // line 125
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "');

\$('#button-category-add').on('click', function() {
\t\$.ajax({
\t\turl: 'index.php?route=extension/feed/google_base/addcategory&user_token=";
        // line 129
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
\t\ttype: 'post',
\t\tdataType: 'json',
\t\tdata: 'google_base_category_id=' + \$('input[name=\\'google_base_category_id\\']').val() + '&category_id=' + \$('input[name=\\'category_id\\']').val(),
\t\tbeforeSend: function() {
\t\t\t\$('#button-category-add').button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$('#button-category-add').button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\tif (json['error']) {
\t\t\t\t\$('#category').before('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t}

\t\t\tif (json['success']) {
\t\t\t\t\$('#category').load('index.php?route=extension/feed/google_base/category&user_token=";
        // line 147
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "');

\t\t\t\t\$('#category').before('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');

\t\t\t\t\$('input[name=\\'category\\']').val('');
          \$('input[name=\\'category_id\\']').val('');
          \$('input[name=\\'google_base_category\\']').val('');
          \$('input[name=\\'google_base_category_id\\']').val('');
\t\t\t}
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('#category').delegate('.btn-danger', 'click', function() {
\tvar node = this;

\t\$.ajax({
\t\turl: 'index.php?route=extension/feed/google_base/removecategory&user_token=";
        // line 167
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
\t\ttype: 'post',
\t\tdata: 'category_id=' + encodeURIComponent(this.value),
\t\tdataType: 'json',
\t\tcrossDomain: true,
\t\tbeforeSend: function() {
\t\t\t\$(node).button('loading');
\t\t},
\t\tcomplete: function() {
\t\t\t\$(node).button('reset');
\t\t},
\t\tsuccess: function(json) {
\t\t\t\$('.alert-dismissible').remove();

\t\t\t// Check for errors
\t\t\tif (json['error']) {
\t\t\t\t\$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t}

      if (json['success']) {
\t\t\t\t\$('#category').load('index.php?route=extension/feed/google_base/category&user_token=";
        // line 187
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "');

\t\t\t\t\$('#category').before('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
\t\t\t}
\t\t},
\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t}
\t});
});

\$('#button-import').on('click', function() {
\t\$('#form-upload').remove();

\t\$('body').prepend('<form enctype=\"multipart/form-data\" id=\"form-upload\" style=\"display: none;\"><input type=\"file\" name=\"file\" /></form>');

\t\$('#form-upload input[name=\\'file\\']').trigger('click');

\tif (typeof timer != 'undefined') {
    clearInterval(timer);
\t}

\ttimer = setInterval(function() {
\t\tif (\$('#form-upload input[name=\\'file\\']').val() != '') {
\t\t\tclearInterval(timer);

\t\t\t\$.ajax({
\t\t\t\turl: 'index.php?route=extension/feed/google_base/import&user_token=";
        // line 214
        echo (isset($context["user_token"]) ? $context["user_token"] : null);
        echo "',
\t\t\t\ttype: 'post',
\t\t\t\tdataType: 'json',
\t\t\t\tdata: new FormData(\$('#form-upload')[0]),
\t\t\t\tcache: false,
\t\t\t\tcontentType: false,
\t\t\t\tprocessData: false,
\t\t\t\tbeforeSend: function() {
\t\t\t\t\t\$('#button-import').button('loading');
\t\t\t\t},
\t\t\t\tcomplete: function() {
\t\t\t\t\t\$('#button-import').button('reset');
\t\t\t\t},
\t\t\t\tsuccess: function(json) {
\t\t\t\t\t\$('.alert-dismissible').remove();

          if (json['error']) {
        \t\t\$('#content > .container-fluid').prepend('<div class=\"alert alert-danger alert-dismissible\"><i class=\"fa fa-exclamation-circle\"></i> ' + json['error'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
        \t}

        \tif (json['success']) {
        \t\t\$('#content > .container-fluid').prepend('<div class=\"alert alert-success alert-dismissible\"><i class=\"fa fa-check-circle\"></i> ' + json['success'] + ' <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button></div>');
        \t}
\t\t\t\t},
\t\t\t\terror: function(xhr, ajaxOptions, thrownError) {
\t\t\t\t\talert(thrownError + \"\\r\\n\" + xhr.statusText + \"\\r\\n\" + xhr.responseText);
\t\t\t\t}
\t\t\t});
\t\t}
\t}, 500);
});
//--></script>
</div>
";
        // line 247
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/feed/google_base.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  375 => 247,  339 => 214,  309 => 187,  286 => 167,  263 => 147,  242 => 129,  235 => 125,  205 => 98,  177 => 73,  163 => 61,  158 => 59,  153 => 58,  148 => 56,  143 => 55,  141 => 54,  135 => 51,  128 => 47,  123 => 45,  115 => 40,  110 => 38,  104 => 35,  99 => 33,  92 => 29,  88 => 28,  82 => 25,  78 => 23,  70 => 19,  68 => 18,  62 => 14,  51 => 12,  47 => 11,  42 => 9,  36 => 8,  32 => 7,  28 => 6,  19 => 1,);
    }
}
/* {{ header }}{{ column_left }}*/
/* <div id="content">*/
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="button" id="button-import" data-toggle="tooltip" title="{{ button_import }}" class="btn btn-success"><i class="fa fa fa-upload"></i></button>*/
/*         <button type="submit" form="form-feed" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*     {% if error_warning %}*/
/*     <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*       <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*     </div>*/
/*     {% endif %}*/
/*     <div class="panel panel-default">*/
/*       <div class="panel-heading">*/
/*         <h3 class="panel-title"><i class="fa fa-pencil"></i> {{ text_edit }}</h3>*/
/*       </div>*/
/*       <div class="panel-body">*/
/*         <div class="alert alert-info"><i class="fa fa-info-circle"></i> {{ text_import }} <button type="button" class="close" data-dismiss="alert">×</button></div>*/
/*         <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-feed" class="form-horizontal">*/
/*             <div id="category"></div>*/
/*             <br />*/
/*             <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-data-feed">{{ entry_google_category }}</label>*/
/*               <div class="col-sm-10">*/
/*                   <input type="text" name="google_base_category" value="" placeholder="{{ entry_google_category }}" id="input-google-category" class="form-control" />*/
/*                   <input type="hidden" name="google_base_category_id" value="" />*/
/*                   <div class="input-group">*/
/*                     <input type="text" name="category" value="" placeholder="{{ entry_category }}" id="input-category" class="form-control" />*/
/*                     <input type="hidden" name="category_id" value="" />*/
/*                     <span class="input-group-btn"><button type="button" id="button-category-add" data-toggle="tooltip" title="{{ button_category_add }}" class="btn btn-primary"><i class="fa fa-plus"></i></button></span>*/
/*                   </div>*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-data-feed">{{ entry_data_feed }}</label>*/
/*               <div class="col-sm-10">*/
/*                 <textarea rows="5" id="input-data-feed" class="form-control" readonly>{{ data_feed }}</textarea>*/
/*               </div>*/
/*             </div>*/
/*             <div class="form-group">*/
/*               <label class="col-sm-2 control-label" for="input-status">{{ entry_status }}</label>*/
/*               <div class="col-sm-10">*/
/*                 <select name="feed_google_base_status" id="input-status" class="form-control">*/
/*                   {% if feed_google_base_status %}*/
/*                   <option value="1" selected="selected">{{ text_enabled }}</option>*/
/*                   <option value="0">{{ text_disabled }}</option>*/
/*                   {% else %}*/
/*                   <option value="1">{{ text_enabled }}</option>*/
/*                   <option value="0" selected="selected">{{ text_disabled }}</option>*/
/*                   {% endif %}*/
/*                 </select>*/
/*               </div>*/
/*           </div>*/
/*         </form>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   <script type="text/javascript"><!--*/
/* // Google Category*/
/* $('input[name=\'google_base_category\']').autocomplete({*/
/*     'source': function(request, response) {*/
/*       $.ajax({*/
/*         url: 'index.php?route=extension/feed/google_base/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/*         dataType: 'json',*/
/*         success: function(json) {*/
/*           response($.map(json, function(item) {*/
/*             return {*/
/*               label: item['name'],*/
/*               value: item['google_base_category_id']*/
/*             }*/
/*           }));*/
/*         },*/
/*         error: function(xhr, ajaxOptions, thrownError) {*/
/*           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*         }*/
/*       });*/
/*     },*/
/*     'select': function(item) {*/
/*       $(this).val(item['label']);*/
/*       $('input[name=\'google_base_category_id\']').val(item['value']);*/
/*   }*/
/* });*/
/* */
/* // Category*/
/* $('input[name=\'category\']').autocomplete({*/
/* 	'source': function(request, response) {*/
/* 		$.ajax({*/
/* 			url: 'index.php?route=catalog/category/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),*/
/* 			dataType: 'json',*/
/* 			success: function(json) {*/
/* 				response($.map(json, function(item) {*/
/* 					return {*/
/* 						label: item['name'],*/
/* 						value: item['category_id']*/
/* 					}*/
/* 				}));*/
/* 			},*/
/*       error: function(xhr, ajaxOptions, thrownError) {*/
/*         alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/*       }*/
/*     });*/
/* 	},*/
/* 	'select': function(item) {*/
/*       $(this).val(item['label']);*/
/*       $('input[name="category_id"]').val(item['value']);*/
/*     }*/
/* });*/
/* */
/* $('#category').delegate('.pagination a', 'click', function(e) {*/
/* 	e.preventDefault();*/
/* */
/* 	$('#category').load(this.href);*/
/* });*/
/* */
/* $('#category').load('index.php?route=extension/feed/google_base/category&user_token={{ user_token }}');*/
/* */
/* $('#button-category-add').on('click', function() {*/
/* 	$.ajax({*/
/* 		url: 'index.php?route=extension/feed/google_base/addcategory&user_token={{ user_token }}',*/
/* 		type: 'post',*/
/* 		dataType: 'json',*/
/* 		data: 'google_base_category_id=' + $('input[name=\'google_base_category_id\']').val() + '&category_id=' + $('input[name=\'category_id\']').val(),*/
/* 		beforeSend: function() {*/
/* 			$('#button-category-add').button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$('#button-category-add').button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible').remove();*/
/* */
/* 			if (json['error']) {*/
/* 				$('#category').before('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* 			}*/
/* */
/* 			if (json['success']) {*/
/* 				$('#category').load('index.php?route=extension/feed/google_base/category&user_token={{ user_token }}');*/
/* */
/* 				$('#category').before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* */
/* 				$('input[name=\'category\']').val('');*/
/*           $('input[name=\'category_id\']').val('');*/
/*           $('input[name=\'google_base_category\']').val('');*/
/*           $('input[name=\'google_base_category_id\']').val('');*/
/* 			}*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*/
/* });*/
/* */
/* $('#category').delegate('.btn-danger', 'click', function() {*/
/* 	var node = this;*/
/* */
/* 	$.ajax({*/
/* 		url: 'index.php?route=extension/feed/google_base/removecategory&user_token={{ user_token }}',*/
/* 		type: 'post',*/
/* 		data: 'category_id=' + encodeURIComponent(this.value),*/
/* 		dataType: 'json',*/
/* 		crossDomain: true,*/
/* 		beforeSend: function() {*/
/* 			$(node).button('loading');*/
/* 		},*/
/* 		complete: function() {*/
/* 			$(node).button('reset');*/
/* 		},*/
/* 		success: function(json) {*/
/* 			$('.alert-dismissible').remove();*/
/* */
/* 			// Check for errors*/
/* 			if (json['error']) {*/
/* 				$('#content > .container-fluid').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* 			}*/
/* */
/*       if (json['success']) {*/
/* 				$('#category').load('index.php?route=extension/feed/google_base/category&user_token={{ user_token }}');*/
/* */
/* 				$('#category').before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/* 			}*/
/* 		},*/
/* 		error: function(xhr, ajaxOptions, thrownError) {*/
/* 			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 		}*/
/* 	});*/
/* });*/
/* */
/* $('#button-import').on('click', function() {*/
/* 	$('#form-upload').remove();*/
/* */
/* 	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');*/
/* */
/* 	$('#form-upload input[name=\'file\']').trigger('click');*/
/* */
/* 	if (typeof timer != 'undefined') {*/
/*     clearInterval(timer);*/
/* 	}*/
/* */
/* 	timer = setInterval(function() {*/
/* 		if ($('#form-upload input[name=\'file\']').val() != '') {*/
/* 			clearInterval(timer);*/
/* */
/* 			$.ajax({*/
/* 				url: 'index.php?route=extension/feed/google_base/import&user_token={{ user_token }}',*/
/* 				type: 'post',*/
/* 				dataType: 'json',*/
/* 				data: new FormData($('#form-upload')[0]),*/
/* 				cache: false,*/
/* 				contentType: false,*/
/* 				processData: false,*/
/* 				beforeSend: function() {*/
/* 					$('#button-import').button('loading');*/
/* 				},*/
/* 				complete: function() {*/
/* 					$('#button-import').button('reset');*/
/* 				},*/
/* 				success: function(json) {*/
/* 					$('.alert-dismissible').remove();*/
/* */
/*           if (json['error']) {*/
/*         		$('#content > .container-fluid').prepend('<div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*         	}*/
/* */
/*         	if (json['success']) {*/
/*         		$('#content > .container-fluid').prepend('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');*/
/*         	}*/
/* 				},*/
/* 				error: function(xhr, ajaxOptions, thrownError) {*/
/* 					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);*/
/* 				}*/
/* 			});*/
/* 		}*/
/* 	}, 500);*/
/* });*/
/* //--></script>*/
/* </div>*/
/* {{ footer }}*/
/* */
