<?php

/* default/template/extension/module/ocdev_smart_cart/ocdev_smart_cart_index.twig */
class __TwigTemplate_eefc7b423fe0b9bd120f811faac6bc4cd32a08ff188f954050d2bc8ac2354f69 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"w-r-p\">

    <a class=\"basket__close\" href=\"javascript:void(0)\" data-dismiss=\"dialog\"></a>
    <div class=\"basket__title\"> ";
        // line 4
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</div>

    ";
        // line 6
        if ((isset($context["error_stock"]) ? $context["error_stock"] : null)) {
            // line 7
            echo "        <div class=\"alert alert-danger\">";
            echo (isset($context["error_stock"]) ? $context["error_stock"] : null);
            echo "
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
        </div>
    ";
        }
        // line 11
        echo "

    ";
        // line 13
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 14
            echo "        <div class=\"basket__info\">
            <div class=\"basket__name\">Товар</div>
            <div class=\"basket__count\">Кол-во</div>
            <div class=\"basket__price\">Цена</div>
        </div>
        <div class=\"basket__products basket-product\">
            ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 21
                echo "                <div class=\"basket-product__item\">
                    <input name=\"product_key\" value=\"";
                // line 22
                echo $this->getAttribute($context["product"], "key", array());
                echo "\" type=\"hidden\"/>
                    <input name=\"product_id\" value=\"";
                // line 23
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\" type=\"hidden\"/>

                    <div class=\"basket-product__wrap\">
                        <a class=\"basket-product__remove icon-remove\" onclick=\"update_cart(this, 'remove')\"></a>
                        ";
                // line 27
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    // line 28
                    echo "                            <img class=\"basket-product__image\" src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\"
                                 title=\"";
                    // line 29
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\">
                        ";
                }
                // line 31
                echo "
                        <span class=\"basket-product__details\">
                            <span class=\"basket-product__title\">";
                // line 33
                echo $this->getAttribute($context["product"], "name", array());
                echo "</span>
                            <span class=\"basket-product__art\">";
                // line 34
                echo $this->getAttribute($context["product"], "stock_text", array());
                echo "</span>
                        </span>
                        <span class=\"basket-product__count\">
                        <a class=\"icon-minus basket-product__icon\" href=\"javascript:void(0)\"
                           onclick=\"\$(this).next().text(~~\$(this).next().text()-1); update_cart(this, 'update')\"></a>
                        <span type=\"text\" class=\"basket-product__in\">";
                // line 39
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "</span>
                        <a class=\"icon-plus basket-product__icon\" href=\"javascript:void(0)\"
                           onclick=\"\$(this).prev().text(~~\$(this).prev().text()+1); update_cart(this, 'update')\"></a>
                    </span>
                        <span class=\"basket-product__price\">";
                // line 43
                echo $this->getAttribute($context["product"], "price", array());
                echo "</span>
                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "        </div>
        <div class=\"basket__total\">
            <div class=\"basket__total-title\">";
            // line 49
            echo (isset($context["text_total_bottom"]) ? $context["text_total_bottom"] : null);
            echo "</div>
            <div class=\"basket__total-sum\">";
            // line 50
            echo (isset($context["total"]) ? $context["total"] : null);
            echo "</div>
        </div>
        <div class=\"basket__action\">
            <a class=\"btn__default-outline\" href=\"javascript:void(0)\" onclick=\"update_cart(this, 'remove_all')\">Очистить</a>
            <a class=\"btn__primary\" href=\"/zakaz\">Заказать</a>
        </div>
    ";
        } else {
            // line 57
            echo "        <div class=\"basket__empty\">";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</div>
    ";
        }
        // line 59
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "default/template/extension/module/ocdev_smart_cart/ocdev_smart_cart_index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 59,  136 => 57,  126 => 50,  122 => 49,  118 => 47,  108 => 43,  101 => 39,  93 => 34,  89 => 33,  85 => 31,  80 => 29,  73 => 28,  71 => 27,  64 => 23,  60 => 22,  57 => 21,  53 => 20,  45 => 14,  43 => 13,  39 => 11,  31 => 7,  29 => 6,  24 => 4,  19 => 1,);
    }
}
/* <div class="w-r-p">*/
/* */
/*     <a class="basket__close" href="javascript:void(0)" data-dismiss="dialog"></a>*/
/*     <div class="basket__title"> {{ heading_title }}</div>*/
/* */
/*     {% if error_stock %}*/
/*         <div class="alert alert-danger">{{ error_stock }}*/
/*             <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*         </div>*/
/*     {% endif %}*/
/* */
/* */
/*     {% if products %}*/
/*         <div class="basket__info">*/
/*             <div class="basket__name">Товар</div>*/
/*             <div class="basket__count">Кол-во</div>*/
/*             <div class="basket__price">Цена</div>*/
/*         </div>*/
/*         <div class="basket__products basket-product">*/
/*             {% for product in products %}*/
/*                 <div class="basket-product__item">*/
/*                     <input name="product_key" value="{{ product.key }}" type="hidden"/>*/
/*                     <input name="product_id" value="{{ product.product_id }}" type="hidden"/>*/
/* */
/*                     <div class="basket-product__wrap">*/
/*                         <a class="basket-product__remove icon-remove" onclick="update_cart(this, 'remove')"></a>*/
/*                         {% if product.thumb %}*/
/*                             <img class="basket-product__image" src="{{ product.thumb }}" alt="{{ product.name }}"*/
/*                                  title="{{ product.name }}">*/
/*                         {% endif %}*/
/* */
/*                         <span class="basket-product__details">*/
/*                             <span class="basket-product__title">{{ product.name }}</span>*/
/*                             <span class="basket-product__art">{{ product.stock_text }}</span>*/
/*                         </span>*/
/*                         <span class="basket-product__count">*/
/*                         <a class="icon-minus basket-product__icon" href="javascript:void(0)"*/
/*                            onclick="$(this).next().text(~~$(this).next().text()-1); update_cart(this, 'update')"></a>*/
/*                         <span type="text" class="basket-product__in">{{ product.quantity }}</span>*/
/*                         <a class="icon-plus basket-product__icon" href="javascript:void(0)"*/
/*                            onclick="$(this).prev().text(~~$(this).prev().text()+1); update_cart(this, 'update')"></a>*/
/*                     </span>*/
/*                         <span class="basket-product__price">{{ product.price }}</span>*/
/*                     </div>*/
/*                 </div>*/
/*             {% endfor %}*/
/*         </div>*/
/*         <div class="basket__total">*/
/*             <div class="basket__total-title">{{ text_total_bottom }}</div>*/
/*             <div class="basket__total-sum">{{ total }}</div>*/
/*         </div>*/
/*         <div class="basket__action">*/
/*             <a class="btn__default-outline" href="javascript:void(0)" onclick="update_cart(this, 'remove_all')">Очистить</a>*/
/*             <a class="btn__primary" href="/zakaz">Заказать</a>*/
/*         </div>*/
/*     {% else %}*/
/*         <div class="basket__empty">{{ text_empty }}</div>*/
/*     {% endif %}*/
/* */
/* </div>*/
