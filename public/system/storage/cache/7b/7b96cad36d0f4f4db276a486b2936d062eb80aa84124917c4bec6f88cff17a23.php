<?php

/* default/template/common/success.twig */
class __TwigTemplate_7b596270da08320d2ce8fcf32c828bd0ca5345606e1b996cc23e48806e314f77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'link' => array($this, 'block_link'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        $this->displayBlock('link', $context, $blocks);
        // line 6
        $this->displayBlock('scripts', $context, $blocks);
        // line 9
        echo "
<main class=\"order_confirmed\">
  <div class=\"order_confirmed__wrap\">
    <h1 class=\"order_confirmed__title\">";
        // line 12
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
    <div class=\"order_confirmed__animation\">
      <div class=\"check_mark\">
        <div class=\"sa-icon sa-success animate\"><span class=\"sa-line sa-tip animateSuccessTip\"></span>
          <span class=\"sa-line sa-long animateSuccessLong\"></span>
          <div class=\"sa-placeholder\"></div>
          <div class=\"sa-fix\"></div>
        </div>
      </div>
    </div>
    <div class=\"order_confirmed__desc\">";
        // line 22
        echo (isset($context["text_message"]) ? $context["text_message"] : null);
        echo "</div>
    <div class=\"order_confirmed__action\"><a class=\"btn__primary\" href=\"";
        // line 23
        echo (isset($context["continue"]) ? $context["continue"] : null);
        echo "\">";
        echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
        echo "</a></div>
  </div>
</main>
";
        // line 26
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    // line 3
    public function block_link($context, array $blocks = array())
    {
        // line 4
        echo "  <link href=\"catalog/view/theme/default/stylesheet/order_confirmed.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_scripts($context, array $blocks = array())
    {
        // line 7
        echo "  <script src=\"catalog/view/theme/default/js/order_confirmed.bundle.js\" type=\"text/javascript\"></script>
";
    }

    public function getTemplateName()
    {
        return "default/template/common/success.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 7,  72 => 6,  67 => 4,  64 => 3,  60 => 26,  52 => 23,  48 => 22,  35 => 12,  30 => 9,  28 => 6,  26 => 3,  21 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% block link %}*/
/*   <link href="catalog/view/theme/default/stylesheet/order_confirmed.css" rel="stylesheet">*/
/* {% endblock  %}*/
/* {% block scripts %}*/
/*   <script src="catalog/view/theme/default/js/order_confirmed.bundle.js" type="text/javascript"></script>*/
/* {% endblock  %}*/
/* */
/* <main class="order_confirmed">*/
/*   <div class="order_confirmed__wrap">*/
/*     <h1 class="order_confirmed__title">{{ heading_title }}</h1>*/
/*     <div class="order_confirmed__animation">*/
/*       <div class="check_mark">*/
/*         <div class="sa-icon sa-success animate"><span class="sa-line sa-tip animateSuccessTip"></span>*/
/*           <span class="sa-line sa-long animateSuccessLong"></span>*/
/*           <div class="sa-placeholder"></div>*/
/*           <div class="sa-fix"></div>*/
/*         </div>*/
/*       </div>*/
/*     </div>*/
/*     <div class="order_confirmed__desc">{{ text_message }}</div>*/
/*     <div class="order_confirmed__action"><a class="btn__primary" href="{{ continue }}">{{ button_continue }}</a></div>*/
/*   </div>*/
/* </main>*/
/* {{ footer }}*/
