//Обновление колва в шапке
function buttonManipulate() {
  $.ajax({
    type: 'get',
    url:  'index.php?route=extension/module/ocdev_smart_cart/cartProducts',
    dataType: 'json',
    success: function(json) {
      $('.header__bucket-badge').html(json['total']);
    }
  });
}


//Обновление корзины
function update_cart(target, status) {


    var wrap = $(target).closest('.basket-product__item');
    var input_val    = $(".basket-product__in",wrap).text(),
        quantity     = parseInt(input_val),
        product_id   = $('[name=product_id]', wrap).val(),
        product_id_q = $('[name=product_id_q]',wrap).val(),
        product_key  = $('[name=product_key]',wrap).val(),
        urls         = null;


    if (quantity == 0) {
        quantity = $(".basket-product__in",wrap).text(1);
        return;
    }

    if (status == 'update') {
        urls = 'index.php?route=extension/module/ocdev_smart_cart&update=' + product_key + '&quantity=' + quantity;
    } else if (status == 'add') {
        urls = 'index.php?route=extension/module/ocdev_smart_cart&add=' + target + '&quantity=1';
    } else if (status == 'remove_all') {
        var all_keys = [];
        $('[name=product_key]').each(function(input,val){
            all_keys.push(val.value);
        });

        if(!all_keys.length) return false;
        var params = all_keys.join(";");

        urls = 'index.php?route=extension/module/ocdev_smart_cart&remove=' + params;
    } else {

        urls = 'index.php?route=extension/module/ocdev_smart_cart&remove=' + product_key;
    }

    $.ajax({
        url: urls,
        type: 'get',
        dataType: 'html',
        success: function(data) {
            var basket = document.querySelector('.basket');
            basket.innerHTML = data;
            buttonManipulate();
        }
    });
}


$(function () {
    buttonManipulate();
});
