<?php
/**
 * Brainy Filter Pro 5.1.3 OC3, September 18, 2017 / brainyfilter.com 
 * Copyright 2015-2017 Giant Leap Lab / www.giantleaplab.com 
 * License: Commercial. Reselling of this software or its derivatives is not allowed. You may use this software for one website ONLY including all its subdomains if the top level domain belongs to you and all subdomains are parts of the same OpenCart store. 
 * Support: http://support.giantleaplab.com
 */
$_['heading_title'] 		= '';
$_['button_clear']			= 'Очистить';
$_['default_value_select'] 	= '- Выбрать -';
$_['price_header']			= 'Цена';
$_['categories_header']	    = 'Категории';
$_['min_max']				= 'Min - Max:';
$_['reset']					= 'Очистить';
$_['submit']				= 'Apply';
$_['stock_status']          = 'Наличие';
$_['manufacturers']         = 'Производители';
$_['entry_show_more']       = 'Подробнее';
$_['entry_show_less']       = 'Shrink';
$_['rating']       			= 'Rating';
$_['option']       			= 'Option';
$_['entry_block_title']	    = 'Refine Search';
$_['entry_search']	        = 'Keywords';
$_['text_bf_page_title']	= 'Product filter';
$_['empty_slider_value']	= 'Not Set';
