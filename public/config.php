<?php
// HTTP
define('HTTP_SERVER', 'http://192.168.88.65/');

// HTTPS
define('HTTPS_SERVER', 'http://192.168.88.65/');

// DIR
define('DIR_APPLICATION', 'C:/OSPanel/domains/store.test/public/catalog/');
define('DIR_SYSTEM', 'C:/OSPanel/domains/store.test/public/system/');
define('DIR_IMAGE', 'C:/OSPanel/domains/store.test/public/image/');
define('DIR_STORAGE', 'C:/OSPanel/domains/store.test/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', '192.168.88.65');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'store');
define('DB_PORT', '3306');
define('DB_PREFIX', '');